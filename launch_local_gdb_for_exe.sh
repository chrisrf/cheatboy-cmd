#!/bin/bash

export PVAR_PROJ_ROOT_DIR="${MY_DEV_SP}/proj-myboy/CheatBoy-Cmd"
cd "${PVAR_PROJ_ROOT_DIR}"

export PVAR_NDK_ROOT_DIR="${MY_ANDROID_NDK}"
export PVAR_TOOL_CHAIN_RELATIVE_PATH="toolchains/x86-4.9/prebuilt/linux-x86_64/bin/i686-linux-android-gdb"
#export PVAR_TOOL_CHAIN_RELATIVE_PATH="toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-gdb"
export PVAR_TARGET_ARCH_ABI="x86"
#export PVAR_TARGET_ARCH_ABI="armeabi"
#export PVAR_TARGET_ARCH_ABI="armeabi-v7a"
export PVAR_EXE_NAME="cheatboy"

"${PVAR_NDK_ROOT_DIR}/${PVAR_TOOL_CHAIN_RELATIVE_PATH}" -x "./libs/${PVAR_TARGET_ARCH_ABI}/gdb.setup" "./obj/local/${PVAR_TARGET_ARCH_ABI}/${PVAR_EXE_NAME}"
# target remote :5039
# l
# b 6
# c
# c

read -p "Press [Enter] key to exit..."