#include "StringTrimmerTests.h"

#include <assert.h>
#include <string>
#include <tuple>
#include <vector>
#include <purelib/string/StringTrimmer.h>

namespace pure {

std::vector<std::array<char, StringTrimmerTests::MAX_SIZE_OF_TEST_CSTRING>> StringTrimmerTests::createTestCstringsForTestCTrim() {
	std::vector<std::array<char, MAX_SIZE_OF_TEST_CSTRING>> testCstrings(MAX_NUM_OF_TEST_CSTRINGS_FOR_TEST_CTRIM);
	//snprintf(testCstrings[0].data(), testCstrings[0].size(), "");
	char* temp = testCstrings[0].data();temp[0] = '\0';
	snprintf(testCstrings[1].data(), testCstrings[1].size(), " \t \n");
	snprintf(testCstrings[2].data(), testCstrings[2].size(), "abc abc");
	snprintf(testCstrings[3].data(), testCstrings[3].size(), " \t \nabc abc \t \n");
	return testCstrings;
}

TEST_F(StringTrimmerTests,testCTrim) {
	{
		std::vector<std::array<char, MAX_SIZE_OF_TEST_CSTRING>> testCstrings = createTestCstringsForTestCTrim();
		std::vector<std::tuple<bool, size_t, size_t, const char*, const char*>> expectedResults = {
				std::make_tuple(true, 0, 0, "", ""),
				std::make_tuple(true, 0, 0, "", ""),
				std::make_tuple(true, 7, 7, "abc abc", "abc abc"),
				std::make_tuple(false, 15, 11, " \t \nabc abc \t \n", "abc abc \t \n")
		};
		assert(testCstrings.size() == expectedResults.size());
		for (size_t i = 0; i < testCstrings.size(); i++) {
			auto& expected = expectedResults[i];
			char* str = testCstrings[i].data();

			size_t newLen = 0;
			char* newStr = StringTrimmer::cTrimHead(str, newLen);
			EXPECT_EQ(std::get<0>(expected), (str == newStr));
			EXPECT_EQ(std::get<1>(expected), strlen(str));
			EXPECT_EQ(std::get<2>(expected), strlen(newStr));
			EXPECT_EQ(std::get<2>(expected), newLen);
			EXPECT_STREQ(std::get<3>(expected), str);
			EXPECT_STREQ(std::get<4>(expected), newStr);
		}
	}

	{
		std::vector<std::array<char, MAX_SIZE_OF_TEST_CSTRING>> testCstrings = createTestCstringsForTestCTrim();
		std::vector<std::tuple<bool, size_t, size_t, const char*, const char*>> expectedResults = {
				std::make_tuple(true, 0, 0, "", ""),
				std::make_tuple(true, 0, 0, "", ""),
				std::make_tuple(true, 7, 7, "abc abc", "abc abc"),
				std::make_tuple(true, 11, 11, " \t \nabc abc", " \t \nabc abc")
		};
		assert(testCstrings.size() == expectedResults.size());
		for (size_t i = 0; i < testCstrings.size(); i++) {
			auto& expected = expectedResults[i];
			char* str = testCstrings[i].data();

			size_t newLen = 0;
			char* newStr = StringTrimmer::cTrimTail(str, newLen);
			EXPECT_EQ(std::get<0>(expected), (str == newStr));
			EXPECT_EQ(std::get<1>(expected), strlen(str));
			EXPECT_EQ(std::get<2>(expected), strlen(newStr));
			EXPECT_EQ(std::get<2>(expected), newLen);
			EXPECT_STREQ(std::get<3>(expected), str);
			EXPECT_STREQ(std::get<4>(expected), newStr);
		}
	}

	{
		std::vector<std::array<char, MAX_SIZE_OF_TEST_CSTRING>> testCstrings = createTestCstringsForTestCTrim();
		std::vector<std::tuple<bool, size_t, size_t, const char*, const char*>> expectedResults = {
				std::make_tuple(true, 0, 0, "", ""),
				std::make_tuple(true, 0, 0, "", ""),
				std::make_tuple(true, 7, 7, "abc abc", "abc abc"),
				std::make_tuple(false, 11, 7, " \t \nabc abc", "abc abc")
		};
		assert(testCstrings.size() == expectedResults.size());
		for (size_t i = 0; i < testCstrings.size(); i++) {
			auto& expected = expectedResults[i];
			char* str = testCstrings[i].data();

			size_t newLen = 0;
			char* newStr = StringTrimmer::cTrim(str, newLen);
			EXPECT_EQ(std::get<0>(expected), (str == newStr));
			EXPECT_EQ(std::get<1>(expected), strlen(str));
			EXPECT_EQ(std::get<2>(expected), strlen(newStr));
			EXPECT_EQ(std::get<2>(expected), newLen);
			EXPECT_STREQ(std::get<3>(expected), str);
			EXPECT_STREQ(std::get<4>(expected), newStr);
		}
	}
}

TEST_F(StringTrimmerTests,testCTrimAndShift) {
	{
		std::vector<std::array<char, MAX_SIZE_OF_TEST_CSTRING>> testCstrings = createTestCstringsForTestCTrim();
		std::vector<std::tuple<size_t, const char*>> expectedResults = {
				std::make_tuple(0, ""),
				std::make_tuple(0, ""),
				std::make_tuple(7, "abc abc"),
				std::make_tuple(11, "abc abc \t \n")
		};
		assert(testCstrings.size() == expectedResults.size());
		for (size_t i = 0; i < testCstrings.size(); i++) {
			auto& expected = expectedResults[i];
			char* str = testCstrings[i].data();

			size_t newLen = 0;
			StringTrimmer::cTrimHeadAndShift(str, newLen);
			EXPECT_EQ(std::get<0>(expected), strlen(str));
			EXPECT_EQ(std::get<0>(expected), newLen);
			EXPECT_STREQ(std::get<1>(expected), str);
		}
	}

	{
		std::vector<std::array<char, MAX_SIZE_OF_TEST_CSTRING>> testCstrings = createTestCstringsForTestCTrim();
		std::vector<std::tuple<size_t, const char*>> expectedResults = {
				std::make_tuple(0, ""),
				std::make_tuple(0, ""),
				std::make_tuple(7, "abc abc"),
				std::make_tuple(7, "abc abc")
		};
		assert(testCstrings.size() == expectedResults.size());
		for (size_t i = 0; i < testCstrings.size(); i++) {
			auto& expected = expectedResults[i];
			char* str = testCstrings[i].data();

			size_t newLen = 0;
			StringTrimmer::cTrimAndShift(str, newLen);
			EXPECT_EQ(std::get<0>(expected), strlen(str));
			EXPECT_EQ(std::get<0>(expected), newLen);
			EXPECT_STREQ(std::get<1>(expected), str);
		}
	}
}

std::vector<std::string> StringTrimmerTests::createTestStringsForTestTrim() {
	std::vector<std::string> testStrings = {
			"",
			" \t \n",
			"abc abc",
			" \t \nabc abc \t \n"
	};
	return testStrings;
}

TEST_F(StringTrimmerTests,testTrim) {
	{
		std::vector<std::string> testStrings = createTestStringsForTestTrim();
		std::vector<std::string> expectedStrings = {
				"",
				"",
				"abc abc",
				"abc abc \t \n"
		};
		assert(testStrings.size() == expectedStrings.size());
		for (size_t i = 0; i < testStrings.size(); i++) {
			StringTrimmer::trimHead(testStrings[i]);
			EXPECT_EQ(expectedStrings[i], testStrings[i]);
		}
	}

	{
		std::vector<std::string> testStrings = createTestStringsForTestTrim();
		std::vector<std::string> expectedStrings = {
				"",
				"",
				"abc abc",
				" \t \nabc abc"
		};
		assert(testStrings.size() == expectedStrings.size());
		for (size_t i = 0; i < testStrings.size(); i++) {
			StringTrimmer::trimTail(testStrings[i]);
			EXPECT_EQ(expectedStrings[i], testStrings[i]);
		}
	}

	{
		std::vector<std::string> testStrings = createTestStringsForTestTrim();
		std::vector<std::string> expectedStrings = {
				"",
				"",
				"abc abc",
				"abc abc"
		};
		assert(testStrings.size() == expectedStrings.size());
		for (size_t i = 0; i < testStrings.size(); i++) {
			StringTrimmer::trim(testStrings[i]);
			EXPECT_EQ(expectedStrings[i], testStrings[i]);
		}
	}
}

} /* END of namespace */
