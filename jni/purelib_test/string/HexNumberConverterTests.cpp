#include "HexNumberConverterTests.h"

#include <assert.h>
#include <iostream>
#include <limits>
#include <purelib/string/HexNumberConverter.h>

//#define __PURE_UNIT_TEST_HEXNUMBER_CONVERTER_PRINT__

namespace pure {

template<typename T>
void HexNumberConverterTests::testConvertUnsuccessfully(HexNumberConverter& converter, std::vector<std::string>& testStrings) {
	for (size_t i = 0; i < testStrings.size(); i++) {
		T value = 0;
		EXPECT_FALSE(converter.convert(testStrings[i], value));
#ifdef __PURE_UNIT_TEST_HEXNUMBER_CONVERTER_PRINT__
		printConvertedValue<T>(false, value);
#endif
	}
}

template<typename T>
void HexNumberConverterTests::testConvertSuccessfully(HexNumberConverter& converter, std::vector<std::string>& testStrings, std::vector<T>& expectedValues) {
	assert(testStrings.size() == expectedValues.size());
	for (size_t i = 0; i < testStrings.size(); i++) {
		T value = 0;
		EXPECT_TRUE(converter.convert(testStrings[i], value));
		EXPECT_EQ(expectedValues[i], value);
#ifdef __PURE_UNIT_TEST_HEXNUMBER_CONVERTER_PRINT__
		printConvertedValue<T>(true, value);
#endif
	}
}

template<typename T>
void HexNumberConverterTests::printConvertedValue(bool isSuccessful, T value) {
	const char* tag = (isSuccessful ? "Successfully" : "Unsuccessfully");
	if (std::is_integral<T>::value) {
		if (std::is_signed<T>::value) {
			int64_t temp = value;
			std::cout << "[" << tag << "] [" << temp << "]" << std::endl;
		} else {
			uint64_t temp = value;
			std::cout << "[" << tag << "] [" << temp << "]" << std::endl;
		}
	} else {
		std::cout << "[" << tag << "] [" << value << "]" << std::endl;
	}
}

void HexNumberConverterTests::printStartLine(const char* tag) {
#ifdef __PURE_UNIT_TEST_HEXNUMBER_CONVERTER_PRINT__
	std::cout << "####### [" << tag << "]" << std::endl;
#endif
}

void HexNumberConverterTests::printEndLine(const char* tag) {
#ifdef __PURE_UNIT_TEST_HEXNUMBER_CONVERTER_PRINT__
	std::cout << "####### [" << tag << "] END" << std::endl;
#endif
}

TEST_F(HexNumberConverterTests,testConvert) {
	static_assert(((sizeof(long) == sizeof(int32_t)) || (sizeof(long) == sizeof(int64_t))), "The size of long is NOT correct.");
	static_assert(((sizeof(unsigned long) == sizeof(uint32_t)) || (sizeof(unsigned long) == sizeof(uint64_t))), "The size of unsigned long is NOT correct.");

	HexNumberConverter converter;

	std::vector<std::string> invalidTestStrings = {
			"", "0xff0xff", "-0x1f", "0x", "1x1f", "0xg1f",
			" ", "\t", "\n",
			" 1f", "\t1f", "\n1f", "1f ", "1f\t", "1f\n", "1f 1f", "1fggg",
			" 0x1f", "\t0x1f", "\n0x1f", "0x1f ", "0x1f\t", "0x1f\n", "0x1f 0x1f", "0x1fggg"
	};

	{
		printStartLine("uint8_t");
		uint8_t min = std::numeric_limits<uint8_t>::min();
		uint8_t max = std::numeric_limits<uint8_t>::max();
		std::vector<std::string> overflowTestStrings = { "fff", "FFF", "0xfff", "0xFFF" };
		testConvertUnsuccessfully<uint8_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<uint8_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "0", "00", "0x0", "0x00", "ff", "FF", "0xff", "0xFF" };
		std::vector<uint8_t> expectedValues = {
				min, min, min, min, max, max, max, max
		};
		testConvertSuccessfully<uint8_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("uint8_t");
	}

	{
		printStartLine("uint16_t");
		uint16_t min = std::numeric_limits<uint16_t>::min();
		uint16_t max = std::numeric_limits<uint16_t>::max();
		std::vector<std::string> overflowTestStrings = { "fffff", "FFFFF", "0xfffff", "0xFFFFF" };
		testConvertUnsuccessfully<uint16_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<uint16_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "0", "00", "0x0", "0x00", "ffff", "FFFF", "0xffff", "0xFFFF" };
		std::vector<uint16_t> expectedValues = {
				min, min, min, min, max, max, max, max
		};
		testConvertSuccessfully<uint16_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("uint16_t");
	}

	{
		printStartLine("uint32_t");
		uint32_t min = std::numeric_limits<uint32_t>::min();
		uint32_t max = std::numeric_limits<uint32_t>::max();
		std::vector<std::string> overflowTestStrings = { "fffffffff", "FFFFFFFFF", "0xfffffffff", "0xFFFFFFFFF" };
		testConvertUnsuccessfully<uint32_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<uint32_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "0", "00", "0x0", "0x00", "ffffffff", "FFFFFFFF", "0xffffffff", "0xFFFFFFFF" };
		std::vector<uint32_t> expectedValues = {
				min, min, min, min, max, max, max, max
		};
		testConvertSuccessfully<uint32_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("uint32_t");
		if (sizeof(unsigned long) == sizeof(uint32_t)) {
			printStartLine("unsigned long");
			unsigned long min = std::numeric_limits<unsigned long>::min();
			unsigned long max = std::numeric_limits<unsigned long>::max();
			testConvertUnsuccessfully<unsigned long>(converter, invalidTestStrings);
			testConvertUnsuccessfully<unsigned long>(converter, overflowTestStrings);
			std::vector<unsigned long> expectedValues = {
					min, min, min, min, max, max, max, max
			};
			testConvertSuccessfully<unsigned long>(converter, acceptedTestStrings, expectedValues);
			printEndLine("unsigned long");
		}
	}

	{
		printStartLine("uint64_t");
		uint64_t min = std::numeric_limits<uint64_t>::min();
		uint64_t max = std::numeric_limits<uint64_t>::max();
		std::vector<std::string> overflowTestStrings = { "fffffffffffffffff", "FFFFFFFFFFFFFFFFF", "0xfffffffffffffffff", "0xFFFFFFFFFFFFFFFFF" };
		testConvertUnsuccessfully<uint64_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<uint64_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "0", "00", "0x0", "0x00", "ffffffffffffffff", "FFFFFFFFFFFFFFFF", "0xffffffffffffffff", "0xFFFFFFFFFFFFFFFF" };
		std::vector<uint64_t> expectedValues = {
				min, min, min, min, max, max, max, max
		};
		testConvertSuccessfully<uint64_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("uint64_t");
		if (sizeof(unsigned long) == sizeof(uint64_t)) {
			printStartLine("unsigned long");
			unsigned long min = std::numeric_limits<unsigned long>::min();
			unsigned long max = std::numeric_limits<unsigned long>::max();
			testConvertUnsuccessfully<unsigned long>(converter, invalidTestStrings);
			testConvertUnsuccessfully<unsigned long>(converter, overflowTestStrings);
			std::vector<unsigned long> expectedValues = {
					min, min, min, min, max, max, max, max
			};
			testConvertSuccessfully<unsigned long>(converter, acceptedTestStrings, expectedValues);
			printEndLine("unsigned long");
		}
	}
}

} /* END of namespace */
