#ifndef __HexNumberConverterTests_H__
#define __HexNumberConverterTests_H__

#include <string>
#include <vector>
#include <gtest/gtest.h>

namespace pure {
class HexNumberConverter;

class HexNumberConverterTests: public ::testing::Test {
public:
	HexNumberConverterTests() {}
	virtual ~HexNumberConverterTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	template<typename T> void testConvertUnsuccessfully(HexNumberConverter& converter, std::vector<std::string>& testStrings);
	template<typename T> void testConvertSuccessfully(HexNumberConverter& converter, std::vector<std::string>& testStrings, std::vector<T>& expectedValues);
protected:
	template<typename T> void printConvertedValue(bool isSuccessful, T value);
	void printStartLine(const char* tag);
	void printEndLine(const char* tag);
};

} /* END of namespace */

#endif
