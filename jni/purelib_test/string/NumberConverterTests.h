#ifndef __NumberConverterTests_H__
#define __NumberConverterTests_H__

#include <string>
#include <vector>
#include <gtest/gtest.h>

namespace pure {
class NumberConverter;

class NumberConverterTests: public ::testing::Test {
public:
	NumberConverterTests() {}
	virtual ~NumberConverterTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	template<typename T> void testConvertUnsuccessfully(NumberConverter& converter, std::vector<std::string>& testStrings);
	template<typename T> void testConvertSuccessfully(NumberConverter& converter, std::vector<std::string>& testStrings, std::vector<T>& expectedValues);
protected:
	template<typename T> void printConvertedValue(bool isSuccessful, T value);
	void printStartLine(const char* tag);
	void printEndLine(const char* tag);
};

} /* END of namespace */

#endif
