#include "NumberConverterTests.h"

#include <assert.h>
#include <iostream>
#include <limits>
#include <purelib/string/NumberConverter.h>

//#define __PURE_UNIT_TEST_NUMBER_CONVERTER_PRINT__

namespace pure {

template<typename T>
void NumberConverterTests::testConvertUnsuccessfully(NumberConverter& converter, std::vector<std::string>& testStrings) {
	for (size_t i = 0; i < testStrings.size(); i++) {
		T value = 0;
		EXPECT_FALSE(converter.convert(testStrings[i], value));
#ifdef __PURE_UNIT_TEST_NUMBER_CONVERTER_PRINT__
		printConvertedValue<T>(false, value);
#endif
	}
}

template<typename T>
void NumberConverterTests::testConvertSuccessfully(NumberConverter& converter, std::vector<std::string>& testStrings, std::vector<T>& expectedValues) {
	assert(testStrings.size() == expectedValues.size());
	for (size_t i = 0; i < testStrings.size(); i++) {
		T value = 0;
		EXPECT_TRUE(converter.convert(testStrings[i], value));
		EXPECT_EQ(expectedValues[i], value);
#ifdef __PURE_UNIT_TEST_NUMBER_CONVERTER_PRINT__
		printConvertedValue<T>(true, value);
#endif
	}
}

template<typename T>
void NumberConverterTests::printConvertedValue(bool isSuccessful, T value) {
	const char* tag = (isSuccessful ? "Successfully" : "Unsuccessfully");
	if (std::is_integral<T>::value) {
		if (std::is_signed<T>::value) {
			int64_t temp = value;
			std::cout << "[" << tag << "] [" << temp << "]" << std::endl;
		} else {
			uint64_t temp = value;
			std::cout << "[" << tag << "] [" << temp << "]" << std::endl;
		}
	} else {
		std::cout << "[" << tag << "] [" << value << "]" << std::endl;
	}
}

void NumberConverterTests::printStartLine(const char* tag) {
#ifdef __PURE_UNIT_TEST_NUMBER_CONVERTER_PRINT__
	std::cout << "####### [" << tag << "]" << std::endl;
#endif
}

void NumberConverterTests::printEndLine(const char* tag) {
#ifdef __PURE_UNIT_TEST_NUMBER_CONVERTER_PRINT__
	std::cout << "####### [" << tag << "] END" << std::endl;
#endif
}

TEST_F(NumberConverterTests,testConvert) {
	static_assert(((sizeof(long) == sizeof(int32_t)) || (sizeof(long) == sizeof(int64_t))), "The size of long is NOT correct.");
	static_assert(((sizeof(unsigned long) == sizeof(uint32_t)) || (sizeof(unsigned long) == sizeof(uint64_t))), "The size of unsigned long is NOT correct.");

	NumberConverter converter;

	std::vector<std::string> invalidTestStrings = {
			"", "-", "a",
			" ", "\t", "\n",
			" 123", "\t123", "\n123", "123 ", "123\t", "123\n", "123 123", "123aaa",
			" -123", "\t-123", "\n-123", "-123 ", "-123\t", "-123\n", "-123 -123", "-123aaa"
	};

	{
		printStartLine("int8_t");
		std::vector<std::string> overflowTestStrings = { "-129", "128" };
		testConvertUnsuccessfully<int8_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<int8_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "-128", "127" };
		std::vector<int8_t> expectedValues = { std::numeric_limits<int8_t>::min(), std::numeric_limits<int8_t>::max() };
		testConvertSuccessfully<int8_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("int8_t");
	}

	{
		printStartLine("uint8_t");
		std::vector<std::string> overflowTestStrings = { "-1", "256" };
		testConvertUnsuccessfully<uint8_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<uint8_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "0", "255" };
		std::vector<uint8_t> expectedValues = { std::numeric_limits<uint8_t>::min(), std::numeric_limits<uint8_t>::max() };
		testConvertSuccessfully<uint8_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("uint8_t");
	}

	{
		printStartLine("int16_t");
		std::vector<std::string> overflowTestStrings = { "-32769", "32768" };
		testConvertUnsuccessfully<int16_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<int16_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "-32768", "32767" };
		std::vector<int16_t> expectedValues = { std::numeric_limits<int16_t>::min(), std::numeric_limits<int16_t>::max() };
		testConvertSuccessfully<int16_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("int16_t");
	}

	{
		printStartLine("uint16_t");
		std::vector<std::string> overflowTestStrings = { "-1", "65536" };
		testConvertUnsuccessfully<uint16_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<uint16_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "0", "65535" };
		std::vector<uint16_t> expectedValues = { std::numeric_limits<uint16_t>::min(), std::numeric_limits<uint16_t>::max() };
		testConvertSuccessfully<uint16_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("uint16_t");
	}

	{
		printStartLine("int32_t");
		std::vector<std::string> overflowTestStrings = { "-2147483649", "2147483648" };
		testConvertUnsuccessfully<int32_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<int32_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "-2147483648", "2147483647" };
		std::vector<int32_t> expectedValues = { std::numeric_limits<int32_t>::min(), std::numeric_limits<int32_t>::max() };
		testConvertSuccessfully<int32_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("int32_t");
		if (sizeof(long) == sizeof(int32_t)) {
			printStartLine("long");
			testConvertUnsuccessfully<long>(converter, invalidTestStrings);
			testConvertUnsuccessfully<long>(converter, overflowTestStrings);
			std::vector<long> expectedValues = { std::numeric_limits<long>::min(), std::numeric_limits<long>::max() };
			testConvertSuccessfully<long>(converter, acceptedTestStrings, expectedValues);
			printEndLine("long");
		}
	}

	{
		printStartLine("uint32_t");
		std::vector<std::string> overflowTestStrings = { "-1", "4294967296" };
		testConvertUnsuccessfully<uint32_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<uint32_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "0", "4294967295" };
		std::vector<uint32_t> expectedValues = { std::numeric_limits<uint32_t>::min(), std::numeric_limits<uint32_t>::max() };
		testConvertSuccessfully<uint32_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("uint32_t");
		if (sizeof(unsigned long) == sizeof(uint32_t)) {
			printStartLine("unsigned long");
			testConvertUnsuccessfully<unsigned long>(converter, invalidTestStrings);
			testConvertUnsuccessfully<unsigned long>(converter, overflowTestStrings);
			std::vector<unsigned long> expectedValues = { std::numeric_limits<unsigned long>::min(), std::numeric_limits<unsigned long>::max() };
			testConvertSuccessfully<unsigned long>(converter, acceptedTestStrings, expectedValues);
			printEndLine("unsigned long");
		}
	}

	{
		printStartLine("int64_t");
		std::vector<std::string> overflowTestStrings = { "-9223372036854775809", "9223372036854775808" };
		testConvertUnsuccessfully<int64_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<int64_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "-9223372036854775808", "9223372036854775807" };
		std::vector<int64_t> expectedValues = { std::numeric_limits<int64_t>::min(), std::numeric_limits<int64_t>::max() };
		testConvertSuccessfully<int64_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("int64_t");
		if (sizeof(long) == sizeof(int64_t)) {
			printStartLine("long");
			testConvertUnsuccessfully<long>(converter, invalidTestStrings);
			testConvertUnsuccessfully<long>(converter, overflowTestStrings);
			std::vector<long> expectedValues = { std::numeric_limits<long>::min(), std::numeric_limits<long>::max() };
			testConvertSuccessfully<long>(converter, acceptedTestStrings, expectedValues);
			printEndLine("long");
		}
	}

	{
		printStartLine("uint64_t");
		std::vector<std::string> overflowTestStrings = { "-1", "18446744073709551616" };
		testConvertUnsuccessfully<uint64_t>(converter, invalidTestStrings);
		testConvertUnsuccessfully<uint64_t>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = { "0", "18446744073709551615" };
		std::vector<uint64_t> expectedValues = { std::numeric_limits<uint64_t>::min(), std::numeric_limits<uint64_t>::max() };
		testConvertSuccessfully<uint64_t>(converter, acceptedTestStrings, expectedValues);
		printEndLine("uint64_t");
		if (sizeof(unsigned long) == sizeof(uint64_t)) {
			printStartLine("unsigned long");
			testConvertUnsuccessfully<unsigned long>(converter, invalidTestStrings);
			testConvertUnsuccessfully<unsigned long>(converter, overflowTestStrings);
			std::vector<unsigned long> expectedValues = { std::numeric_limits<unsigned long>::min(), std::numeric_limits<unsigned long>::max() };
			testConvertSuccessfully<unsigned long>(converter, acceptedTestStrings, expectedValues);
			printEndLine("unsigned long");
		}
	}

	{
		printStartLine("float");
		std::vector<std::string> overflowTestStrings = {
				"-3.40282346638528859812e+39", "3.40282346638528859812e+39",
				//"-1.17549435082228750797e-39", "1.17549435082228750797e-39"
		};
		testConvertUnsuccessfully<float>(converter, invalidTestStrings);
		testConvertUnsuccessfully<float>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = {
				"-3.40282346638528859812e+38", "3.40282346638528859812e+38",
				"-1.17549435082228750797e-38", "1.17549435082228750797e-38"
		};
		std::vector<float> expectedValues = {
				-std::numeric_limits<float>::max(), std::numeric_limits<float>::max(),
				-std::numeric_limits<float>::min(), std::numeric_limits<float>::min()
		};
		testConvertSuccessfully<float>(converter, acceptedTestStrings, expectedValues);
		printEndLine("float");
	}

	{
		printStartLine("double");
		std::vector<std::string> overflowTestStrings = {
				"-1.79769313486231570815e+309", "1.79769313486231570815e+309",
				//"-2.22507385850720138309e-309", "2.22507385850720138309e-309"
		};
		testConvertUnsuccessfully<double>(converter, invalidTestStrings);
		testConvertUnsuccessfully<double>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = {
				"-1.79769313486231570815e+308", "1.79769313486231570815e+308",
				"-2.22507385850720138309e-308", "2.22507385850720138309e-308"
		};
		std::vector<double> expectedValues = {
				-std::numeric_limits<double>::max(), std::numeric_limits<double>::max(),
				-std::numeric_limits<double>::min(), std::numeric_limits<double>::min()
		};
		testConvertSuccessfully<double>(converter, acceptedTestStrings, expectedValues);
		printEndLine("double");
	}

	if (sizeof(long double) == sizeof(double)) {
		printStartLine("long double");
		std::vector<std::string> overflowTestStrings = {
				"-1.79769313486231570815e+309", "1.79769313486231570815e+309",
				//"-2.22507385850720138309e-309", "2.22507385850720138309e-309"
		};
		testConvertUnsuccessfully<long double>(converter, invalidTestStrings);
		testConvertUnsuccessfully<long double>(converter, overflowTestStrings);
		std::vector<std::string> acceptedTestStrings = {
				"-1.79769313486231570815e+308", "1.79769313486231570815e+308",
				"-2.22507385850720138309e-308", "2.22507385850720138309e-308"
		};
		std::vector<long double> expectedValues = {
				-std::numeric_limits<long double>::max(), std::numeric_limits<long double>::max(),
				-std::numeric_limits<long double>::min(), std::numeric_limits<long double>::min()
		};
		testConvertSuccessfully<long double>(converter, acceptedTestStrings, expectedValues);
		printEndLine("long double");
	} else {
		ASSERT_TRUE(false);
	}
}

} /* END of namespace */
