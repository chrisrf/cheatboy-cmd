#include "StringSplitterTests.h"

#include <assert.h>
#include <string>
#include <tuple>
#include <vector>
#include <purelib/string/StringSplitter.h>

namespace pure {

TEST_F(StringSplitterTests,testSplitForTwoString) {
	char delimiter = '-';
	std::vector<std::string> testStrings = {
			"", "123",
			"-", "-123", "123-", "123-123-123", "123-123"
	};
	std::vector<std::tuple<bool, std::string, std::string>> expectedResults = {
			std::make_tuple(false, "", ""),
			std::make_tuple(false, "", ""),
			std::make_tuple(true, "", ""),
			std::make_tuple(true, "", "123"),
			std::make_tuple(true, "123", ""),
			std::make_tuple(true, "123", "123-123"),
			std::make_tuple(true, "123", "123")
	};
	assert(testStrings.size() == expectedResults.size());

	std::string temp1;
	std::string temp2;
	for (size_t i = 0; i < testStrings.size(); i++) {
		auto& expected = expectedResults[i];
		temp1.clear();
		temp2.clear();

		bool result = StringSplitter::splitForTwoString(testStrings[i], delimiter, temp1, temp2);
		EXPECT_EQ(std::get<0>(expected), result);
		EXPECT_EQ(std::get<1>(expected), temp1);
		EXPECT_EQ(std::get<2>(expected), temp2);
	}
}

} /* END of namespace */
