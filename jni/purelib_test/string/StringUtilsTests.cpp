#include "StringUtilsTests.h"

#include <stdlib.h>
#include <string.h>
#include <string>
#include <vector>
#include <purelib/string/StringUtils.h>

namespace pure {

TEST_F(StringUtilsTests,testIsInteger) {
	{
		std::vector<const char*> testCstrs = {
				"", "-", "-a123",
				" 123", "\t123", "\n123", "123 ", "123\t", "123\n", "123 123", "123aaa"
				" -123", "\t-123", "\n-123", "-123 ", "-123\t", "-123\n", "-123 -123", "-123aaa"
		};

		for (const auto& cstr : testCstrs) {
			EXPECT_FALSE(StringUtils::isInteger(cstr));
		}

		std::string numStr1("123");
		EXPECT_TRUE(StringUtils::isInteger(numStr1));
		std::string numStr2("-123");
		EXPECT_TRUE(StringUtils::isInteger(numStr2));
	}

	{
		std::vector<const char*> testCstrs = {
				"", "-123",
				" 123", "\t123", "\n123", "123 ", "123\t", "123\n", "123 123", "123aaa"
		};

		for (const auto& cstr : testCstrs) {
			EXPECT_FALSE(StringUtils::isInteger(cstr, false));
		}

		std::string numStr1("123");
		EXPECT_TRUE(StringUtils::isInteger(numStr1, false));
	}
}

TEST_F(StringUtilsTests,testIsHexNumber) {
	std::vector<const char*> testCstrs = {
			"", "0xff0xff", "-0x1f", "0x", "1x1f", "0xg1f",
			" 1f", "\t1f", "\n1f", "1f ", "1f\t", "1f\n", "1f 1f", "1fggg",
			" 0x1f", "\t0x1f", "\n0x1f", "0x1f ", "0x1f\t", "0x1f\n", "0x1f 0x1f", "0x1fggg"
	};

	for (const auto& cstr : testCstrs) {
		EXPECT_FALSE(StringUtils::isHexNumber(cstr));
	}

	std::string numStr1("1af3");
	EXPECT_TRUE(StringUtils::isHexNumber(numStr1));
	std::string numStr2("0x1af3");
	EXPECT_TRUE(StringUtils::isHexNumber(numStr2));
}

} /* END of namespace */
