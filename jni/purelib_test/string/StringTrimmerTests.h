#ifndef __StringTrimmerTests_H__
#define __StringTrimmerTests_H__

#include <string>
#include <array>
#include <vector>
#include <gtest/gtest.h>

namespace pure {

class StringTrimmerTests: public ::testing::Test {
protected:
	enum {
		MAX_SIZE_OF_TEST_CSTRING = 64,
		MAX_NUM_OF_TEST_CSTRINGS_FOR_TEST_CTRIM = 4
	};
public:
	StringTrimmerTests() {}
	virtual ~StringTrimmerTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	std::vector<std::array<char, StringTrimmerTests::MAX_SIZE_OF_TEST_CSTRING>> createTestCstringsForTestCTrim();
	std::vector<std::string> createTestStringsForTestTrim();
};

} /* END of namespace */

#endif
