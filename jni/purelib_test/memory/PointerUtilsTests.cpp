#include "PointerUtilsTests.h"

#include <stdint.h>
#include <iostream>
#include <purelib/memory/PointerUtils.h>

//#define __PURE_UNIT_TEST_POINTER_UTILS_PRINT__

namespace pure {

TEST_F(PointerUtilsTests,testPointerUtils) {
	{
		uint8_t testVar = 123;
		void* ptr = &testVar;

		uintptr_t newValuePtrTestVar = PointerUtils::toValue(ptr);
		void* newPtr = PointerUtils::toPointer(newValuePtrTestVar);
#ifdef __PURE_UNIT_TEST_POINTER_UTILS_PRINT__
		std::cout << std::dec << "ptr = [" <<  ptr << "]" << std::endl;
		std::cout << std::hex << "newValuePtrTestVar = [" <<  newValuePtrTestVar << "]" << std::endl;
		std::cout << std::dec << "newPtr = [" <<  newPtr << "]" << std::endl;
#endif
		EXPECT_EQ(ptr, newPtr);
	}

	{
		uintptr_t valuePtr = 0x01020304;

		void* newPtr = PointerUtils::toPointer(valuePtr);
		uintptr_t newValuePtr = PointerUtils::toValue(newPtr);
#ifdef __PURE_UNIT_TEST_POINTER_UTILS_PRINT__
		std::cout << std::hex << "valuePtr = [" <<  valuePtr << "]" << std::endl;
		std::cout << std::dec << "newPtr = [" <<  newPtr << "]" << std::endl;
		std::cout << std::hex << "newValuePtr = [" <<  newValuePtr << "]" << std::endl;
#endif
		EXPECT_EQ(valuePtr, newValuePtr);
	}
#ifdef __PURE_UNIT_TEST_POINTER_UTILS_PRINT__
	std::cout << std::dec;
#endif
}

} /* END of namespace */
