#include "MemoryUtilsTests.h"

#include <string>
#include <purelib/string/StringFormat.h>
#include <purelib/terminal/TerminalPrint.h>
#include <purelib/memory/PointerUtils.h>
#include <purelib/memory/MemoryUtils.h>

namespace pure {

void MemoryUtilsTests::printByMemoryUtils(uint8_t* ptr, size_t size) {
	pure::print("### [Readable Bytes]\n");
	MemoryUtils::printReadableLineBytes(ptr, size, true);
	MemoryUtils::printReadableBlockBytes(ptr, size);
	pure::print("{0}", MemoryUtils::readableLineBytesToString(ptr, size, true));
	pure::print("{0}", MemoryUtils::readableBlockBytesToString(ptr, size));

	pure::print("### [Copyable Bytes]\n");
	MemoryUtils::printCopyableLineBytes(ptr, size, true);
	MemoryUtils::printCopyableBlockBytes(ptr, size);
	pure::print("{0}", MemoryUtils::copyableLineBytesToString(ptr, size, true));
	pure::print("{0}", MemoryUtils::copyableBlockBytesToString(ptr, size));

	pure::print("### [Parsable Bytes]\n");
	MemoryUtils::printParsableLineBytes(ptr, size, true);
	MemoryUtils::printParsableBlockBytes(ptr, size);
	pure::print("{0}", MemoryUtils::parsableLineBytesToString(ptr, size, true));
	pure::print("{0}", MemoryUtils::parsableBlockBytesToString(ptr, size));
}

TEST_F(MemoryUtilsTests,testConvertToAddressString) {
	uintptr_t addrValue = 0x010a030b;
	std::string addrString = MemoryUtils::convertToAddressString(addrValue);
	EXPECT_STREQ("010a030b", addrString.c_str());
}

TEST_F(MemoryUtilsTests,testMemoryUtils) {
	uint8_t testMemory001[18] = {
			0xf8, 0xf7, 0xf6, 0xf5,
			0xf4, 0xf3, 0xf2, 0xf1,
			0x08, 0x07, 0x06, 0x05,
			0x04, 0x03, 0x02, 0x01,
			0x12, 0x34
	};
	uintptr_t testMemoryAddr001 = PointerUtils::toValue(testMemory001);
	uint8_t testMemory002[6] = {
			0xf8, 0xf7, 0xf6, 0xf5,
			0xf4, 0xf3
	};
	uintptr_t testMemoryAddr002 = PointerUtils::toValue(testMemory002);
//	pure::print("address (testMemory001) [{0}]\n", (void*)testMemory001);
//	pure::print("address (testMemory002) [{0}]\n", (void*)testMemory002);
//	printByMemoryUtils(testMemory001, sizeof(testMemory001));
//	printByMemoryUtils(testMemory002, sizeof(testMemory002));

	// Test readableBytesToString
	{
		fmt::MemoryWriter writer;
		uintptr_t addr01 = testMemoryAddr001;
		uintptr_t addr02 = testMemoryAddr001+(MemoryUtils::DEFAULT_WIDTH_FOR_READABLE_BLOCK_BYTES);
		uintptr_t addr03 = testMemoryAddr001+(MemoryUtils::DEFAULT_WIDTH_FOR_READABLE_BLOCK_BYTES*2);
		std::string addrString01 = MemoryUtils::convertToAddressString(writer, addr01);
		std::string addrString02 = MemoryUtils::convertToAddressString(writer, addr02);
		std::string addrString03 = MemoryUtils::convertToAddressString(writer, addr03);

		uint8_t* testMem = testMemory001;
		size_t testMemSize = sizeof(testMemory001);

		std::string expectedString001 = pure::fmt(
				"[0x{0}] [f8] [f7] [f6] [f5] [f4] [f3] [f2] [f1] [08] [07] [06] [05] [04] [03] [02] [01] [12] [34]\n",
				addrString01);
		EXPECT_EQ(expectedString001, MemoryUtils::readableLineBytesToString(testMem, testMemSize, true));

		std::string expectedString002 = pure::fmt(
				"[0x{0}] [f8] [f7] [f6] [f5] [f4] [f3] [f2] [f1] [08] [07] [06] [05] [04] [03] [02] [01] [12] [34]",
				addrString01);
		EXPECT_EQ(expectedString002, MemoryUtils::readableLineBytesToString(testMem, testMemSize, false));

		std::string expectedString003 = pure::fmt(
				"[0x{0}] [f8] [f7] [f6] [f5] [f4] [f3] [f2] [f1]\n"
				"[0x{1}] [08] [07] [06] [05] [04] [03] [02] [01]\n"
				"[0x{2}] [12] [34]\n",
				addrString01, addrString02, addrString03);
		EXPECT_EQ(expectedString003, MemoryUtils::readableBlockBytesToString(testMem, testMemSize));
	}
	{
		fmt::MemoryWriter writer;
		uintptr_t addr01 = testMemoryAddr002;
		std::string addrString01 = MemoryUtils::convertToAddressString(writer, addr01);

		uint8_t* testMem = testMemory002;
		size_t testMemSize = sizeof(testMemory002);

		std::string expectedString001 = pure::fmt("[0x{0}] [f8] [f7] [f6] [f5] [f4] [f3]\n", addrString01);
		EXPECT_EQ(expectedString001, MemoryUtils::readableLineBytesToString(testMem, testMemSize, true));

		std::string expectedString002 = pure::fmt("[0x{0}] [f8] [f7] [f6] [f5] [f4] [f3]", addrString01);
		EXPECT_EQ(expectedString002, MemoryUtils::readableLineBytesToString(testMem, testMemSize, false));

		std::string expectedString003 = pure::fmt("[0x{0}] [f8] [f7] [f6] [f5] [f4] [f3]\n", addrString01);
		EXPECT_EQ(expectedString003, MemoryUtils::readableBlockBytesToString(testMem, testMemSize));
	}

	// Test copyableBytesToString
	{
		uint8_t* testMem = testMemory001;
		size_t testMemSize = sizeof(testMemory001);

		std::string expectedString001 =
				"0xf8, 0xf7, 0xf6, 0xf5, 0xf4, 0xf3, 0xf2, 0xf1, 0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x12, 0x34, \n";
		EXPECT_EQ(expectedString001, MemoryUtils::copyableLineBytesToString(testMem, testMemSize, true));

		std::string expectedString002 =
				"0xf8, 0xf7, 0xf6, 0xf5, 0xf4, 0xf3, 0xf2, 0xf1, 0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x12, 0x34, ";
		EXPECT_EQ(expectedString002, MemoryUtils::copyableLineBytesToString(testMem, testMemSize, false));

		std::string expectedString003 =
				"0xf8, 0xf7, 0xf6, 0xf5, 0xf4, 0xf3, 0xf2, 0xf1, \n"
				"0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, \n"
				"0x12, 0x34, \n";
		EXPECT_EQ(expectedString003, MemoryUtils::copyableBlockBytesToString(testMem, testMemSize));
	}
	{
		uint8_t* testMem = testMemory002;
		size_t testMemSize = sizeof(testMemory002);

		std::string expectedString001 = "0xf8, 0xf7, 0xf6, 0xf5, 0xf4, 0xf3, \n";
		EXPECT_EQ(expectedString001, MemoryUtils::copyableLineBytesToString(testMem, testMemSize, true));

		std::string expectedString002 = "0xf8, 0xf7, 0xf6, 0xf5, 0xf4, 0xf3, ";
		EXPECT_EQ(expectedString002, MemoryUtils::copyableLineBytesToString(testMem, testMemSize, false));

		std::string expectedString003 = "0xf8, 0xf7, 0xf6, 0xf5, 0xf4, 0xf3, \n";
		EXPECT_EQ(expectedString003, MemoryUtils::copyableBlockBytesToString(testMem, testMemSize));
	}

	// Test parsableBytesToString
	{
		uint8_t* testMem = testMemory001;
		size_t testMemSize = sizeof(testMemory001);

		std::string expectedString001 =
				"f8 f7 f6 f5 f4 f3 f2 f1 08 07 06 05 04 03 02 01 12 34 \n";
		EXPECT_EQ(expectedString001, MemoryUtils::parsableLineBytesToString(testMem, testMemSize, true));

		std::string expectedString002 =
				"f8 f7 f6 f5 f4 f3 f2 f1 08 07 06 05 04 03 02 01 12 34 ";
		EXPECT_EQ(expectedString002, MemoryUtils::parsableLineBytesToString(testMem, testMemSize, false));

		std::string expectedString003 =
				"f8 f7 f6 f5 f4 f3 f2 f1 \n"
				"08 07 06 05 04 03 02 01 \n"
				"12 34 \n";
		EXPECT_EQ(expectedString003, MemoryUtils::parsableBlockBytesToString(testMem, testMemSize));
	}
	{
		uint8_t* testMem = testMemory002;
		size_t testMemSize = sizeof(testMemory002);

		std::string expectedString001 = "f8 f7 f6 f5 f4 f3 \n";
		EXPECT_EQ(expectedString001, MemoryUtils::parsableLineBytesToString(testMem, testMemSize, true));

		std::string expectedString002 = "f8 f7 f6 f5 f4 f3 ";
		EXPECT_EQ(expectedString002, MemoryUtils::parsableLineBytesToString(testMem, testMemSize, false));

		std::string expectedString003 = "f8 f7 f6 f5 f4 f3 \n";
		EXPECT_EQ(expectedString003, MemoryUtils::parsableBlockBytesToString(testMem, testMemSize));
	}
}

} /* END of namespace */
