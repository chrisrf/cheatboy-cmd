#ifndef __MemoryUtilsTests_H__
#define __MemoryUtilsTests_H__

#include <stdint.h>
#include <gtest/gtest.h>

namespace pure {

class MemoryUtilsTests: public ::testing::Test {
public:
	MemoryUtilsTests() {}
	virtual ~MemoryUtilsTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	void printByMemoryUtils(uint8_t* ptr, size_t size);
};

} /* END of namespace */

#endif
