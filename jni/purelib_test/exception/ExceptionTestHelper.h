#ifndef __ExceptionTestHelper_H__
#define __ExceptionTestHelper_H__

#include <gtest/gtest.h>
#include <PureMacro.h>

namespace pure {

class ExceptionTestHelper {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(ExceptionTestHelper);
public:
	static void assertException(Exception& e, const char* name, int32_t code, const char* file, int64_t line, const char* description) {
		EXPECT_STREQ(name, e.getName());
		EXPECT_EQ(code, e.getCode());
		EXPECT_STREQ(file, e.getFile().c_str());
		EXPECT_EQ(line, e.getLine());
		EXPECT_STREQ(description, e.getDescription().c_str());
	}
	static void assertExceptionEmpty(Exception& e, const char* name, int32_t code) {
		EXPECT_STREQ(name, e.getName());
		EXPECT_EQ(code, e.getCode());
		EXPECT_EQ(true, e.getFile().empty());
		EXPECT_EQ(0, e.getLine());
		EXPECT_EQ(true, e.getDescription().empty());
	}
};

} /* END of namespace */

#endif
