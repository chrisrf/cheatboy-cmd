#include "ExceptionTests.h"

#include <purelib/string/StringFormat.h>
#include <purelib/exception/Exception.h>
#include <purelib_test/exception/ExceptionTestHelper.h>

namespace pure {

TEST_F(ExceptionTests,testException) {
	using Helper = ExceptionTestHelper;

	bool isCaught = false;

	try {
		throw Exception("Exception_1", 1, "file_111", 111, "aaa");
	} catch (Exception& e) {
		isCaught = true;
		Helper::assertException(e, "Exception_1", 1, "file_111", 111, "aaa");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		std::string s("bbb");
		throw Exception("Exception_2", 2, "file_222", 222, s);
	} catch (Exception& e) {
		isCaught = true;
		Helper::assertException(e, "Exception_2", 2, "file_222", 222, "bbb");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		throw Exception("Exception_3", 3, "file_333", 333, pure::fmt("{0}{1}{0}", "ccc", 333));
	} catch (Exception& e) {
		isCaught = true;
		Helper::assertException(e, "Exception_3", 3, "file_333", 333, "ccc333ccc");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		std::string s("ddd");
		throw Exception("Exception_4", 4, "file_444", 444, std::move(s));
	} catch (Exception& e) {
		isCaught = true;
		Helper::assertException(e, "Exception_4", 4, "file_444", 444, "ddd");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		Exception ex("Exception_5", 5, "file_555", 555, "eee");
		throw ex;
	} catch (Exception& e) {
		isCaught = true;
		Helper::assertException(e, "Exception_5", 5, "file_555", 555, "eee");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		throw Exception("Exception_6", 6, "file_666", 666, "fff");
	} catch (Exception& e) {
		Helper::assertException(e, "Exception_6", 6, "file_666", 666, "fff");

		try {
			Exception ex("Exception_77", 7, "file_777", 777, "ggg");
			ex = std::move(e);
			Helper::assertExceptionEmpty(e, "Exception_6", 6);
			Helper::assertException(ex, "Exception_6", 6, "file_666", 666, "fff");
			throw ex;
		} catch (Exception& rethrownEx) {
			isCaught = true;

			Helper::assertException(rethrownEx, "Exception_6", 6, "file_666", 666, "fff");
		}
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;
}

} /* END of namespace */
