#include "UnImplementExceptionTests.h"

#include <purelib/string/StringFormat.h>
#include <purelib/exception/UnImplementException.h>
#include <purelib_test/exception/ExceptionTestHelper.h>

namespace pure {

TEST_F(UnImplementExceptionTests,testUnImplementException) {
	using Helper = ExceptionTestHelper;
	const char* expectedName = "UnImplementException";
	const int32_t expectedCode = 0;

	bool isCaught = false;

	try {
		throw UnImplementException("file_111", 111, "aaa");
	} catch (UnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_111", 111, "aaa");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		std::string s("bbb");
		throw UnImplementException("file_222", 222, s);
	} catch (UnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_222", 222, "bbb");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		throw UnImplementException("file_333", 333, pure::fmt("{0}{1}{0}", "ccc", 333));
	} catch (UnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_333", 333, "ccc333ccc");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		std::string s("ddd");
		throw UnImplementException("file_444", 444, std::move(s));
	} catch (UnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_444", 444, "ddd");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		UnImplementException ex("file_555", 555, "eee");
		throw ex;
	} catch (UnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_555", 555, "eee");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		throw UnImplementException("file_666", 666, "fff");
	} catch (UnImplementException& e) {
		Helper::assertException(e, expectedName, expectedCode, "file_666", 666, "fff");

		try {
			UnImplementException ex("file_777", 777, "ggg");
			ex = std::move(e);
			Helper::assertExceptionEmpty(e, expectedName, expectedCode);
			Helper::assertException(ex, expectedName, expectedCode, "file_666", 666, "fff");
			throw ex;
		} catch (UnImplementException& rethrownEx) {
			isCaught = true;

			Helper::assertException(rethrownEx, expectedName, expectedCode, "file_666", 666, "fff");
		}
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;
}

TEST_F(UnImplementExceptionTests,testUltraUnImplementException) {
	using Helper = ExceptionTestHelper;
	const char* expectedName = "UltraUnImplementException";
	const int32_t expectedCode = 0;

	bool isCaught = false;

	try {
		throw UltraUnImplementException("file_111", 111, "aaa");
	} catch (UltraUnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_111", 111, "aaa");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		std::string s("bbb");
		throw UltraUnImplementException("file_222", 222, s);
	} catch (UltraUnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_222", 222, "bbb");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		throw UltraUnImplementException("file_333", 333, pure::fmt("{0}{1}{0}", "ccc", 333));
	} catch (UltraUnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_333", 333, "ccc333ccc");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		std::string s("ddd");
		throw UltraUnImplementException("file_444", 444, std::move(s));
	} catch (UltraUnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_444", 444, "ddd");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		UltraUnImplementException ex("file_555", 555, "eee");
		throw ex;
	} catch (UltraUnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_555", 555, "eee");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		throw UltraUnImplementException("file_666", 666, "fff");
	} catch (UltraUnImplementException& e) {
		Helper::assertException(e, expectedName, expectedCode, "file_666", 666, "fff");

		try {
			UltraUnImplementException ex("file_777", 777, "ggg");
			ex = std::move(e);
			Helper::assertExceptionEmpty(e, expectedName, expectedCode);
			Helper::assertException(ex, expectedName, expectedCode, "file_666", 666, "fff");
			throw ex;
		} catch (UltraUnImplementException& rethrownEx) {
			isCaught = true;

			Helper::assertException(rethrownEx, expectedName, expectedCode, "file_666", 666, "fff");
		}
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;

	try {
		throw UltraUnImplementException("file_888", 888, "ultra");
	} catch (UnImplementException& e) {
		isCaught = true;
		Helper::assertException(e, expectedName, expectedCode, "file_888", 888, "ultra");
	}
	EXPECT_EQ(true, isCaught);
	isCaught = false;
}

} /* END of namespace */
