LOCAL_PATH := $(call my-dir)
$(call __ndk_info, Android.mk at [$(LOCAL_PATH)])

include $(CLEAR_VARS)

####### Get platform
ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
	PVAL_MAIN_NBIT_PALTFORM := 64
else ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
	PVAL_MAIN_NBIT_PALTFORM := 32
else ifeq ($(TARGET_ARCH_ABI),armeabi-v7a-hard)
	PVAL_MAIN_NBIT_PALTFORM := 32
else ifeq ($(TARGET_ARCH_ABI),armeabi)
	PVAL_MAIN_NBIT_PALTFORM := 32
else ifeq ($(TARGET_ARCH_ABI),x86_64)
	PVAL_MAIN_NBIT_PALTFORM := 64
else ifeq ($(TARGET_ARCH_ABI),x86)
	PVAL_MAIN_NBIT_PALTFORM := 32
else ifeq ($(TARGET_ARCH_ABI),mips64)
	PVAL_MAIN_NBIT_PALTFORM := 64
else ifeq ($(TARGET_ARCH_ABI),mips)
	PVAL_MAIN_NBIT_PALTFORM := 32
else
	$(error Unsupported platform)
endif
$(call __ndk_info, PVAL_MAIN_NBIT_PALTFORM is [$(PVAL_MAIN_NBIT_PALTFORM)])

####### global useful personal variables
PVAL_MAIN_SOURCE_FILE_ROOT_PATH		:= $(APP_PROJECT_PATH)/jni

ifeq ($(PVAL_MAIN_BUILD_FOR_COMPAT),false)
PVAL_MAIN_PREBUILT_LIB_ROOT_PATH	:= $(APP_PROJECT_PATH)/jni/prebuilt
else
PVAL_MAIN_PREBUILT_LIB_ROOT_PATH	:= $(APP_PROJECT_PATH)/jni/prebuilt/_compat
endif

ifeq ($(PVAL_MAIN_BUILD_FOR_CHEAT_BOY_TUNNEL_TERMINAL),false)
PVAL_MAIN_MAKE_FILES		:= $(addprefix $(LOCAL_PATH)/, $(addsuffix /Android.mk, \
		cheatboy/terminal \
		cheatboy/engine_test \
		purelib_test \
	))
else
PVAL_MAIN_MAKE_FILES		:= $(addprefix $(LOCAL_PATH)/, $(addsuffix /Android.mk, \
		cheatboy/tunnel \
		cheatboy/terminal \
		cheatboy/engine_test \
		purelib_test \
	))
endif
#		purelib \
		cheatboy/engine \
		cheatboy/terminal \
#PVAL_MAIN_MAKE_FILES		:= $(call all-makefiles-under, $(LOCAL_PATH)/cheatboy)
$(call __ndk_info, PVAL_MAIN_PREBUILT_LIB_ROOT_PATH is [$(PVAL_MAIN_PREBUILT_LIB_ROOT_PATH)])
$(foreach e, $(PVAL_MAIN_MAKE_FILES), $(call __ndk_info, Will execute the makefile [$(e)]))

PVAL_MAIN_ARM_MODE	:= arm
$(call __ndk_info, PVAL_MAIN_ARM_MODE is [$(PVAL_MAIN_ARM_MODE)])

include $(PVAL_MAIN_MAKE_FILES)

