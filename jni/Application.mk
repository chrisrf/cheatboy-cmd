LOCAL_PATH := $(call my-dir)
$(call __ndk_info, Application.mk at [$(LOCAL_PATH)])
# Note: Use the command when building debug version
# ndk-build -j4 NDK_LOG=1 NDK_DEBUG=1
# ndk-build -j4 NDK_LOG=1 NDK_DEBUG=0

include $(CLEAR_VARS)
PVAL_MAIN_BUILD_FOR_COMPAT										:= false
PVAL_MAIN_BUILD_FOR_CHEAT_BOY_TUNNEL_TERMINAL	:= true

APP_PROJECT_PATH	:= $(realpath .)
APP_BUILD_SCRIPT	:= $(APP_PROJECT_PATH)/jni/Android.mk
$(call __ndk_info, APP_PROJECT_PATH is [$(APP_PROJECT_PATH)])
$(call __ndk_info, APP_BUILD_SCRIPT is [$(APP_BUILD_SCRIPT)])

# 64bit version only for api 21 (LOLLIPOP) and higher
# If you set the api level below api 21,
# when building 64bit version, it will auto set the api level to 21.
#APP_PLATFORM		:= android-14

ifeq ($(PVAL_MAIN_BUILD_FOR_COMPAT),false)
APP_PLATFORM	:= android-21
APP_ABI				:= armeabi-v7a
APP_PIE				:= true
$(call __ndk_info, PVAL_MAIN_BUILD_FOR_COMPAT is false)
else
APP_PLATFORM	:= android-14
APP_ABI				:= armeabi-v7a
APP_PIE				:= false
$(call __ndk_info, PVAL_MAIN_BUILD_FOR_COMPAT is true)
endif
#APP_ABI			:= armeabi armeabi-v7a x86 arm64-v8a x86_64

NDK_TOOLCHAIN_VERSION		:= 4.9
#NDK_TOOLCHAIN_VERSION	:= clang3.6

### Following is true if you use exception.
# If you set APP_PLATFORM := android-16,
# the ndk build system will auto set APP_PIE := true.
# Your binary will segfault on older (under API 21 (lollipop)) SDKs.
### Following is true if you do NOT use exception.
# If you can live with only supporting Android 4.1+ (API 16+),
# just set APP_PLATFORM := android-16 and you'll be good to go.
# Behind the scenes it sets APP_PIE := true.
# Your binary will segfault on older (under API 16) SDKs.
#APP_PIE := true

### APP_STL
# GNU STL implements most C++11 features. Use either gnustl_static or gnustl_shared.
# Without this your C++ code will not be able to access headers like <thread>, <mutex>.
APP_STL					:= gnustl_static
#APP_STL				:= c++_static
#LIBCXX_FORCE_REBUILD	:= true
#APP_STL				:= stlport_shared
#STLPORT_FORCE_REBUILD	:= true

### APP_CFLAGS
### A set of C compiler flags passed when compiling any C or C++ source code of any of the modules.
APP_CFLAGS	+= -D__STDC_FORMAT_MACROS
#APP_CFLAGS += -Wall -Wextra
APP_CFLAGS	+= -Wtype-limits -Wsign-compare -Wconversion -Wstrict-aliasing

### APP_CPPFLAGS
### A set of C++ compiler flags passed when building C++ sources *only*.
# C++11 and threading enabling features.
# Otherwise c++11, pthread, rtti and exceptions are not enabled by default
APP_CPPFLAGS	+= -std=c++11 -frtti -fexceptions -pthread

### APP_LDFLAGS
#APP_LDFLAGS		+=

ifeq ($(PVAL_MAIN_BUILD_FOR_CHEAT_BOY_TUNNEL_TERMINAL),true)
	ifeq ($(NDK_DEBUG),1)
		$(error You should NOT build Terminal Tunnel with NDK_DEBUG=1)
	endif
endif

$(call __ndk_info, NDK_DEBUG is $(NDK_DEBUG))
ifeq ($(NDK_DEBUG),1)
	APP_OPTIM			:= debug
	APP_CPPFLAGS	+= -DDEBUG
	APP_CPPFLAGS	+= -D__PURE_ENABLE_DEBUG_LOG__
	APP_CPPFLAGS	+= -D__CBOY_ASSERTION_SCANNED_DATA_UTILS__
$(call __ndk_info, NDK_DEBUG is 1)
else
	APP_OPTIM		:= release
	APP_CFLAGS	+= -DNDEBUG
$(call __ndk_info, NDK_DEBUG is 0)
endif

####### global useful personal functions
### pfunc_dump_directory
# If you pass the parameter $(1) as [jni/modulefolder/engine],
# it will return the file name list with the [parent path] (the parameter $(1)),
# like [jni/modulefolder/engine ... jni/modulefolder/engine/somedir/somesubdir/YourClass.cpp],
# But sometimes we only need [somedir/somesubdir/YourClass.cpp],
# so you may need pfunc_get_all_file_list_under_local_path.
define pfunc_dump_directory
$(wildcard $(1)) $(foreach e, $(wildcard $(1)/*), $(call pfunc_dump_directory, $(e)))
endef

define pfunc_get_all_file_list
$(filter-out $(1), $(call pfunc_dump_directory, $(1)))
endef

define pfunc_get_all_cpp_file_list
$(filter %.cpp, $(call pfunc_get_all_file_list, $(1)))
endef

### pfunc_get_all_file_list_under_local_path
# can not use following script:
# 	$(patsubst $(1)/%, %, $(call pfunc_get_all_file_list, $(1)))
# the first $(1) can not be expanded.
define pfunc_get_all_file_list_under_local_path
$(patsubst $(LOCAL_PATH)/%, %, $(call pfunc_get_all_file_list, $(1)))
endef

define pfunc_get_all_cpp_file_list_under_local_path
$(patsubst $(LOCAL_PATH)/%, %, $(call pfunc_get_all_cpp_file_list, $(1)))
endef