#ifndef __TunnelHelper_H__
#define __TunnelHelper_H__

#include <stdint.h>
#include <string>

namespace pure {
class Exception;
}

namespace cboy {
class ICheatEngine;

int32_t tagState(const ICheatEngine& cheatEngine);
int32_t tagEnd();
int32_t tagJsonResponse(const char* json);
int32_t tagJsonResponse(const std::string& json);
int32_t tagErrorCode(int32_t code);
int32_t tagErrorCodeWithException(int32_t code, const pure::Exception& exception);
int32_t tagErrorCodeAndExceptionInCatchBlock(int32_t code, const char* file, int64_t line);

} /* END of namespace */

#endif
