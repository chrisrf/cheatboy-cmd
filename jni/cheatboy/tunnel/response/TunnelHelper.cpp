#include "TunnelHelper.h"

#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

int32_t tagException(const pure::Exception& exception);
int32_t tagExceptionInCatchBlock(const char* file, int64_t line);

int32_t tagState(const ICheatEngine& cheatEngine) {
	pure::println("[#State#]{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}",
			static_cast<int>(cheatEngine.getScannedRegionLevel()),
			cheatEngine.getScannedValueTypeFlag().getFlag(),
			cheatEngine.getThresholdOfListableMatchedValues(),
			static_cast<int>(cheatEngine.getMemoryReaderMode()),
			(cheatEngine.isSmartReaderModeEnabled() ? 1 : 0),
			cheatEngine.getReaderBufferPageSizeInMB(),
			cheatEngine.getPid(),
			(cheatEngine.hasAttachedProcess() ? 1 : 0),
			cheatEngine.getNumOfLoadedRegions(),
			cheatEngine.getNumOfScannedRegions(),
			cheatEngine.getNumOfCurrentMatchedValues());
	return 0;
}

int32_t tagEnd() {
	pure::println("[#End#]");
	return 0;
}

int32_t tagJsonResponse(const char* json) {
	pure::println("[#JSON#]");
	pure::println("{0}", json);
	pure::println("[##JSON##]");
	return 0;
}

int32_t tagJsonResponse(const std::string& json) {
	pure::println("[#JSON#]");
	pure::println("{0}", json);
	pure::println("[##JSON##]");
	return 0;
}

int32_t tagErrorCode(int32_t code) {
	pure::println("[#Error#]{0},{1}", code, 0);
	return 0;
}

int32_t tagErrorCodeWithException(int32_t code, const pure::Exception& exception) {
	pure::println("[#Error#]{0},{1}", code, 1);
	tagException(exception);
	return 0;
}

int32_t tagErrorCodeAndExceptionInCatchBlock(int32_t code, const char* file, int64_t line) {
	pure::println("[#Error#]{0},{1}", code, 1);
	tagExceptionInCatchBlock(file, line);
	return 0;
}

int32_t tagException(const pure::Exception& exception) {
	pure::println("[#Exception#]");
	pure::println("{0}", exception.getName());
	pure::println("{0}", exception.getCode());
	pure::println("{0}", exception.getFile());
	pure::println("{0}", exception.getLine());
	pure::println("{0}", exception.getDescription());
	pure::println("[##Exception##]");
	return 0;
}

int32_t tagExceptionInCatchBlock(const char* file, int64_t line) {
	try {
		throw;
	} catch (pure::Exception& e) {
		tagException(e);
	} catch (std::exception& e) {
		tagException(pure::Exception("StdException", 0, file, line, e.what()));
	} catch (...) {
		tagException(pure::Exception("UnknownException", 0, file, line, "UnknownException"));
	}
	return 0;
}

} /* END of namespace */
