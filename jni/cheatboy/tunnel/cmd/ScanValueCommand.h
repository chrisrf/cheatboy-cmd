#ifndef __ScanValueCommand_H__
#define __ScanValueCommand_H__

#include <vector>
#include <map>
#include <functional>

#include <cheatboy/tunnel/cmd/base/TerminalCommand.h>

namespace cboy {
enum class ComparisonMethod;
class ScanArguments;

class ScanValueCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ScanValueCommand);
private:
	typedef std::function<int32_t(ICheatEngine&, const std::string&)> CommandHandler;
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;
public:
	static const std::vector<std::string> CMD_INDEXES;
	static const std::string INDEX_CAPTURE;
	static const std::string INDEX_UPDATE;
	static const std::string INDEX_EQUAL_TO;
	static const std::string INDEX_NOT_EQUAL_TO;
	static const std::string INDEX_GREATER_THAN;
	static const std::string INDEX_LESS_THAN;
	static const std::string INDEX_INCREASED_BY;
	static const std::string INDEX_DECREASED_BY;
private:
	std::map<std::string, CommandHandler> _handlers;

public:
	ScanValueCommand();
	virtual ~ScanValueCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
private:
	int32_t captureAndUpdate(ICheatEngine& cheatEngine, const std::string& valueString);
	int32_t equalTo(ICheatEngine& cheatEngine, const std::string& valueString);
	int32_t notEqualTo(ICheatEngine& cheatEngine, const std::string& valueString);
	int32_t greaterThan(ICheatEngine& cheatEngine, const std::string& valueString);
	int32_t lessThan(ICheatEngine& cheatEngine, const std::string& valueString);
	int32_t increasedBy(ICheatEngine& cheatEngine, const std::string& valueString);
	int32_t decreasedBy(ICheatEngine& cheatEngine, const std::string& valueString);
private:
	int32_t scanWithOrWithOutUserValue(
		ICheatEngine& cheatEngine, const std::string& valueString,
		ComparisonMethod usedMethodIfUserValueProvided, ComparisonMethod usedMethodIfNoUserValue);
	int32_t scanWithUserValue(
		ICheatEngine& cheatEngine, ComparisonMethod method, const std::string& valueString);
	bool checkValueTypeFlagIsValidBeforeScanning(ICheatEngine& cheatEngine, ScanArguments& args);
private:
	void handleErrorOfConversionForUserValue(ComparisonMethod method, const std::string& valueString);
	void handleErrorOfBuildArgumentsWithOutUserValue(ComparisonMethod method);
	void handleErrorOfWithOutUserValue(ComparisonMethod method);
	void handleErrorOfUsingAnOldValueRequiredMethodWithoutScannedRegions(ComparisonMethod method);
	void handleErrorOfValueTypeFlagIsInvalid();
};

} /* END of namespace */

#endif
