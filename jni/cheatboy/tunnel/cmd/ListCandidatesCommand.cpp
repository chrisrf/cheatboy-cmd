#include "ListCandidatesCommand.h"

#include <sstream>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <PureCppCompatibility.h>
#include <PureLib.h>
#include <CheatEngineLib.h>

#include <cheatboy/tunnel/response/TunnelHelper.h>
#include <cheatboy/tunnel/response/TunnelReturnCode.h>

namespace cboy {

class ListCandidatesCommandImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ListCandidatesCommandImpl);
public:
	ListCandidatesCommandImpl() {}
	virtual ~ListCandidatesCommandImpl() noexcept {}
public:
	void encapsulateMatchedValue(
		rapidjson::Document& document, rapidjson::Document::AllocatorType& allocator,
		const MatchedValue& value);
private:
	template<typename T1, typename T2> void checkAndPush(
		rapidjson::Value& valueArray, rapidjson::Document::AllocatorType& allocator,
		const MatchedValue& value);
};

const std::string ListCandidatesCommand::NAME = "ListCandidatesCommand";
const std::string ListCandidatesCommand::DIGEST = "ListCandidatesCommand";
const std::string ListCandidatesCommand::DOCUMENT = "ListCandidatesCommand";

ListCandidatesCommand::ListCandidatesCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT),
	_impl(std::make_unique<ListCandidatesCommandImpl>()) {}

ListCandidatesCommand::~ListCandidatesCommand() {}

int32_t ListCandidatesCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	rapidjson::Document document;
	//document.SetObject();	// It is a JSON object.
	document.SetArray();	// It is a JSON array.
	rapidjson::Document::AllocatorType& allocator = document.GetAllocator();

	typedef ICheatEngine::IMatchedValueHandler IMatchedValueHandler;
	IMatchedValueHandler handler = [this, &document, &allocator] (const MatchedValue& value) {
		_impl->encapsulateMatchedValue(document, allocator, value);
	};
	cheatEngine.iterateOverMatchedValues(handler);

	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	document.Accept(writer);

	cboy::tagJsonResponse(buffer.GetString());
	return 0;
}

void ListCandidatesCommandImpl::encapsulateMatchedValue(
	rapidjson::Document& document, rapidjson::Document::AllocatorType& allocator,
	const MatchedValue& value) {

	rapidjson::Value jsonMatchedValue(rapidjson::kObjectType);
	std::string strRegionID = pure::fmt("{0}", value.getRegionID());
	std::string strMatchID = pure::fmt("{0}", value.getMatchID());
	std::string strRemoteAddress = pure::fmt("{0}", pure::PointerUtils::toPointer(value.getRemoteAddress()));
	rapidjson::Value valueRegionID(rapidjson::kStringType);
	rapidjson::Value valueMatchID(rapidjson::kStringType);
	rapidjson::Value valueRemoteAddress(rapidjson::kStringType);
	valueRegionID.SetString(strRegionID.c_str(), strRegionID.length(), allocator);
	valueMatchID.SetString(strMatchID.c_str(), strMatchID.length(), allocator);
	valueRemoteAddress.SetString(strRemoteAddress.c_str(), strRemoteAddress.length(), allocator);
	jsonMatchedValue.AddMember("regionID", valueRegionID, allocator);
	jsonMatchedValue.AddMember("matchID", valueMatchID, allocator);
	jsonMatchedValue.AddMember("remoteAddress", valueRemoteAddress, allocator);

	MemoryValueTypeFlag flag = value.getMatchFlag();
	rapidjson::Value valueArray(rapidjson::kArrayType);
	checkAndPush<int8_t, int64_t>(valueArray, allocator, value);
	checkAndPush<uint8_t, uint64_t>(valueArray, allocator, value);
	checkAndPush<int16_t, int64_t>(valueArray, allocator, value);
	checkAndPush<uint16_t, uint64_t>(valueArray, allocator, value);
	checkAndPush<int32_t, int64_t>(valueArray, allocator, value);
	checkAndPush<uint32_t, uint64_t>(valueArray, allocator, value);
	checkAndPush<int64_t, int64_t>(valueArray, allocator, value);
	checkAndPush<uint64_t, uint64_t>(valueArray, allocator, value);
	checkAndPush<float, float>(valueArray, allocator, value);
	checkAndPush<double, double>(valueArray, allocator, value);
	jsonMatchedValue.AddMember("matchedValues", valueArray, allocator);

	document.PushBack(jsonMatchedValue, allocator);
}

template<typename T1, typename T2>
void ListCandidatesCommandImpl::checkAndPush(
	rapidjson::Value& valueArray, rapidjson::Document::AllocatorType& allocator,
	const MatchedValue& value) {

	if (value.getMatchFlag().isMarkedFor<T1>()) {
		T1 printValue = value.getAs<T1>();

		rapidjson::Value typeValue(rapidjson::kObjectType);
		typeValue.AddMember("typeID", MemoryValueTypeIdUtils::idOf<T1>(), allocator);

		std::string strPrintValue = pure::fmt("{0}", (T2)printValue);
		rapidjson::Value valuePrintValue(rapidjson::kStringType);
		valuePrintValue.SetString(strPrintValue.c_str(), strPrintValue.length(), allocator);
		typeValue.AddMember("value", valuePrintValue, allocator);

		valueArray.PushBack(typeValue, allocator);
	}
}


} /* END of namespace */
