#ifndef __TerminalCommand_H__
#define __TerminalCommand_H__

#include <cheatboy/tunnel/cmd/base/ITerminalCommand.h>

namespace cboy {

class TerminalCommand: public ITerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(TerminalCommand);
private:
	const std::string& _name;
	const std::string& _digest;
	const std::string& _document;

protected:
	TerminalCommand(const std::string& name, const std::string& digest, const std::string& document)
		: _name(name), _digest(digest), _document(document) {}
public:
	virtual ~TerminalCommand() noexcept {}
public:
	virtual const std::string& getName() const override { return _name; }
	virtual const std::string& getDigest() const override { return _digest; }
	virtual const std::string& getDocument() const override { return _document; }
};

} /* END of namespace */

#endif
