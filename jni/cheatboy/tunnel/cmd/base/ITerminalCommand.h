#ifndef __ITerminalCommand_H__
#define __ITerminalCommand_H__

#include <stdint.h>
#include <string>
#include <PureMacro.h>

namespace cboy {
class ICheatEngine;

class ITerminalCommand {
PURE_DECLARE_CLASS_AS_INTERFACE(ITerminalCommand);
public:
	virtual const std::string& getName() const = 0;
	virtual const std::string& getDigest() const = 0;
	virtual const std::string& getDocument() const = 0;
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) = 0;
};

} /* END of namespace */

#endif
