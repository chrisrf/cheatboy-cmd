#include "DetachCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string DetachCommand::NAME = "DetachCommand";
const std::string DetachCommand::DIGEST = "DetachCommand";
const std::string DetachCommand::DOCUMENT = "DetachCommand";

DetachCommand::DetachCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

DetachCommand::~DetachCommand() {}

int32_t DetachCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	cheatEngine.detach();
	return 0;
}

} /* END of namespace */
