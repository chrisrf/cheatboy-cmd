#ifndef __ExitCommand_H__
#define __ExitCommand_H__

#include <cheatboy/tunnel/cmd/base/TerminalCommand.h>

namespace cboy {

class ExitCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ExitCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	ExitCommand();
	virtual ~ExitCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
};

} /* END of namespace */

#endif
