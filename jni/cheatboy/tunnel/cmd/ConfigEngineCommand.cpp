#include "ConfigEngineCommand.h"

#include <map>
#include <sstream>
#include <PureCppCompatibility.h>
#include <PureLib.h>
#include <CheatEngineLib.h>

#include <cheatboy/tunnel/response/TunnelHelper.h>
#include <cheatboy/tunnel/response/TunnelReturnCode.h>

namespace cboy {

class ConfigEngineCommandImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ConfigEngineCommandImpl);
public:
	typedef std::function<int32_t(ICheatEngine&, const std::string&)> OptionHandler;
public:
	static const int SUCCESS = 0;
	static const int ERROR = -1;
public:
	std::map<std::string, OptionHandler> _handlers;

public:
	ConfigEngineCommandImpl(): _handlers() {
		_handlers.insert(std::make_pair("a",
			[this] (ICheatEngine& engine, const std::string& value) { return handleAllOption(engine, value); }));
		_handlers.insert(std::make_pair("l",
			[this] (ICheatEngine& engine, const std::string& value) { return handleLevelOption(engine, value); }));
		_handlers.insert(std::make_pair("t",
			[this] (ICheatEngine& engine, const std::string& value) { return handleTypeOption(engine, value); }));
		_handlers.insert(std::make_pair("s",
			[this] (ICheatEngine& engine, const std::string& value) { return handleThresholdOption(engine, value); }));
		_handlers.insert(std::make_pair("m",
			[this] (ICheatEngine& engine, const std::string& value) { return handleReaderModeOption(engine, value); }));
		_handlers.insert(std::make_pair("r",
			[this] (ICheatEngine& engine, const std::string& value) { return handleSmartReaderModeOption(engine, value); }));
		_handlers.insert(std::make_pair("p",
			[this] (ICheatEngine& engine, const std::string& value) { return handlePageSizeOption(engine, value); }));
	}
	virtual ~ConfigEngineCommandImpl() noexcept {}
public:
	int32_t showConfig(ICheatEngine& cheatEngine);
	int32_t handleAllOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleLevelOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleTypeOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleThresholdOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleReaderModeOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleSmartReaderModeOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handlePageSizeOption(ICheatEngine& cheatEngine, const std::string& argString);
private:
	bool parseLevel(const std::string& argString, pure::NumberConverter& numConverter, MemoryRegionLevel& outLevel);
	bool parseType(const std::string& argString, MemoryValueTypeFlag& outFlag, std::string& outTypeString);
	bool parseThreshold(const std::string& argString, pure::NumberConverter& numConverter, size_t& outThreshold);
	bool parseReaderMode(const std::string& argString, pure::NumberConverter& numConverter, MemoryReaderMode& outReaderMode);
	bool parseSmartReaderMode(const std::string& argString, pure::NumberConverter& numConverter, bool& outSmartReaderModeEnabled);
	bool parsePageSize(const std::string& argString, pure::NumberConverter& numConverter, size_t& outPageSize);
};

const std::string ConfigEngineCommand::NAME = "ConfigEngineCommand";
const std::string ConfigEngineCommand::DIGEST = "ConfigEngineCommand";
const std::string ConfigEngineCommand::DOCUMENT = "ConfigEngineCommand";

ConfigEngineCommand::ConfigEngineCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT),
	_impl(std::make_unique<ConfigEngineCommandImpl>()) {}

ConfigEngineCommand::~ConfigEngineCommand() {}

int32_t ConfigEngineCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (argsLine.empty()) {
		return _impl->showConfig(cheatEngine);
	}

	std::stringstream ss(argsLine);
	std::string optionString;
	std::string argString;
	if ((!(ss >> optionString)) || (!(std::getline(ss, argString)))) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_NUM_OF_ARGS_IS_WRONG);
		return ConfigEngineCommandImpl::ERROR;
	}
	pure::StringTrimmer::trim(argString);

	typedef std::map<std::string, ConfigEngineCommandImpl::OptionHandler> HandlerMap;
	HandlerMap::iterator it = (_impl->_handlers).find(optionString);
	if (it == (_impl->_handlers).end()) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_INVALID_OPTION);
		return ConfigEngineCommandImpl::ERROR;
	}
	auto& handler = it->second;
	return handler(cheatEngine, argString);
}

int32_t ConfigEngineCommandImpl::showConfig(ICheatEngine& cheatEngine) {
	// Get the information from state tag.
	return ConfigEngineCommandImpl::SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleAllOption(ICheatEngine& cheatEngine, const std::string& argString) {
	std::stringstream ss(argString);
	std::string levelArgString;
	std::string typeArgString;
	std::string thresholdArgString;
	std::string readerModeArgString;
	std::string smartReaderModeArgString;
	std::string pageSizeArgString;
	if ((!(ss >> levelArgString)) || (!(ss >> typeArgString)) || (!(ss >> thresholdArgString)) ||
		(!(ss >> readerModeArgString)) || (!(ss >> smartReaderModeArgString)) || (!(ss >> pageSizeArgString))) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_NUM_OF_ARGS_IS_WRONG);
		return ERROR;
	}
	if (handleLevelOption(cheatEngine, levelArgString) != SUCCESS) {
		return ERROR;
	}
	if (handleTypeOption(cheatEngine, typeArgString) != SUCCESS) {
		return ERROR;
	}
	if (handleThresholdOption(cheatEngine, thresholdArgString) != SUCCESS) {
		return ERROR;
	}
	if (handleReaderModeOption(cheatEngine, readerModeArgString) != SUCCESS) {
		return ERROR;
	}
	if (handleSmartReaderModeOption(cheatEngine, smartReaderModeArgString) != SUCCESS) {
		return ERROR;
	}
	if (handlePageSizeOption(cheatEngine, pageSizeArgString) != SUCCESS) {
		return ERROR;
	}
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleLevelOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	MemoryRegionLevel level;
	if (!parseLevel(argString, numConverter, level)) {
		return ERROR;
	}
	cheatEngine.setScannedRegionLevel(level);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleTypeOption(ICheatEngine& cheatEngine, const std::string& argString) {
	MemoryValueTypeFlag flag;
	std::string typeString;
	if (!parseType(argString, flag, typeString)) {
		return ERROR;
	}
	cheatEngine.setScannedValueTypeFlag(flag);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleThresholdOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	size_t threshold = 0;
	if (!parseThreshold(argString, numConverter, threshold)) {
		return ERROR;
	}
	cheatEngine.setThresholdOfListableMatchedValues(threshold);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleReaderModeOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	MemoryReaderMode mode;
	if (!parseReaderMode(argString, numConverter, mode)) {
		return ERROR;
	}
	cheatEngine.setMemoryReaderMode(mode);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleSmartReaderModeOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	bool smartReaderModeEnabled = 0;
	if (!parseSmartReaderMode(argString, numConverter, smartReaderModeEnabled)) {
		return ERROR;
	}
	cheatEngine.setSmartReaderModeEnabled(smartReaderModeEnabled);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handlePageSizeOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	size_t pageSize = 0;
	if (!parsePageSize(argString, numConverter, pageSize)) {
		return ERROR;
	}
	cheatEngine.setReaderBufferPageSizeInMB(pageSize);
	return SUCCESS;
}

bool ConfigEngineCommandImpl::parseLevel(const std::string& argString, pure::NumberConverter& numConverter, MemoryRegionLevel& outLevel) {
	int intLevel = 0;
	if (!numConverter.convert(argString, intLevel)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_FAILED_TO_PARSE_REGION_LEVEL);
		return false;
	}
	if (!pure::EnumUtils<MemoryRegionLevel>::convert(intLevel, outLevel)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_INVALID_REGION_LEVEL);
		return false;
	}
	return true;
}

bool ConfigEngineCommandImpl::parseType(const std::string& argString, MemoryValueTypeFlag& outFlag, std::string& outTypeString) {
	if (argString.compare("i") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_SIGNED_INTEGER);
		outTypeString = "ALL_SIGNED_INTEGER";
	} else if (argString.compare("u") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER);
		outTypeString = "ALL_UNSIGNED_INTEGER";
	} else if (argString.compare("I") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_INTEGER);
		outTypeString = "ALL_INTEGER";
	} else if (argString.compare("f") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_FLOAT);
		outTypeString = "ALL_FLOAT";
	} else if (argString.compare("s") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_SIGNED_NUMBER);
		outTypeString = "ALL_SIGNED_NUMBER";
	} else if (argString.compare("n") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL);
		outTypeString = "ALL";
	} else {
		cboy::tagErrorCode(TunnelReturnCode::ERR_FAILED_TO_PARSE_SCANNED_VALUE_TYPE);
		return false;
	}
	return true;
}

bool ConfigEngineCommandImpl::parseThreshold(const std::string& argString, pure::NumberConverter& numConverter, size_t& outThreshold) {
	if (!numConverter.convert(argString, outThreshold)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_FAILED_TO_PARSE_THRESHOLD);
		return false;
	}
	return true;
}

bool ConfigEngineCommandImpl::parseReaderMode(const std::string& argString, pure::NumberConverter& numConverter, MemoryReaderMode& outReaderMode) {
	int intMode = 0;
	if (!numConverter.convert(argString, intMode)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_FAILED_TO_PARSE_MEMORY_READER_MODE);
		return false;
	}
	if (!pure::EnumUtils<MemoryReaderMode>::convert(intMode, outReaderMode)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_INVALID_MEMORY_READER_MODE);
		return false;
	}
	return true;
}

bool ConfigEngineCommandImpl::parseSmartReaderMode(const std::string& argString, pure::NumberConverter& numConverter, bool& outSmartReaderModeEnabled) {
	int intSmartReaderModeEnabled = 0;
	if (!numConverter.convert(argString, intSmartReaderModeEnabled)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_FAILED_TO_PARSE_SMART_MEMORY_READER_MODE);
		return false;
	}
	outSmartReaderModeEnabled = (intSmartReaderModeEnabled == 0 ? false : true);
	return true;
}

bool ConfigEngineCommandImpl::parsePageSize(const std::string& argString, pure::NumberConverter& numConverter, size_t& outPageSize) {
	if (!numConverter.convert(argString, outPageSize)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_FAILED_TO_PARSE_READER_BUFFER_PAGE_SIZE);
		return false;
	}
	return true;
}

} /* END of namespace */
