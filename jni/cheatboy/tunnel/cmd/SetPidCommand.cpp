#include "SetPidCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

#include <cheatboy/tunnel/response/TunnelHelper.h>
#include <cheatboy/tunnel/response/TunnelReturnCode.h>

namespace cboy {

const std::string SetPidCommand::NAME = "SetPidCommand";
const std::string SetPidCommand::DIGEST = "SetPidCommand";
const std::string SetPidCommand::DOCUMENT = "SetPidCommand";

SetPidCommand::SetPidCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

SetPidCommand::~SetPidCommand() {}

int32_t SetPidCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	std::stringstream ss(argsLine);
	std::string pidString;
	if (!(ss >> pidString)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_NUM_OF_ARGS_IS_WRONG);
		return 0;
	}
	pure::NumberConverter numConverter;
	pid_t pid = EngineConstants::NULL_PROCESS_ID;
	if (!numConverter.convert(pidString, pid)) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_FAILED_TO_PARSE_PID);
		return 0;
	}
	if (pid < EngineConstants::MIN_PROCESS_ID) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_INVALID_PID);
		return 0;
	}
	cheatEngine.setPid(pid);
	return 0;
}

} /* END of namespace */
