#ifndef __AttachCommand_H__
#define __AttachCommand_H__

#include <cheatboy/tunnel/cmd/base/TerminalCommand.h>

namespace cboy {

class AttachCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(AttachCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	AttachCommand();
	virtual ~AttachCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
};

} /* END of namespace */

#endif
