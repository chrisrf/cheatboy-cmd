#include "AttachCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

#include <cheatboy/tunnel/response/TunnelHelper.h>
#include <cheatboy/tunnel/response/TunnelReturnCode.h>

namespace cboy {

const std::string AttachCommand::NAME = "AttachCommand";
const std::string AttachCommand::DIGEST = "AttachCommand";
const std::string AttachCommand::DOCUMENT = "AttachCommand";

AttachCommand::AttachCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

AttachCommand::~AttachCommand() {}

int32_t AttachCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (cheatEngine.getPid() == EngineConstants::NULL_PROCESS_ID) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_PID_NOT_SET);
		return 0;
	}
	cheatEngine.attach();
	return 0;
}

} /* END of namespace */
