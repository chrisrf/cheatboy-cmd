#include "ScanValueCommand.h"

#include <PureLib.h>
#include <CheatEngineLib.h>

#include <cheatboy/tunnel/response/TunnelHelper.h>
#include <cheatboy/tunnel/response/TunnelReturnCode.h>

namespace cboy {

const std::string ScanValueCommand::NAME = "ScanValueCommand";
const std::string ScanValueCommand::DIGEST = "ScanValueCommand";
const std::string ScanValueCommand::DOCUMENT = "ScanValueCommand";

const std::vector<std::string> ScanValueCommand::CMD_INDEXES = {
		"capture", "update",
		"=", "!=",
		">", "<",
		"+", "-"
};

const std::string ScanValueCommand::INDEX_CAPTURE		= "capture";
const std::string ScanValueCommand::INDEX_UPDATE		= "update";
const std::string ScanValueCommand::INDEX_EQUAL_TO		= "=";
const std::string ScanValueCommand::INDEX_NOT_EQUAL_TO	= "!=";
const std::string ScanValueCommand::INDEX_GREATER_THAN	= ">";
const std::string ScanValueCommand::INDEX_LESS_THAN		= "<";
const std::string ScanValueCommand::INDEX_INCREASED_BY	= "+";
const std::string ScanValueCommand::INDEX_DECREASED_BY	= "-";

ScanValueCommand::ScanValueCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT),
	_handlers() {

	_handlers.insert(std::make_pair(INDEX_CAPTURE,
		[this] (ICheatEngine& engine, const std::string& value) { return captureAndUpdate(engine, value); }));
	_handlers.insert(std::make_pair(INDEX_UPDATE,
		[this] (ICheatEngine& engine, const std::string& value) { return captureAndUpdate(engine, value); }));
	_handlers.insert(std::make_pair(INDEX_EQUAL_TO,
		[this] (ICheatEngine& engine, const std::string& value) { return equalTo(engine, value); }));
	_handlers.insert(std::make_pair(INDEX_NOT_EQUAL_TO,
		[this] (ICheatEngine& engine, const std::string& value) { return notEqualTo(engine, value); }));
	_handlers.insert(std::make_pair(INDEX_GREATER_THAN,
		[this] (ICheatEngine& engine, const std::string& value) { return greaterThan(engine, value); }));
	_handlers.insert(std::make_pair(INDEX_LESS_THAN,
		[this] (ICheatEngine& engine, const std::string& value) { return lessThan(engine, value); }));
	_handlers.insert(std::make_pair(INDEX_INCREASED_BY,
		[this] (ICheatEngine& engine, const std::string& value) { return increasedBy(engine, value); }));
	_handlers.insert(std::make_pair(INDEX_DECREASED_BY,
		[this] (ICheatEngine& engine, const std::string& value) { return decreasedBy(engine, value); }));
}

ScanValueCommand::~ScanValueCommand() {}

int32_t ScanValueCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (cheatEngine.getPid() == EngineConstants::NULL_PROCESS_ID) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_PID_NOT_SET);
		return 0;
	}

	typedef std::map<std::string, CommandHandler> HandlerMap;
	std::stringstream ss(argsLine);
	std::string valueString;
	ss >> valueString;

	HandlerMap::iterator it = _handlers.find(cmd);
	if (it == _handlers.end()) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_CMD_NOT_FOUND);
		return 0;
	}
	auto& handler = it->second;
	return handler(cheatEngine, valueString);
}

int32_t ScanValueCommand::captureAndUpdate(ICheatEngine& cheatEngine, const std::string& valueString) {
	ScanArguments args;
	ScanArgumentsBuilder::buildCaptureAndUpdateArguments(args);
//	if (!checkValueTypeFlagIsValidBeforeScanning(cheatEngine, args)) {
//		handleErrorOfValueTypeFlagIsInvalid();
//		return 0;
//	}
	cheatEngine.scan(args);
	return 0;
}

int32_t ScanValueCommand::equalTo(ICheatEngine& cheatEngine, const std::string& valueString) {
	return scanWithOrWithOutUserValue(cheatEngine, valueString, ComparisonMethod::EQUAL_TO, ComparisonMethod::NOT_CHANGED);
}

int32_t ScanValueCommand::notEqualTo(ICheatEngine& cheatEngine, const std::string& valueString) {
	return scanWithOrWithOutUserValue(cheatEngine, valueString, ComparisonMethod::NOT_EQUAL_TO, ComparisonMethod::CHANGED);
}

int32_t ScanValueCommand::greaterThan(ICheatEngine& cheatEngine, const std::string& valueString) {
	return scanWithUserValue(cheatEngine, ComparisonMethod::GREATER_THAN, valueString);
}

int32_t ScanValueCommand::lessThan(ICheatEngine& cheatEngine, const std::string& valueString) {
	return scanWithUserValue(cheatEngine, ComparisonMethod::LESS_THAN, valueString);
}

int32_t ScanValueCommand::increasedBy(ICheatEngine& cheatEngine, const std::string& valueString) {
	return scanWithOrWithOutUserValue(cheatEngine, valueString, ComparisonMethod::INCREASED_BY, ComparisonMethod::INCREASED);
}

int32_t ScanValueCommand::decreasedBy(ICheatEngine& cheatEngine, const std::string& valueString) {
	return scanWithOrWithOutUserValue(cheatEngine, valueString, ComparisonMethod::DECREASED_BY, ComparisonMethod::DECREASED);
}

int32_t ScanValueCommand::scanWithOrWithOutUserValue(
	ICheatEngine& cheatEngine, const std::string& valueString,
	ComparisonMethod usedMethodIfUserValueProvided, ComparisonMethod usedMethodIfNoUserValue) {

	ScanArguments args;
	if (!valueString.empty()) {
		if (!ScanArgumentsBuilder::build(usedMethodIfUserValueProvided, valueString, args)) {
			handleErrorOfConversionForUserValue(usedMethodIfUserValueProvided, valueString);
			return 0;
		}
	} else {
		if (!ScanArgumentsBuilder::buildWithOutUserValue(usedMethodIfNoUserValue, args)) {
			handleErrorOfBuildArgumentsWithOutUserValue(usedMethodIfNoUserValue);
			return 0;
		}
	}
	if ((!cheatEngine.hasScannedRegions()) && (ComparisonMethodUtils::isAnOldValueRequiredMethod(args.getComparisonMethod()))) {
		handleErrorOfUsingAnOldValueRequiredMethodWithoutScannedRegions(args.getComparisonMethod());
		return 0;
	}
//	if (!checkValueTypeFlagIsValidBeforeScanning(cheatEngine, args)) {
//		handleErrorOfValueTypeFlagIsInvalid();
//		return 0;
//	}
	cheatEngine.scan(args);
	return 0;
}

int32_t ScanValueCommand::scanWithUserValue(
	ICheatEngine& cheatEngine, ComparisonMethod method, const std::string& valueString) {

	ScanArguments args;
	if (valueString.empty()) {
		handleErrorOfWithOutUserValue(method);
		return 0;
	}
	if (!ScanArgumentsBuilder::build(method, valueString, args)) {
		handleErrorOfConversionForUserValue(method, valueString);
		return 0;
	}
//	if (!checkValueTypeFlagIsValidBeforeScanning(cheatEngine, args)) {
//		handleErrorOfValueTypeFlagIsInvalid();
//		return 0;
//	}
	cheatEngine.scan(args);
	return 0;
}

bool ScanValueCommand::checkValueTypeFlagIsValidBeforeScanning(ICheatEngine& cheatEngine, ScanArguments& args) {
	MemoryValueTypeFlag checkFlag = cheatEngine.getScannedValueTypeFlag();
	checkFlag.setFlag(checkFlag.getFlag() & args.getValueTypeFlag().getFlag());
	if (!checkFlag.isMarkedForAny()) {
		return false;
	}
	return true;
}

void ScanValueCommand::handleErrorOfConversionForUserValue(ComparisonMethod method, const std::string& valueString) {
	cboy::tagErrorCode(TunnelReturnCode::ERR_FAILED_TO_PARSE_USER_VALUE);
}

void ScanValueCommand::handleErrorOfBuildArgumentsWithOutUserValue(ComparisonMethod method) {
	cboy::tagErrorCode(TunnelReturnCode::ERR_CMP_METHOD_REQUIRES_USER_VALUE);
}

void ScanValueCommand::handleErrorOfWithOutUserValue(ComparisonMethod method) {
	cboy::tagErrorCode(TunnelReturnCode::ERR_CMP_METHOD_REQUIRES_USER_VALUE);
}

void ScanValueCommand::handleErrorOfUsingAnOldValueRequiredMethodWithoutScannedRegions(ComparisonMethod method) {
	cboy::tagErrorCode(TunnelReturnCode::ERR_CMP_METHOD_REQUIRES_SCANNED_REGIONS);
}

void ScanValueCommand::handleErrorOfValueTypeFlagIsInvalid() {
	cboy::tagErrorCode(TunnelReturnCode::ERR_INVALID_TYPE_FLAG);
}

} /* END of namespace */
