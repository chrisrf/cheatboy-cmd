#ifndef __TunnelTerminal_H__
#define __TunnelTerminal_H__

#include <stdint.h>
#include <memory>
#include <map>
#include <vector>

#include <cheatboy/tunnel/ITunnelTerminal.h>
#include <cheatboy/tunnel/reader/ICommandMatcher.h>

namespace cboy {
class ITerminalReader;
class ITerminalCommand;
class ICheatEngine;

class TunnelTerminal: public ITunnelTerminal, public ICommandMatcher {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(TunnelTerminal);
private:
	volatile bool _keepRunning;

	std::unique_ptr<ICheatEngine> _cheatEngine;
	std::unique_ptr<ITerminalReader> _terminalReader;
	std::vector<std::unique_ptr<ITerminalCommand>> _terminalCmds;
	std::map<std::string, std::reference_wrapper<ITerminalCommand>> _indexedCmds;
	std::vector<std::string> _userIndexes;

public:
	TunnelTerminal();
	virtual ~TunnelTerminal() noexcept;
public:
	virtual int32_t run() override;
	virtual int32_t stop() override;

	virtual size_t getNumberOfCommands() const override;
	virtual const std::string& findMatchedCommand(const std::string& text, uint32_t& searchIndex) const override;
private:
	void buildCommands();
	void addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, const char* index);
	void addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, std::initializer_list<const char*>&& indexes);
	void addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, const std::vector<std::string>& indexes);
	void indexCommand(ITerminalCommand& cmd, bool isUserCmd, const char* index);
	int32_t executeCommand(const std::string& cmdLine);
};

} /* END of namespace */

#endif
