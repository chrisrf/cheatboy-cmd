#ifndef __ITunnelTerminal_H__
#define __ITunnelTerminal_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {

class ITunnelTerminal {
PURE_DECLARE_CLASS_AS_INTERFACE(ITunnelTerminal);
public:
	virtual int32_t run() = 0;
	virtual int32_t stop() = 0;
};

} /* END of namespace */

#endif
