#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <memory>
#include <iostream>
#include <PureLib.h>

#include <cheatboy/tunnel/TunnelTerminal.h>
#include <cheatboy/tunnel/Constants.h>
#include <cheatboy/tunnel/response/TunnelHelper.h>
#include <cheatboy/tunnel/response/TunnelReturnCode.h>

using namespace pure;
using namespace cboy;

int main(int argc, char* argv[]) {
	if (getuid() != 0) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_NOT_RUN_AS_ROOT);
		return EXIT_FAILURE;
	}
	pure::println("[{0} command line tool]", Constants::APP_NAME);

	if (!LoggerInstaller::install()) {
		cboy::tagErrorCode(TunnelReturnCode::ERR_INSTALL_LOGGER);
		return EXIT_FAILURE;
	}
	pure::println("Logger is installed successfully.");

	int processReturnCode = EXIT_SUCCESS;
	try {
		pure::println("{0} starts running...", Constants::LC_APP_NAME);
		Singleton<ITunnelTerminal>::createInstance<TunnelTerminal>();
		Singleton<ITunnelTerminal>::getInstance().run();
	} catch (...) {
		processReturnCode = EXIT_FAILURE;
		cboy::tagErrorCodeAndExceptionInCatchBlock(TunnelReturnCode::ERR_UNCAUGHT_EXCEPTION_THROWN, __PURE_FILE__, __LINE__);
	}
	Singleton<ITunnelTerminal>::destroyInstance();
	pure::println("{0} stopped running...", Constants::LC_APP_NAME);
	cboy::tagEnd();

	LoggerInstaller::uninstall();
	return processReturnCode;
}
