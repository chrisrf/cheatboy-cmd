#include "TunnelTerminal.h"

#include <PureCppCompatibility.h>
#include <PureLib.h>
#include <CheatEngineLib.h>

#include <cheatboy/tunnel/Constants.h>
#include <cheatboy/tunnel/cmd/base/ITerminalCommand.h>
#include <cheatboy/tunnel/cmd/ExitCommand.h>
#include <cheatboy/tunnel/cmd/SetPidCommand.h>
#include <cheatboy/tunnel/cmd/AttachCommand.h>
#include <cheatboy/tunnel/cmd/DetachCommand.h>
#include <cheatboy/tunnel/cmd/ConfigEngineCommand.h>
#include <cheatboy/tunnel/cmd/DeleteRegionsCommand.h>
#include <cheatboy/tunnel/cmd/ListCandidatesCommand.h>
#include <cheatboy/tunnel/cmd/EditCandidateCommand.h>
#include <cheatboy/tunnel/cmd/ScanValueCommand.h>
#include <cheatboy/tunnel/reader/ITerminalReader.h>
#include <cheatboy/tunnel/reader/TerminalReader.h>
#include <cheatboy/tunnel/response/TunnelHelper.h>
#include <cheatboy/tunnel/response/TunnelReturnCode.h>

namespace cboy {

TunnelTerminal::TunnelTerminal()
	:
	_keepRunning(false),
	_cheatEngine(CheatEngineFactory::createCheatEngine()),
	_terminalReader(std::make_unique<TerminalReader>()) {

	buildCommands();
	_terminalReader->setCommandMatcher(this);
}

TunnelTerminal::~TunnelTerminal() {
	_userIndexes.clear();
	_indexedCmds.clear();
	_terminalCmds.clear();
}

int32_t TunnelTerminal::run() {
	if (_keepRunning) { return 0; }
	_keepRunning = true;

	std::string prompt;
	while (_keepRunning) {
		cboy::tagState(*_cheatEngine);
		std::string cmdLine = _terminalReader->readCommandLine(prompt);
		try {
			executeCommand(cmdLine);
		} catch(cboy::AttachException& e) {
			cboy::tagErrorCodeWithException(TunnelReturnCode::ERR_PROCESS_NOT_FOUND, e);
		} catch (...) {
			cboy::tagErrorCodeAndExceptionInCatchBlock(TunnelReturnCode::ERR_FAILURE_CMD, __PURE_FILE__, __LINE__);
		}
	}
	return 0;
}

int32_t TunnelTerminal::stop() {
	_keepRunning = false;
	return 0;
}

size_t TunnelTerminal::getNumberOfCommands() const {
	return _userIndexes.size();
}

const std::string& TunnelTerminal::findMatchedCommand(const std::string& text, uint32_t& searchIndex) const {
	static const std::string& nullCmdName("");
	while (searchIndex < _userIndexes.size()) {
		const std::string& indexName = _userIndexes[searchIndex];

		searchIndex++;
		if (indexName.compare(0, text.length(), text) == 0) {
			return indexName;
		}
	}
	return nullCmdName;
}

void TunnelTerminal::buildCommands() {
	// XXX: Add commands here
	addCommand(std::make_unique<ExitCommand>(), true, { "quit", "q" });
	addCommand(std::make_unique<SetPidCommand>(), true, "id");
	addCommand(std::make_unique<AttachCommand>(), true, "attach");
	addCommand(std::make_unique<DetachCommand>(), true, "detach");
	addCommand(std::make_unique<ConfigEngineCommand>(), true, "econfig");
	addCommand(std::make_unique<DeleteRegionsCommand>(), true, "fregions");
	addCommand(std::make_unique<ListCandidatesCommand>(), true, "matches");
	addCommand(std::make_unique<EditCandidateCommand>(), true, "set");
	addCommand(std::make_unique<ScanValueCommand>(), true, ScanValueCommand::CMD_INDEXES);
}

void TunnelTerminal::addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, const char* index) {
	indexCommand(*cmd, isUserCmd, index);
	_terminalCmds.emplace_back(std::move(cmd));
}

void TunnelTerminal::addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, std::initializer_list<const char*>&& indexes) {
	for (const auto& index : indexes) {
		indexCommand(*cmd, isUserCmd, index);
	}
	_terminalCmds.emplace_back(std::move(cmd));
}

void TunnelTerminal::addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, const std::vector<std::string>& indexes) {
	for (const auto& index : indexes) {
		indexCommand(*cmd, isUserCmd, index.c_str());
	}
	_terminalCmds.emplace_back(std::move(cmd));
}

void TunnelTerminal::indexCommand(ITerminalCommand& cmd, bool isUserCmd, const char* index) {
	_indexedCmds.emplace(index, cmd);
	if (isUserCmd) {
		_userIndexes.emplace_back(index);
	}
}

int32_t TunnelTerminal::executeCommand(const std::string& cmdLine) {
	std::stringstream ss(cmdLine);
	std::string cmd;
	std::string argsLine;
	ss >> cmd;
	std::getline(ss, argsLine);

	const auto& it = _indexedCmds.find(cmd);
	if (it != _indexedCmds.end()) {
		ITerminalCommand& target = (it->second);
		target.execute(*_cheatEngine, cmd, argsLine);
	} else {
		cboy::tagErrorCode(TunnelReturnCode::ERR_CMD_NOT_FOUND);
	}
	return 0;
}

} /* END of namespace */
