LOCAL_PATH := $(call my-dir)
$(call __ndk_info, at [$(LOCAL_PATH)])

####### Build atunnel
include $(CLEAR_VARS)

$(call import-add-path, $(PVAL_MAIN_SOURCE_FILE_ROOT_PATH))

# Set LOCAL_ARM_MODE.
LOCAL_ARM_MODE := $(PVAL_MAIN_ARM_MODE)

LOCAL_MODULE	:= atunnel

### Following is true if you use exception.
# If you set APP_PLATFORM := android-16,
# the ndk build system will auto set APP_PIE := true.
# Your binary will segfault on older (under API 21 (lollipop)) SDKs.
### Following is true if you do NOT use exception.
# If you can live with only supporting Android 4.1+ (API 16+),
# just set APP_PLATFORM := android-16 and you'll be good to go.
# Behind the scenes it sets APP_PIE := true.
# Your binary will segfault on older (under API 16) SDKs.
#LOCAL_CFLAGS += -fPIE
#LOCAL_LDFLAGS += -fPIE -pie

#LOCAL_STATIC_LIBRARIES	+= readline CheatBoyEngine
LOCAL_STATIC_LIBRARIES	+= CheatBoyEngine
LOCAL_LDLIBS						+= -llog

LOCAL_C_INCLUDES	+= $(PVAL_MAIN_SOURCE_FILE_ROOT_PATH)
LOCAL_SRC_FILES		+= $(call pfunc_get_all_cpp_file_list_under_local_path, $(LOCAL_PATH))
$(foreach e, $(LOCAL_C_INCLUDES), $(call __ndk_info, Will include [$(e)]))
$(foreach e, $(LOCAL_SRC_FILES), $(call __ndk_info, Will build [$(e)]))

include $(BUILD_EXECUTABLE)

$(call import-module, cheatboy/engine)
$(call import-module, prebuilt)