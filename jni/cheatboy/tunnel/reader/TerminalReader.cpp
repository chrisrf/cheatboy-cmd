#include "TerminalReader.h"

#include <PureLib.h>	// it should be put on top of readline.h and history.h
#define __CBOY_USE_SIMPLE_READER__
#if (!defined __CBOY_USE_SIMPLE_READER__)
#include <readline/readline.h>
#include <readline/history.h>
#else
#include <iostream>
#endif

#include <cheatboy/tunnel/Constants.h>
#include <cheatboy/tunnel/reader/ICommandMatcher.h>

namespace cboy {

#if (!defined __CBOY_USE_SIMPLE_READER__)
/*
 * When user presses TAB,
 * commandGenerator will be called and "state" is reset to 0.
 *
 * If you return a C string (means a matched string was found),
 * commandGenerator will be called again, and "state" is increased.
 *
 * If you return NULL,
 * commandGenerator will stop to be called again.
 */
static char* commandGenerator(const char* text, int state) {
	static uint32_t index = 0;

	// Reset generator if state == 0, otherwise continue from last time.
	index = state ? index : 0;

	std::string sText = text;
	const std::string& cmd = TerminalReader::getSharedCommandMatcher()->findMatchedCommand(sText, index);
	if (cmd.empty()) {
    	return NULL;
	}
	return strdup(cmd.c_str());
}

static char** commandCompletion(const char* text, int start, int end) {
	(void) end;

	// Never use default completer (filenames), even if I don't generate any matches.
	rl_attempted_completion_over = 1;

	// Only complete on the first word, the command.
	return start ? NULL : rl_completion_matches(text, commandGenerator);
}
#endif

ICommandMatcher* TerminalReader::sharedCommandMatcher = nullptr;

TerminalReader::TerminalReader() {}

TerminalReader::~TerminalReader() {
	sharedCommandMatcher = nullptr;
}

std::string TerminalReader::readCommandLine(const std::string& prompt) {
#if (!defined __CBOY_USE_SIMPLE_READER__)
	rl_readline_name = Constants::LC_APP_NAME.c_str();
	rl_attempted_completion_function = commandCompletion;
	// Disable auto-complete.
	rl_bind_key('\t', rl_abort);

	while (true) {
		char* line = NULL;
		if ((line = readline(prompt.c_str())) == NULL) {
			PURE_THROW_EXCEPTION("EOF is encountered while reading a line.");
		}

		size_t newLen = 0;
		char* trimmedLine = pure::StringTrimmer::cTrim(line, newLen);
		if (newLen == 0) {
			free(line);
			continue;
		}

		// Record this line to readline history.
		std::string cmdLine(trimmedLine, newLen);
		// Disable recording history.
		//add_history(trimmedLine);
		free(line);
		return cmdLine;
	}
#else
	std::string cmdLine;
	while (std::getline(std::cin, cmdLine)) {
		if (std::cin.eof()) {
			PURE_THROW_EXCEPTION("EOF is encountered while reading a line.");
		}
		pure::StringTrimmer::trim(cmdLine);
		if (cmdLine.empty()) {
			continue;
		}
		return cmdLine;
	}
	PURE_THROW_EXCEPTION("Error is encountered while reading a line.");
#endif
}

void TerminalReader::setCommandMatcher(ICommandMatcher* commandMatcher) {
	sharedCommandMatcher = commandMatcher;
}

} /* END of namespace */
