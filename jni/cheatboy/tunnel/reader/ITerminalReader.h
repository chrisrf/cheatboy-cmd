#ifndef __ITerminalReader_H__
#define __ITerminalReader_H__

#include <string>
#include <PureMacro.h>

namespace cboy {
class ICommandMatcher;

class ITerminalReader {
PURE_DECLARE_CLASS_AS_INTERFACE(ITerminalReader);
public:
	virtual std::string readCommandLine(const std::string& prompt) = 0;
	virtual void setCommandMatcher(ICommandMatcher* commandMatcher) = 0;
};

} /* END of namespace */

#endif
