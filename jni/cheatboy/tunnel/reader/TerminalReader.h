#ifndef __TerminalReader_H__
#define __TerminalReader_H__

#include <cheatboy/tunnel/reader/ITerminalReader.h>

namespace cboy {
class ICommandMatcher;

class TerminalReader: public ITerminalReader {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(TerminalReader);
private:
	static ICommandMatcher* sharedCommandMatcher;

public:
	static ICommandMatcher* getSharedCommandMatcher() { return sharedCommandMatcher; }
public:
	TerminalReader();
	virtual ~TerminalReader();
public:
	virtual std::string readCommandLine(const std::string& prompt) override;
	virtual void setCommandMatcher(ICommandMatcher* commandMatcher) override;
};

} /* END of namespace */

#endif
