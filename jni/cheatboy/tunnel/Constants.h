#ifndef __Constants_H__
#define __Constants_H__

#include <string>
#include <PureMacro.h>

namespace cboy {

class Constants {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(Constants);
public:
	static const std::string APP_NAME;
	static const std::string LC_APP_NAME;
};

} /* END of namespace */

#endif
