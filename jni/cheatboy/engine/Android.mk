LOCAL_PATH := $(call my-dir)
$(call __ndk_info, at [$(LOCAL_PATH)])

####### Build libCheatBoyEngine
include $(CLEAR_VARS)

$(call import-add-path, $(PVAL_MAIN_SOURCE_FILE_ROOT_PATH))

# Set LOCAL_ARM_MODE.
LOCAL_ARM_MODE := $(PVAL_MAIN_ARM_MODE)

LOCAL_MODULE					:= CheatBoyEngine
LOCAL_MODULE_FILENAME	:= libCheatBoyEngine

LOCAL_EXPORT_C_INCLUDES += $(PVAL_MAIN_SOURCE_FILE_ROOT_PATH)
LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)/include

LOCAL_STATIC_LIBRARIES	+= PureLib

LOCAL_C_INCLUDES	+= $(PVAL_MAIN_SOURCE_FILE_ROOT_PATH)
LOCAL_SRC_FILES		+= $(call pfunc_get_all_cpp_file_list_under_local_path, $(LOCAL_PATH))
$(foreach e, $(LOCAL_C_INCLUDES), $(call __ndk_info, Will include [$(e)]))
$(foreach e, $(LOCAL_SRC_FILES), $(call __ndk_info, Will build [$(e)]))

include $(BUILD_STATIC_LIBRARY)

$(call import-module, purelib)