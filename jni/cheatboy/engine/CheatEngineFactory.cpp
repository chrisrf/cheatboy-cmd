#include "CheatEngineFactory.h"

#include <PureCppCompatibility.h>

#include <cheatboy/engine/CheatEngine.h>

namespace cboy {

std::unique_ptr<ICheatEngine> CheatEngineFactory::createCheatEngine() {
	return std::make_unique<CheatEngine>();
}

} /* END of namespace */
