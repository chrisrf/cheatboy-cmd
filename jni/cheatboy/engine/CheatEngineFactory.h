#ifndef __CheatEngineFactory_H__
#define __CheatEngineFactory_H__

#include <memory>
#include <PureMacro.h>

namespace cboy {
class ICheatEngine;

class CheatEngineFactory {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(CheatEngineFactory);
public:
	static std::unique_ptr<ICheatEngine> createCheatEngine();
};

} /* END of namespace */

#endif
