#include "ScanArguments.h"

#include <PureCppCompatibility.h>

#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/value/UserValue.h>
#include <cheatboy/engine/value/comparator/ComparisonMethod.h>

namespace cboy {

class ScanArgumentsImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ScanArgumentsImpl)
public:
	ComparisonMethod _method;
	MemoryValueTypeFlag _valueTypeFlag;
	UserValue _userValue;

public:
	ScanArgumentsImpl() : _method(ComparisonMethod::ANY), _valueTypeFlag(), _userValue() {}
	virtual ~ScanArgumentsImpl() noexcept {}
};

ScanArguments::ScanArguments()
	:
	_impl(std::make_unique<ScanArgumentsImpl>()) {}

ScanArguments::~ScanArguments() noexcept {}

ScanArguments::ScanArguments(ScanArguments&&) = default;
ScanArguments& ScanArguments::operator =(ScanArguments&&) = default;

bool ScanArguments::isValidState() const {
	return (_impl != nullptr);
}

ComparisonMethod ScanArguments::getComparisonMethod() const {
	return _impl->_method;
}

void ScanArguments::setComparisonMethod(ComparisonMethod method) {
	_impl->_method = method;
}

UserValue& ScanArguments::getUserValue() {
	return _impl->_userValue;
}

MemoryValueTypeFlag& ScanArguments::getValueTypeFlag() {
	return _impl->_valueTypeFlag;
}

} /* END of namespace */
