#include "ScanArgumentsBuilder.h"

#include <stdint.h>
#include <PureLib.h>

#include <cheatboy/engine/cheater/args/ScanArguments.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/value/UserValue.h>
#include <cheatboy/engine/value/comparator/ComparisonMethod.h>
#include <cheatboy/engine/value/comparator/ComparisonMethodUtils.h>

namespace cboy {

void ScanArgumentsBuilder::buildCaptureAndUpdateArguments(ScanArguments& outArgs) {
	UserValue& userValue = outArgs.getUserValue();
	MemoryValueTypeFlag& userValueTypeFlag = outArgs.getValueTypeFlag();

	outArgs.setComparisonMethod(ComparisonMethod::ANY);
	userValue.clear();
	userValueTypeFlag.setFlag(MemoryValueTypeFlag::ALL);
}

bool ScanArgumentsBuilder::buildWithOutUserValue(ComparisonMethod method, ScanArguments& outArgs) {
	if (ComparisonMethodUtils::isAnUserValueRequiredMethod(method)) {
		//PURE_THROW_EXCEPTION(pure::fmt("The comparison method[{0}] requires an user value.", static_cast<int>(method)));
		return false;
	}

	UserValue& userValue = outArgs.getUserValue();
	MemoryValueTypeFlag& userValueTypeFlag = outArgs.getValueTypeFlag();

	outArgs.setComparisonMethod(method);
	userValue.clear();
	userValueTypeFlag.setFlag(MemoryValueTypeFlag::ALL);
	return true;
}

bool ScanArgumentsBuilder::build(ComparisonMethod method, const std::string& valueString, ScanArguments& outArgs) {
	UserValue& userValue = outArgs.getUserValue();
	MemoryValueTypeFlag& userValueTypeFlag = outArgs.getValueTypeFlag();

	outArgs.setComparisonMethod(method);
	userValue.clear();
	if (!ComparisonMethodUtils::isAnUserValueRequiredMethod(method)) {
		userValueTypeFlag.setFlag(MemoryValueTypeFlag::ALL);
		return true;
	}
	userValueTypeFlag.clearFlag();

	pure::NumberConverter converter;
	testAndSetUserValue<int8_t>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<uint8_t>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<int16_t>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<uint16_t>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<int32_t>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<uint32_t>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<int64_t>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<uint64_t>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<float>(converter, valueString, userValue, userValueTypeFlag);
	testAndSetUserValue<double>(converter, valueString, userValue, userValueTypeFlag);
	if (!userValueTypeFlag.isMarkedForAny()) {
		//PURE_THROW_EXCEPTION(pure::fmt("The conversion for user value[{0}] failed.", valueString));
		return false;
	}
	return true;
}

template<typename T>
void ScanArgumentsBuilder::testAndSetUserValue(
	pure::NumberConverter& converter, const std::string& valueString,
	UserValue& uservalue, MemoryValueTypeFlag& userValueTypeFlag) {

	T value = 0;
	if (converter.convert(valueString, value)) {
		uservalue.setFor<T>(value);
		userValueTypeFlag.markFor<T>();
	}
}

} /* END of namespace */
