#ifndef __ScanArgumentsBuilder_H__
#define __ScanArgumentsBuilder_H__

#include <string>
#include <PureMacro.h>

namespace pure {
class NumberConverter;
}

namespace cboy {
enum class ComparisonMethod;
class ScanArguments;
class MemoryValueTypeFlag;
class UserValue;

class ScanArgumentsBuilder {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(ScanArgumentsBuilder);
public:
	static void buildCaptureAndUpdateArguments(ScanArguments& outArgs);
	static bool buildWithOutUserValue(ComparisonMethod method, ScanArguments& outArgs);
	static bool build(ComparisonMethod method, const std::string& valueString, ScanArguments& outArgs);
private:
	template<typename T> static void testAndSetUserValue(
		pure::NumberConverter& converter, const std::string& valueString,
		UserValue& uservalue, MemoryValueTypeFlag& userValueTypeFlag);
};

} /* END of namespace */

#endif
