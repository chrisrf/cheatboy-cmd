#ifndef __ScanArguments_H__
#define __ScanArguments_H__

#include <memory>
#include <PureMacro.h>

namespace cboy {
enum class ComparisonMethod;
class MemoryValueTypeFlag;
class UserValue;
class ScanArgumentsImpl;

class ScanArguments {
PURE_DECLARE_CLASS_AS_NONCOPYABLE(ScanArguments);
private:
	std::unique_ptr<ScanArgumentsImpl> _impl;

public:
	ScanArguments();
	virtual ~ScanArguments() noexcept;
	ScanArguments(ScanArguments&&);
	ScanArguments& operator =(ScanArguments&&);
public:
	bool isValidState() const;
	ComparisonMethod getComparisonMethod() const;
	void setComparisonMethod(ComparisonMethod method);
	MemoryValueTypeFlag& getValueTypeFlag();
	UserValue& getUserValue();
};

} /* END of namespace */

#endif
