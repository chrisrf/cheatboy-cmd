#include "RegionScanner.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/cheater/default/data/ScannedRegion.h>
#include <cheatboy/engine/cheater/default/data/ScannedData.h>
#include <cheatboy/engine/editor/reader/IMemoryReader.h>
#include <cheatboy/engine/region/IMemoryRegion.h>
#include <cheatboy/engine/value/defs/MemoryValueDefs.h>
#include <cheatboy/engine/value/comparator/ComparisonMethod.h>
#include <cheatboy/engine/value/comparator/ComparisonMethodUtils.h>

//#define __CBOY_UNIT_TEST_REGION_SCANNER__

namespace cboy {

RegionScanner::RegionScanner()
	:
	_isScanProcedureStarted(false), _isOldValueRequired(false),
	_dataUtils(),
	_comparator(),
	_newValue(), _oldValue(), _userValue(), _filterFlag(), _cmpFlag(), _savedFlag(),
	_readerBuffer(nullptr), _readerBufferSize(0) {}

RegionScanner::~RegionScanner() noexcept {
	if (hasReaderBuffer()) {
		free(_readerBuffer);
	}
}

void RegionScanner::startScanProcedure(
	ComparisonMethod method, const UserValue& userValue, const MemoryValueTypeFlag& filterFlag,
	size_t readerBufSize) {

	static_assert(sizeof(uint8_t) == 1, "Size of uint8_t is NOT 1.");
	throwIfScanProcedureIsNotEnded();
	throwIfComparisonMethodIsInvalid(method);
	//throwExceptionIfFilterFlagIsZero(filterFlag);
	throwIfAssignedReaderBufferSizeIsZero(readerBufSize);
	throwIfReaderBufferIsAlreadyAllocated();

	_readerBuffer = (uint8_t*)malloc(readerBufSize);
	if (_readerBuffer == nullptr) {
		PURE_THROW_EXCEPTION(pure::fmt("Fail to allocate memory for size[{0}].", readerBufSize));
	}
	_readerBufferSize = readerBufSize;
	try {
		_isOldValueRequired = ComparisonMethodUtils::isAnOldValueRequiredMethod(method);
		_comparator.setCurrentComparisonMethod(method);
		_newValue.clear();
		_oldValue.clear();
		_userValue = userValue;
		_filterFlag = filterFlag;
		_cmpFlag.clearFlag();
		_savedFlag.clearFlag();

		memset(&_matchedByte, 0, sizeof(_matchedByte));
		memset(&_recordedByte, 0, sizeof(_recordedByte));
	} catch (...) {
		endScanProcedure();
		throw;
	}
	_isScanProcedureStarted = true;
}

void RegionScanner::endScanProcedure() {
	if (hasReaderBuffer()) {
		free(_readerBuffer);
		_readerBuffer = nullptr;
	}
	_readerBufferSize = 0;

	_isScanProcedureStarted = false;
}

void RegionScanner::initialScan(ScannedRegion& region, IMemoryReader& reader) {
	throwIfScanProcedureIsNotStarted();
	throwIfOldValueIsRequired();
	throwIfMemoryRegionSizeIsGreaterThanReaderBufferSize(region);
	throwIfScannedRegionHasRecordBufferAlready(region);
	if (!_filterFlag.isMarkedForAny()) { return; }

	const IMemoryRegion& memoryRegion = region.getMemoryRegion();
	const uintptr_t memoryRegionRemoteAddress = memoryRegion.getStartAddress();
	const size_t memoryRegionSize = memoryRegion.getSize();
	if (memoryRegionSize == 0) { return; } // XXX: Is it possible? XD

	size_t nRead = 0;
	reader.readMemory(memoryRegionRemoteAddress, _readerBuffer, memoryRegionSize, nRead);
	assert(nRead == memoryRegionSize);

	region.allocMaxRecordBuffer();
	region.clearRecordBufferMetaData();
	uint8_t* const recordBuffer = region.getRecordBuffer();
	const size_t recordBufferSize = region.getRecordBufferSize();
	_dataUtils.setLocalValidAddress(pure::PointerUtils::toValue(recordBuffer));
	_dataUtils.setLocalValidSize(recordBufferSize);

	MatchedHeader* matchedHeaderPtr = (MatchedHeader*) recordBuffer;
	_dataUtils.setAsNullTerminator(matchedHeaderPtr);

	const size_t nFinalBytes = MemoryValue::getCapacity() - 1;
	size_t offset = 0;
	size_t remainedBytesNeedToBeRecorded = 0;
	// Handle the normal case (We can copy [MemoryValueDefs::MAX_BYTES] bytes).
	if (memoryRegionSize > nFinalBytes) {
		for (; offset < (memoryRegionSize - nFinalBytes); offset++) {
			prepareDataForInitialScan((_readerBuffer + offset), MemoryValue::getCapacity());
			compareAndAddMatchedByte(
				(memoryRegionRemoteAddress + offset), MemoryValue::getCapacity(),
				region, matchedHeaderPtr, remainedBytesNeedToBeRecorded);
		}
	}
	// Special handling for last [MemoryValueDefs::MAX_BYTES - 1] bytes.
	for (; offset < memoryRegionSize; offset++) {
		const size_t nBytesRemain = memoryRegionSize - offset;
		prepareDataForInitialScan((_readerBuffer + offset), nBytesRemain);
		unmarkImpossibleComparisonFlagByPossibleMaxBytes(nBytesRemain);
		compareAndAddMatchedByte(
			(memoryRegionRemoteAddress + offset), nBytesRemain,
			region, matchedHeaderPtr, remainedBytesNeedToBeRecorded);
	}
	assert(remainedBytesNeedToBeRecorded == 0);

	finalizeScanProcedure(region, matchedHeaderPtr);
	assert(region.getRecordBufferSize() <= recordBufferSize);
}

void RegionScanner::scanMatches(ScannedRegion& region, IMemoryReader& reader) {
	throwIfScanProcedureIsNotStarted();
	throwIfMaxSizedMatchedBufferSizeIsGreaterThanReaderBufferSize(region);
	throwIfScannedRegionDoesNotHaveValidRecordBuffer(region);
	if (!_filterFlag.isMarkedForAny()) { region.releaseRecordBuffer(); return; }

	// Record the last time meta data of the region.
	const size_t lastMaxNumOfMatchedBytesOfMatchedByteBuffer = region.getMaxNumOfMatchedBytesOfMatchedByteBuffer();
	const size_t lastNumOfMatchededHeaders = region.getNumOfMatchedHeaders();
	const size_t lastNumOfMatchedBytes = region.getNumOfMatchedBytes();
	const size_t lastNumOfMatchedValues = region.getNumOfMatchedValues();

	region.clearRecordBufferMetaData();
	uint8_t* const recordBuffer = region.getRecordBuffer();
	const size_t recordBufferSize = region.getRecordBufferSize();
	_dataUtils.setLocalValidAddress(pure::PointerUtils::toValue(recordBuffer));
	_dataUtils.setLocalValidSize(recordBufferSize);

	MatchedHeader* matchedHeaderPtr = (MatchedHeader*) recordBuffer;
	// Capture the header.
	ReadingMatchedHeader readingMatchedHeader;
	_dataUtils.captureMatchedHeaderToReadingMatchedHeader(readingMatchedHeader, matchedHeaderPtr);
	// Clear the writing header.
	_dataUtils.setAsNullTerminator(matchedHeaderPtr);

	size_t iRound;
	assert(!_dataUtils.isReadingNullTerminator(readingMatchedHeader));
	for (iRound = 0; !_dataUtils.isReadingNullTerminator(readingMatchedHeader); iRound++) {
#ifdef __CBOY_UNIT_TEST_REGION_SCANNER__
		PURE_D_LOG_LN("# Round[{0}]", iRound);
#endif

		assert(readingMatchedHeader.numOfBytes <= _readerBufferSize);
		size_t nRead = 0;
		reader.readMemory(readingMatchedHeader.remoteAddress, _readerBuffer, readingMatchedHeader.numOfBytes, nRead);
		assert(nRead == readingMatchedHeader.numOfBytes);

		size_t remainedBytesNeedToBeRecorded = 0;
		for (size_t offset = 0; offset < readingMatchedHeader.numOfBytes; offset++) {
			if (!checkComparisonFlagBeforePreparingDataForScanMatches(readingMatchedHeader, offset)) {
				if (remainedBytesNeedToBeRecorded > 0) {
					// Not a matched value, but some remained bytes need to be recorded.
					addRemainedByteNeedToBeRecorded(
							_readerBuffer[offset],
							region, matchedHeaderPtr, remainedBytesNeedToBeRecorded);
#ifdef __CBOY_UNIT_TEST_REGION_SCANNER__
					PURE_D_LOG_LN("\t\t\t# We add a remained byte at [{0}].",
						pure::PointerUtils::toPointer(_dataUtils.getRemoteAddress(readingMatchedHeader, offset)));
#endif
				}
				continue;
			}

			const size_t matchedValueMaxSize = _dataUtils.getMatchedValueMaxSize(readingMatchedHeader, offset);
			assert(matchedValueMaxSize <= (readingMatchedHeader.numOfBytes - offset));
			if (!_isOldValueRequired) {
				prepareDataForScanMatchesWithoutOldValue(
					(_readerBuffer + offset), matchedValueMaxSize);
			} else {
				prepareDataForScanMatchesWithOldValue(
					(_readerBuffer + offset), matchedValueMaxSize,
					readingMatchedHeader, offset);
			}
			// XXX: [Test Case]#2
			//		If the matched byte is not matched this time and the "remainedBytesNeedToBeRecorded" is greater than zero,
			//		then you still need to record it.
			compareAndAddMatchedByte(
				_dataUtils.getRemoteAddress(readingMatchedHeader, offset), matchedValueMaxSize,
				region, matchedHeaderPtr, remainedBytesNeedToBeRecorded);
		}
		assert(remainedBytesNeedToBeRecorded == 0);
		_dataUtils.captureNextMatchedHeaderToReadingMatchedHeader(readingMatchedHeader);
	}
	assert(iRound == lastNumOfMatchededHeaders);

	finalizeScanProcedure(region, matchedHeaderPtr);
	PURE_D_LOG_LN("\t\t[{0}] <= [{1}]", region.getMaxNumOfMatchedBytesOfMatchedByteBuffer(), lastMaxNumOfMatchedBytesOfMatchedByteBuffer);
	PURE_D_LOG_LN("\t\t[{0}] <= [{1}]", region.getNumOfMatchedHeaders(), lastNumOfMatchededHeaders);
	PURE_D_LOG_LN("\t\t[{0}] <= [{1}]", region.getNumOfMatchedBytes(), lastNumOfMatchedBytes);
	PURE_D_LOG_LN("\t\t[{0}] <= [{1}]", region.getNumOfMatchedValues(), lastNumOfMatchedValues);
	PURE_D_LOG_LN("\t\t[{0}] <= [{1}]", region.getRecordBufferSize(), recordBufferSize);
	assert(region.getMaxNumOfMatchedBytesOfMatchedByteBuffer() <= lastMaxNumOfMatchedBytesOfMatchedByteBuffer);
	//assert(region.getNumOfMatchedHeaders() <= lastNumOfMatchededHeaders);//XXX: [Test Case]#1 Number of headers might grow.
	assert(region.getNumOfMatchedBytes() <= lastNumOfMatchedBytes);
	assert(region.getNumOfMatchedValues() <= lastNumOfMatchedValues);
	assert(region.getRecordBufferSize() <= recordBufferSize);
}

bool RegionScanner::hasReaderBuffer() const {
	return (_readerBuffer != nullptr);
}

void RegionScanner::prepareDataForInitialScan(uint8_t* remoteMemoryBuf, size_t size) {
	memcpy(_newValue.getBuffer(), remoteMemoryBuf, size);
	_cmpFlag = _filterFlag;
	_savedFlag.clearFlag();
}

bool RegionScanner::checkComparisonFlagBeforePreparingDataForScanMatches(ReadingMatchedHeader& header, size_t index) {
	_cmpFlag.setFlag(_dataUtils.getMatchFlag(header, index));
	_cmpFlag.setFlag(_cmpFlag.getFlag() & _filterFlag.getFlag());
	return _cmpFlag.isMarkedForAny();
}

void RegionScanner::prepareDataForScanMatchesWithoutOldValue(
	uint8_t* remoteMemoryBuf, size_t size) {

	memcpy(_newValue.getBuffer(), remoteMemoryBuf, size);
	//_cmpFlag should be set by "checkComparisonFlagBeforePreparingDataForScanMatches"
	_savedFlag.clearFlag();
}

void RegionScanner::prepareDataForScanMatchesWithOldValue(
	uint8_t* remoteMemoryBuf, size_t size,
	ReadingMatchedHeader& header, size_t index) {

	memcpy(_newValue.getBuffer(), remoteMemoryBuf, size);
	size_t oldValueMaxSize = _dataUtils.getForMemoryValue(header, index, _oldValue);
	assert(oldValueMaxSize == size);
	//_cmpFlag should be set by "checkComparisonFlagBeforePreparingDataForScanMatches"
	_savedFlag.clearFlag();
}

void RegionScanner::unmarkImpossibleComparisonFlagByPossibleMaxBytes(size_t possibleMaxBytes) {
	// Unmark impossible flags.
	if (possibleMaxBytes < sizeof(int64_t)) {
		_cmpFlag.unmark(MemoryValueTypeFlag::I64 | MemoryValueTypeFlag::U64 | MemoryValueTypeFlag::F64);
		if (possibleMaxBytes < sizeof(int32_t)) {
			_cmpFlag.unmark(MemoryValueTypeFlag::I32 | MemoryValueTypeFlag::U32 | MemoryValueTypeFlag::F32);
			if (possibleMaxBytes < sizeof(int16_t)) {
				_cmpFlag.unmark(MemoryValueTypeFlag::I16 | MemoryValueTypeFlag::U16);
				if (possibleMaxBytes < sizeof(int8_t)) {
					_cmpFlag.unmark(MemoryValueTypeFlag::I08 | MemoryValueTypeFlag::U08);
					// XXX: Is it possible? XD
				}
			}
		}
	}
}

void RegionScanner::compareAndAddMatchedByte(
	uintptr_t matchedByteRemoteAddress, size_t possibleMaxBytes,
	ScannedRegion& region, MatchedHeader*& matchedHeaderPtr, size_t& remainedBytesNeedToBeRecorded) {

	size_t maxSize = _comparator.compareValue(_newValue, _oldValue, _userValue, _cmpFlag, _savedFlag);
	assert(maxSize <= possibleMaxBytes);
	if (maxSize > 0) {
		// Find a matched value.
		uint8_t* valueBuffer = _newValue.getBuffer();
		_matchedByte.byte = valueBuffer[0];
		_matchedByte.matchedValueMaxSize = static_cast<uint8_t>(maxSize);
		_matchedByte.matchFlag = _savedFlag.getFlag();
		matchedHeaderPtr = _dataUtils.addMatchedByteOrCreateNewMatchedHeaderAndAdd(
			region, matchedHeaderPtr, _matchedByte, matchedByteRemoteAddress);
		// Count the number of matched values.
		region.addNumOfMatchedValuesByOne();

		// XXX:	[Test Case]#3
		//		If "remainedBytesNeedToBeRecorded" is greater than the possible max size of current matched byte
		// 		(Ex: the previous matched byte has a possible max size [8], and current one has a possible max size [4]),
		//		then you should NOT replace the max size to "remainedBytesNeedToBeRecorded".
		//
		//		Avoid the bug like following example:
		//		1) For the memory like:
		//			[addr: 0x01] [addr: 0x02] [addr: 0x03] [addr: 0x04] [addr: 0x05] [addr: 0x06] [addr: 0x07] [addr: 0x08]
		//		2) Found a matched byte which max size is 8 at address [0x01].
		//		3) Found a matched byte which max size is 4 at address [0x02]. (continuous)
		//			Current "remainedBytesNeedToBeRecorded" is 7.
		//			If you replace "remainedBytesNeedToBeRecorded" to 4,
		//			it will lose 3 bytes you need to record.
		if (remainedBytesNeedToBeRecorded <= maxSize) {
			remainedBytesNeedToBeRecorded = maxSize;
		}
		remainedBytesNeedToBeRecorded--;
#ifdef __CBOY_UNIT_TEST_REGION_SCANNER__
		PURE_D_LOG_LN("\t\t\t# We found a match (flag = {0}) value at [{1}].",
			_savedFlag.getFlag(),
			pure::PointerUtils::toPointer(matchedByteRemoteAddress));
#endif
	} else if (remainedBytesNeedToBeRecorded > 0) {
		// Not a matched value, but some remained bytes need to be recorded.
		uint8_t* valueBuffer = _newValue.getBuffer();
		addRemainedByteNeedToBeRecorded(
			valueBuffer[0],
			region, matchedHeaderPtr, remainedBytesNeedToBeRecorded);
#ifdef __CBOY_UNIT_TEST_REGION_SCANNER__
		PURE_D_LOG_LN("\t\t\t# We add a remained byte at [{0}].",
			pure::PointerUtils::toPointer(matchedByteRemoteAddress));
#endif
	}
}

inline void RegionScanner::addRemainedByteNeedToBeRecorded(
	uint8_t byte,
	ScannedRegion& region, MatchedHeader*& matchedHeaderPtr, size_t& remainedBytesNeedToBeRecorded) {

	// Not a matched value, but some remained bytes need to be recorded.
	_recordedByte.byte = byte;
	_dataUtils.addMatchedByte(region, matchedHeaderPtr, _recordedByte);

	remainedBytesNeedToBeRecorded--;
}

void RegionScanner::finalizeScanProcedure(ScannedRegion& region, MatchedHeader*& matchedHeaderPtr) {
	PURE_D_LOG_LN("\t\t# finalizeScanProcedure");
	PURE_D_LOG_LN("\t\t# region [{0}] [{1}] [{2}] [{3}] [{4}]",
		region.getMaxNumOfMatchedBytesOfMatchedByteBuffer(),
		region.getNumOfMatchedHeaders(),
		region.getNumOfMatchedBytes(),
		region.getNumOfMatchedValues(),
		region.getRecordBufferSize());
	PURE_D_LOG_LN("\t\t# final header [{0}] [{1}]",
		pure::PointerUtils::toPointer(matchedHeaderPtr->remoteAddress),
		matchedHeaderPtr->numOfBytes);
	if (_dataUtils.isNullTerminator(matchedHeaderPtr)) {
		region.releaseRecordBuffer();
	} else {
		matchedHeaderPtr = _dataUtils.addNullTerminatorBehindTheMatchedByteBuffer(region, matchedHeaderPtr);
		assert(_dataUtils.isNullTerminator(matchedHeaderPtr));
		size_t newSmallSize = _dataUtils.computeTheRecordBufferNewSize(region.getRecordBuffer(), matchedHeaderPtr);
		region.shrinkRecordBuffer(newSmallSize);
	}
	PURE_D_LOG_LN("\t\t### region [{0}] [{1}] [{2}] [{3}] [{4}]",
		region.getMaxNumOfMatchedBytesOfMatchedByteBuffer(),
		region.getNumOfMatchedHeaders(),
		region.getNumOfMatchedBytes(),
		region.getNumOfMatchedValues(),
		region.getRecordBufferSize());
	PURE_D_LOG_LN("\t\t### finalizeScanProcedure [Done]");
}

void RegionScanner::throwIfScanProcedureIsNotEnded() {
	if (_isScanProcedureStarted) {
		PURE_THROW_EXCEPTION(pure::fmt("Scan procedure[{0}] is not ended."));
	}
}

void RegionScanner::throwIfComparisonMethodIsInvalid(ComparisonMethod method) {
	if (!pure::EnumUtils<ComparisonMethod>::isValid(method)) {
		PURE_THROW_EXCEPTION(pure::fmt("Invalid ComparisonMethod: [{0}].", static_cast<int>(method)));
	}
}

void RegionScanner::throwIfFilterFlagIsZero(const MemoryValueTypeFlag& filterFlag) {
	if (!filterFlag.isMarkedForAny()) {
		PURE_THROW_EXCEPTION(pure::fmt("Filter flag is zero."));
	}
}

void RegionScanner::throwIfAssignedReaderBufferSizeIsZero(size_t readerBufSize) {
	if (readerBufSize == 0) {
		PURE_THROW_EXCEPTION(pure::fmt("Size of assigned reader buffer is [0]."));
	}
}

void RegionScanner::throwIfReaderBufferIsAlreadyAllocated() {
	if (hasReaderBuffer()) {
		PURE_THROW_EXCEPTION(pure::fmt("Reader buffer is already allocated."));
	}
}

void RegionScanner::throwIfScanProcedureIsNotStarted() {
	if (!_isScanProcedureStarted) {
		PURE_THROW_EXCEPTION(pure::fmt("Scan procedure is not started."));
	}
}

void RegionScanner::throwIfOldValueIsRequired() {
	if (_isOldValueRequired) {
		PURE_THROW_EXCEPTION(pure::fmt("Initial scan can NOT use an old-value-required comparison method[{0}].",
			static_cast<int>(_comparator.getCurrentComparisonMethod())));
	}
}

void RegionScanner::throwIfMemoryRegionSizeIsGreaterThanReaderBufferSize(const ScannedRegion& region) {
	const IMemoryRegion& memoryRegion = region.getMemoryRegion();
	if (memoryRegion.getSize() > _readerBufferSize) {
		PURE_THROW_EXCEPTION(pure::fmt("Size [{0}] of memory region is greater than size [{1}] of reader buffer.",
			memoryRegion.getSize(), _readerBufferSize));
	}
}

void RegionScanner::throwIfMaxSizedMatchedBufferSizeIsGreaterThanReaderBufferSize(const ScannedRegion& region) {
	if (region.getMaxNumOfMatchedBytesOfMatchedByteBuffer() > _readerBufferSize) {
		PURE_THROW_EXCEPTION(pure::fmt("Size [{0}] of max sized matched buffer is greater than size [{1}] of reader buffer.",
			region.getMaxNumOfMatchedBytesOfMatchedByteBuffer(), _readerBufferSize));
	}
}

void RegionScanner::throwIfScannedRegionHasRecordBufferAlready(const ScannedRegion& region) {
	const IMemoryRegion& memoryRegion = region.getMemoryRegion();
	if (region.hasRecordBuffer()) {
		PURE_THROW_EXCEPTION(pure::fmt("Scanned region [{0}, \"{1}\"] has record buffer already.",
			memoryRegion.getID(), memoryRegion.getName()));
	}
}

void RegionScanner::throwIfScannedRegionDoesNotHaveValidRecordBuffer(const ScannedRegion& region) {
	const IMemoryRegion& memoryRegion = region.getMemoryRegion();
	if (!region.hasRecordBuffer()) {
		PURE_THROW_EXCEPTION(pure::fmt("Scanned region [{0}, \"{1}\"] does NOT have record buffer.",
			memoryRegion.getID(), memoryRegion.getName()));
	}
	if (region.getRecordBufferSize() < ((sizeof(MatchedHeader)*2) + sizeof(MatchedByte))) {
		PURE_THROW_EXCEPTION(pure::fmt("Scanned region [{0}, \"{1}\"] have a invalid record buffer.",
			memoryRegion.getID(), memoryRegion.getName()));
	}
}

} /* END of namespace */
