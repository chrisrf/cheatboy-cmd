#ifndef __ScannedDataUtils_H__
#define __ScannedDataUtils_H__

#include <stdint.h>
#include <PureMacro.h>

#include <cheatboy/engine/cheater/default/data/defs/ScannedDataDefs.h>
#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>
#include <cheatboy/engine/value/defs/MemoryValueTypeFlagDefs.h>

namespace cboy {
struct MatchedHeader;
struct MatchedByte;
struct ReadingMatchedHeader;
class ScannedRegion;
class MemoryValue;
class MemoryValueTypeFlag;
class MatchedValue;
class EditableMatchedValue;

class ScannedDataUtils {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ScannedDataUtils);
private:
	typedef MemoryValueTypeFlagDefs::flag_t flag_t;
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef ScannedDataDefs::match_id_t match_id_t;
private:
	uintptr_t _localValidAddress;
	size_t _localValidSize;

public:
	ScannedDataUtils();
	virtual ~ScannedDataUtils() noexcept;
public:
	uintptr_t getLocalValidAddress() const { return _localValidAddress; }
	size_t getLocalValidSize() const { return _localValidSize; }
	void setLocalValidAddress(uintptr_t address) { _localValidAddress = address; }
	void setLocalValidSize(size_t size) { _localValidSize = size; }

// ------- MatchedHeader related methods
public:
	bool isNullTerminator(MatchedHeader* header);
	void setAsNullTerminator(MatchedHeader* header);
	MatchedHeader* addNullTerminatorBehindTheMatchedByteBuffer(
		ScannedRegion& region, MatchedHeader* header);
	MatchedHeader* addMatchedByteOrCreateNewMatchedHeaderAndAdd(
		ScannedRegion& region, MatchedHeader* header, MatchedByte& addedMatchedByte, uintptr_t remoteAddress);
	void addMatchedByte(ScannedRegion& region, MatchedHeader* header, MatchedByte& addedMatchedByte);
	size_t computeTheRecordBufferNewSize(uint8_t* recordBuffer, MatchedHeader* nullTerminatorHeader);
private:
	MatchedHeader* allocateNewMatchedHeader(MatchedHeader* header, uintptr_t remoteAddress);
	MatchedByte* allocateNewMatchedByte(MatchedHeader* header);
	MatchedByte* getMatchedByteBuffer(MatchedHeader* header);
private:
	uintptr_t getLastMatchedByteRemoteAddress(MatchedHeader* header);
	uint8_t* getLastMatchedByteLocalEndAddress(MatchedHeader* header);

// ------- ReadingMatchedHeader related methods
public:
	bool isReadingNullTerminator(ReadingMatchedHeader& header);
	void captureMatchedHeaderToReadingMatchedHeader(ReadingMatchedHeader& header, MatchedHeader* matchedHeader);
	void captureNextMatchedHeaderToReadingMatchedHeader(ReadingMatchedHeader& header);
	size_t getForMemoryValue(
		ReadingMatchedHeader& header, size_t index, MemoryValue& value);
	void getForMatchedValue(
		ReadingMatchedHeader& header, size_t index,
		region_id_t regionID, match_id_t matchedByteID, MatchedValue& value);
	size_t getMatchedValueMaxSize(ReadingMatchedHeader& header, size_t index);
	flag_t getMatchFlag(ReadingMatchedHeader& header, size_t index);
	uintptr_t getRemoteAddress(ReadingMatchedHeader& header, size_t index);

// ------- Edit related methods
public:
	uintptr_t checkEditableMatchedValue(ReadingMatchedHeader& header, size_t index, EditableMatchedValue& editableValue);
	MatchedHeader* getWritingMatchedHeader(ReadingMatchedHeader& header);
	void editMatchedValue(MatchedHeader* header, size_t index, EditableMatchedValue& editableValue);

// ------- Assertion related methods
private:
	void assertLocalMemoryAccessedIsValid(void* addressAccessed, size_t sizeAccessed);
};

} /* END of namespace */

#endif
