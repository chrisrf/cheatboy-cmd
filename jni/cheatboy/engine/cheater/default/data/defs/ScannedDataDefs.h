#ifndef __ScannedDataDefs_H__
#define __ScannedDataDefs_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {

class ScannedDataDefs {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(ScannedDataDefs);
public:
	typedef size_t match_id_t;
};

} /* END of namespace */

#endif
