#ifndef __ScannedData_H__
#define __ScannedData_H__

#include <stdint.h>

#include <cheatboy/engine/cheater/default/data/defs/ScannedDataDefs.h>

namespace cboy {

struct MatchedHeader {
	uintptr_t remoteAddress;
	ScannedDataDefs::match_id_t numOfBytes;
};
static_assert(((sizeof(MatchedHeader) == 8) || (sizeof(MatchedHeader) == 16)), "Size of MatchedHeader is NOT 8 or 16.");

struct MatchedByte {
	uint8_t byte;
	uint8_t matchedValueMaxSize;
	uint16_t matchFlag;
};
static_assert(sizeof(MatchedByte) == 4, "Size of MatchedByte is NOT 4.");

struct ReadingMatchedHeader {
	uintptr_t remoteAddress;
	ScannedDataDefs::match_id_t numOfBytes;
	MatchedHeader* realHeaderAddress;
	MatchedByte* buffer;
};

} /* END of namespace */

#endif
