#ifndef __ScannedRegion_H__
#define __ScannedRegion_H__

#include <stdint.h>
#include <functional>
#include <PureMacro.h>

namespace cboy {
class IMemoryRegion;

class ScannedRegion {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ScannedRegion);
private:
	std::reference_wrapper<IMemoryRegion> _memoryRegion;
	uint8_t* _recordBuffer;
	size_t _recordBufferSize;
	size_t _maxNumOfMatchedBytesOfMatchedByteBuffer;
	size_t _numOfMatchededHeaders;
	size_t _numOfMatchedBytes;
	size_t _numOfMatchedValues;

public:
	ScannedRegion(IMemoryRegion& memoryRegion);
	virtual ~ScannedRegion();
public:
	void allocMaxRecordBuffer();
	void shrinkRecordBuffer(size_t newSmallSize);
	void releaseRecordBuffer();
	uint8_t* getRecordBuffer() { return _recordBuffer; }
	bool hasRecordBuffer() const { return (_recordBuffer != nullptr); }
	size_t getRecordBufferSize() const { return _recordBufferSize; }
public:
	const IMemoryRegion& getMemoryRegion() const { return _memoryRegion; }
	size_t getMaxNumOfMatchedBytesOfMatchedByteBuffer() const { return _maxNumOfMatchedBytesOfMatchedByteBuffer; }
	size_t getNumOfMatchedHeaders() const { return _numOfMatchededHeaders; }
	size_t getNumOfMatchedBytes() const { return _numOfMatchedBytes; }
	size_t getNumOfMatchedValues() const { return _numOfMatchedValues; }
public:
	void setMaxNumOfMatchedBytesOfMatchedByteBuffer(size_t size) {
		if (size > _maxNumOfMatchedBytesOfMatchedByteBuffer) {
			_maxNumOfMatchedBytesOfMatchedByteBuffer = size;
		}
	}
	void addNumOfMatchedHeadersByOne() { _numOfMatchededHeaders += 1; }
	void addNumOfMatchedBytesByOne() { _numOfMatchedBytes += 1; }
	void addNumOfMatchedValuesByOne() { _numOfMatchedValues += 1; }
	void addNumOfMatchedHeadersBy(size_t num) { _numOfMatchededHeaders += num; }
	void addNumOfMatchedBytesBy(size_t num) { _numOfMatchedBytes += num; }
	void addNumOfMatchedValuesBy(size_t num) { _numOfMatchedValues += num; }
	void clearRecordBufferMetaData();
};

} /* END of namespace */

#endif
