#include "ScannedRegion.h"

#include <stdlib.h>
#include <PureLib.h>

#include <cheatboy/engine/region/IMemoryRegion.h>
#include <cheatboy/engine/cheater/default/data/ScannedData.h>

namespace cboy {

ScannedRegion::ScannedRegion(IMemoryRegion& memoryRegion)
	:
	_memoryRegion(memoryRegion),
	_recordBuffer(nullptr), _recordBufferSize(0),
	_maxNumOfMatchedBytesOfMatchedByteBuffer(0),
	_numOfMatchededHeaders(0), _numOfMatchedBytes(0), _numOfMatchedValues(0) {}

ScannedRegion::~ScannedRegion() {
	if (hasRecordBuffer()) {
		free(_recordBuffer);
	}
}

void ScannedRegion::allocMaxRecordBuffer() {
	static_assert(sizeof(uint8_t) == 1, "Size of uint8_t is NOT 1.");
	if (hasRecordBuffer()) {
		PURE_THROW_EXCEPTION(pure::fmt("Already has a record buffer [{0}].", _recordBufferSize));
	}

	// TODO: Use unit tests to test size.
	IMemoryRegion& region = _memoryRegion;
	// XXX: [Header= MatchedHeader] + ([Record= MatchedByte] * [N= region size]) + [Null terminator= MatchedHeader]
	size_t maxSize = (sizeof(MatchedHeader)) + (region.getSize() * sizeof(MatchedByte)) + (sizeof(MatchedHeader));
	_recordBuffer = (uint8_t*)malloc(maxSize);
	if (_recordBuffer == nullptr) {
		PURE_THROW_EXCEPTION(pure::fmt("Can NOT allocate memory for size[{0}].", maxSize));
	}
	_recordBufferSize = maxSize;
}

void ScannedRegion::shrinkRecordBuffer(size_t newSmallSize) {
	if (!hasRecordBuffer()) {
		PURE_THROW_EXCEPTION(pure::fmt("No record buffer allocated."));
	}
	if (newSmallSize > _recordBufferSize) {
		PURE_THROW_EXCEPTION(pure::fmt(
			"Can NOT shrink memory for size[{0}] greater than current size[{0}].",
			newSmallSize, _recordBufferSize));
	}
	if (newSmallSize == 0) {
		releaseRecordBuffer();
		return;
	}
	if (newSmallSize < _recordBufferSize) {
		uint8_t* ptr = (uint8_t*)realloc(_recordBuffer, newSmallSize);
		if (ptr == nullptr) {
			PURE_THROW_EXCEPTION(pure::fmt(
				"Can NOT shrink memory for size[{0}] less than current size[{0}].",
				newSmallSize, _recordBufferSize));
		}
		_recordBuffer = ptr;
		_recordBufferSize = newSmallSize;
	}
}

void ScannedRegion::releaseRecordBuffer() {
	if (hasRecordBuffer()) {
		free(_recordBuffer);
		_recordBuffer = nullptr;
	}
	_recordBufferSize = 0;
	clearRecordBufferMetaData();
}

void ScannedRegion::clearRecordBufferMetaData() {
	_maxNumOfMatchedBytesOfMatchedByteBuffer = 0;

	_numOfMatchededHeaders = 0;
	_numOfMatchedBytes = 0;
	_numOfMatchedValues = 0;
}

} /* END of namespace */
