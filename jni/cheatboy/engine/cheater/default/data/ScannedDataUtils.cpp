#include "ScannedDataUtils.h"

#include <assert.h>
#include <PureLib.h>

#include <cheatboy/engine/cheater/default/data/ScannedRegion.h>
#include <cheatboy/engine/cheater/default/data/ScannedData.h>
#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/value/MemoryValueTypeFlagUtils.h>
#include <cheatboy/engine/value/match/MatchedValue.h>
#include <cheatboy/engine/value/match/EditableMatchedValue.h>

//#define __CBOY_ASSERTION_SCANNED_DATA_UTILS__

#ifdef __CBOY_ASSERTION_SCANNED_DATA_UTILS__
#define CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(mpExpression)\
	assert(mpExpression)
#define CBOY_ASSERTION_SCANNED_DATA_UTILS_MEMORY_ASSERT(mpAddressAccessed, mpSizeAccessed)\
	assertLocalMemoryAccessedIsValid(mpAddressAccessed, mpSizeAccessed)
#else
#define CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(mpExpression) ((void)0)
#define CBOY_ASSERTION_SCANNED_DATA_UTILS_MEMORY_ASSERT(mpAddressAccessed, mpSizeAccessed) ((void)0)
#endif

namespace cboy {

ScannedDataUtils::ScannedDataUtils()
	:
	_localValidAddress(0), _localValidSize(0) {}

ScannedDataUtils::~ScannedDataUtils() noexcept {}

bool ScannedDataUtils::isNullTerminator(MatchedHeader* header) {
	return (header->numOfBytes == 0);
}

void ScannedDataUtils::setAsNullTerminator(MatchedHeader* header) {
	header->remoteAddress = 0;
	header->numOfBytes = 0;
}

MatchedHeader* ScannedDataUtils::addNullTerminatorBehindTheMatchedByteBuffer(
	ScannedRegion& region, MatchedHeader* header) {

	// XXX: Null terminator is not counted as a matched header.
	region.setMaxNumOfMatchedBytesOfMatchedByteBuffer(header->numOfBytes);
	region.addNumOfMatchedHeadersByOne();
	region.addNumOfMatchedBytesBy(header->numOfBytes);
	return allocateNewMatchedHeader(header, 0);
}


// XXX: Explanation
//	addMatchedByteOrCreateNewMatchedHeaderAndAdd {
//		if (no byte is recorded) {
//			// this is the first byte to be recorded.
//
//			// set the remote address of the byte to the buffer (header)
//			// set num of bytes of header to zero
//		} else {
//			if (it is a memory efficient case) {
//				// means the gap is too large
//				// [last recorded byte] [0 gap] [0 gap] ... [0 gap] [this byte]
//
//				// start a new buffer (header)
//				// start at the first zero.
//
//				// set the remote address of the byte to the buffer (header)
//				// set num of bytes of header to zero
//			} else {
//				// means the gap is NOT so large
//				// [last recorded byte] [0 gap] [0 gap] [this byte]
//
//				// set the gap to all zero
//				// add num of bytes of gap to num of bytes of header
//			}
//		}
//
//		// record this byte.
//		// num of bytes of header PLUS 1
//	}
MatchedHeader* ScannedDataUtils::addMatchedByteOrCreateNewMatchedHeaderAndAdd(
	ScannedRegion& region, MatchedHeader* header, MatchedByte& addedMatchedByte, uintptr_t remoteAddress) {

	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(header != nullptr);
	if (header->numOfBytes == 0) {
		CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(header->remoteAddress == 0);
		header->remoteAddress = remoteAddress;
	} else {
		// const uintptr_t lastMatchedByteRemoteEndAddress = getLastMatchedByteRemoteEndAddress(header);
		// XXX: Do NOT use "getLastMatchedByteRemoteEndAddress(header)".
		// It will touch the system memory end address (invalid), but... is it possible?

		const uintptr_t lastMatchedByteRemoteAddress = getLastMatchedByteRemoteAddress(header);
		CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(remoteAddress > lastMatchedByteRemoteAddress);
		const size_t nUnrecordedGapBytes = remoteAddress - (lastMatchedByteRemoteAddress + 1);
		const size_t sizeUnrecordedGapBytes = nUnrecordedGapBytes * sizeof(MatchedByte);

		if (sizeUnrecordedGapBytes >= sizeof(MatchedHeader)) {
			region.setMaxNumOfMatchedBytesOfMatchedByteBuffer(header->numOfBytes);
			region.addNumOfMatchedHeadersByOne();
			region.addNumOfMatchedBytesBy(header->numOfBytes);
			header = allocateNewMatchedHeader(header, remoteAddress);
		} else if (sizeUnrecordedGapBytes != 0) {
			memset(getLastMatchedByteLocalEndAddress(header), 0, sizeUnrecordedGapBytes);
			header->numOfBytes += nUnrecordedGapBytes;
		}
	}
	addMatchedByte(region, header, addedMatchedByte);
	return header;
}

// XXX: Use it carefully. You should make sure that you are adding a continuous matched byte.
void ScannedDataUtils::addMatchedByte(ScannedRegion& region, MatchedHeader* header, MatchedByte& addedMatchedByte) {
	CBOY_ASSERTION_SCANNED_DATA_UTILS_MEMORY_ASSERT(
		header, sizeof(MatchedHeader) + ((header->numOfBytes)*sizeof(MatchedByte)) + sizeof(MatchedByte));
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(header != nullptr);
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(header->remoteAddress != 0);

	MatchedByte* newMatchedByte = allocateNewMatchedByte(header);
	*newMatchedByte = addedMatchedByte;
	header->numOfBytes = (header->numOfBytes) + 1;
}

size_t ScannedDataUtils::computeTheRecordBufferNewSize(uint8_t* recordBuffer, MatchedHeader* nullTerminatorHeader) {
	// uint8_t* nullTerminatorHeaderPtr = (uint8_t*) (nullTerminatorHeader);
	// nullTerminatorHeaderPtr = (nullTerminatorHeaderPtr + sizeof(MatchedHeader));
	// XXX: Do NOT use "nullTerminatorHeaderPtr".
	// It will touch the system memory end address (invalid), but... is it possible?

	uintptr_t endAddress = pure::PointerUtils::toValue(nullTerminatorHeader);
	uintptr_t startAddress = pure::PointerUtils::toValue(recordBuffer);
	return ((endAddress - startAddress) + sizeof(MatchedHeader));
}

MatchedHeader* ScannedDataUtils::allocateNewMatchedHeader(MatchedHeader* header, uintptr_t remoteAddress) {
	CBOY_ASSERTION_SCANNED_DATA_UTILS_MEMORY_ASSERT(
		header, sizeof(MatchedHeader) + ((header->numOfBytes)*sizeof(MatchedByte)) + sizeof(MatchedHeader));
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(header != nullptr);

	MatchedHeader* newHeader = (MatchedHeader*)getLastMatchedByteLocalEndAddress(header);
	newHeader->remoteAddress = remoteAddress;
	newHeader->numOfBytes = 0;
	return newHeader;
}

MatchedByte* ScannedDataUtils::allocateNewMatchedByte(MatchedHeader* header) {
	return (MatchedByte*) getLastMatchedByteLocalEndAddress(header);
}

MatchedByte* ScannedDataUtils::getMatchedByteBuffer(MatchedHeader* header) {
	uint8_t* headerPtr = (uint8_t*) (header);
	return ((MatchedByte*) (headerPtr + sizeof(MatchedHeader)));
}

uintptr_t ScannedDataUtils::getLastMatchedByteRemoteAddress(MatchedHeader* header) {
	// XXX: You should guarantee that "numOfBytes" is greater than zero.
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(header->numOfBytes > 0);
	// XXX: You should guarantee that it never touch system memory end address.
	return ((header->remoteAddress) + (header->numOfBytes - 1));
}

uint8_t* ScannedDataUtils::getLastMatchedByteLocalEndAddress(MatchedHeader* header) {
	// XXX: You should guarantee that it never touch system memory end address.
	MatchedByte* buffer = getMatchedByteBuffer(header);
	return (uint8_t*)(&(buffer[(header->numOfBytes)]));
}

bool ScannedDataUtils::isReadingNullTerminator(ReadingMatchedHeader& header) {
	return (header.numOfBytes == 0);
}

void ScannedDataUtils::captureMatchedHeaderToReadingMatchedHeader(ReadingMatchedHeader& header, MatchedHeader* matchedHeader) {
	header.remoteAddress = matchedHeader->remoteAddress;
	header.numOfBytes = matchedHeader->numOfBytes;
	header.realHeaderAddress = matchedHeader;
	header.buffer = getMatchedByteBuffer(matchedHeader);
}

void ScannedDataUtils::captureNextMatchedHeaderToReadingMatchedHeader(ReadingMatchedHeader& header) {
	MatchedHeader* nextMatchedHeader = (MatchedHeader*)(&(header.buffer[header.numOfBytes]));
	captureMatchedHeaderToReadingMatchedHeader(header, nextMatchedHeader);
}

size_t ScannedDataUtils::getForMemoryValue(
	ReadingMatchedHeader& header, size_t index, MemoryValue& value) {

	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(index < (header.numOfBytes));
	MatchedByte* buffer = &(header.buffer[index]);

	const size_t size = buffer[0].matchedValueMaxSize;
	uint8_t* valueBuffer = value.getBuffer();
	for (size_t i = 0; i < size; i++) {
		valueBuffer[i] = buffer[i].byte;
	}
	return size;
}

void ScannedDataUtils::getForMatchedValue(
	ReadingMatchedHeader& header, size_t index,
	region_id_t regionID, match_id_t matchedByteID, MatchedValue& value) {

	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(index < (header.numOfBytes));
	MatchedByte* buffer = &(header.buffer[index]);

	const size_t size = buffer[0].matchedValueMaxSize;
	uint8_t* valueBuffer = value.getBuffer();
	for (size_t i = 0; i < size; i++) {
		valueBuffer[i] = buffer[i].byte;
	}

	const uintptr_t remoteAddress = (header.remoteAddress + index);
	value.setRegionID(regionID);
	value.setMatchID(matchedByteID);
	value.setRemoteAddress(remoteAddress);
	value.setSize(size);
	value.setMatchFlag(buffer[0].matchFlag);
}

size_t ScannedDataUtils::getMatchedValueMaxSize(ReadingMatchedHeader& header, size_t index) {
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(index < (header.numOfBytes));
	return header.buffer[index].matchedValueMaxSize;
}

ScannedDataUtils::flag_t ScannedDataUtils::getMatchFlag(ReadingMatchedHeader& header, size_t index) {
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(index < (header.numOfBytes));
	return header.buffer[index].matchFlag;
}

uintptr_t ScannedDataUtils::getRemoteAddress(ReadingMatchedHeader& header, size_t index) {
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(index < (header.numOfBytes));
	return (header.remoteAddress + index);
}

uintptr_t ScannedDataUtils::checkEditableMatchedValue(ReadingMatchedHeader& header, size_t index, EditableMatchedValue& editableValue) {
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(index < (header.numOfBytes));

	flag_t editableMatchedValueMatchFlag = MemoryValueTypeFlagUtils::fromTypeID(editableValue.getTypeID());
	MemoryValueTypeFlag matchFlag;
	matchFlag.setFlag(getMatchFlag(header, index));
	if (!matchFlag.isMarked(editableMatchedValueMatchFlag)) {
		PURE_THROW_EXCEPTION(pure::fmt("The type[{0}] of editable matched value is not matched to [{1}].",
			editableMatchedValueMatchFlag, matchFlag.getFlag()));
	}

	const size_t size = getMatchedValueMaxSize(header, index);
	if (editableValue.getSize() > size) {
		PURE_THROW_EXCEPTION(pure::fmt("The size[{0}] of editable matched value is not matched to [{1}].",
			editableValue.getSize(), size));
	}
	if (editableValue.getSize() > (header.numOfBytes - index)) {
		PURE_THROW_EXCEPTION(pure::fmt("The size[{0}] of editable matched value is not too large for [{1}= {2}-{3}].",
			editableValue.getSize(), (header.numOfBytes - index), header.numOfBytes, index));
	}
	return getRemoteAddress(header, index);
}

MatchedHeader* ScannedDataUtils::getWritingMatchedHeader(ReadingMatchedHeader& header) {
	return header.realHeaderAddress;
}

void ScannedDataUtils::editMatchedValue(MatchedHeader* header, size_t index, EditableMatchedValue& editableValue) {
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(index < (header->numOfBytes));
	MatchedByte* buffer = (getMatchedByteBuffer(header) + index);
	const size_t size = editableValue.getSize();
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(size <= buffer[0].matchedValueMaxSize);
	CBOY_ASSERTION_SCANNED_DATA_UTILS_ASSERT(size <= (header->numOfBytes - index));
	uint8_t* valueBuffer = editableValue.getBuffer();
	for (size_t i = 0; i < size; i++) {
		buffer[i].byte = valueBuffer[i];
	}
}

#ifdef __CBOY_ASSERTION_SCANNED_DATA_UTILS__
void ScannedDataUtils::assertLocalMemoryAccessedIsValid(void* addressAccessed, size_t sizeAccessed) {
	uintptr_t address = pure::PointerUtils::toValue(addressAccessed);
//	void* localValidAddressAccessed = pure::PointerUtils::toPointer(_localValidAddress);
//	PURE_D_LOG_LN("# =assertLocalMemoryAccessedIsValid= [{0}][{1}][{2}][{3}]",
//			addressAccessed, address, sizeAccessed, (address + (sizeAccessed-1)));
//	PURE_D_LOG_LN("# =assertLocalMemoryAccessedIsValid= [{0}][{1}][{2}][{3}]",
//			localValidAddressAccessed, _localValidAddress, _localValidSize, (_localValidAddress + (_localValidSize-1)));
	assert((address) >= _localValidAddress);
	assert((address + (sizeAccessed-1)) <= (_localValidAddress + (_localValidSize-1)));
}
#endif

} /* END of namespace */
