#ifndef __RegionMatchesEditor_H__
#define __RegionMatchesEditor_H__

#include <stdint.h>
#include <vector>
#include <PureMacro.h>

#include <cheatboy/engine/cheater/defs/MemoryCheaterDefs.h>
#include <cheatboy/engine/cheater/default/data/defs/ScannedDataDefs.h>
#include <cheatboy/engine/cheater/default/data/ScannedDataUtils.h>
#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>
#include <cheatboy/engine/value/defs/MemoryValueTypeFlagDefs.h>
#include <cheatboy/engine/value/match/MatchedValue.h>

namespace cboy {
class ScannedRegion;
class IMemoryEditor;
class MatchedValue;
class EditableMatchedValue;

class RegionMatchesEditor {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(RegionMatchesEditor);
private:
	typedef MemoryCheaterDefs::IMatchedValueHandler IMatchedValueHandler;
private:
	typedef MemoryValueTypeFlagDefs::flag_t flag_t;
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef ScannedDataDefs::match_id_t match_id_t;
private:
	ScannedDataUtils _dataUtils;
	MatchedValue _matchedValue;

public:
	RegionMatchesEditor();
	virtual ~RegionMatchesEditor() noexcept;
public:
	void getMatchedValues(ScannedRegion& region, std::vector<MatchedValue>& values);
	void iterateOverMatchedValues(ScannedRegion& region, IMatchedValueHandler& handler);
	void editMatchedValue(ScannedRegion& region, IMemoryEditor& editor, EditableMatchedValue& editableValue);
private:
	void throwIfScannedRegionDoesNotHaveValidRecordBuffer(const ScannedRegion& region);
	void throwIfEditableMatchedValueIsInvalid(const ScannedRegion& region, const EditableMatchedValue& editableValue);
};

} /* END of namespace */

#endif
