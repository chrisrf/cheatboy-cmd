#ifndef __RegionScanner_H__
#define __RegionScanner_H__

#include <stdint.h>
#include <PureMacro.h>

#include <cheatboy/engine/cheater/default/data/ScannedData.h>
#include <cheatboy/engine/cheater/default/data/ScannedDataUtils.h>
#include <cheatboy/engine/value/comparator/MemoryValueComparator.h>
#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/UserValue.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>

namespace cboy {
enum class ComparisonMethod;
class ScannedRegion;
class IMemoryReader;

class RegionScanner {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(RegionScanner);
private:
	bool _isScanProcedureStarted;
	bool _isOldValueRequired;
private:
	ScannedDataUtils _dataUtils;
	MemoryValueComparator _comparator;
	MemoryValue _newValue;
	MemoryValue _oldValue;
	UserValue _userValue;
	MemoryValueTypeFlag _filterFlag;
	MemoryValueTypeFlag _cmpFlag;
	MemoryValueTypeFlag _savedFlag;
	uint8_t* _readerBuffer;
	size_t _readerBufferSize;
	MatchedByte _matchedByte;
	MatchedByte _recordedByte;

public:
	RegionScanner();
	virtual ~RegionScanner() noexcept;
public:
	void startScanProcedure(
		ComparisonMethod method, const UserValue& userValue, const MemoryValueTypeFlag& filterFlag,
		size_t readerBufSize);
	void endScanProcedure();
	void initialScan(ScannedRegion& region, IMemoryReader& reader);
	void scanMatches(ScannedRegion& region, IMemoryReader& reader);
private:
	bool hasReaderBuffer() const;
	void prepareDataForInitialScan(uint8_t* remoteMemoryBuf, size_t size);
	bool checkComparisonFlagBeforePreparingDataForScanMatches(ReadingMatchedHeader& header, size_t index);
	void prepareDataForScanMatchesWithoutOldValue(
		uint8_t* remoteMemoryBuf, size_t size);
	void prepareDataForScanMatchesWithOldValue(
		uint8_t* remoteMemoryBuf, size_t size,
		ReadingMatchedHeader& header, size_t index);
	void unmarkImpossibleComparisonFlagByPossibleMaxBytes(size_t possibleMaxBytes);
	void compareAndAddMatchedByte(
		uintptr_t matchedByteRemoteAddress, size_t possibleMaxBytes,
		ScannedRegion& region, MatchedHeader*& matchedHeaderPtr, size_t& remainedBytesNeedToBeRecorded);
	inline void addRemainedByteNeedToBeRecorded(
		uint8_t byte,
		ScannedRegion& region, MatchedHeader*& matchedHeaderPtr, size_t& remainedBytesNeedToBeRecorded);
	void finalizeScanProcedure(ScannedRegion& region, MatchedHeader*& matchedHeaderPtr);
private:
	void throwIfScanProcedureIsNotEnded();
	void throwIfComparisonMethodIsInvalid(ComparisonMethod method);
	void throwIfFilterFlagIsZero(const MemoryValueTypeFlag& filterFlag);
	void throwIfAssignedReaderBufferSizeIsZero(size_t readerBufSize);
	void throwIfReaderBufferIsAlreadyAllocated();
	void throwIfScanProcedureIsNotStarted();
	void throwIfOldValueIsRequired();
	void throwIfMemoryRegionSizeIsGreaterThanReaderBufferSize(const ScannedRegion& region);
	void throwIfMaxSizedMatchedBufferSizeIsGreaterThanReaderBufferSize(const ScannedRegion& region);
	void throwIfScannedRegionHasRecordBufferAlready(const ScannedRegion& region);
	void throwIfScannedRegionDoesNotHaveValidRecordBuffer(const ScannedRegion& region);
};

} /* END of namespace */

#endif
