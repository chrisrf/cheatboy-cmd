#include "RegionMatchesEditor.h"

#include <assert.h>
#include <string.h>
#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/cheater/default/data/ScannedRegion.h>
#include <cheatboy/engine/cheater/default/data/ScannedData.h>
#include <cheatboy/engine/editor/IMemoryEditor.h>
#include <cheatboy/engine/region/IMemoryRegion.h>
#include <cheatboy/engine/value/match/EditableMatchedValue.h>

namespace cboy {

RegionMatchesEditor::RegionMatchesEditor()
	:
	_dataUtils(),
	_matchedValue() {}

RegionMatchesEditor::~RegionMatchesEditor() noexcept {}

void RegionMatchesEditor::getMatchedValues(ScannedRegion& region, std::vector<MatchedValue>& values) {
	throwIfScannedRegionDoesNotHaveValidRecordBuffer(region);

	std::vector<MatchedValue> tempValues;
	tempValues.reserve(region.getNumOfMatchedValues());
	IMatchedValueHandler handler = [&tempValues] (const MatchedValue& matchedValue) {
		MatchedValue newValue;
		matchedValue.copyTo(newValue);
		tempValues.push_back(std::move(newValue));
	};
	iterateOverMatchedValues(region, handler);
	values = std::move(tempValues);
}

void RegionMatchesEditor::iterateOverMatchedValues(ScannedRegion& region, IMatchedValueHandler& handler) {
	throwIfScannedRegionDoesNotHaveValidRecordBuffer(region);
	_matchedValue.clear();
	const IMemoryRegion& memoryRegion = region.getMemoryRegion();

	uint8_t* const recordBuffer = region.getRecordBuffer();
	const size_t recordBufferSize = region.getRecordBufferSize();
	_dataUtils.setLocalValidAddress(pure::PointerUtils::toValue(recordBuffer));
	_dataUtils.setLocalValidSize(recordBufferSize);

	// Capture the header.
	ReadingMatchedHeader readingMatchedHeader;
	_dataUtils.captureMatchedHeaderToReadingMatchedHeader(readingMatchedHeader, (MatchedHeader*) recordBuffer);

	match_id_t matchedByteID = 0;
	assert(!_dataUtils.isReadingNullTerminator(readingMatchedHeader));
	while (!_dataUtils.isReadingNullTerminator(readingMatchedHeader)) {

		for (size_t offset = 0; offset < readingMatchedHeader.numOfBytes; offset++, matchedByteID++) {
			if (_dataUtils.getMatchFlag(readingMatchedHeader, offset) == 0) {
				continue;
			}

			_dataUtils.getForMatchedValue(readingMatchedHeader, offset, memoryRegion.getID(), matchedByteID, _matchedValue);
			handler(_matchedValue);
		}
		_dataUtils.captureNextMatchedHeaderToReadingMatchedHeader(readingMatchedHeader);
	}
	assert(matchedByteID == region.getNumOfMatchedBytes());
}

void RegionMatchesEditor::editMatchedValue(ScannedRegion& region, IMemoryEditor& editor, EditableMatchedValue& editableValue) {
	throwIfScannedRegionDoesNotHaveValidRecordBuffer(region);
	throwIfEditableMatchedValueIsInvalid(region, editableValue);

	uint8_t* const recordBuffer = region.getRecordBuffer();
	const size_t recordBufferSize = region.getRecordBufferSize();
	_dataUtils.setLocalValidAddress(pure::PointerUtils::toValue(recordBuffer));
	_dataUtils.setLocalValidSize(recordBufferSize);

	// Capture the header.
	ReadingMatchedHeader readingMatchedHeader;
	_dataUtils.captureMatchedHeaderToReadingMatchedHeader(readingMatchedHeader, (MatchedHeader*) recordBuffer);

	match_id_t currentBaseMatchedByteID = 0;
	match_id_t currentMaxLimitMatchedByteID = 0;
	assert(!_dataUtils.isReadingNullTerminator(readingMatchedHeader));
	while (!_dataUtils.isReadingNullTerminator(readingMatchedHeader)) {
		currentMaxLimitMatchedByteID += readingMatchedHeader.numOfBytes;
		if (editableValue.getMatchID() < currentMaxLimitMatchedByteID) {
			// In this matched byte buffer.
			break;
		}
		// NOT in this matched byte buffer, jump to next one.
		currentBaseMatchedByteID += readingMatchedHeader.numOfBytes;
		_dataUtils.captureNextMatchedHeaderToReadingMatchedHeader(readingMatchedHeader);
	}

	if (_dataUtils.isReadingNullTerminator(readingMatchedHeader)) {
		PURE_THROW_EXCEPTION(pure::fmt("No matched value is found for [{0}].", editableValue.getMatchID()));
	}
	const size_t index = editableValue.getMatchID() - currentBaseMatchedByteID;

	// It will throw exception if the editable matched value is invalid.
	const uintptr_t remoteAddress = _dataUtils.checkEditableMatchedValue(readingMatchedHeader, index, editableValue);

	// It will throw exception if writing remote memory failed.
	editor.writeMemory(remoteAddress, editableValue.getBuffer(), editableValue.getSize());
	{
		// A deep check
		uint8_t* buf = editableValue.getBuffer();
		uint8_t tempBuf[EditableMatchedValue::getCapacity()] = { 0 };
		editor.readMemory(remoteAddress, tempBuf, editableValue.getSize());
		for (size_t i = 0; i < editableValue.getSize(); i++) {
			if (buf[i] != tempBuf[i]) {
				PURE_THROW_EXCEPTION(pure::fmt("Writing memory value failed at [{0}].",
					pure::PointerUtils::toPointer(remoteAddress)));
			}
		}
	}

	MatchedHeader* matchedHeaderPtr = _dataUtils.getWritingMatchedHeader(readingMatchedHeader);
	_dataUtils.editMatchedValue(matchedHeaderPtr, index, editableValue);
}

void RegionMatchesEditor::throwIfScannedRegionDoesNotHaveValidRecordBuffer(const ScannedRegion& region) {
	const IMemoryRegion& memoryRegion = region.getMemoryRegion();
	if (!region.hasRecordBuffer()) {
		PURE_THROW_EXCEPTION(pure::fmt("Scanned region [{0}, \"{1}\"] does NOT have record buffer.",
			memoryRegion.getID(), memoryRegion.getName()));
	}
	if (region.getRecordBufferSize() < ((sizeof(MatchedHeader)*2) + sizeof(MatchedByte))) {
		PURE_THROW_EXCEPTION(pure::fmt("Scanned region [{0}, \"{1}\"] have a invalid record buffer.",
			memoryRegion.getID(), memoryRegion.getName()));
	}
}

void RegionMatchesEditor::throwIfEditableMatchedValueIsInvalid(const ScannedRegion& region, const EditableMatchedValue& editableValue) {
	const IMemoryRegion& memoryRegion = region.getMemoryRegion();
	if (editableValue.getRegionID() != memoryRegion.getID()) {
		PURE_THROW_EXCEPTION(pure::fmt("Region ID [{0}] of EditableMatchedValue is not matched to [{1}, \"{2}\"].",
			editableValue.getRegionID(), memoryRegion.getID(), memoryRegion.getName()));
	}
	if (editableValue.getMatchID() >= region.getNumOfMatchedBytes()) {
		PURE_THROW_EXCEPTION(pure::fmt("Match ID [{0}] of EditableMatchedValue should NOT be greater than to [{1}].",
			editableValue.getMatchID(), (region.getNumOfMatchedBytes() - 1)));
	}
}

} /* END of namespace */
