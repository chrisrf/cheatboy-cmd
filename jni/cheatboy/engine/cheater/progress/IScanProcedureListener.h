#ifndef __IScanProcedureListener_H__
#define __IScanProcedureListener_H__

#include <PureMacro.h>

class IScanProcedureListener {
PURE_DECLARE_CLASS_AS_INTERFACE(IScanProcedureListener);
public:
	void notify();
};

#endif
