#ifndef __IMemoryCheater_H__
#define __IMemoryCheater_H__

#include <stdint.h>
#include <vector>
#include <functional>
#include <PureMacro.h>

#include <cheatboy/engine/cheater/defs/MemoryCheaterDefs.h>

namespace cboy {
class IMemoryRegion;
class MemoryValueTypeFlag;
class ScanArguments;
class MatchedValue;
class EditableMatchedValue;

class IMemoryCheater {
PURE_DECLARE_CLASS_AS_INTERFACE(IMemoryCheater);
protected:
	typedef MemoryCheaterDefs::ILoadedRegionHandler ILoadedRegionHandler;
	typedef MemoryCheaterDefs::IScannedRegionHandler IScannedRegionHandler;
	typedef MemoryCheaterDefs::IMatchedValueHandler IMatchedValueHandler;

public:
	virtual bool hasLoadedRegions() const = 0;
	virtual size_t getNumOfLoadedRegions() const = 0;
	virtual void getLoadedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) = 0;
	virtual void getLoadedRegions(ILoadedRegionHandler& handler) = 0;
	virtual void loadRegions() = 0;
	virtual bool hasScannedRegions() const = 0;
	virtual size_t getNumOfScannedRegions() const = 0;
	virtual void getScannedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) = 0;
	virtual void getScannedRegions(IScannedRegionHandler& handler) = 0;
	virtual void deleteRegions() = 0;
public:
	virtual size_t getNumOfCurrentMatchedValues() const = 0;
	virtual void scan(ScanArguments& args) = 0;
	virtual void getMatchedValues(std::vector<MatchedValue>& values) = 0;
	virtual void iterateOverMatchedValues(IMatchedValueHandler& handler) = 0;
	virtual void editMatchedValue(EditableMatchedValue& value) = 0;
};

} /* END of namespace */

#endif
