#include "MemoryCheater.h"

#include <assert.h>
#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/config/IEngineConfig.h>
#include <cheatboy/engine/editor/IMemoryEditorManager.h>
#include <cheatboy/engine/editor/IMemoryEditor.h>
#include <cheatboy/engine/editor/reader/IMemoryStreamReader.h>
#include <cheatboy/engine/region/MemoryRegion.h>
#include <cheatboy/engine/region/IMemoryRegionLoader.h>
#include <cheatboy/engine/cheater/args/ScanArguments.h>
#include <cheatboy/engine/cheater/default/data/ScannedRegion.h>
#include <cheatboy/engine/cheater/default/RegionScanner.h>
#include <cheatboy/engine/cheater/default/RegionMatchesEditor.h>
#include <cheatboy/engine/value/comparator/ComparisonMethod.h>
#include <cheatboy/engine/value/comparator/ComparisonMethodUtils.h>
#include <cheatboy/engine/value/match/MatchedValue.h>
#include <cheatboy/engine/value/match/EditableMatchedValue.h>

namespace cboy {

MemoryCheater::MemoryCheater(
	IEngineConfig& engineConfig,
	IProcessAttacher& processAttacher,
	IMemoryEditorManager& editorManager,
	IMemoryRegionLoader& regionLoader)
	:
	_engineConfig(engineConfig),
	_processAttacher(processAttacher),
	_editorManager(editorManager),
	_regionLoader(regionLoader),
	_regionScanner(std::make_unique<RegionScanner>()),
	_regionMatchesEditor(std::make_unique<RegionMatchesEditor>()),
	_isScanned(false),
	_regions(),
	_maxSizedRegionSize(0),
	_maxNumOfMatchedBytesOfMatchedByteBuffer(0),
	_numOfMatchededHeaders(0),
	_numOfMatchedBytes(0),
	_numOfMatchedValues(0) {}

MemoryCheater::~MemoryCheater() noexcept {
	_regions.clear();
}

bool MemoryCheater::hasLoadedRegions() const {
	return (!_regions.empty());
}

size_t MemoryCheater::getNumOfLoadedRegions() const {
	return _regions.size();
}

void MemoryCheater::getLoadedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) {
	std::vector<std::reference_wrapper<IMemoryRegion>> tempRegions;
	tempRegions.reserve(_regions.size());
	ILoadedRegionHandler handler = [&tempRegions] (IMemoryRegion& region) {
		tempRegions.push_back(region);
	};
	getLoadedRegions(handler);
	regions = std::move(tempRegions);
}

void MemoryCheater::getLoadedRegions(ILoadedRegionHandler& handler) {
	for (auto& pair : _regions) {
		auto& regionTuple = pair.second;
		auto& region = std::get<0>(regionTuple);
		handler(*region);
	}
}

void MemoryCheater::loadRegions() {
	typedef MemoryRegionLoaderDefs::MemoryRegionHandler MemoryRegionHandler;

	if (hasLoadedRegions()) { deleteRegions(); }

	RegionMap tempRegions;
	size_t tempMaxSizedRegionSize = 0;
	MemoryRegionHandler handler = [&tempRegions, &tempMaxSizedRegionSize] (std::unique_ptr<MemoryRegion> newRegion) {
		region_id_t regionID = newRegion->getID();
		std::unique_ptr<ScannedRegion> scannedRegion = std::make_unique<ScannedRegion>(*newRegion);

		size_t regionSize = newRegion->getSize();
		std::pair<RegionMap::iterator, bool> res = tempRegions.insert(std::make_pair(
			regionID,
			std::make_tuple(std::move(newRegion), std::move(scannedRegion))
		));
		if (!res.second) {
			PURE_THROW_EXCEPTION(pure::fmt("Insert a duplicate region with region ID [{0}].", regionID));
		}
		if (regionSize > tempMaxSizedRegionSize) {
			tempMaxSizedRegionSize = regionSize;
		}
	};

	IEngineConfig& config = _engineConfig;
	IMemoryRegionLoader& loader = _regionLoader;
	PURE_D_LOG_LN("Load memory regions at level [{0}].", static_cast<int>(config.getScannedRegionLevel()));
	loader.loadAsMutableRegions(config.getScannedRegionLevel(), handler);
	_regions = std::move(tempRegions);
	_maxSizedRegionSize = tempMaxSizedRegionSize;
	PURE_D_LOG_LN("Size of max sized region = [{0}].", _maxSizedRegionSize);
	PURE_D_LOG_LN("Memory regions are loaded.");
}

bool MemoryCheater::hasScannedRegions() const {
	// XXX: Use it to determine if you have performed the initial scan.
	return _isScanned;
}

size_t MemoryCheater::getNumOfScannedRegions() const {
	if (!hasScannedRegions()) { return 0; }
	return _regions.size();
}

void MemoryCheater::getScannedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) {
	if (!hasScannedRegions()) { return; }
	getLoadedRegions(regions);
}

void MemoryCheater::getScannedRegions(IScannedRegionHandler& handler) {
	if (!hasScannedRegions()) { return; }
	getLoadedRegions(handler);
}

void MemoryCheater::deleteRegions() {
	PURE_D_LOG_LN("Delete all loaded regions...");
	_isScanned = false;
	clearMetaData();
	_regions.clear();
	PURE_D_LOG_LN("Delete all loaded regions...Done");
}

size_t MemoryCheater::getNumOfCurrentMatchedValues() const {
	return _numOfMatchedValues;
}

void MemoryCheater::scan(ScanArguments& args) {
	throwIfComparisonMethodIsInvalid(args.getComparisonMethod());
	//throwExceptionIfValueTypeFlagOfScanArgumentsIsZero(args.getValueTypeFlag());

	// Prepare filter flag
	IEngineConfig& config = _engineConfig;
	MemoryValueTypeFlag filterFalg = config.getScannedValueTypeFlag();
	filterFalg.setFlag(filterFalg.getFlag() & args.getValueTypeFlag().getFlag());
	if (!filterFalg.isMarkedForAny()) {
		deleteRegions();
		return;
	}

	// Prepare memory region
	if ((!hasScannedRegions()) && (!hasLoadedRegions())) {
		loadRegions();
		if (!hasLoadedRegions()) {
			// No region can be scanned.
			return;
		}
	}

	// Preparing for handler
	IMemoryEditorManager& editorManager = _editorManager;
	IMemoryEditor& streamingEditor = editorManager.getStreamingEditor();
	std::unique_ptr<IMemoryStreamReader> streamReader = streamingEditor.createMemoryStreamReader();
	std::vector<region_id_t> removedRegionIDs;
	ScannedRegionHandler handler = [this, &streamReader, &removedRegionIDs] (ScannedRegion& scannedRegion) -> bool {
		const IMemoryRegion& memoryRegion = scannedRegion.getMemoryRegion();
		PURE_D_LOG_LN("====# Start scanning region [{0}] \"{1}\"", memoryRegion.getID(), memoryRegion.getName());
		IMemoryReader& reader = _editorManager.get().chooseSuitableReader(memoryRegion.getName(), *streamReader);
		try {
			// XXX: It might throw exceptions if the region (Ex: a temporary thread stack) is already not there.
			if (!hasScannedRegions()) { _regionScanner->initialScan(scannedRegion, reader); }
			else { _regionScanner->scanMatches(scannedRegion, reader); }
		} catch (pure::Exception& e) {
			// XXX: It should be a ReadMemoryException.
			PURE_D_LOG(e);
			scannedRegion.releaseRecordBuffer();
		}
		if (scannedRegion.hasRecordBuffer()) {
			recordMetaData(scannedRegion);
		} else {
			PURE_D_LOG_LN("\t# Region [{0}] is DELETED after scanning", memoryRegion.getID());
			removedRegionIDs.push_back(memoryRegion.getID());
		}
		PURE_D_LOG_LN("====# End scanning region [{0}]", memoryRegion.getID());
		return true;
	};

	// Preparing for scan procedure.
	size_t readerBufferSize = (hasScannedRegions() ? _maxNumOfMatchedBytesOfMatchedByteBuffer : _maxSizedRegionSize);
	clearMetaData();

	// Perform the initial scan procedure.
	PURE_D_LOG_LN("####### Start scanning");
	try {
		_regionScanner->startScanProcedure(args.getComparisonMethod(), args.getUserValue(), filterFalg, readerBufferSize);
		streamReader->openStream();
		iterateOverScannedRegions(handler);
	} catch (...) {
		streamReader->closeStream();
		_regionScanner->endScanProcedure();
		deleteRegions();
		throw;
	}
	streamReader->closeStream();
	_regionScanner->endScanProcedure();
	PURE_D_LOG_LN("####### End scanning");

	// Delete no-matches regions.
	try {
		removeRegions(removedRegionIDs);
	} catch (...) {
		deleteRegions();
		throw;
	}
	_isScanned = hasLoadedRegions();
}

void MemoryCheater::getMatchedValues(std::vector<MatchedValue>& values) {
	if (!hasScannedRegions()) { return; }
	if (_numOfMatchedValues >_engineConfig.get().getThresholdOfListableMatchedValues()) { return; }
	std::vector<MatchedValue> tempValues;
	tempValues.reserve(_numOfMatchedValues);
	IMatchedValueHandler handler = [&tempValues] (const MatchedValue& matchedValue) {
		MatchedValue newValue;
		matchedValue.copyTo(newValue);
		tempValues.push_back(std::move(newValue));
	};
	iterateOverMatchedValues(handler);
	values = std::move(tempValues);
}

void MemoryCheater::iterateOverMatchedValues(IMatchedValueHandler& handler) {
	if (!hasScannedRegions()) { return; }
	if (_numOfMatchedValues >_engineConfig.get().getThresholdOfListableMatchedValues()) { return; }
	ScannedRegionHandler sRegionHandler = [this, &handler] (ScannedRegion& scannedRegion) -> bool {
		_regionMatchesEditor->iterateOverMatchedValues(scannedRegion, handler);
		return true;
	};
	iterateOverScannedRegions(sRegionHandler);
}

void MemoryCheater::editMatchedValue(EditableMatchedValue& value) {
	throwIfNoScannedRegions();
	const auto& pair = _regions.find(value.getRegionID());
	if (pair == _regions.end()) {
		throwIfThatRegionIsNotFound(value.getRegionID());
	}
	auto& regionTuple = pair->second;
	auto& scannedRegion = std::get<1>(regionTuple);
	IMemoryEditorManager& editorManager = _editorManager;
	_regionMatchesEditor->editMatchedValue(*scannedRegion, editorManager.getCurrentEditor(), value);
}

void MemoryCheater::recordMetaData(ScannedRegion& region) {
	size_t tempMaxSizedRegionSize = region.getMemoryRegion().getSize();
	if (tempMaxSizedRegionSize > _maxSizedRegionSize) {
		_maxSizedRegionSize = tempMaxSizedRegionSize;
	}
	if (region.getMaxNumOfMatchedBytesOfMatchedByteBuffer() > _maxNumOfMatchedBytesOfMatchedByteBuffer) {
		_maxNumOfMatchedBytesOfMatchedByteBuffer = region.getMaxNumOfMatchedBytesOfMatchedByteBuffer();
	}
	_numOfMatchededHeaders += region.getNumOfMatchedHeaders();
	_numOfMatchedBytes += region.getNumOfMatchedBytes();
	_numOfMatchedValues += region.getNumOfMatchedValues();
	PURE_D_LOG_LN("\t# Region [{0}] after scanning", region.getMemoryRegion().getID());
	PURE_D_LOG_LN("\t\tMax number of matched bytes of matched byte buffer: [{0}]", region.getMaxNumOfMatchedBytesOfMatchedByteBuffer());
	PURE_D_LOG_LN("\t\tNumber of matched headers: [{0}]", region.getNumOfMatchedHeaders());
	PURE_D_LOG_LN("\t\tNumber of matched bytes:   [{0}]", region.getNumOfMatchedBytes());
	PURE_D_LOG_LN("\t\tNumber of matched values:  [{0}]", region.getNumOfMatchedValues());
}

void MemoryCheater::clearMetaData() {
	_maxSizedRegionSize = 0;
	_maxNumOfMatchedBytesOfMatchedByteBuffer = 0;
	_numOfMatchededHeaders = 0;
	_numOfMatchedBytes = 0;
	_numOfMatchedValues = 0;
}

void MemoryCheater::removeRegions(std::vector<region_id_t>& removedRegionIDs) {
	for (const auto& rid : removedRegionIDs) {
		_regions.erase(rid);
	}
}

void MemoryCheater::iterateOverScannedRegions(ScannedRegionHandler& handler) {
	for (auto& pair : _regions) {
		auto& regionTuple = pair.second;
		auto& scannedRegion = std::get<1>(regionTuple);
		if (!handler(*scannedRegion)) {
			break;
		}
	}
}

void MemoryCheater::throwIfNoScannedRegions() {
	if (!hasScannedRegions()) {
		PURE_THROW_EXCEPTION(pure::fmt("Have not performed the initial scan before."));
	}
}

void MemoryCheater::throwIfComparisonMethodIsInvalid(ComparisonMethod method) {
	if (!pure::EnumUtils<ComparisonMethod>::isValid(method)) {
		PURE_THROW_EXCEPTION(pure::fmt("Invalid ComparisonMethod: [{0}].", static_cast<int>(method)));
	}
	if (!hasScannedRegions()) {
		throwIfOldValueIsRequired(method);
	}
}

void MemoryCheater::throwIfValueTypeFlagOfScanArgumentsIsZero(const MemoryValueTypeFlag& flag) {
	if (!flag.isMarkedForAny()) {
		PURE_THROW_EXCEPTION(pure::fmt("Value type flag of scan arguments is zero."));
	}
	IEngineConfig& config = _engineConfig;
	MemoryValueTypeFlag filterFalg = config.getScannedValueTypeFlag();
	filterFalg.setFlag(filterFalg.getFlag() & flag.getFlag());
	if (!filterFalg.isMarkedForAny()) {
		PURE_THROW_EXCEPTION(pure::fmt(
			"The type of scanned value for engine does NOT match to your value type of scan arguments."));
	}
}

void MemoryCheater::throwIfOldValueIsRequired(ComparisonMethod method) {
	if (ComparisonMethodUtils::isAnOldValueRequiredMethod(method)) {
		PURE_THROW_EXCEPTION(pure::fmt("Initial scan can NOT use an old-value-required comparison method[{0}].",
			static_cast<int>(method)));
	}
}

void MemoryCheater::throwIfThatRegionIsNotFound(region_id_t id) {
	PURE_THROW_EXCEPTION(pure::fmt("Region[{0}] is not found.", id));
}

} /* END of namespace */
