#ifndef __MemoryCheater_H__
#define __MemoryCheater_H__

#include <memory>
#include <map>
#include <tuple>
#include <functional>

#include <cheatboy/engine/cheater/IMemoryCheater.h>
#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>

namespace cboy {
enum class ComparisonMethod;
class IEngineConfig;
class IProcessAttacher;
class IMemoryEditorManager;
class IMemoryRegionLoader;
class MemoryRegion;
class ScannedRegion;
class RegionScanner;
class RegionMatchesEditor;

class MemoryCheater: public IMemoryCheater {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(MemoryCheater);
private:
	typedef std::function<bool(ScannedRegion&)> ScannedRegionHandler;
private:
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef std::map<region_id_t, std::tuple<std::unique_ptr<MemoryRegion>, std::unique_ptr<ScannedRegion>>> RegionMap;
private:
	std::reference_wrapper<IEngineConfig> _engineConfig;
	std::reference_wrapper<IProcessAttacher> _processAttacher;
	std::reference_wrapper<IMemoryEditorManager> _editorManager;
	std::reference_wrapper<IMemoryRegionLoader> _regionLoader;
private:
	std::unique_ptr<RegionScanner> _regionScanner;
	std::unique_ptr<RegionMatchesEditor> _regionMatchesEditor;
	bool _isScanned;
	RegionMap _regions;
	size_t _maxSizedRegionSize;
	size_t _maxNumOfMatchedBytesOfMatchedByteBuffer;
	size_t _numOfMatchededHeaders;
	size_t _numOfMatchedBytes;
	size_t _numOfMatchedValues;

public:
	MemoryCheater(
		IEngineConfig& engineConfig,
		IProcessAttacher& processAttacher,
		IMemoryEditorManager& editorManager,
		IMemoryRegionLoader& regionLoader);
	virtual ~MemoryCheater() noexcept;
public:
	virtual bool hasLoadedRegions() const override;
	virtual size_t getNumOfLoadedRegions() const override;
	virtual void getLoadedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) override;
	virtual void getLoadedRegions(ILoadedRegionHandler& handler) override;
	virtual void loadRegions() override;
	virtual bool hasScannedRegions() const override;
	virtual size_t getNumOfScannedRegions() const override;
	virtual void getScannedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) override;
	virtual void getScannedRegions(IScannedRegionHandler& handler) override;
	virtual void deleteRegions() override;
public:
	virtual size_t getNumOfCurrentMatchedValues() const override;
	virtual void scan(ScanArguments& args) override;
	virtual void getMatchedValues(std::vector<MatchedValue>& values) override;
	virtual void iterateOverMatchedValues(IMatchedValueHandler& handler) override;
	virtual void editMatchedValue(EditableMatchedValue& value) override;
private:
	void recordMetaData(ScannedRegion& region);
	void clearMetaData();
	void removeRegions(std::vector<region_id_t>& removedRegionIDs);
	void iterateOverScannedRegions(ScannedRegionHandler& handler);
private:
	void throwIfNoScannedRegions();
	void throwIfComparisonMethodIsInvalid(ComparisonMethod method);
	void throwIfValueTypeFlagOfScanArgumentsIsZero(const MemoryValueTypeFlag& flag);
	void throwIfOldValueIsRequired(ComparisonMethod method);
private:
	void throwIfThatRegionIsNotFound(region_id_t id);
};

} /* END of namespace */

#endif
