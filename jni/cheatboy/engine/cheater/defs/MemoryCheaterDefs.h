#ifndef __MemoryCheaterDefs_H__
#define __MemoryCheaterDefs_H__

#include <functional>
#include <PureMacro.h>

namespace cboy {
class IMemoryRegion;
class MatchedValue;

class MemoryCheaterDefs {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryCheaterDefs);
public:
	typedef std::function<void(IMemoryRegion&)> ILoadedRegionHandler;
	typedef ILoadedRegionHandler IScannedRegionHandler;
	typedef std::function<void(const MatchedValue&)> IMatchedValueHandler;
};

} /* END of namespace */

#endif
