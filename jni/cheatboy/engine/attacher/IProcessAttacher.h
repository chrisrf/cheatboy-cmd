#ifndef __IProcessAttacher_H__
#define __IProcessAttacher_H__

#include <stdint.h>
#include <functional>
#include <PureMacro.h>

namespace cboy {

class IProcessAttacher {
PURE_DECLARE_CLASS_AS_INTERFACE(IProcessAttacher);
public:
	virtual pid_t getAttachedProcessID() const = 0;
	virtual bool hasAttachedProcess() const = 0;
	virtual void attachProcess(pid_t pid) = 0;
	virtual void detachProcess() = 0;
	virtual void throwIfProcessIdIsInvalid(pid_t pid) const = 0;
	virtual void throwIfAlreadyAttachedToPreocess() const = 0;
	virtual void throwIfNoProcessAttached() const = 0;
	virtual void throwIfNoProcessAttached(std::function<void(void)>& cleaner) const = 0;
};

} /* END of namespace */

#endif
