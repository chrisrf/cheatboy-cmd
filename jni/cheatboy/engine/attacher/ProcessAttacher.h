#ifndef __ProcessAttacher_H__
#define __ProcessAttacher_H__

#include <stdint.h>
#include <dirent.h>
#include <memory>
#include <vector>
#include <string>

#include <cheatboy/engine/attacher/IProcessAttacher.h>

namespace cboy {

class ProcessAttacher: public IProcessAttacher {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ProcessAttacher);
private:
	pid_t _pid;
	std::vector<pid_t> _tids;

public:
	ProcessAttacher();
	virtual ~ProcessAttacher() noexcept;
public:
	virtual pid_t getAttachedProcessID() const override;
	virtual bool hasAttachedProcess() const override;
	virtual void attachProcess(pid_t pid) override;
	virtual void detachProcess() override;
	virtual void throwIfProcessIdIsInvalid(pid_t pid) const override;
	virtual void throwIfAlreadyAttachedToPreocess() const override;
	virtual void throwIfNoProcessAttached() const override;
	virtual void throwIfNoProcessAttached(std::function<void(void)>& cleaner) const override;
private:
	void sendStopSignal(pid_t pid);
	void sendContSignal(pid_t pid);
	std::vector<pid_t> loadThreadIds(pid_t pid);
	void closeDirectoryStream(DIR* dir, const std::string& taskDir);
	bool checkThreadState(pid_t pid, std::vector<pid_t>& tids, const char* state);
	void attachThreads(pid_t pid, std::vector<pid_t>& tids);
	void detachThreads(std::vector<pid_t>& tids);
	void detachThread(pid_t& tid);
};

} /* END of namespace */

#endif
