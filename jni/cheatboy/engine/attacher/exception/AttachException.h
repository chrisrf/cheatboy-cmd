#ifndef __AttachException_H__
#define __AttachException_H__

#include <purelib/exception/Exception.h>
#include <purelib/exception/ExceptionMacro.h>

namespace cboy {

PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION(AttachException);

} /* END of namespace */

#endif
