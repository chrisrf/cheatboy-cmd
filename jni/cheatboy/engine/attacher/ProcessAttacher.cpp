#include "ProcessAttacher.h"

#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <dirent.h>
#include <unistd.h>
#include <fstream>
#include <PureLib.h>

#include <cheatboy/engine/EngineConstants.h>
#include <cheatboy/engine/attacher/exception/AttachException.h>

namespace cboy {

ProcessAttacher::ProcessAttacher()
	:
	_pid(EngineConstants::NULL_PROCESS_ID) {}

ProcessAttacher::~ProcessAttacher() noexcept {
	detachProcess();
}

pid_t ProcessAttacher::getAttachedProcessID() const {
	return _pid;
}

bool ProcessAttacher::hasAttachedProcess() const {
	return (_pid != EngineConstants::NULL_PROCESS_ID);
}

void ProcessAttacher::attachProcess(pid_t pid) {
	throwIfProcessIdIsInvalid(pid);
	throwIfAlreadyAttachedToPreocess();

	try {
		sendStopSignal(pid);
		_pid = pid;

		std::vector<pid_t> tids;
		int retry = 3;
		for (int i = 0; i < retry; i++) {
			if (i != 0) {
				usleep(250000);
			}

			// Use it to test whether sending the signal will stop all threads.
			//usleep(10000000);
			tids = loadThreadIds(pid);
			if (checkThreadState(pid, tids, "T")) {
				break;
			}
		}
		attachThreads(pid, tids);
		_tids = std::move(tids);
	} catch (pure::Exception& e) {
		detachProcess();
		PURE_THROW_WRAPPED_EXCEPTION(AttachException, std::move(e));
	} catch (...) {
		detachProcess();
		PURE_THROW_DERIVED_EXCEPTION(AttachException, "Failed to attach process.");
	}
	//checkThreadState(pid, _tids, "t");
}

void ProcessAttacher::detachProcess() {
	if (!hasAttachedProcess()) { return; }

	if (!_tids.empty()) {
		detachThreads(_tids);
		_tids.clear();
	}
	sendContSignal(_pid);
	_pid = EngineConstants::NULL_PROCESS_ID;
	// TODO: Count how many zombies.
}

void ProcessAttacher::throwIfProcessIdIsInvalid(pid_t pid) const {
	if (pid < EngineConstants::MIN_PROCESS_ID) {
		PURE_THROW_EXCEPTION(pure::fmt("Process ID [{0}] should be greater than [{1}].", pid, EngineConstants::MIN_PROCESS_ID));
	}
}

void ProcessAttacher::throwIfAlreadyAttachedToPreocess() const {
	if (hasAttachedProcess()) {
		PURE_THROW_EXCEPTION(pure::fmt("Already attached to Process [{0}].", _pid));
	}
}

void ProcessAttacher::throwIfNoProcessAttached() const {
	if (!hasAttachedProcess()) {
		PURE_THROW_EXCEPTION("No process attached.");
	}
}

void ProcessAttacher::throwIfNoProcessAttached(std::function<void(void)>& cleaner) const {
	if (!hasAttachedProcess()) {
		cleaner();
		PURE_THROW_EXCEPTION("No process attached.");
	}
}

void ProcessAttacher::sendStopSignal(pid_t pid) {
	if (kill(pid, SIGSTOP) == -1) {
		PURE_THROW_EXCEPTION(pure::fmt(
			"Failed to send the signal(SIGSTOP) to ({0}).\n"
			"[{1}] {2}.", pid, errno, strerror(errno)));
	}
}

void ProcessAttacher::sendContSignal(pid_t pid) {
	if (kill(pid, SIGCONT) == -1) {
		PURE_D_LOG_AS_EXCEPTION(pure::fmt(
			"Failed to send the signal(SIGCONT) to ({0}).\n"
			"[{1}] {2}.", pid, errno, strerror(errno)));
	}
}

std::vector<pid_t> ProcessAttacher::loadThreadIds(pid_t pid) {
	std::vector<pid_t> tids;

	std::string taskDir = pure::fmt("/proc/{0}/task", pid);
	DIR* dir = opendir(taskDir.c_str());
	if (dir == NULL) {
		PURE_THROW_EXCEPTION(pure::fmt(
			"Failed to open directory [{0}].\n"
			"[{1}] {2}.", taskDir, errno, strerror(errno)));
	}

	try {
		// TODO: Use readdir_r instead.
		struct dirent* ent = NULL;
		pure::NumberConverter converter;
		errno = 0;
		while ((ent = readdir(dir)) != NULL) {
			pid_t tid = EngineConstants::NULL_THREAD_ID;
			if (!converter.convert(ent->d_name, tid)) {
				continue;
			}
			tids.push_back(tid);
		}
		if (errno != 0) {
			PURE_THROW_EXCEPTION(pure::fmt(
				"Failed to read directory [{0}].\n"
				"[{1}] {2}.", taskDir, errno, strerror(errno)));
		}
	} catch (...) {
		closeDirectoryStream(dir, taskDir);
		throw;
	}
	closeDirectoryStream(dir, taskDir);

	for (pid_t& tid : tids) {
		PURE_D_LOG_LN("ProcessID[{0}], ThreadID[{1}]", pid, tid);
	}
	return tids;
}

void ProcessAttacher::closeDirectoryStream(DIR* dir, const std::string& taskDir) {
	if (closedir(dir) == -1) {
		PURE_D_LOG_AS_EXCEPTION(pure::fmt(
			"Failed to close directory [{0}].\n"
			"[{1}] {2}.", taskDir, errno, strerror(errno)));
	}
}

bool ProcessAttacher::checkThreadState(pid_t pid, std::vector<pid_t>& tids, const char* expectedStat) {
	std::string dummy;
	std::string stat;
	for (pid_t& tid : tids) {
		std::string taskStatusFile = pure::fmt("/proc/{0}/task/{1}/status", pid, tid);
		std::ifstream inputFile;
		inputFile.open(taskStatusFile);
		if (!inputFile) {
			// XXX: Sending the signal will stop all threads.
			// So the threads will NOT be destroyed after loadThreadIds(...).
			PURE_THROW_EXCEPTION(pure::fmt(
				"Failed to open [{0}].\n"
				"[{1}] {2}.", taskStatusFile, errno, strerror(errno)));
		}

		std::string errorMsg(pure::fmt("Failed to parse [{0}].", taskStatusFile));

		// TODO: Check What will happen if task name is string of unicode.
		if (!(std::getline(inputFile, dummy))) {
			PURE_THROW_EXCEPTION(std::move(errorMsg));
		}
		PURE_D_LOG_LN("dummy 1 [{0}]", dummy);

		if (!(inputFile >> dummy)) {
			PURE_THROW_EXCEPTION(std::move(errorMsg));
		}
		PURE_D_LOG_LN("dummy 2 [{0}]", dummy);

		stat.clear();
		if (!(inputFile >> stat)) {
			PURE_THROW_EXCEPTION(pure::fmt(
				"Failed to parse the state of task in [{0}].", taskStatusFile));
		}
		PURE_D_LOG_LN("taskStat [{0}]", stat);

		if (stat.compare(expectedStat) != 0) {
			return false;
		}
	}
	return true;
}

void ProcessAttacher::attachThreads(pid_t pid, std::vector<pid_t>& tids) {
	std::vector<pid_t> attachedThreadIds;
	for (pid_t& tid : tids) {
		// Attach to the target task, which should cause a SIGSTOP.
		if (ptrace(PTRACE_ATTACH, tid, NULL, NULL) == -1L) {
			// XXX: Sending the signal will stop all threads.
			// So the threads will NOT be destroyed after loadThreadIds(...).
			detachThreads(attachedThreadIds);
			PURE_THROW_EXCEPTION(pure::fmt(
				"Failed to attach to {0}.\n"
				"[{1}] {2}.", tid, errno, strerror(errno)));
		}

		try {
			attachedThreadIds.push_back(tid);
		} catch (...) {
			detachThread(tid);
			detachThreads(attachedThreadIds);
			PURE_THROW_EXCEPTION(pure::fmt(
				"Failed to push id of task {0} to list.\n"
				"[{1}] {2}.", tid, errno, strerror(errno)));
		}
	}

	// Wait for the SIGSTOP to take place.
	int status;
	if (waitpid(pid, &status, 0) == -1 || !WIFSTOPPED(status)) {
		detachThreads(attachedThreadIds);
		PURE_THROW_EXCEPTION(pure::fmt(
			"Failed to wait for process {0} to stop.\n"
			"[{1}] {2}.", pid, errno, strerror(errno)));
	}
}

void ProcessAttacher::detachThreads(std::vector<pid_t>& tids) {
	for (pid_t& tid : tids) {
		detachThread(tid);
	}
}

void ProcessAttacher::detachThread(pid_t& tid) {
	if (ptrace(PTRACE_DETACH, tid, NULL, NULL) == -1L) {
		PURE_D_LOG_AS_EXCEPTION(pure::fmt(
			"Failed to detach from {0}.\n"
			"[{1}] {2}.", tid, errno, strerror(errno)));
	}
}

} /* END of namespace */
