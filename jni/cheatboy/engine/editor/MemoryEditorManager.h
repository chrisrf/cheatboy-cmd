#ifndef __MemoryEditorManager_H__
#define __MemoryEditorManager_H__

#include <set>
#include <memory>

#include <cheatboy/engine/editor/IMemoryEditorManager.h>

namespace cboy {
class IEngineConfig;
class IProcessAttacher;

class MemoryEditorManager: public IMemoryEditorManager {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(MemoryEditorManager);
private:
	std::reference_wrapper<IEngineConfig> _engineConfig;
	std::reference_wrapper<IProcessAttacher> _processAttacher;
	std::unique_ptr<IMemoryEditor> _defaultEditor;
	std::unique_ptr<IMemoryEditor> _streamingEditor;
	std::set<std::string> _streamableRegions;

public:
	MemoryEditorManager(IEngineConfig& engineConfig, IProcessAttacher& processAttacher);
	virtual ~MemoryEditorManager() noexcept;
public:
	virtual IMemoryEditor& getCurrentEditor() override;
	virtual IMemoryEditor& getDefaultEditor() override;
	virtual IMemoryEditor& getStreamingEditor() override;
	virtual IMemoryEditor& getSuitableEditor(const std::string& regionName) override;
	virtual IMemoryReader& chooseSuitableReader(const std::string& regionName, IMemoryStreamReader& streamReader) override;
public:
	bool isStreamableRegion(const std::string& regionName);
	void addStreamableRegion(const std::string& regionName);
};

} /* END of namespace */

#endif
