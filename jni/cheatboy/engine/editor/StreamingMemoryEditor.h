#ifndef __StreamingMemoryEditor_H__
#define __StreamingMemoryEditor_H__

#include <functional>

#include <cheatboy/engine/editor/IMemoryEditor.h>

namespace cboy {
class IProcessAttacher;
class IMemoryStreamReader;

class StreamingMemoryEditor: public IMemoryEditor {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(StreamingMemoryEditor);
private:
	std::reference_wrapper<IProcessAttacher> _processAttacher;

public:
	StreamingMemoryEditor(IProcessAttacher& processAttacher);
	virtual ~StreamingMemoryEditor() noexcept;
public:
	virtual void readValue(ReadValue& readValue) override;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) override;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) override;
	virtual void writeValue(WriteValue& writeValue) override;
	virtual void writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size) override;
	virtual void writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size, size_t& nWrite) override;
	virtual std::unique_ptr<IMemoryStreamReader> createMemoryStreamReader();
};

} /* END of namespace */

#endif
