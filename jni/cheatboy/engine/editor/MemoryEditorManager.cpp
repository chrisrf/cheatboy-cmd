#include "MemoryEditorManager.h"

#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/config/IEngineConfig.h>
#include <cheatboy/engine/editor/DefaultMemoryEditor.h>
#include <cheatboy/engine/editor/StreamingMemoryEditor.h>
#include <cheatboy/engine/editor/reader/MemoryReaderMode.h>
#include <cheatboy/engine/editor/reader/IMemoryReader.h>
#include <cheatboy/engine/editor/reader/IMemoryStreamReader.h>
#include <cheatboy/engine/region/MemoryRegionNames.h>

namespace cboy {

MemoryEditorManager::MemoryEditorManager(IEngineConfig& engineConfig, IProcessAttacher& processAttacher)
	:
	_engineConfig(engineConfig),
	_processAttacher(processAttacher),
	_defaultEditor(std::make_unique<DefaultMemoryEditor>(_processAttacher)),
	_streamingEditor(std::make_unique<StreamingMemoryEditor>(_processAttacher)),
	_streamableRegions() {

	addStreamableRegion(MemoryRegionNames::ANDROID_ART_HEAP_NAME);
}

MemoryEditorManager::~MemoryEditorManager() noexcept {}

IMemoryEditor& MemoryEditorManager::getCurrentEditor() {
	IEngineConfig& engineConfig = _engineConfig;
	if (engineConfig.getMemoryReaderMode() == MemoryReaderMode::DEFAULT) {
		return getDefaultEditor();
	}
	return getStreamingEditor();
}

IMemoryEditor& MemoryEditorManager::getDefaultEditor() {
	return *_defaultEditor;
}

IMemoryEditor& MemoryEditorManager::getStreamingEditor() {
	return *_streamingEditor;
}

IMemoryEditor& MemoryEditorManager::getSuitableEditor(const std::string& regionName) {
	IEngineConfig& engineConfig = _engineConfig;
	if (engineConfig.isSmartReaderModeEnabled()) {
		if (!isStreamableRegion(regionName)) {
			return getDefaultEditor();
		}
		return getStreamingEditor();
	}
	return getCurrentEditor();
}

IMemoryReader& MemoryEditorManager::chooseSuitableReader(const std::string& regionName, IMemoryStreamReader& streamReader) {
	IEngineConfig& engineConfig = _engineConfig;
	if (engineConfig.isSmartReaderModeEnabled()) {
		if (!isStreamableRegion(regionName)) {
			return getDefaultEditor();
		}
		return streamReader;
	} else {
		if (engineConfig.getMemoryReaderMode() == MemoryReaderMode::DEFAULT) {
			return getDefaultEditor();
		}
		return streamReader;
	}
}

bool MemoryEditorManager::isStreamableRegion(const std::string& regionName) {
	if (_streamableRegions.count(regionName) == 0) { return false; }
	return true;
}

void MemoryEditorManager::addStreamableRegion(const std::string& regionName) {
	typedef std::set<std::string> StreamableRegionSet;
	std::pair<StreamableRegionSet::iterator, bool> res = _streamableRegions.insert(regionName);
	if (!res.second) {
		PURE_THROW_EXCEPTION(pure::fmt("Insert a duplicate streamable region [{0}].", regionName));
	}
}

} /* END of namespace */
