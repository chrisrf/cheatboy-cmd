#ifndef __MemoryReaderMode_H__
#define __MemoryReaderMode_H__

namespace cboy {

enum class MemoryReaderMode {
	ENUM_MIN = 0,
	// =======
	DEFAULT = 0,
	STREAMING = 1,
	// =======
	ENUM_MAX = STREAMING,
	ENUM_NUMS = 2
};

} /* END of namespace */

#endif
