#include "DelegatedMemoryReader.h"

#include <cheatboy/engine/value/ReadValue.h>
#include <cheatboy/engine/editor/reader/IMemoryReader.h>
#include <cheatboy/engine/editor/exception/ReadMemoryException.h>

namespace cboy {

DelegatedMemoryReader::DelegatedMemoryReader(IMemoryReader& memoryReader)
	:
	_memoryReader(memoryReader) {}

DelegatedMemoryReader::~DelegatedMemoryReader() noexcept {}

bool DelegatedMemoryReader::isOpened() const {
	return true;
}

void DelegatedMemoryReader::openStream() {}

void DelegatedMemoryReader::readValue(ReadValue& readValue) {
	IMemoryReader& memoryReader = _memoryReader;
	size_t nRead = 0;
	memoryReader.readMemory(readValue.getRemoteAddress(), readValue.getBuffer(), readValue.getSize(), nRead);
}

void DelegatedMemoryReader::readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) {
	IMemoryReader& memoryReader = _memoryReader;
	size_t nRead = 0;
	memoryReader.readMemory(remoteAddress, readBuffer, size, nRead);
}

void DelegatedMemoryReader::readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) {
	IMemoryReader& memoryReader = _memoryReader;
	memoryReader.readMemory(remoteAddress, readBuffer, size, nRead);
}

void DelegatedMemoryReader::closeStream() {}

} /* END of namespace */
