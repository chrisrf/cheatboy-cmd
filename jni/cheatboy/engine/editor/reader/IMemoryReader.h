#ifndef __IMemoryReader_H__
#define __IMemoryReader_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {
class ReadValue;

class IMemoryReader {
PURE_DECLARE_CLASS_AS_INTERFACE(IMemoryReader);
public:
	virtual void readValue(ReadValue& readValue) = 0;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) = 0;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) = 0;
};

} /* END of namespace */

#endif
