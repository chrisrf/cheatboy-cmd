/* @author Chris Chao */
#include "StreamingMemoryReader.h"

// XXX: Compile your programs with "gcc -D_FILE_OFFSET_BITS=64".
//    This forces all file access calls to use the 64 bit variants.
//    For Android, check this url: https://code.google.com/p/android/issues/detail?id=64613

// XXX: Enable the lseek64. We do not need to define _LARGEFILE64_SOURCE on Android.
//#define _LARGEFILE64_SOURCE

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <cerrno>
#include <limits>
#include <PureLib.h>

#include <cheatboy/engine/attacher/IProcessAttacher.h>
#include <cheatboy/engine/editor/IMemoryEditor.h>
#include <cheatboy/engine/editor/exception/ReadMemoryException.h>
#include <cheatboy/engine/value/ReadValue.h>

namespace cboy {

StreamingMemoryReader::StreamingMemoryReader(IProcessAttacher& processAttacher, IMemoryEditor& memoryEditor)
	:
	_processAttacher(processAttacher), _memoryEditor(memoryEditor),
	_cleaner(std::bind(&StreamingMemoryReader::closeStream, this)),
	_memFilePath(), _memFileFD(-1) {}

StreamingMemoryReader::~StreamingMemoryReader() noexcept {
	closeStream();
	//PURE_D_LOG_LN("~StreamingMemoryReader()");
}

bool StreamingMemoryReader::isOpened() const {
	return (_memFileFD != -1);
}

void StreamingMemoryReader::openStream() {
	IProcessAttacher& attacher = _processAttacher;
	attacher.throwIfNoProcessAttached();
	if (isOpened()) { return; }

	// Attempt to open the file.
	std::string memFilePath = pure::fmt("/proc/{0}/mem", attacher.getAttachedProcessID());
	// XXX: Enable the lseek64. Use open(...) with O_LARGEFILE flag.
	int memFileFD = ::open(memFilePath.c_str(), O_RDONLY | O_LARGEFILE);
	//int memFileFD = ::open(memFilePath.c_str(), O_RDONLY);
	if (memFileFD == -1) {
		PURE_THROW_DERIVED_EXCEPTION(ReadMemoryException, pure::fmt(
			"Failed to open [{0}].\n"
			"[{1}] {2}.", memFilePath, errno, ::strerror(errno)));
	}
	_memFileFD = memFileFD;
	_memFilePath = std::move(memFilePath);
}

void StreamingMemoryReader::readValue(ReadValue& readValue) {
	readMemory(readValue.getRemoteAddress(), readValue.getBuffer(), readValue.getSize());
}

void StreamingMemoryReader::readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) {
	size_t nRead = 0;
	readMemory(remoteAddress, readBuffer, size, nRead);
}

void StreamingMemoryReader::readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) {
	nRead = 0;
	IProcessAttacher& attacher = _processAttacher;
	attacher.throwIfNoProcessAttached(_cleaner);
	throwIfStreamIsNotOpened();

	// XXX: Use size 1 to test paging capability.
	//const size_t BUFFER_PAGE_SIZE = std::numeric_limits<ssize_t>::max();
	const size_t BUFFER_PAGE_SIZE = 1 * pure::ByteUnit::GB;

	// XXX: Enable the lseek64.
	if (::lseek64(_memFileFD, remoteAddress, SEEK_SET) == -1) {
		PURE_THROW_DERIVED_EXCEPTION(ReadMemoryException, pure::fmt(
			"Failed to lseek64 [{0}].\n"
			"[{1}] {2}.", _memFilePath, errno, ::strerror(errno)));
	}
	// XXX: Not use the lseek.
	//		Because off_t is a signed integer type and 32 bits on 32-bit platform,
	//		it is only useful when a file is small than 2GB.
//	if (::lseek(_memFileFD, remoteAddress, SEEK_SET) == -1) {
//		PURE_THROW_DERIVED_EXCEPTION(ReadMemoryException, pure::fmt(
//			"Failed to lseek [{0}].\n"
//			"[{1}] {2}.", _memFilePath, errno, ::strerror(errno)));
//	}

	// XXX: Avoid that size is greater than INT32_MAX or INT64_MAX.
	// ssize_t is signed, but size_t is unsigned.
	for (int iRound = 1; nRead < size; ++iRound) {
		const size_t unread = size - nRead;
		const size_t needRead = (unread <= BUFFER_PAGE_SIZE) ? unread : BUFFER_PAGE_SIZE;

		errno = 0;
		//ssize_t temp = ::pread(_memFileFD, readBuffer+nRead, needRead, remoteAddress+nRead);
		ssize_t temp = ::read(_memFileFD, readBuffer+nRead, needRead);
		PURE_D_LOG_LN("Reading the memory: [{0}] [{1}] [{2}] {3}", iRound, temp, errno, ::strerror(errno));
		if (temp == -1) {
			// XXX: When you set up a signal handler for a process (by using signal(...), sigaction(...), ...),
			//		if the process is interrupted by a signal handler (catch a signal):
			//
			//		1) The blocking call will fail with the errno(EINTR) after the signal handler returns.
			//
			//		2) If the signal handler was established using the SA_RESTART flag by sigaction(...),
			//		the blocking call "MAY" be automatically restarted after the signal handler returns,
			//
			//		3) Some blocking system calls will always fail with the errno(EINTR), regardless of the use of SA_RESTART.
			// XXX: On Linux, even in the absence of signal handlers, certain blocking interfaces
			//		can fail with the errno(EINTR) after the process is stopped by one of the stop signals
			//		and then resumed via SIGCONT.
			if (errno == EINTR) {
				PURE_D_LOG_LN("Interrupt occurred while reading the memory: [{0}] [{1}] [{2}] {3}", iRound, temp, errno, ::strerror(errno));
				continue;
			}
			// XXX: If you read memory out of range, "read" system call returns -1, and "errno" is set to "EIO (5)".
			// But those bytes in the valid range is read successfully by previous "read" system call.
			void* addr = pure::PointerUtils::toPointer(remoteAddress);
			if (nRead == 0) {
				PURE_THROW_DERIVED_EXCEPTION(ReadMemoryException, pure::fmt(
					"Failed to read [{0}] at [{1}].\n"
					"[{2}] {3}.", addr, _memFilePath, errno, ::strerror(errno)));
			} else {
				PURE_THROW_DERIVED_EXCEPTION(PartiallyReadMemoryException, pure::fmt(
					"Failed to read [{0}] at [{1}].\n"
					"(already read [{2}] bytes)\n"
					"[{3}] {4}.", addr, _memFilePath, nRead, errno, ::strerror(errno)));
			}
		}
		nRead += temp;
	}
}

void StreamingMemoryReader::closeStream() {
	if (!isOpened()) { return; }

	if (::close(_memFileFD) == -1) {
		PURE_D_LOG_AS_EXCEPTION(pure::fmt(
			"Failed to close [{0}].\n"
			"[{1}] {2}.", _memFilePath, errno, strerror(errno)));
	}
	_memFileFD = -1;
	_memFilePath.clear();
}

void StreamingMemoryReader::throwIfStreamIsNotOpened() const {
	if (!isOpened()) {
		PURE_THROW_EXCEPTION(pure::fmt("Memory stream is not opened."));
	}
}

} /* END of namespace */
