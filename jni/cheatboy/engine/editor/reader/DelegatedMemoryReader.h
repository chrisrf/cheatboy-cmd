#ifndef __DelegatedMemoryReader_H__
#define __DelegatedMemoryReader_H__

#include <functional>

#include <cheatboy/engine/editor/reader/IMemoryStreamReader.h>

namespace cboy {
class IMemoryReader;

class DelegatedMemoryReader: public IMemoryStreamReader {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(DelegatedMemoryReader);
private:
	std::reference_wrapper<IMemoryReader> _memoryReader;

public:
	DelegatedMemoryReader(IMemoryReader& memoryReader);
	virtual ~DelegatedMemoryReader() noexcept;
public:
	virtual bool isOpened() const override;
	virtual void openStream() override;
	virtual void readValue(ReadValue& readValue) override;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) override;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) override;
	virtual void closeStream() override;
};

} /* END of namespace */

#endif
