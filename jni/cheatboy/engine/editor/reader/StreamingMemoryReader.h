/* @author Chris Chao */
#pragma once

#include <functional>
#include <string>

#include <cheatboy/engine/editor/reader/IMemoryStreamReader.h>

namespace cboy {
class IProcessAttacher;
class IMemoryEditor;

class StreamingMemoryReader: public IMemoryStreamReader {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(StreamingMemoryReader);
private:
	std::reference_wrapper<IProcessAttacher> _processAttacher;
	std::reference_wrapper<IMemoryEditor> _memoryEditor;
	std::function<void(void)> _cleaner;
	std::string _memFilePath;
	int _memFileFD;

public:
	StreamingMemoryReader(IProcessAttacher& processAttacher, IMemoryEditor& memoryEditor);
	virtual ~StreamingMemoryReader() noexcept;
public:
	virtual bool isOpened() const override;
	virtual void openStream() override;
	virtual void readValue(ReadValue& readValue) override;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) override;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) override;
	virtual void closeStream() override;
private:
	void throwIfStreamIsNotOpened() const;
};

} /* END of namespace */
