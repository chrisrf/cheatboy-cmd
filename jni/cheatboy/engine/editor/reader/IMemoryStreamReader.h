#ifndef __IMemoryStreamReader_H__
#define __IMemoryStreamReader_H__

#include <cheatboy/engine/editor/reader/IMemoryReader.h>

namespace cboy {

class IMemoryStreamReader: public IMemoryReader {
PURE_DECLARE_CLASS_AS_INTERFACE(IMemoryStreamReader);
public:
	virtual bool isOpened() const = 0;
	virtual void openStream() = 0;
	virtual void closeStream() = 0;
};

} /* END of namespace */

#endif
