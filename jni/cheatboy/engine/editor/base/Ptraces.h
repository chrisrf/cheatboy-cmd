#ifndef __Ptraces_H__
#define __Ptraces_H__

#include <sys/types.h>
#include <PureMacro.h>

namespace cboy {

class Ptraces {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(Ptraces);
public:
	static bool peekDataSilently(const pid_t pid, const uintptr_t remoteAddress, uint8_t* readBuffer);
	static void peekData(const pid_t pid, const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t nRead);
	static bool pokeDataSilently(const pid_t pid, const uintptr_t remoteAddress, uint8_t* writeBuffer);
	static void pokeData(const pid_t pid, const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t nWrite);
};

} /* END of namespace */

#endif
