#include "Ptraces.h"

#include <sys/ptrace.h>
#include <errno.h>
#include <PureLib.h>

#include <cheatboy/engine/editor/exception/ReadMemoryException.h>
#include <cheatboy/engine/editor/exception/WriteMemoryException.h>

/**
 * Test Environment:
 * 	android-ndk-r10c-linux-x86_64.bin
 * 	genymotion-2.4.0_x64.bin
 * 	android (4.2.2)
 *
 * Note: (Bug #1)
 * 	I found that ptrace(PTRACE_PEEKDATA, ...) has a bug on Android platform.
 * 	If you try to peek data which is equal to "-1", then the return value of ptrace is "-1",
 * 	but "errno" is set to "EPERM (1)", "errno" is NOT set to "0" as the man page said.
 *
 * Note: (Bug #2)
 * 	I found that ptrace(PTRACE_PEEKDATA, ...) has a bug on Android platform.
 * 	If you try to peek memory which is out of bound or not readable,
 * 	then "errno" is set to "EPERM (1)", "errno" is NOT set to "EFAULT (14)" or "EIO (5)" as the man page said.
 */

namespace cboy {

bool Ptraces::peekDataSilently(const pid_t pid, const uintptr_t remoteAddress, uint8_t* readBuffer) {
	errno = 0;
	void* addr = pure::PointerUtils::toPointer(remoteAddress);
	long readValue = ptrace(PTRACE_PEEKDATA, pid, addr, NULL);// XXX: It has bugs. See (Bug #1 and Bud #2)
	if ((readValue == -1L) && (errno != 0)) {
		return false;
	}
	memcpy(readBuffer, &readValue, sizeof(readValue));
	return true;
}

void Ptraces::peekData(const pid_t pid, const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t nRead) {
	if (!peekDataSilently(pid, remoteAddress, readBuffer)) {
		void* addr = pure::PointerUtils::toPointer(remoteAddress);
		if (nRead == 0) {
			PURE_THROW_DERIVED_EXCEPTION(ReadMemoryException, pure::fmt(
				"Failed to read [{0}].\n"
				"[{1}] {2}.", addr, errno, strerror(errno)));
		} else {
			PURE_THROW_DERIVED_EXCEPTION(PartiallyReadMemoryException, pure::fmt(
				"Failed to read [{0}].\n"
				"(already read [{1}] bytes)\n"
				"[{2}] {3}.", addr, nRead, errno, strerror(errno)));
		}
	}
}

bool Ptraces::pokeDataSilently(const pid_t pid, const uintptr_t remoteAddress, uint8_t* writeBuffer) {
	errno = 0;
	void* addr = pure::PointerUtils::toPointer(remoteAddress);
	long writeValue = *reinterpret_cast<long*>(writeBuffer);
	long ret = ptrace(PTRACE_POKEDATA, pid, addr, writeValue);
	if (ret == -1L) {
		return false;
	}
	return true;
}

void Ptraces::pokeData(const pid_t pid, const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t nWrite) {
	if (!pokeDataSilently(pid, remoteAddress, writeBuffer)) {
		void* addr = pure::PointerUtils::toPointer(remoteAddress);
		if (nWrite == 0) {
			PURE_THROW_DERIVED_EXCEPTION(WriteMemoryException, pure::fmt(
				"Failed to write [{0}].\n"
				"[{1}] {2}.", addr, errno, strerror(errno)));
		} else {
			PURE_THROW_DERIVED_EXCEPTION(PartiallyWriteMemoryException, pure::fmt(
				"Failed to write [{0}].\n"
				"(already write [{1}] bytes)\n"
				"[{2}] {3}.", addr, nWrite, errno, strerror(errno)));
		}
	}
}

} /* END of namespace */
