#ifndef __IMemoryEditorManager_H__
#define __IMemoryEditorManager_H__

#include <string>
#include <PureMacro.h>

namespace cboy {
class IMemoryEditor;
class IMemoryReader;
class IMemoryStreamReader;

class IMemoryEditorManager {
PURE_DECLARE_CLASS_AS_INTERFACE(IMemoryEditorManager);
public:
	virtual IMemoryEditor& getCurrentEditor() = 0;
	virtual IMemoryEditor& getDefaultEditor() = 0;
	virtual IMemoryEditor& getStreamingEditor() = 0;
	virtual IMemoryEditor& getSuitableEditor(const std::string& regionName) = 0;
	virtual IMemoryReader& chooseSuitableReader(const std::string& regionName, IMemoryStreamReader& streamReader) = 0;
};

} /* END of namespace */

#endif
