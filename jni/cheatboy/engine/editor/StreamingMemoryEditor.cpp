#include "StreamingMemoryEditor.h"

#include <assert.h>
#include <errno.h>
#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/attacher/IProcessAttacher.h>
#include <cheatboy/engine/value/ReadValue.h>
#include <cheatboy/engine/value/WriteValue.h>
#include <cheatboy/engine/editor/base/Ptraces.h>
#include <cheatboy/engine/editor/exception/ReadMemoryException.h>
#include <cheatboy/engine/editor/exception/WriteMemoryException.h>
#include <cheatboy/engine/editor/reader/StreamingMemoryReader.h>

namespace cboy {

StreamingMemoryEditor::StreamingMemoryEditor(IProcessAttacher& processAttacher)
	:
	_processAttacher(processAttacher) {}

StreamingMemoryEditor::~StreamingMemoryEditor() noexcept {}

void StreamingMemoryEditor::readValue(ReadValue& readValue) {
	size_t nRead = 0;
	readMemory(readValue.getRemoteAddress(), readValue.getBuffer(), readValue.getSize(), nRead);
}

void StreamingMemoryEditor::readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) {
	size_t nRead = 0;
	readMemory(remoteAddress, readBuffer, size, nRead);
}

void StreamingMemoryEditor::readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) {
	nRead = 0;
	std::unique_ptr<IMemoryStreamReader> reader = createMemoryStreamReader();
	reader->openStream();
	reader->readMemory(remoteAddress, readBuffer, size, nRead);
}

void StreamingMemoryEditor::writeValue(WriteValue& writeValue) {
	size_t nRead = 0;
	writeMemory(writeValue.getRemoteAddress(), writeValue.getBuffer(), writeValue.getSize(), nRead);
}

void StreamingMemoryEditor::writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size) {
	size_t nWrite = 0;
	writeMemory(remoteAddress, writeBuffer, size, nWrite);
}

void StreamingMemoryEditor::writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size, size_t& nWrite) {
	nWrite = 0;
	IProcessAttacher& attacher = _processAttacher;
	attacher.throwIfNoProcessAttached();

	const pid_t pid = attacher.getAttachedProcessID();
	const size_t wordSize = sizeof(long);
	for (nWrite = 0; (nWrite + wordSize) <= size; nWrite += wordSize) {
		Ptraces::pokeData(pid, (remoteAddress + nWrite), (writeBuffer + nWrite), nWrite);
	}

	// Deal with partial hit problem.
	if (size - nWrite > 0) {
		// Possible number of left bytes: 1, 2, ..., sizeof(long)-1
		PURE_D_LOG_LN("Deal with partial write problem");
		if (size >= wordSize) {
			PURE_D_LOG_LN("Re-write last {0} bytes.", wordSize);
			// Re-write last sizeof(long) bytes of the buffer.
			Ptraces::pokeData(pid, (remoteAddress + size - wordSize), (writeBuffer + size - wordSize), nWrite);
			nWrite = size;
		} else {
			assert(nWrite == 0);
			PURE_D_LOG_LN("Try to read and shift, then write.");

			uint8_t tempBuf[wordSize];
			size_t nRead = 0;
			size_t nShift = 0;
			std::unique_ptr<IMemoryStreamReader> reader = createMemoryStreamReader();
			try {
				reader->openStream();
				reader->readMemory(remoteAddress, tempBuf, wordSize, nRead);
			} catch (PartiallyReadMemoryException& e) {
				if (nRead < size) {
					// It is a real exception.
					PURE_THROW_WRAPPED_EXCEPTION(WriteMemoryException, std::move(e));
				}
				// Use write [last address - 1] i8 [value] to check (nRead == size) condition.
				// Use write [last address - 2] i8 [value] to check (nRead > size) condition.
				// It is an out-of-range read operation, so try to shift.
				nShift = wordSize - nRead;
				PURE_D_LOG_LN("Failed to read (already read [{0}] bytes), need a shift ({1}).", nRead, nShift);
				try {
					reader->readMemory(remoteAddress - nShift, tempBuf, wordSize);
				} catch (ReadMemoryException& exception) {
					PURE_THROW_WRAPPED_EXCEPTION(WriteMemoryException, std::move(exception));
				}
			} catch (ReadMemoryException& e) {
				PURE_THROW_WRAPPED_EXCEPTION(WriteMemoryException, std::move(e));
			}
			PURE_D_LOG_LN("Write with a shift ({0})", nShift);
			memcpy(tempBuf + nShift, writeBuffer, size);
			Ptraces::pokeData(pid, (remoteAddress - nShift), (tempBuf), nWrite);
			nWrite = size;
		}
	}
}

std::unique_ptr<IMemoryStreamReader> StreamingMemoryEditor::createMemoryStreamReader() {
	return std::make_unique<StreamingMemoryReader>(_processAttacher.get(), *this);
}

} /* END of namespace */
