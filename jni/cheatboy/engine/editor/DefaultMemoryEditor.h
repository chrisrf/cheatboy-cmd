#ifndef __DefaultMemoryEditor_H__
#define __DefaultMemoryEditor_H__

#include <functional>

#include <cheatboy/engine/editor/IMemoryEditor.h>

namespace cboy {
class IProcessAttacher;
class IMemoryStreamReader;

class DefaultMemoryEditor: public IMemoryEditor {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(DefaultMemoryEditor);
private:
	std::reference_wrapper<IProcessAttacher> _processAttacher;

public:
	DefaultMemoryEditor(IProcessAttacher& processAttacher);
	virtual ~DefaultMemoryEditor() noexcept;
public:
	virtual void readValue(ReadValue& readValue) override;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) override;
	virtual void readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) override;
	virtual void writeValue(WriteValue& writeValue) override;
	virtual void writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size) override;
	virtual void writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size, size_t& nWrite) override;
	virtual std::unique_ptr<IMemoryStreamReader> createMemoryStreamReader();
};

} /* END of namespace */

#endif
