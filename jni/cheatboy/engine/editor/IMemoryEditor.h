#ifndef __IMemoryEditor_H__
#define __IMemoryEditor_H__

#include <memory>

#include <cheatboy/engine/editor/reader/IMemoryReader.h>

namespace cboy {
class ReadValue;
class WriteValue;
class IMemoryStreamReader;

class IMemoryEditor: public IMemoryReader {
PURE_DECLARE_CLASS_AS_INTERFACE(IMemoryEditor);
public:
	virtual void writeValue(WriteValue& writeValue) = 0;
	virtual void writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size) = 0;
	virtual void writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size, size_t& nWrite) = 0;
	virtual std::unique_ptr<IMemoryStreamReader> createMemoryStreamReader() = 0;
};

} /* END of namespace */

#endif
