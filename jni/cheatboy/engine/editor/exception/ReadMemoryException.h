#ifndef __ReadMemoryException_H__
#define __ReadMemoryException_H__

#include <purelib/exception/Exception.h>
#include <purelib/exception/ExceptionMacro.h>

namespace cboy {

PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION(ReadMemoryException);
PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION_BASED_ON(PartiallyReadMemoryException, ReadMemoryException);

} /* END of namespace */

#endif
