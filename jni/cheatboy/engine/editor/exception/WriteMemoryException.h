#ifndef __WriteMemoryException_H__
#define __WriteMemoryException_H__

#include <purelib/exception/Exception.h>
#include <purelib/exception/ExceptionMacro.h>

namespace cboy {

PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION(WriteMemoryException);
PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION_BASED_ON(PartiallyWriteMemoryException, WriteMemoryException);

} /* END of namespace */

#endif
