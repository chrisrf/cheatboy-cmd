#include "DefaultMemoryEditor.h"

#include <assert.h>
#include <errno.h>
#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/attacher/IProcessAttacher.h>
#include <cheatboy/engine/value/ReadValue.h>
#include <cheatboy/engine/value/WriteValue.h>
#include <cheatboy/engine/editor/base/Ptraces.h>
#include <cheatboy/engine/editor/exception/ReadMemoryException.h>
#include <cheatboy/engine/editor/exception/WriteMemoryException.h>
#include <cheatboy/engine/editor/reader/DelegatedMemoryReader.h>

namespace cboy {

DefaultMemoryEditor::DefaultMemoryEditor(IProcessAttacher& processAttacher)
	:
	_processAttacher(processAttacher) {}

DefaultMemoryEditor::~DefaultMemoryEditor() noexcept {}

void DefaultMemoryEditor::readValue(ReadValue& readValue) {
	size_t nRead = 0;
	readMemory(readValue.getRemoteAddress(), readValue.getBuffer(), readValue.getSize(), nRead);
}

void DefaultMemoryEditor::readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size) {
	size_t nRead = 0;
	readMemory(remoteAddress, readBuffer, size, nRead);
}

void DefaultMemoryEditor::readMemory(const uintptr_t remoteAddress, uint8_t* readBuffer, const size_t size, size_t& nRead) {
	nRead = 0;
	IProcessAttacher& attacher = _processAttacher;
	attacher.throwIfNoProcessAttached();

	const pid_t pid = attacher.getAttachedProcessID();
	const size_t wordSize = sizeof(long);
	for (nRead = 0; (nRead + wordSize) <= size; nRead += wordSize) {
		Ptraces::peekData(pid, (remoteAddress + nRead), (readBuffer + nRead), nRead);
	}

	// Deal with partial hit problem.
	if (size - nRead > 0) {
		// Possible number of left bytes: 1, 2, ..., sizeof(long)-1
		PURE_D_LOG_LN("Deal with partial read problem.");
		if (size >= wordSize) {
			PURE_D_LOG_LN("Re-read last {0} bytes.", wordSize);
			// Re-read last sizeof(long) bytes of the buffer.
			Ptraces::peekData(pid, (remoteAddress + size - wordSize), (readBuffer + size - wordSize), nRead);
			nRead = size;
		} else {
			assert(nRead == 0);
			PURE_D_LOG_LN("Try to read and shift, then copy.");
			// We have to play with bytes.
			// (Times to retry) = (first try) + (times to shift left)
			const size_t retryTimes = 1 + (wordSize - size);
			uint8_t tempBuf[wordSize];
			size_t nShift = 0;
			bool success = false;
			for (nShift = 0; nShift < retryTimes; nShift++) {
				PURE_D_LOG_LN("Try to read with shift[{0}]", nShift);
				if (!Ptraces::peekDataSilently(pid, (remoteAddress - nShift), (tempBuf))) {
					if (errno == EIO || errno == EFAULT) {// XXX: It has a bug. See (Ptraces#Bug #2)
						// Try next shift.
						PURE_D_LOG_LN("Failed to read, need a shift. [{0}] {1}", errno, strerror(errno));
						continue;
					}
					break;
				} else {
					PURE_D_LOG_LN("Shift and read successfully.");
					// Read successfully.
					memcpy(readBuffer, tempBuf + nShift, size);
					nRead = size;
					success = true;
					break;
				}
			}
			if (!success) {
				void* addr = pure::PointerUtils::toPointer(remoteAddress);
				PURE_THROW_DERIVED_EXCEPTION(ReadMemoryException, pure::fmt(
					"Failed to read [{0}] with shift[{1}].\n"
					"[{2}] {3}.", addr, nShift, errno, strerror(errno)));
			}
		}
	}
}

void DefaultMemoryEditor::writeValue(WriteValue& writeValue) {
	size_t nWrite = 0;
	writeMemory(writeValue.getRemoteAddress(), writeValue.getBuffer(), writeValue.getSize(), nWrite);
}

void DefaultMemoryEditor::writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size) {
	size_t nWrite = 0;
	writeMemory(remoteAddress, writeBuffer, size, nWrite);
}

void DefaultMemoryEditor::writeMemory(const uintptr_t remoteAddress, uint8_t* writeBuffer, const size_t size, size_t& nWrite) {
	nWrite = 0;
	IProcessAttacher& attacher = _processAttacher;
	attacher.throwIfNoProcessAttached();

	const pid_t pid = attacher.getAttachedProcessID();
	const size_t wordSize = sizeof(long);
	for (nWrite = 0; (nWrite + wordSize) <= size; nWrite += wordSize) {
		Ptraces::pokeData(pid, (remoteAddress + nWrite), (writeBuffer + nWrite), nWrite);
	}

	// Deal with partial hit problem.
	if (size - nWrite > 0) {
		// Possible number of left bytes: 1, 2, ..., sizeof(long)-1
		PURE_D_LOG_LN("Deal with partial write problem");
		if (size >= wordSize) {
			PURE_D_LOG_LN("Re-write last {0} bytes.", wordSize);
			// Re-write last sizeof(long) bytes of the buffer.
			Ptraces::pokeData(pid, (remoteAddress + size - wordSize), (writeBuffer + size - wordSize), nWrite);
			nWrite = size;
		} else {
			assert(nWrite == 0);
			PURE_D_LOG_LN("Try to read and shift, then write.");
			// We have to play with bytes.
			// (Times to retry) = (first try) + (times to shift left)
			const size_t retryTimes = 1 + (wordSize - size);
			uint8_t tempBuf[wordSize];
			size_t nShift = 0;
			bool success = false;
			for (nShift = 0; nShift < retryTimes; nShift++) {
				PURE_D_LOG_LN("Try to read with shift[{0}]", nShift);
				if (!Ptraces::peekDataSilently(pid, (remoteAddress - nShift), (tempBuf))) {
					if (errno == EIO || errno == EFAULT) {// XXX: It has a bug. See (Ptraces#Bug #2)
						// Try next shift.
						PURE_D_LOG_LN("Failed to read, need a shift. [{0}] {1}", errno, strerror(errno));
						continue;
					}
					break;
				} else {
					PURE_D_LOG_LN("Shift and read successfully.");
					// Read successfully.
					memcpy(tempBuf + nShift, writeBuffer, size);
					Ptraces::pokeData(pid, (remoteAddress - nShift), (tempBuf), nWrite);
					nWrite = size;
					success = true;
					break;
				}
			}
			if (!success) {
				void* addr = pure::PointerUtils::toPointer(remoteAddress);
				PURE_THROW_DERIVED_EXCEPTION(WriteMemoryException, pure::fmt(
					"Failed to read [{0}] with shift[{1}].\n"
					"[{2}] {3}.", addr, nShift, errno, strerror(errno)));
			}
		}
	}
}

std::unique_ptr<IMemoryStreamReader> DefaultMemoryEditor::createMemoryStreamReader() {
	return std::make_unique<DelegatedMemoryReader>(*this);
}

} /* END of namespace */
