#include "CheatEngine.h"

#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/EngineConstants.h>
#include <cheatboy/engine/config/EngineConfig.h>
#include <cheatboy/engine/attacher/IProcessAttacher.h>
#include <cheatboy/engine/attacher/ProcessAttacher.h>
#include <cheatboy/engine/editor/IMemoryEditorManager.h>
#include <cheatboy/engine/editor/MemoryEditorManager.h>
#include <cheatboy/engine/editor/IMemoryEditor.h>
#include <cheatboy/engine/editor/reader/MemoryReaderMode.h>
#include <cheatboy/engine/region/IMemoryRegionLoader.h>
#include <cheatboy/engine/region/MemoryRegionLoader.h>
#include <cheatboy/engine/region/IMemoryRegion.h>
#include <cheatboy/engine/region/MemoryRegionLevel.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/cheater/IMemoryCheater.h>
#include <cheatboy/engine/cheater/MemoryCheater.h>
#include <cheatboy/engine/tester/IEngineTester.h>
#include <cheatboy/engine/tester/EngineTester.h>

namespace cboy {

CheatEngine::CheatEngine()
	:
	_pid(EngineConstants::NULL_PROCESS_ID),
	_engineConfig(std::make_unique<EngineConfig>()),
	_processAttacher(std::make_unique<ProcessAttacher>()),
	_editorManager(std::make_unique<MemoryEditorManager>(*_engineConfig, *_processAttacher)),
	_regionLoader(std::make_unique<MemoryRegionLoader>(*_processAttacher)),
	_memoryCheater(std::make_unique<MemoryCheater>(*_engineConfig, *_processAttacher, *_editorManager, *_regionLoader)),
	_engineTester(std::make_unique<EngineTester>(*_engineConfig, *_processAttacher, *_editorManager, *_regionLoader, *_memoryCheater)) {}

CheatEngine::~CheatEngine() noexcept {}

void CheatEngine::clean() {
	_memoryCheater->deleteRegions();
	_processAttacher->detachProcess();
	_pid = EngineConstants::NULL_PROCESS_ID;
}

bool CheatEngine::isPidSet() const {
	return (_pid != EngineConstants::NULL_PROCESS_ID);
}

pid_t CheatEngine::getPid() const {
	return _pid;
}

void CheatEngine::setPid(pid_t pid) {
	_processAttacher->throwIfProcessIdIsInvalid(pid);
	if (hasAttachedProcess() && (_processAttacher->getAttachedProcessID() != pid)) {
		clean();
	}
	_pid = pid;
}

bool CheatEngine::hasAttachedProcess() const {
	return _processAttacher->hasAttachedProcess();
}

void CheatEngine::attach() {
	throwIfPidIsNotSet();
	if (hasAttachedProcess()) { return; }
	safelyAttach();
}

void CheatEngine::detach() {
	if (!hasAttachedProcess()) { return; }
	_processAttacher->detachProcess();
}

void CheatEngine::readValue(ReadValue& readValue) {
	performAutoAttachIfNotAttached([this, &readValue] () {
		IMemoryEditor& editor = _editorManager->getCurrentEditor();
		editor.readValue(readValue);
	});
}

void CheatEngine::writeValue(WriteValue& writeValue) {
	performAutoAttachIfNotAttached([this, &writeValue] () {
		IMemoryEditor& editor = _editorManager->getCurrentEditor();
		editor.writeValue(writeValue);
	});
}

void CheatEngine::getMemoryRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<IMemoryRegion>>& regions) {
	performAutoAttachIfNotAttached([this, &level, &regions] () {
		_regionLoader->loadAsImmutableRegions(level, regions);
	});
}

void CheatEngine::getMemoryRegions(MemoryRegionLevel level, IMemoryRegionHandler& handler) {
	performAutoAttachIfNotAttached([this, &level, &handler] () {
		_regionLoader->loadAsImmutableRegions(level, handler);
	});
}

MemoryRegionLevel CheatEngine::getScannedRegionLevel() const {
	return _engineConfig->getScannedRegionLevel();
}

MemoryValueTypeFlag CheatEngine::getScannedValueTypeFlag() const {
	return _engineConfig->getScannedValueTypeFlag();
}

size_t CheatEngine::getThresholdOfListableMatchedValues() const {
	return _engineConfig->getThresholdOfListableMatchedValues();
}

MemoryReaderMode CheatEngine::getMemoryReaderMode() const {
	return _engineConfig->getMemoryReaderMode();
}

bool CheatEngine::isSmartReaderModeEnabled() const {
	return _engineConfig->isSmartReaderModeEnabled();
}

size_t CheatEngine::getReaderBufferPageSizeInMB() const {
	return _engineConfig->getReaderBufferPageSizeInMB();
}

void CheatEngine::setScannedRegionLevel(MemoryRegionLevel level) {
	_engineConfig->setScannedRegionLevel(level);
}

void CheatEngine::setScannedValueTypeFlag(const MemoryValueTypeFlag& flag) {
	_engineConfig->setScannedValueTypeFlag(flag);
}

void CheatEngine::setThresholdOfListableMatchedValues(size_t threshold) {
	_engineConfig->setThresholdOfListableMatchedValues(threshold);
}

void CheatEngine::setMemoryReaderMode(MemoryReaderMode mode) {
	_engineConfig->setMemoryReaderMode(mode);
}

void CheatEngine::setSmartReaderModeEnabled(bool enabled) {
	_engineConfig->setSmartReaderModeEnabled(enabled);
}

void CheatEngine::setReaderBufferPageSizeInMB(size_t sizeInMB) {
	_engineConfig->setReaderBufferPageSizeInMB(sizeInMB);
}

bool CheatEngine::hasLoadedRegions() const {
	return _memoryCheater->hasLoadedRegions();
}

size_t CheatEngine::getNumOfLoadedRegions() const {
	return _memoryCheater->getNumOfLoadedRegions();
}

void CheatEngine::getLoadedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) {
	_memoryCheater->getLoadedRegions(regions);
}

void CheatEngine::getLoadedRegions(ILoadedRegionHandler& handler) {
	_memoryCheater->getLoadedRegions(handler);
}

void CheatEngine::loadRegions() {
	performAutoAttachIfNotAttached([this] () {
		_memoryCheater->loadRegions();
	});
}

bool CheatEngine::hasScannedRegions() const {
	return _memoryCheater->hasScannedRegions();
}

size_t CheatEngine::getNumOfScannedRegions() const {
	return _memoryCheater->getNumOfScannedRegions();
}

void CheatEngine::getScannedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) {
	_memoryCheater->getScannedRegions(regions);
}

void CheatEngine::getScannedRegions(IScannedRegionHandler& handler) {
	_memoryCheater->getScannedRegions(handler);
}

void CheatEngine::deleteRegions() {
	_memoryCheater->deleteRegions();
}

size_t CheatEngine::getNumOfCurrentMatchedValues() const {
	return _memoryCheater->getNumOfCurrentMatchedValues();
}

void CheatEngine::scan(ScanArguments& args) {
	performAutoAttachIfNotAttached([this, &args] () {
		_memoryCheater->scan(args);
	});
}

void CheatEngine::getMatchedValues(std::vector<MatchedValue>& values) {
	_memoryCheater->getMatchedValues(values);
}

void CheatEngine::iterateOverMatchedValues(IMatchedValueHandler& handler) {
	_memoryCheater->iterateOverMatchedValues(handler);
}

void CheatEngine::editMatchedValue(EditableMatchedValue& value) {
	performAutoAttachIfNotAttached([this, &value] () {
		_memoryCheater->editMatchedValue(value);
	});
}

int32_t CheatEngine::runTest(uint32_t testCase, const std::string& argsLine) {
	int32_t returnValue = 0;
	performAutoAttachIfNotAttached([this, &returnValue, &testCase, &argsLine] () {
		returnValue = _engineTester->runTest(testCase, argsLine);
	});
	return returnValue;
}

void CheatEngine::safelyAttach() {
	try {
		_processAttacher->attachProcess(_pid);
	} catch (...) {
		clean();
		throw;
	}
}

void CheatEngine::performAutoAttachIfNotAttached(std::function<void(void)> executor) {
	throwIfPidIsNotSet();
	bool isUsingAutoAttach = false;
	if (!hasAttachedProcess()) {
		safelyAttach();
		isUsingAutoAttach = true;
	}
	try {
		executor();
	} catch (...) {
		if (isUsingAutoAttach) { _processAttacher->detachProcess(); }
		throw;
	}
	if (isUsingAutoAttach) { _processAttacher->detachProcess(); }
}

void CheatEngine::throwIfPidIsNotSet() {
	if (!isPidSet()) {
		PURE_THROW_EXCEPTION(pure::fmt("Process ID is not set."));
	}
}

} /* END of namespace */
