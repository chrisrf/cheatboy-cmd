#include "EngineConfig.h"

#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/EngineConstants.h>
#include <cheatboy/engine/region/MemoryRegionLevel.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/editor/reader/MemoryReaderMode.h>

namespace cboy {

EngineConfig::EngineConfig()
	:
	_regionLevel(MemoryRegionLevel::HEAP),
	_valueTypeFlag(MemoryValueTypeFlag::ALL_SIGNED_INTEGER),
	_threshold(EngineConstants::MAX_NUM_OF_LISTABLE_MATCHED_VALUES),
	_readerMode(MemoryReaderMode::STREAMING),
	_smartReaderModeEnabled(false),
	_readerBufferPageSizeInMB(50) {}

EngineConfig::~EngineConfig() noexcept {}

MemoryRegionLevel EngineConfig::getScannedRegionLevel() const {
	return _regionLevel;
}

const MemoryValueTypeFlag& EngineConfig::getScannedValueTypeFlag() const {
	return _valueTypeFlag;
}

MemoryReaderMode EngineConfig::getMemoryReaderMode() const {
	return _readerMode;
}

size_t EngineConfig::getThresholdOfListableMatchedValues() const {
	return _threshold;
}

bool EngineConfig::isSmartReaderModeEnabled() const {
	return _smartReaderModeEnabled;
}

size_t EngineConfig::getReaderBufferPageSizeInMB() const {
	return _readerBufferPageSizeInMB;
}

size_t EngineConfig::getReaderBufferPageSizeInBytes() const {
	return _readerBufferPageSizeInMB * pure::ByteUnit::MB;
}

void EngineConfig::setScannedRegionLevel(MemoryRegionLevel level) {
	if (pure::EnumUtils<MemoryRegionLevel>::isValid(level)) {
		_regionLevel = level;
	}
}

void EngineConfig::setScannedValueTypeFlag(const MemoryValueTypeFlag& flag) {
	if (flag.isMarkedForAny()) {
		_valueTypeFlag = flag;
	}
}

void EngineConfig::setThresholdOfListableMatchedValues(size_t threshold) {
	if (threshold <= EngineConstants::MAX_NUM_OF_LISTABLE_MATCHED_VALUES) {
		_threshold = threshold;
	}
}

void EngineConfig::setMemoryReaderMode(MemoryReaderMode mode) {
	if (pure::EnumUtils<MemoryReaderMode>::isValid(mode)) {
		_readerMode = mode;
	}
}

void EngineConfig::setSmartReaderModeEnabled(bool enabled) {
	_smartReaderModeEnabled = enabled;
}

void EngineConfig::setReaderBufferPageSizeInMB(size_t sizeInMB) {
	if ((sizeInMB >= EngineConstants::MINIMUM_OF_READER_BUFFER_PAGE_SIZE_IN_MB) && (sizeInMB <= EngineConstants::MAXIMUM_OF_READER_BUFFER_PAGE_SIZE_IN_MB)) {
		_readerBufferPageSizeInMB = sizeInMB;
	}
}

} /* END of namespace */
