#ifndef __IEngineConfig_H__
#define __IEngineConfig_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {
enum class MemoryRegionLevel;
enum class MemoryReaderMode;
class MemoryValueTypeFlag;

class IEngineConfig {
PURE_DECLARE_CLASS_AS_INTERFACE(IEngineConfig);
public:
	virtual MemoryRegionLevel getScannedRegionLevel() const = 0;
	virtual const MemoryValueTypeFlag& getScannedValueTypeFlag() const = 0;
	virtual size_t getThresholdOfListableMatchedValues() const = 0;
	virtual MemoryReaderMode getMemoryReaderMode() const = 0;
	virtual bool isSmartReaderModeEnabled() const = 0;
	virtual size_t getReaderBufferPageSizeInMB() const = 0;
	virtual size_t getReaderBufferPageSizeInBytes() const = 0;
};

} /* END of namespace */

#endif
