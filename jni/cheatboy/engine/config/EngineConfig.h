#ifndef __EngineConfig_H__
#define __EngineConfig_H__

#include <memory>

#include <cheatboy/engine/config/IEngineConfig.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>

namespace cboy {

class EngineConfig: public IEngineConfig {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(EngineConfig);
private:
	MemoryRegionLevel _regionLevel;
	MemoryValueTypeFlag _valueTypeFlag;
	size_t _threshold;
	MemoryReaderMode _readerMode;
	bool _smartReaderModeEnabled;
	size_t _readerBufferPageSizeInMB;

public:
	EngineConfig();
	virtual ~EngineConfig() noexcept;
public:
	virtual MemoryRegionLevel getScannedRegionLevel() const override;
	virtual const MemoryValueTypeFlag& getScannedValueTypeFlag() const override;
	virtual size_t getThresholdOfListableMatchedValues() const override;
	virtual MemoryReaderMode getMemoryReaderMode() const override;
	virtual bool isSmartReaderModeEnabled() const override;
	virtual size_t getReaderBufferPageSizeInMB() const override;
	virtual size_t getReaderBufferPageSizeInBytes() const override;
	void setScannedRegionLevel(MemoryRegionLevel level);
	void setScannedValueTypeFlag(const MemoryValueTypeFlag& flag);
	void setThresholdOfListableMatchedValues(size_t threshold);
	void setMemoryReaderMode(MemoryReaderMode mode);
	void setSmartReaderModeEnabled(bool enabled);
	void setReaderBufferPageSizeInMB(size_t sizeInMB);
};

} /* END of namespace */

#endif
