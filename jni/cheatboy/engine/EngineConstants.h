#ifndef __EngineConstants_H__
#define __EngineConstants_H__

#include <sys/types.h>
#include <PureMacro.h>

namespace cboy {

class EngineConstants {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(EngineConstants);
public:
	static const pid_t NULL_PROCESS_ID;
	static const pid_t MIN_PROCESS_ID;
	static const pid_t NULL_THREAD_ID;
	static const pid_t MIN_THREAD_ID;
public:
	static const size_t MAX_NUM_OF_LISTABLE_MATCHED_VALUES;
	static const size_t MAXIMUM_OF_READER_BUFFER_PAGE_SIZE_IN_MB;
	static const size_t MINIMUM_OF_READER_BUFFER_PAGE_SIZE_IN_MB;
};

} /* END of namespace */

#endif
