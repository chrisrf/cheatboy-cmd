#ifndef __IEngineTester_H__
#define __IEngineTester_H__

#include <PureMacro.h>

namespace cboy {

class IEngineTester {
PURE_DECLARE_CLASS_AS_INTERFACE(IEngineTester);
public:
	virtual int32_t runTest(uint32_t testCase, const std::string& argsLine) = 0;
};

} /* END of namespace */

#endif
