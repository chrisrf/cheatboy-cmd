#include "EngineTester.h"

#include <sstream>
#include <PureLib.h>

#include <cheatboy/engine/attacher/IProcessAttacher.h>
#include <cheatboy/engine/editor/IMemoryEditorManager.h>
#include <cheatboy/engine/editor/IMemoryEditor.h>
#include <cheatboy/engine/editor/reader/IMemoryStreamReader.h>

namespace cboy {

EngineTester::EngineTester(
	IEngineConfig& engineConfig,
	IProcessAttacher& processAttacher,
	IMemoryEditorManager& editorManager,
	IMemoryRegionLoader& regionLoader,
	IMemoryCheater& memoryCheater)
	:
	_engineConfig(engineConfig),
	_processAttacher(processAttacher),
	_editorManager(editorManager),
	_regionLoader(regionLoader),
	_memoryCheater(memoryCheater) {

	_handlers.push_back([this] (const std::string& s) { return testEcho(s); });
}

EngineTester::~EngineTester() noexcept {
	_handlers.clear();
}

int32_t EngineTester::runTest(uint32_t testCase, const std::string& argsLine) {
	if (testCase >= _handlers.size()) {
		return 0;
	}
	return (_handlers[testCase])(argsLine);
}

int32_t EngineTester::testEcho(const std::string& argsLine) {
	PURE_D_LOG_LN("Run test case [testEcho]...");
	PURE_D_LOG_LN("{0}", argsLine);
	PURE_D_LOG_LN("End test case [testEcho]");
	return 0;
}

} /* END of namespace */
