#ifndef __EngineTester_H__
#define __EngineTester_H__

#include <functional>
#include <vector>
#include <string>

#include <cheatboy/engine/tester/IEngineTester.h>

namespace cboy {
class IEngineConfig;
class IProcessAttacher;
class IMemoryEditorManager;
class IMemoryRegionLoader;
class IMemoryCheater;

class EngineTester: public IEngineTester {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(EngineTester);
private:
	typedef std::function<int32_t(const std::string&)> TestHandler;
private:
	std::reference_wrapper<IEngineConfig> _engineConfig;
	std::reference_wrapper<IProcessAttacher> _processAttacher;
	std::reference_wrapper<IMemoryEditorManager> _editorManager;
	std::reference_wrapper<IMemoryRegionLoader> _regionLoader;
	std::reference_wrapper<IMemoryCheater> _memoryCheater;
	std::vector<TestHandler> _handlers;

public:
	EngineTester(
		IEngineConfig& engineConfig,
		IProcessAttacher& processAttacher,
		IMemoryEditorManager& editorManager,
		IMemoryRegionLoader& regionLoader,
		IMemoryCheater& memoryCheater);
	virtual ~EngineTester() noexcept;
public:
	virtual int32_t runTest(uint32_t testCase, const std::string& argsLine) override;
private:
	int32_t testEcho(const std::string& argsLine);
};

} /* END of namespace */

#endif
