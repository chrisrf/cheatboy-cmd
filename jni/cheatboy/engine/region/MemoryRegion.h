#ifndef __MemoryRegion_H__
#define __MemoryRegion_H__

#include <string>

#include <cheatboy/engine/region/IMemoryRegion.h>

namespace cboy {

class MemoryRegion: public IMemoryRegion {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(MemoryRegion);
private:
	pid_t _pid;	// If it is a thread stack region, then it stores thread id.
	region_id_t _id;
	MemoryRegionType _type;
	struct {
		uint16_t read :1;
		uint16_t write :1;
		uint16_t exec :1;
		uint16_t shared :1;
	} _permission;
	uintptr_t _startAddress;
	size_t _size;
	std::string _name;

public:
	MemoryRegion();
	virtual ~MemoryRegion() noexcept;
public:
	virtual pid_t getPid() const override { return _pid; }
	virtual region_id_t getID() const override { return _id; }
	virtual MemoryRegionType getType() const override { return _type; }
	virtual bool hasReadPermission() const override { return (_permission.read == 1); }
	virtual bool hasWritePermission() const override { return (_permission.write == 1); }
	virtual bool hasExecutePermission() const override { return (_permission.exec == 1); }
	virtual bool hasSharedPermission() const override { return (_permission.shared == 1); }
	virtual uintptr_t getStartAddress() const override { return _startAddress; }
	virtual size_t getSize() const override { return _size; }
	virtual const std::string& getName() const override { return _name; }
public:
	void setPid(pid_t pid) { _pid = pid; }
	void setID(region_id_t id) { _id = id; }
	void setType(MemoryRegionType type) { _type = type; }
	void setReadPermission(bool isEnable) { _permission.read = (isEnable ? 1 : 0); }
	void setWritePermission(bool isEnable) { _permission.write = (isEnable ? 1 : 0); }
	void setExecutePermission(bool isEnable) { _permission.exec = (isEnable ? 1 : 0); }
	void setSharedPermission(bool isEnable) { _permission.shared = (isEnable ? 1 : 0); }
	void setStartAddress(uintptr_t addr) { _startAddress = addr; }
	void setSize(size_t size) { _size = size; }
	void setName(std::string name) { _name = std::move(name); }
};

} /* END of namespace */

#endif
