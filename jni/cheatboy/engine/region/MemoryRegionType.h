#ifndef __MemoryRegionType_H__
#define __MemoryRegionType_H__

namespace cboy {

enum class MemoryRegionType {
	ENUM_MIN = 0,
	// =======
	MAIN_STACK = 0,
	THREAD_STACK = 1,
	HEAP = 2,
	ANDROID_DALVIK_HEAP = 3,
	ANDROID_ART_NATIVE_HEAP = 4,
	ANDROID_ART_HEAP = 5,
	OTHER = 6,
	// =======
	ENUM_MAX = OTHER,
	ENUM_NUMS = 7,
	// =======
	UNKNOWN = 7,
	ENUM_MAX_PLUS_UNKNOWN = UNKNOWN,
	ENUM_NUMS_PLUS_UNKNOWN = 8,
};

} /* END of namespace */

#endif
