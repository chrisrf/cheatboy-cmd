#ifndef __MemoryRegionConverter_H__
#define __MemoryRegionConverter_H__

#include <stdint.h>
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <memory>
#include <PureMacro.h>

#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>

namespace pure {
class NumberConverter;
class HexNumberConverter;
}

namespace cboy {
enum class MemoryRegionLevel;
enum class MemoryRegionType;
class MemoryRegion;

class MemoryRegionConverter {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(MemoryRegionConverter);
private:
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef std::function<bool(const std::string&, MemoryRegionType&)> RegionFilter;
private:
	enum {
		FIELD_ID_ADDRESS		= 0,
		FIELD_ID_PERMISSIONS	= 1,
		FIELD_ID_OFFSET			= 2,
		FIELD_ID_DEVICE			= 3,
		FIELD_ID_I_NODE			= 4,
		FIELD_ID_PATH_NAME		= 5,
		MAX_NUM_OF_FIELDS		= 6
	};
	enum {
		PERMISSION_BITS_ID_READ		= 0,
		PERMISSION_BITS_ID_WRITE	= 1,
		PERMISSION_BITS_ID_EXEC		= 2,
		PERMISSION_BITS_ID_SHARED	= 3,
		NUM_OF_PERMISSION_BITS		= 4
	};
	static const char PERMISSION_BITS_VALUE_READ;
	static const char PERMISSION_BITS_VALUE_WRITE;
	static const char PERMISSION_BITS_VALUE_EXEC;
	static const char PERMISSION_BITS_VALUE_SHARED;
private:
	std::unique_ptr<pure::NumberConverter> _numConverter;
	std::unique_ptr<pure::HexNumberConverter> _hexNumConverter;
	std::map<std::string, MemoryRegionType> _requiredRegions;
	std::vector<RegionFilter> _regionFilters;
	bool _isParsed;
	pid_t _pid;
	MemoryRegionType _type;
	std::stringstream _sstream;
	std::vector<std::string> _fields;
	std::string _startAddressField;
	std::string _endAddressField;
	uintptr_t _startAddress;
	uintptr_t _endAddress;

public:
	MemoryRegionConverter();
	virtual ~MemoryRegionConverter() noexcept;
public:
	void addRequiredRegion(const std::string& pathName, MemoryRegionType regionType);
public:
	bool parse(MemoryRegionLevel level, pid_t pid, const std::string& input);
	std::unique_ptr<MemoryRegion> generate(region_id_t id);
	void clean();
private:
	bool isRequiredRegion(const std::string& pathName, MemoryRegionType& outType);
	bool isHeapRegion(const std::string& pathName, MemoryRegionType& outType);
	bool isHeapOrStackRegion(const std::string& pathName, MemoryRegionType& outType);
	bool isWritePermissionRegion(const std::string& pathName, MemoryRegionType& outType);
	bool isRegion(const std::string& pathName, MemoryRegionType& outType);
};

} /* END of namespace */

#endif
