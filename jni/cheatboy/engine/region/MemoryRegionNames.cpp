#include "MemoryRegionNames.h"

namespace cboy {

const std::string MemoryRegionNames::MAIN_STACK_NAME = "[stack]";
const std::string MemoryRegionNames::THREAD_STACK_NAME_PREFIX = "[stack:";
const std::string MemoryRegionNames::HEAP_NAME = "[heap]";

const std::string MemoryRegionNames::ANDROID_DALVIK_HEAP_NAME        = "/dev/ashmem/dalvik-heap (deleted)";
const std::string MemoryRegionNames::ANDROID_DALVIK_HEAP_NAME_PREFIX = "/dev/ashmem/dalvik-heap";
const std::string MemoryRegionNames::ANDROID_ART_NATIVE_HEAP_NAME = "[anon:libc_malloc]";
const std::string MemoryRegionNames::ANDROID_ART_HEAP_NAME        = "/dev/ashmem/dalvik-main space (deleted)";
const std::string MemoryRegionNames::ANDROID_ART_HEAP_NAME_PREFIX = "/dev/ashmem/dalvik-main space";

} /* END of namespace */
