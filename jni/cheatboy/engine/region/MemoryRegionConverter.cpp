#include "MemoryRegionConverter.h"

#include <string>
#include <sstream>
#include <PureCppCompatibility.h>
#include <PureLib.h>

#include <cheatboy/engine/EngineConstants.h>
#include <cheatboy/engine/region/MemoryRegion.h>
#include <cheatboy/engine/region/MemoryRegionLevel.h>
#include <cheatboy/engine/region/MemoryRegionType.h>
#include <cheatboy/engine/region/MemoryRegionNames.h>

namespace cboy {

const char MemoryRegionConverter::PERMISSION_BITS_VALUE_READ	= 'r';
const char MemoryRegionConverter::PERMISSION_BITS_VALUE_WRITE	= 'w';
const char MemoryRegionConverter::PERMISSION_BITS_VALUE_EXEC	= 'x';
const char MemoryRegionConverter::PERMISSION_BITS_VALUE_SHARED	= 's';

MemoryRegionConverter::MemoryRegionConverter()
	:
	_numConverter(std::make_unique<pure::NumberConverter>()),
	_hexNumConverter(std::make_unique<pure::HexNumberConverter>()),
	_requiredRegions(),
	_regionFilters(),
	_isParsed(false),
	_pid(EngineConstants::NULL_PROCESS_ID),
	_type(MemoryRegionType::UNKNOWN),
	_sstream(),
	_fields(), _startAddressField(), _endAddressField(),
	_startAddress(0), _endAddress(0) {

	for (int i = 0; i < MAX_NUM_OF_FIELDS; i++) {
		_fields.push_back(std::string());
	}

	// XXX: These are normal regions for Android.
	// TODO: I do not understand [art] too much, check it if you have time.
	addRequiredRegion(MemoryRegionNames::ANDROID_DALVIK_HEAP_NAME, MemoryRegionType::ANDROID_DALVIK_HEAP);
	addRequiredRegion(MemoryRegionNames::ANDROID_ART_NATIVE_HEAP_NAME, MemoryRegionType::ANDROID_ART_NATIVE_HEAP);
	addRequiredRegion(MemoryRegionNames::ANDROID_ART_HEAP_NAME, MemoryRegionType::ANDROID_ART_HEAP);

	_regionFilters.push_back([this] (const std::string& p, MemoryRegionType& oT) { return isHeapRegion(p, oT); });
	_regionFilters.push_back([this] (const std::string& p, MemoryRegionType& oT) { return isHeapOrStackRegion(p, oT); });
	_regionFilters.push_back([this] (const std::string& p, MemoryRegionType& oT) { return isWritePermissionRegion(p, oT); });
	_regionFilters.push_back([this] (const std::string& p, MemoryRegionType& oT) { return isRegion(p, oT); });
}

MemoryRegionConverter::~MemoryRegionConverter() noexcept {
	_fields.clear();
}

void MemoryRegionConverter::addRequiredRegion(const std::string& pathName, MemoryRegionType regionType) {
	typedef std::map<std::string, MemoryRegionType> RequiredRegionMap;
	std::pair<RequiredRegionMap::iterator, bool> res = _requiredRegions.insert(std::make_pair(pathName, regionType));
	if (!res.second) {
		PURE_THROW_EXCEPTION(pure::fmt("Insert a duplicate required region [{0}].", pathName));
	}
}

bool MemoryRegionConverter::parse(MemoryRegionLevel level, pid_t pid, const std::string& input) {
	const int intLevel = static_cast<int>(level);

	clean();
	_sstream.str(input);

	for (size_t i = 0; i < _fields.size() - 1; i++) {
		if (!(_sstream >> _fields[i])) {
			PURE_THROW_EXCEPTION(pure::fmt("Need [{0}] fields but only get [{1}].", _fields.size(), i));
		}
		if (i == FIELD_ID_PERMISSIONS) {
			const std::string& permission = _fields[FIELD_ID_PERMISSIONS];
			if (permission.size() != NUM_OF_PERMISSION_BITS) {
				PURE_THROW_EXCEPTION(pure::fmt("Permission field needs ({0}) bits.", NUM_OF_PERMISSION_BITS));
			}
			if (intLevel <= static_cast<int>(MemoryRegionLevel::ALL_WRITE_PERMISSION_REGION)) {
				if (permission[PERMISSION_BITS_ID_WRITE] != PERMISSION_BITS_VALUE_WRITE) {
					return false;
				}
			}
		}
	}
	// TODO: Check What will happen if path name is string of unicode.
	if (!(std::getline(_sstream, _fields[FIELD_ID_PATH_NAME]))) {
		PURE_THROW_EXCEPTION(pure::fmt("Need [{0}] fields but only get [5].", _fields.size()));
	}
	pure::StringTrimmer::trim(_fields[FIELD_ID_PATH_NAME]);

	// TODO: Find an easy way to detect [.bss] section.
	{
		const std::string& pathName = _fields[FIELD_ID_PATH_NAME];
		_pid = pid;
		RegionFilter& filter = _regionFilters[intLevel];
		if (!filter(pathName, _type)) {
			return false;
		}
	}

	{
		const std::string& address = _fields[FIELD_ID_ADDRESS];
		if (!pure::StringSplitter::splitForTwoString(address, '-', _startAddressField, _endAddressField)) {
			PURE_THROW_EXCEPTION(pure::fmt("Failed to parse address field ({0}).", address));
		}

		if (!_hexNumConverter->convert(_startAddressField, _startAddress)) {
			PURE_THROW_EXCEPTION(pure::fmt("Fail to parse start address ({0}).", _startAddressField));
		}
		if (!_hexNumConverter->convert(_endAddressField, _endAddress)) {
			PURE_THROW_EXCEPTION(pure::fmt("Fail to parse end address ({0}).", _endAddressField));
		}
	}

	_isParsed = true;
	return _isParsed;
}

std::unique_ptr<MemoryRegion> MemoryRegionConverter::generate(region_id_t id) {
	if (!_isParsed) {
		PURE_THROW_EXCEPTION(pure::fmt("Nothing is parsed."));
	}

	const std::string& permission = _fields[FIELD_ID_PERMISSIONS];
	const std::string& pathName = _fields[FIELD_ID_PATH_NAME];
	std::unique_ptr<MemoryRegion> region = std::make_unique<MemoryRegion>();
	region->setID(id);
	region->setType(_type);
	region->setReadPermission((permission[PERMISSION_BITS_ID_READ] == PERMISSION_BITS_VALUE_READ));
	region->setWritePermission((permission[PERMISSION_BITS_ID_WRITE] == PERMISSION_BITS_VALUE_WRITE));
	region->setExecutePermission((permission[PERMISSION_BITS_ID_EXEC] == PERMISSION_BITS_VALUE_EXEC));
	region->setSharedPermission((permission[PERMISSION_BITS_ID_SHARED] == PERMISSION_BITS_VALUE_SHARED));
	region->setStartAddress(_startAddress);
	region->setSize(_endAddress - _startAddress);
	region->setName(pathName);
	region->setPid(_pid);
	return region;
}

void MemoryRegionConverter::clean() {
	_isParsed = false;
	_pid = EngineConstants::NULL_PROCESS_ID;
	_type = MemoryRegionType::UNKNOWN;
	_sstream.clear();
	_sstream.str("");
	for (size_t i = 0; i < _fields.size(); i++) {
		_fields[i].clear();
	}
	_startAddressField.clear();
	_endAddressField.clear();
	_startAddress = 0;
	_endAddress = 0;
}

bool MemoryRegionConverter::isRequiredRegion(const std::string& pathName, MemoryRegionType& outType) {
//	typedef std::map<std::string, MemoryRegionType> RequiredRegionMap;
//	RequiredRegionMap::iterator it = _requiredRegions.find(pathName);
//	if (it != _requiredRegions.end()) {
//		outType = it->second;
//		return true;
//	}
	if (pathName.compare(MemoryRegionNames::ANDROID_ART_NATIVE_HEAP_NAME) == 0) {
		outType = MemoryRegionType::ANDROID_ART_NATIVE_HEAP;
		return true;
	}
	if (pathName.compare(0, MemoryRegionNames::ANDROID_ART_HEAP_NAME_PREFIX.size(), MemoryRegionNames::ANDROID_ART_HEAP_NAME_PREFIX) == 0) {
		outType = MemoryRegionType::ANDROID_ART_HEAP;
		return true;
	}
	if (pathName.compare(0, MemoryRegionNames::ANDROID_DALVIK_HEAP_NAME_PREFIX.size(), MemoryRegionNames::ANDROID_DALVIK_HEAP_NAME_PREFIX) == 0) {
		outType = MemoryRegionType::ANDROID_DALVIK_HEAP;
		return true;
	}
	return false;
}

bool MemoryRegionConverter::isHeapRegion(const std::string& pathName, MemoryRegionType& outType) {
	if (pathName.compare(MemoryRegionNames::HEAP_NAME) == 0) {
		outType = MemoryRegionType::HEAP;
		return true;
	}
	if (isRequiredRegion(pathName, outType)) { return true; }
	return false;
}

bool MemoryRegionConverter::isHeapOrStackRegion(const std::string& pathName, MemoryRegionType& outType) {
	if (pathName.compare(MemoryRegionNames::MAIN_STACK_NAME) == 0) {
		outType = MemoryRegionType::MAIN_STACK;
		return true;
	}
	if (pathName.compare(0, MemoryRegionNames::THREAD_STACK_NAME_PREFIX.size(), MemoryRegionNames::THREAD_STACK_NAME_PREFIX) == 0) {
		outType = MemoryRegionType::THREAD_STACK;

		std::string::size_type found = pathName.find_last_of(']');
		if (found == std::string::npos) {
			PURE_THROW_EXCEPTION(pure::fmt("Failed to parse path name field ({0}) of a thread stack.", pathName));
		}
		std::string tidString = pathName.substr(MemoryRegionNames::THREAD_STACK_NAME_PREFIX.size(), found - MemoryRegionNames::THREAD_STACK_NAME_PREFIX.size());

		_pid = EngineConstants::NULL_THREAD_ID;
		if (!_numConverter->convert(tidString, _pid)) {
			PURE_THROW_EXCEPTION(pure::fmt("Failed to parse thread ID ({0}).", tidString));
		}
		return true;
	}
	if (isHeapRegion(pathName, outType)) { return true; }
	return false;
}

bool MemoryRegionConverter::isWritePermissionRegion(const std::string& pathName, MemoryRegionType& outType) {
	if (isHeapOrStackRegion(pathName, outType)) { return true; }
	outType = MemoryRegionType::OTHER;
	// XXX: we do NOT return false because we filtered the non-write region before.
	return true;
}

bool MemoryRegionConverter::isRegion(const std::string& pathName, MemoryRegionType& outType) {
	if (isHeapOrStackRegion(pathName, outType)) { return true; }
	outType = MemoryRegionType::OTHER;
	return true;
}

} /* END of namespace */
