#ifndef __IMemoryRegionLoader_H__
#define __IMemoryRegionLoader_H__

#include <stdint.h>
#include <vector>
#include <memory>
#include <PureMacro.h>

#include <cheatboy/engine/region/defs/MemoryRegionLoaderDefs.h>

namespace cboy {
enum class MemoryRegionLevel;
class MemoryRegion;

class IMemoryRegionLoader {
PURE_DECLARE_CLASS_AS_INTERFACE(IMemoryRegionLoader);
protected:
	typedef MemoryRegionLoaderDefs::IMemoryRegionHandler IMemoryRegionHandler;
	typedef MemoryRegionLoaderDefs::MemoryRegionHandler MemoryRegionHandler;

public:
	virtual void loadAsImmutableRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<IMemoryRegion>>& regions) = 0;
	virtual void loadAsImmutableRegions(MemoryRegionLevel level, IMemoryRegionHandler& handler) = 0;
	virtual void loadAsMutableRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<MemoryRegion>>& regions) = 0;
	virtual void loadAsMutableRegions(MemoryRegionLevel level, MemoryRegionHandler& handler) = 0;
};

} /* END of namespace */

#endif
