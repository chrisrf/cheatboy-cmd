#include "MemoryRegionLoader.h"

#include <errno.h>
#include <fstream>
#include <PureLib.h>

#include <cheatboy/engine/attacher/IProcessAttacher.h>
#include <cheatboy/engine/region/MemoryRegion.h>
#include <cheatboy/engine/region/MemoryRegionLevel.h>
#include <cheatboy/engine/region/MemoryRegionConverter.h>
#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>
#include <cheatboy/engine/region/exception/LoadMemoryRegionException.h>

namespace cboy {

MemoryRegionLoader::MemoryRegionLoader(IProcessAttacher& processAttacher)
	:
	_processAttacher(processAttacher) {}

MemoryRegionLoader::~MemoryRegionLoader() noexcept {}

void MemoryRegionLoader::loadAsImmutableRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<IMemoryRegion>>& regions) {
	checkBeforeLoading(level);
	loadAsRegions<IMemoryRegion>(level, regions);
}

void MemoryRegionLoader::loadAsImmutableRegions(MemoryRegionLevel level, IMemoryRegionHandler& handler) {
	checkBeforeLoading(level);
	load<IMemoryRegion>(level, handler);
}

void MemoryRegionLoader::loadAsMutableRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<MemoryRegion>>& regions) {
	checkBeforeLoading(level);
	loadAsRegions<MemoryRegion>(level, regions);
}

void MemoryRegionLoader::loadAsMutableRegions(MemoryRegionLevel level, MemoryRegionHandler& handler) {
	checkBeforeLoading(level);
	load<MemoryRegion>(level, handler);
}

template<typename T>
void MemoryRegionLoader::loadAsRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<T>>& regions) {
	std::vector<std::unique_ptr<T>> tempRegions;
	std::function<void(std::unique_ptr<T>)> handler = [&tempRegions] (std::unique_ptr<T> newRegion) {
		tempRegions.push_back(std::move(newRegion));
	};
	load<T>(level, handler);
	regions = std::move(tempRegions);
}

template<typename T>
void MemoryRegionLoader::load(MemoryRegionLevel level, std::function<void(std::unique_ptr<T>)>& handler) {
	typedef MemoryRegionDefs::region_id_t region_id_t;
	IProcessAttacher& attacher = _processAttacher;

	const pid_t pid = attacher.getAttachedProcessID();
	std::string mapsFile = pure::fmt("/proc/{0}/maps", pid);
	std::ifstream inputFile;
	inputFile.open(mapsFile);
	if (!inputFile) {
		PURE_THROW_DERIVED_EXCEPTION(LoadMemoryRegionException, pure::fmt(
			"Failed to open [{0}].\n"
			"[{1}] {2}.", mapsFile, errno, strerror(errno)));
	}

	errno = 0;
	std::string fileLine;
	MemoryRegionConverter converter;
	for (region_id_t i = 0; (std::getline(inputFile, fileLine)); i++) {
		if (fileLine.empty()) {
			continue;
		}
		if (!converter.parse(level, pid, fileLine)) {
			continue;
		}
		handler(converter.generate(i));
	}
	if (!(inputFile.eof())) {
		PURE_THROW_DERIVED_EXCEPTION(LoadMemoryRegionException, pure::fmt(
			"Failed to read [{0}].\n"
			"[{1}] {2}.", mapsFile, errno, strerror(errno)));
	}
}

void MemoryRegionLoader::checkBeforeLoading(MemoryRegionLevel level) const {
	IProcessAttacher& attacher = _processAttacher;
	attacher.throwIfNoProcessAttached();
	throwIfLevelIsInvalid(level);
}

void MemoryRegionLoader::throwIfLevelIsInvalid(MemoryRegionLevel level) const {
	if (!pure::EnumUtils<MemoryRegionLevel>::isValid(level)) {
		PURE_THROW_EXCEPTION(pure::fmt("Invalid MemoryRegionLevel: [{0}]", static_cast<int>(level)));
	}
}

} /* END of namespace */
