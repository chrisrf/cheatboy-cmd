#ifndef __MemoryRegionDefs_H__
#define __MemoryRegionDefs_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {

class MemoryRegionDefs {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryRegionDefs);
public:
	typedef uint32_t region_id_t;
};

} /* END of namespace */

#endif
