#ifndef __MemoryRegionLoaderDefs_H__
#define __MemoryRegionLoaderDefs_H__

#include <functional>
#include <memory>
#include <PureMacro.h>

namespace cboy {
class IMemoryRegion;
class MemoryRegion;

class MemoryRegionLoaderDefs {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryRegionLoaderDefs);
public:
	typedef std::function<void(std::unique_ptr<IMemoryRegion>)> IMemoryRegionHandler;
	typedef std::function<void(std::unique_ptr<MemoryRegion>)> MemoryRegionHandler;
};

} /* END of namespace */

#endif
