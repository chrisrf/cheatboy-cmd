#ifndef __LoadMemoryRegionException_H__
#define __LoadMemoryRegionException_H__

#include <purelib/exception/Exception.h>
#include <purelib/exception/ExceptionMacro.h>

namespace cboy {

PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION(LoadMemoryRegionException);

} /* END of namespace */

#endif
