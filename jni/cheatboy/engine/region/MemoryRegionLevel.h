#ifndef __MemoryRegionLevel_H__
#define __MemoryRegionLevel_H__

namespace cboy {

enum class MemoryRegionLevel {
	ENUM_MIN = 0,
	// =======
	HEAP = 0,
	HEAP_STACK = 1,
	ALL_WRITE_PERMISSION_REGION = 2,
	ALL = 3,
	// =======
	ENUM_MAX = ALL,
	ENUM_NUMS = 4
};

} /* END of namespace */

#endif
