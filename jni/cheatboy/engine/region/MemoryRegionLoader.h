#ifndef __MemoryRegionLoader_H__
#define __MemoryRegionLoader_H__

#include <functional>

#include <cheatboy/engine/region/IMemoryRegionLoader.h>

namespace cboy {
class IProcessAttacher;

class MemoryRegionLoader: public IMemoryRegionLoader {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(MemoryRegionLoader);
private:
	std::reference_wrapper<IProcessAttacher> _processAttacher;

public:
	MemoryRegionLoader(IProcessAttacher& processAttacher);
	virtual ~MemoryRegionLoader() noexcept;
public:
	virtual void loadAsImmutableRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<IMemoryRegion>>& regions) override;
	virtual void loadAsImmutableRegions(MemoryRegionLevel level, IMemoryRegionHandler& handler) override;
	virtual void loadAsMutableRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<MemoryRegion>>& regions) override;
	virtual void loadAsMutableRegions(MemoryRegionLevel level, MemoryRegionHandler& handler) override;
private:
	template<typename T> void loadAsRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<T>>& regions);
	template<typename T> void load(MemoryRegionLevel level, std::function<void(std::unique_ptr<T>)>& handler);
private:
	void checkBeforeLoading(MemoryRegionLevel level) const;
	void throwIfLevelIsInvalid(MemoryRegionLevel level) const;
};

} /* END of namespace */

#endif
