#ifndef __MemoryRegionNames_H__
#define __MemoryRegionNames_H__

#include <string>
#include <PureMacro.h>

namespace cboy {

class MemoryRegionNames {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryRegionNames);
public:
	static const std::string MAIN_STACK_NAME;
	static const std::string THREAD_STACK_NAME_PREFIX;
	static const std::string HEAP_NAME;
public:
	static const std::string ANDROID_DALVIK_HEAP_NAME;
	static const std::string ANDROID_DALVIK_HEAP_NAME_PREFIX;
	static const std::string ANDROID_ART_NATIVE_HEAP_NAME;
	static const std::string ANDROID_ART_HEAP_NAME;
	static const std::string ANDROID_ART_HEAP_NAME_PREFIX;
};

} /* END of namespace */

#endif
