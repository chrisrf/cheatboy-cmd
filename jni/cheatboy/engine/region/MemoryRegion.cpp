#include "MemoryRegion.h"

#include <string.h>
#include <PureLib.h>

#include <cheatboy/engine/EngineConstants.h>
#include <cheatboy/engine/region/MemoryRegionType.h>

namespace cboy {

MemoryRegion::MemoryRegion()
	:
	_pid(EngineConstants::NULL_PROCESS_ID),
	_id(0), _type(MemoryRegionType::UNKNOWN),
	_startAddress(0), _size(0),
	_name() {

	memset(&_permission, 0, sizeof(_permission));
}

MemoryRegion::~MemoryRegion() noexcept {
	//PURE_D_LOG_LN("~MemoryRegion()");
}

} /* END of namespace */
