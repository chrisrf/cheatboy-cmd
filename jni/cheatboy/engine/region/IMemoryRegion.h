#ifndef __IMemoryRegion_H__
#define __IMemoryRegion_H__

#include <stdint.h>
#include <PureMacro.h>

#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>

namespace cboy {
enum class MemoryRegionType;

class IMemoryRegion {
PURE_DECLARE_CLASS_AS_INTERFACE(IMemoryRegion);
protected:
	typedef MemoryRegionDefs::region_id_t region_id_t;

public:
	virtual pid_t getPid() const = 0;
	virtual region_id_t getID() const = 0;
	virtual MemoryRegionType getType() const = 0;
	virtual bool hasReadPermission() const = 0;
	virtual bool hasWritePermission() const = 0;
	virtual bool hasExecutePermission() const = 0;
	virtual bool hasSharedPermission() const = 0;
	virtual uintptr_t getStartAddress() const = 0;
	virtual size_t getSize() const = 0;
	virtual const std::string& getName() const = 0;
};

} /* END of namespace */

#endif
