#ifndef __CheatEngine_H__
#define __CheatEngine_H__

#include <stdint.h>
#include <memory>
#include <functional>

#include <cheatboy/engine/ICheatEngine.h>

namespace cboy {
class EngineConfig;
class IProcessAttacher;
class IMemoryEditorManager;
class IMemoryRegionLoader;
class IMemoryCheater;
class IEngineTester;

class CheatEngine: public ICheatEngine {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(CheatEngine);
private:
	pid_t _pid;
	std::unique_ptr<EngineConfig> _engineConfig;
	std::unique_ptr<IProcessAttacher> _processAttacher;
	std::unique_ptr<IMemoryEditorManager> _editorManager;
	std::unique_ptr<IMemoryRegionLoader> _regionLoader;
	std::unique_ptr<IMemoryCheater> _memoryCheater;
	std::unique_ptr<IEngineTester> _engineTester;

public:
	CheatEngine();
	virtual ~CheatEngine() noexcept;
public:
	virtual void clean() override;
	virtual bool isPidSet() const override;
	virtual pid_t getPid() const override;
	virtual void setPid(pid_t pid) override;
	virtual bool hasAttachedProcess() const override;
	virtual void attach() override;
	virtual void detach() override;
	virtual void readValue(ReadValue& readValue) override;
	virtual void writeValue(WriteValue& writeValue) override;
	virtual void getMemoryRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<IMemoryRegion>>& regions) override;
	virtual void getMemoryRegions(MemoryRegionLevel level, IMemoryRegionHandler& handler) override;
public:
	virtual MemoryRegionLevel getScannedRegionLevel() const override;
	virtual MemoryValueTypeFlag getScannedValueTypeFlag() const override;
	virtual size_t getThresholdOfListableMatchedValues() const override;
	virtual MemoryReaderMode getMemoryReaderMode() const override;
	virtual bool isSmartReaderModeEnabled() const override;
	virtual size_t getReaderBufferPageSizeInMB() const override;
	virtual void setScannedRegionLevel(MemoryRegionLevel level) override;
	virtual void setScannedValueTypeFlag(const MemoryValueTypeFlag& flag) override;
	virtual void setThresholdOfListableMatchedValues(size_t threshold) override;
	virtual void setMemoryReaderMode(MemoryReaderMode mode) override;
	virtual void setSmartReaderModeEnabled(bool enabled) override;
	virtual void setReaderBufferPageSizeInMB(size_t sizeInMB) override;
public:
	virtual bool hasLoadedRegions() const override;
	virtual size_t getNumOfLoadedRegions() const override;
	virtual void getLoadedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) override;
	virtual void getLoadedRegions(ILoadedRegionHandler& handler) override;
	virtual void loadRegions() override;
	virtual bool hasScannedRegions() const override;
	virtual size_t getNumOfScannedRegions() const override;
	virtual void getScannedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) override;
	virtual void getScannedRegions(IScannedRegionHandler& handler) override;
	virtual void deleteRegions() override;
public:
	virtual size_t getNumOfCurrentMatchedValues() const override;
	virtual void scan(ScanArguments& args) override;
	virtual void getMatchedValues(std::vector<MatchedValue>& values) override;
	virtual void iterateOverMatchedValues(IMatchedValueHandler& handler) override;
	virtual void editMatchedValue(EditableMatchedValue& value) override;
public:
	virtual int32_t runTest(uint32_t testCase, const std::string& argsLine) override;
private:
	void safelyAttach();
	void performAutoAttachIfNotAttached(std::function<void(void)> executor);
private:
	void throwIfPidIsNotSet();
};

} /* END of namespace */

#endif
