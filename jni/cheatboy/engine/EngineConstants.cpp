#include "EngineConstants.h"

namespace cboy {

const pid_t EngineConstants::NULL_PROCESS_ID = -1;
const pid_t EngineConstants::MIN_PROCESS_ID = 2;
const pid_t EngineConstants::NULL_THREAD_ID = -1;
const pid_t EngineConstants::MIN_THREAD_ID = 2;

const size_t EngineConstants::MAX_NUM_OF_LISTABLE_MATCHED_VALUES = 50;
const size_t EngineConstants::MAXIMUM_OF_READER_BUFFER_PAGE_SIZE_IN_MB = 1024;
const size_t EngineConstants::MINIMUM_OF_READER_BUFFER_PAGE_SIZE_IN_MB = 1;

} /* END of namespace */
