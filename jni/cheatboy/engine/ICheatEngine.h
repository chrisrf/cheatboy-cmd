#ifndef __ICheatEngine_H__
#define __ICheatEngine_H__

#include <stdint.h>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <PureMacro.h>

#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>
#include <cheatboy/engine/region/defs/MemoryRegionLoaderDefs.h>
#include <cheatboy/engine/cheater/defs/MemoryCheaterDefs.h>

namespace cboy {
enum class MemoryRegionLevel;
enum class MemoryReaderMode;
class ReadValue;
class WriteValue;
class MemoryValueTypeFlag;
class IMemoryRegion;
class ScanArguments;
class MatchedValue;
class EditableMatchedValue;

class ICheatEngine {
PURE_DECLARE_CLASS_AS_INTERFACE(ICheatEngine);
public:
	typedef MemoryRegionLoaderDefs::IMemoryRegionHandler IMemoryRegionHandler;
	typedef MemoryCheaterDefs::ILoadedRegionHandler ILoadedRegionHandler;
	typedef MemoryCheaterDefs::IScannedRegionHandler IScannedRegionHandler;
	typedef MemoryCheaterDefs::IMatchedValueHandler IMatchedValueHandler;

public:
	virtual void clean() = 0;
	virtual bool isPidSet() const = 0;
	virtual pid_t getPid() const = 0;
	virtual void setPid(pid_t pid) = 0;
	virtual bool hasAttachedProcess() const = 0;
	virtual void attach() = 0;
	virtual void detach() = 0;
	virtual void readValue(ReadValue& readValue) = 0;
	virtual void writeValue(WriteValue& writeValue) = 0;
	virtual void getMemoryRegions(MemoryRegionLevel level, std::vector<std::unique_ptr<IMemoryRegion>>& regions) = 0;
	virtual void getMemoryRegions(MemoryRegionLevel level, IMemoryRegionHandler& handler) = 0;
public:
	virtual MemoryRegionLevel getScannedRegionLevel() const = 0;
	virtual MemoryValueTypeFlag getScannedValueTypeFlag() const = 0;
	virtual size_t getThresholdOfListableMatchedValues() const = 0;
	virtual MemoryReaderMode getMemoryReaderMode() const = 0;
	virtual bool isSmartReaderModeEnabled() const = 0;
	virtual size_t getReaderBufferPageSizeInMB() const = 0;
	virtual void setScannedRegionLevel(MemoryRegionLevel level) = 0;
	virtual void setScannedValueTypeFlag(const MemoryValueTypeFlag& flag) = 0;
	virtual void setThresholdOfListableMatchedValues(size_t threshold) = 0;
	virtual void setMemoryReaderMode(MemoryReaderMode mode) = 0;
	virtual void setSmartReaderModeEnabled(bool enabled) = 0;
	virtual void setReaderBufferPageSizeInMB(size_t sizeInMB) = 0;
public:
	virtual bool hasLoadedRegions() const = 0;
	virtual size_t getNumOfLoadedRegions() const = 0;
	virtual void getLoadedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) = 0;
	virtual void getLoadedRegions(ILoadedRegionHandler& handler) = 0;
	virtual void loadRegions() = 0;
	virtual bool hasScannedRegions() const = 0;
	virtual size_t getNumOfScannedRegions() const = 0;
	virtual void getScannedRegions(std::vector<std::reference_wrapper<IMemoryRegion>>& regions) = 0;
	virtual void getScannedRegions(IScannedRegionHandler& handler) = 0;
	virtual void deleteRegions() = 0;
public:
	virtual size_t getNumOfCurrentMatchedValues() const = 0;
	virtual void scan(ScanArguments& args) = 0;
	virtual void getMatchedValues(std::vector<MatchedValue>& values) = 0;
	virtual void iterateOverMatchedValues(IMatchedValueHandler& handler) = 0;
	virtual void editMatchedValue(EditableMatchedValue& value) = 0;
public:
	virtual int32_t runTest(uint32_t testCase, const std::string& argsLine) = 0;
};

} /* END of namespace */

#endif
