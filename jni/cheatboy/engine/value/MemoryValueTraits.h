#ifndef __MemoryValueTraits_H__
#define __MemoryValueTraits_H__

#include <stdint.h>
#include <type_traits>

namespace cboy {

// Reference to std::is_integral

template<typename> struct __is_memory_integer_helper: public std::false_type {};
template<> struct __is_memory_integer_helper<int8_t>: public std::true_type {};
template<> struct __is_memory_integer_helper<uint8_t>: public std::true_type {};
template<> struct __is_memory_integer_helper<int16_t>: public std::true_type {};
template<> struct __is_memory_integer_helper<uint16_t>: public std::true_type {};
template<> struct __is_memory_integer_helper<int32_t>: public std::true_type {};
template<> struct __is_memory_integer_helper<uint32_t>: public std::true_type {};
template<> struct __is_memory_integer_helper<int64_t>: public std::true_type {};
template<> struct __is_memory_integer_helper<uint64_t>: public std::true_type {};

template<typename _Tp>
struct is_memory_integer
	: public std::integral_constant<bool, (__is_memory_integer_helper<_Tp>::value)> {};

template<typename> struct __is_memory_float_helper: public std::false_type {};
template<> struct __is_memory_float_helper<float>: public std::true_type {};
template<> struct __is_memory_float_helper<double>: public std::true_type {};

template<typename _Tp>
struct is_memory_float
	: public std::integral_constant<bool, (__is_memory_float_helper<_Tp>::value)> {};

template<typename _Tp>
struct is_memory_value
	: public std::integral_constant<bool, (__is_memory_integer_helper<_Tp>::value || __is_memory_float_helper<_Tp>::value)> {};

} /* END of namespace */

#endif
