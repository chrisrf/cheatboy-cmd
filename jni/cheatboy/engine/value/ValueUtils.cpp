#include "ValueUtils.h"

#include <stdio.h>
#include <inttypes.h>

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/UserValue.h>

namespace cboy {

void ValueUtils::printMemoryValue(MemoryValue& value) {
	printMemoryValueForAllInteger(value);
	printMemoryValueForAllFloat(value);
}

void ValueUtils::printMemoryValueForAllInteger(MemoryValue& value) {
	printf("[i08] = %" PRId8 "\n", value.getAs<int8_t>());
	printf("[u08] = %" PRIu8 "\n", value.getAs<uint8_t>());
	printf("[i16] = %" PRId16 "\n", value.getAs<int16_t>());
	printf("[u16] = %" PRIu16 "\n", value.getAs<uint16_t>());
	printf("[i32] = %" PRId32 "\n", value.getAs<int32_t>());
	printf("[u32] = %" PRIu32 "\n", value.getAs<uint32_t>());
	printf("[i64] = %" PRId64 "\n", value.getAs<int64_t>());
	printf("[u64] = %" PRIu64 "\n", value.getAs<uint64_t>());
}

void ValueUtils::printMemoryValueForAllFloat(MemoryValue& value) {
	printf("[f32] = %e\n", value.getAs<float>());
	printf("[f64] = %e\n", value.getAs<double>());
}

void ValueUtils::printUserValue(UserValue& value) {
	printUserValueForAllInteger(value);
	printUserValueForAllFloat(value);
}

void ValueUtils::printUserValueForAllInteger(UserValue& value) {
	printf("[i08] = %" PRId8 "\n", value.getFor<int8_t>());
	printf("[u08] = %" PRIu8 "\n", value.getFor<uint8_t>());
	printf("[i16] = %" PRId16 "\n", value.getFor<int16_t>());
	printf("[u16] = %" PRIu16 "\n", value.getFor<uint16_t>());
	printf("[i32] = %" PRId32 "\n", value.getFor<int32_t>());
	printf("[u32] = %" PRIu32 "\n", value.getFor<uint32_t>());
	printf("[i64] = %" PRId64 "\n", value.getFor<int64_t>());
	printf("[u64] = %" PRIu64 "\n", value.getFor<uint64_t>());
}

void ValueUtils::printUserValueForAllFloat(UserValue& value) {
	printf("[f32] = %e\n", value.getFor<float>());
	printf("[f64] = %e\n", value.getFor<double>());
}

} /* END of namespace */
