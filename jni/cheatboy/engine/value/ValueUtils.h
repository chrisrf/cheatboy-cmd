#ifndef __ValueUtils_H__
#define __ValueUtils_H__

#include <PureMacro.h>

namespace cboy {
class MemoryValue;
class UserValue;

class ValueUtils {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(ValueUtils);
public:
	static void printMemoryValue(MemoryValue& value);
	static void printMemoryValueForAllInteger(MemoryValue& value);
	static void printMemoryValueForAllFloat(MemoryValue& value);

	static void printUserValue(UserValue& value);
	static void printUserValueForAllInteger(UserValue& value);
	static void printUserValueForAllFloat(UserValue& value);
};

} /* END of namespace */

#endif
