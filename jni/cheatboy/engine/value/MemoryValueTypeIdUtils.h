#ifndef __MemoryValueTypeIdUtils_H__
#define __MemoryValueTypeIdUtils_H__

#include <stdint.h>
#include <PureMacro.h>

#include <cheatboy/engine/value/MemoryValueTypeID.h>

namespace cboy {

class MemoryValueTypeIdUtils {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryValueTypeIdUtils);
public:
	template<typename T> static inline constexpr int idOf();
	template<typename T> static inline constexpr MemoryValueTypeID enumOf();
};

template<> inline constexpr int MemoryValueTypeIdUtils::idOf<int8_t>() { return static_cast<int>(MemoryValueTypeID::I08); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<uint8_t>() { return static_cast<int>(MemoryValueTypeID::U08); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<int16_t>() { return static_cast<int>(MemoryValueTypeID::I16); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<uint16_t>() { return static_cast<int>(MemoryValueTypeID::U16); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<int32_t>() { return static_cast<int>(MemoryValueTypeID::I32); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<uint32_t>() { return static_cast<int>(MemoryValueTypeID::U32); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<int64_t>() { return static_cast<int>(MemoryValueTypeID::I64); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<uint64_t>() { return static_cast<int>(MemoryValueTypeID::U64); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<float>() { return static_cast<int>(MemoryValueTypeID::F32); }
template<> inline constexpr int MemoryValueTypeIdUtils::idOf<double>() { return static_cast<int>(MemoryValueTypeID::F64); }

template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<int8_t>() { return MemoryValueTypeID::I08; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<uint8_t>() { return MemoryValueTypeID::U08; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<int16_t>() { return MemoryValueTypeID::I16; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<uint16_t>() { return MemoryValueTypeID::U16; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<int32_t>() { return MemoryValueTypeID::I32; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<uint32_t>() { return MemoryValueTypeID::U32; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<int64_t>() { return MemoryValueTypeID::I64; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<uint64_t>() { return MemoryValueTypeID::U64; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<float>() { return MemoryValueTypeID::F32; }
template<> inline constexpr MemoryValueTypeID MemoryValueTypeIdUtils::enumOf<double>() { return MemoryValueTypeID::F64; }

} /* END of namespace */

#endif
