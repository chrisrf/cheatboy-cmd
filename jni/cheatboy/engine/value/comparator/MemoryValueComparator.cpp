#include "MemoryValueComparator.h"

#include <PureLib.h>

#include <cheatboy/engine/value/MemoryValueTypeIdUtils.h>
#include <cheatboy/engine/value/comparator/ComparisonFunction.h>
#include <cheatboy/engine/value/comparator/FloatComparisonFunction.h>

namespace cboy {

MemoryValueComparator::MemoryValueComparator()
	:
	_methodID(static_cast<int>(ComparisonMethod::ANY)) {

	buildFunctions();
}

MemoryValueComparator::~MemoryValueComparator() noexcept {}

ComparisonMethod MemoryValueComparator::getCurrentComparisonMethod() const {
	return ComparisonMethod(_methodID);
}

void MemoryValueComparator::setCurrentComparisonMethod(ComparisonMethod method) {
	throwIfMethodIsInvalid(method);
	_methodID = static_cast<int>(method);
}

size_t MemoryValueComparator::compareValue(
	const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue,
	const MemoryValueTypeFlag& cmpFlag, MemoryValueTypeFlag& savedFlag) const {

	size_t maxBytes = 0;
	if (!cmpFlag.isMarkedForAny()) {
		return maxBytes;
	}
	compareValueFor<int8_t>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<uint8_t>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<int16_t>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<uint16_t>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<int32_t>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<uint32_t>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<int64_t>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<uint64_t>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<float>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	compareValueFor<double>(newValue, oldValue, userValue, cmpFlag, savedFlag, maxBytes);
	return maxBytes;
}

void MemoryValueComparator::buildFunctions() {
	buildComparatorsForInteger<int8_t>();
	buildComparatorsForInteger<uint8_t>();
	buildComparatorsForInteger<int16_t>();
	buildComparatorsForInteger<uint16_t>();
	buildComparatorsForInteger<int32_t>();
	buildComparatorsForInteger<uint32_t>();
	buildComparatorsForInteger<int64_t>();
	buildComparatorsForInteger<uint64_t>();
	buildComparatorsForFloat<float>();
	buildComparatorsForFloat<double>();
}

template<typename T>
void MemoryValueComparator::buildComparatorsForInteger() {
	_functions[static_cast<int>(ComparisonMethod::ANY)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::any;
	_functions[static_cast<int>(ComparisonMethod::EQUAL_TO)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::equalTo;
	_functions[static_cast<int>(ComparisonMethod::NOT_EQUAL_TO)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::notEqualTo;
	_functions[static_cast<int>(ComparisonMethod::GREATER_THAN)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::greaterThan;
	_functions[static_cast<int>(ComparisonMethod::LESS_THAN)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::lessThan;
	_functions[static_cast<int>(ComparisonMethod::NOT_CHANGED)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::notChanged;
	_functions[static_cast<int>(ComparisonMethod::CHANGED)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::changed;
	_functions[static_cast<int>(ComparisonMethod::INCREASED)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::increased;
	_functions[static_cast<int>(ComparisonMethod::DECREASED)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::decreased;
	_functions[static_cast<int>(ComparisonMethod::INCREASED_BY)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::increasedBy;
	_functions[static_cast<int>(ComparisonMethod::DECREASED_BY)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::decreasedBy;
}

template<typename T>
void MemoryValueComparator::buildComparatorsForFloat() {
	_functions[static_cast<int>(ComparisonMethod::ANY)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::any;
	_functions[static_cast<int>(ComparisonMethod::EQUAL_TO)][MemoryValueTypeIdUtils::idOf<T>()] = FloatComparisonFunction<T>::equalTo;
	_functions[static_cast<int>(ComparisonMethod::NOT_EQUAL_TO)][MemoryValueTypeIdUtils::idOf<T>()] = FloatComparisonFunction<T>::notEqualTo;
	_functions[static_cast<int>(ComparisonMethod::GREATER_THAN)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::greaterThan;
	_functions[static_cast<int>(ComparisonMethod::LESS_THAN)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::lessThan;
	_functions[static_cast<int>(ComparisonMethod::NOT_CHANGED)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::notChanged;
	_functions[static_cast<int>(ComparisonMethod::CHANGED)][MemoryValueTypeIdUtils::idOf<T>()] = FloatComparisonFunction<T>::changed;
	_functions[static_cast<int>(ComparisonMethod::INCREASED)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::increased;
	_functions[static_cast<int>(ComparisonMethod::DECREASED)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::decreased;
	_functions[static_cast<int>(ComparisonMethod::INCREASED_BY)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::increasedBy;
	_functions[static_cast<int>(ComparisonMethod::DECREASED_BY)][MemoryValueTypeIdUtils::idOf<T>()] = ComparisonFunction<T>::decreasedBy;
}

template<typename T>
inline void MemoryValueComparator::compareValueFor(
	const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue,
	const MemoryValueTypeFlag& cmpFlag, MemoryValueTypeFlag& savedFlag, size_t& savedMaxBytes) const {

	if (cmpFlag.isMarkedFor<T>()) {
		size_t temp = 0;
		ComparisonFptr comparisonFunction = _functions[_methodID][MemoryValueTypeIdUtils::idOf<T>()];
		if ((temp = comparisonFunction(newValue, oldValue, userValue, savedFlag)) > savedMaxBytes) {
			savedMaxBytes = temp;
		}
	}
}

void MemoryValueComparator::throwIfMethodIsInvalid(ComparisonMethod method) const {
	if (!pure::EnumUtils<ComparisonMethod>::isValid(method)) {
		PURE_THROW_EXCEPTION(pure::fmt("Invalid ComparisonMethod: [{0}]", static_cast<int>(method)));
	}
}

} /* END of namespace */
