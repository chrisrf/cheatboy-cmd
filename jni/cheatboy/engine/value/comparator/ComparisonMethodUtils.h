#ifndef __ComparisonMethodUtils_H__
#define __ComparisonMethodUtils_H__

#include <set>
#include <PureMacro.h>

namespace cboy {
enum class ComparisonMethod;

class ComparisonMethodUtils {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(ComparisonMethodUtils);
private:
	static const std::set<ComparisonMethod> sharedUserValueRequiredMethods;

public:
	/**
	 * XXX: If it return false, it does not mean that the method requires an USER value.
	 */
	static bool isAnOldValueRequiredMethod(ComparisonMethod method);
	/**
	 * XXX: If it return false, it does not mean that the method requires an OLD value.
	 */
	static bool isAnUserValueRequiredMethod(ComparisonMethod method);
};

} /* END of namespace */

#endif
