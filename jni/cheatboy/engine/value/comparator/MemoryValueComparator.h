#ifndef __MemoryValueComparator_H__
#define __MemoryValueComparator_H__

#include <stdint.h>
#include <PureMacro.h>

#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/comparator/ComparisonMethod.h>

namespace cboy {
class MemoryValue;
class UserValue;
class MemoryValueTypeFlag;

// TODO: Should compareValue and compareValueFor be inlined?
class MemoryValueComparator {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(MemoryValueComparator);
private:
	typedef size_t (*ComparisonFptr)(const MemoryValue&, const MemoryValue&, const UserValue&, MemoryValueTypeFlag&);
private:
	int _methodID;
	ComparisonFptr _functions[static_cast<int>(ComparisonMethod::ENUM_NUMS)][static_cast<int>(MemoryValueTypeID::ENUM_NUMS)];

public:
	MemoryValueComparator();
	virtual ~MemoryValueComparator() noexcept;
public:
	ComparisonMethod getCurrentComparisonMethod() const;
	void setCurrentComparisonMethod(ComparisonMethod method);
	size_t compareValue(
		const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue,
		const MemoryValueTypeFlag& cmpFlag, MemoryValueTypeFlag& savedFlag) const;
private:
	void buildFunctions();
	template<typename T> void buildComparatorsForInteger();
	template<typename T> void buildComparatorsForFloat();
	template<typename T> inline void compareValueFor(
		const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue,
		const MemoryValueTypeFlag& cmpFlag, MemoryValueTypeFlag& savedFlag, size_t& savedMaxBytes) const;
private:
	void throwIfMethodIsInvalid(ComparisonMethod method) const;
};

} /* END of namespace */

#endif
