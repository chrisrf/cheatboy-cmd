#ifndef __ComparisonFunction_H__
#define __ComparisonFunction_H__

#include <assert.h>
#include <type_traits>
#include <PureMacro.h>
#include <purelib/terminal/TerminalPrint.h>

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTraits.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/MemoryValueTypeIdUtils.h>
#include <cheatboy/engine/value/UserValue.h>

//#define __CBOY_UNIT_TEST_COMPARISON_FUNCTION__

#ifdef __CBOY_UNIT_TEST_COMPARISON_FUNCTION__
#define CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT(mpFunctionCall, mpNewValue, mpOldvalue, mpUserValue)\
	do {\
		pure::print("Call ComparisonFunction<{0}>::" mpFunctionCall, MemoryValueTypeIdUtils::idOf<T>());\
		printValue(newValue, oldValue, userValue);\
	} while (0)
#else
#define CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT(mpFunctionCall, mpNewValue, mpOldvalue, mpUserValue) ((void)0)
#endif

namespace cboy {

template<typename T>
class ComparisonFunction {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(ComparisonFunction);
public:
	typedef typename std::enable_if<cboy::is_memory_value<T>::value>::type ValueType;

public:
	static inline size_t any(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("any ", newValue, oldValue, userValue);
		return returnForSuccess(savedFlag);
	}

	static inline size_t equalTo(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("equalTo ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() == userValue.getFor<T>()) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t notEqualTo(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("notEqualTo ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() != userValue.getFor<T>()) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t greaterThan(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("greaterThan ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() > userValue.getFor<T>()) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t lessThan(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("lessThan ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() < userValue.getFor<T>()) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}

	static inline size_t notChanged(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("notChanged ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() == oldValue.getAs<T>()) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t changed(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("changed ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() != oldValue.getAs<T>()) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t increased(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("increased ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() > oldValue.getAs<T>()) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t decreased(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("decreased ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() < oldValue.getAs<T>()) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}

	static inline size_t increasedBy(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("increasedBy ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() == (oldValue.getAs<T>() + userValue.getFor<T>())) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t decreasedBy(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_COMPARISON_FUNCTION_PRINT("decreasedBy ", newValue, oldValue, userValue);
		if (newValue.getAs<T>() == (oldValue.getAs<T>() - userValue.getFor<T>())) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
private:
	static inline size_t returnForSuccess(MemoryValueTypeFlag& savedFlag) {
		savedFlag.markFor<T>();
		return sizeof(T);
	}
#ifdef __CBOY_UNIT_TEST_COMPARISON_FUNCTION__
private:
	static inline void printValue(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue) {
		if (cboy::is_memory_integer<T>::value) {
			if (std::is_signed<T>::value) {
				pure::print("I[{0}][{1}][{2}]\n", ((int64_t) newValue.getAs<T>()), ((int64_t) oldValue.getAs<T>()), ((int64_t) userValue.getFor<T>()));
			} else {
				pure::print("U[{0}][{1}][{2}]\n", ((uint64_t) newValue.getAs<T>()), ((uint64_t) oldValue.getAs<T>()), ((uint64_t) userValue.getFor<T>()));
			}
		} else {
			pure::print("F[{0}][{1}][{2}]\n", newValue.getAs<T>(), oldValue.getAs<T>(), userValue.getFor<T>());
		}
	}
#endif
};

// XXX: Specialization for float.
template<>
inline size_t ComparisonFunction<float>::equalTo(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
	assert(false);
	return 0;
}
template<>
inline size_t ComparisonFunction<float>::notEqualTo(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
	assert(false);
	return 0;
}
template<>
inline size_t ComparisonFunction<float>::changed(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
	assert(false);
	return 0;
}

// XXX: Specialization for double.
template<>
inline size_t ComparisonFunction<double>::equalTo(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
	assert(false);
	return 0;
}
template<>
inline size_t ComparisonFunction<double>::notEqualTo(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
	assert(false);
	return 0;
}
template<>
inline size_t ComparisonFunction<double>::changed(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
	assert(false);
	return 0;
}

} /* END of namespace */

#endif
