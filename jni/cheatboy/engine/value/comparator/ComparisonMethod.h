#ifndef __ComparisonMethod_H__
#define __ComparisonMethod_H__

namespace cboy {

enum class ComparisonMethod {
	ENUM_MIN = 0,
	// =======
	// Following: to capture and update (does NOT require a given value or the old value).
	ANY				= 0,
	// Following: compare with a given value.
	EQUAL_TO		= 1,
	NOT_EQUAL_TO	= 2,
	GREATER_THAN	= 3,
	LESS_THAN		= 4,
	// Following: compare with the old value.
	NOT_CHANGED		= 5,
	CHANGED			= 6,
	INCREASED		= 7,
	DECREASED		= 8,
	// Following: compare with both given value and old value.
	INCREASED_BY	= 9,
	DECREASED_BY	= 10,
	// =======
	ENUM_MAX		= 10,
	ENUM_NUMS		= 11,
	// =======
	OLD_VALUE_REQUIRED_METHOD_THRESHOLD = 5
};

} /* END of namespace */

#endif
