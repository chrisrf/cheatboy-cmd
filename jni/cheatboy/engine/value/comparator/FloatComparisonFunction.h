#ifndef __FloatComparisonFunction_H__
#define __FloatComparisonFunction_H__

#include <math.h>
#include <PureMacro.h>
#include <purelib/terminal/TerminalPrint.h>

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTraits.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/UserValue.h>

//#define __CBOY_UNIT_TEST_FLOAT_COMPARISON_FUNCTION__

#ifdef __CBOY_UNIT_TEST_FLOAT_COMPARISON_FUNCTION__
#define CBOY_UNIT_TEST_FLOAT_COMPARISON_FUNCTION_PRINT(mpFunctionCall, mpNewValue, mpOldvalue, mpUserValue)\
	do {\
		pure::print("Call FloatComparisonFunction<{0}>::" mpFunctionCall, MemoryValueTypeID::idOf<T>());\
		printValue(newValue, oldValue, userValue);\
	} while (0)
#else
#define CBOY_UNIT_TEST_FLOAT_COMPARISON_FUNCTION_PRINT(mpFunctionCall, mpNewValue, mpOldvalue, mpUserValue) ((void)0)
#endif

namespace cboy {

template<typename T>
class FloatComparisonFunction {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(FloatComparisonFunction);
public:
	typedef typename std::enable_if<cboy::is_memory_float<T>::value>::type ValueType;

public:
	static inline size_t equalTo(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_FLOAT_COMPARISON_FUNCTION_PRINT("equalTo ", newValue, oldValue, userValue);
		T newNum = truncate(newValue.getAs<T>());
		T userNum = truncate(userValue.getFor<T>());
		if (newNum == userNum) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t notEqualTo(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_FLOAT_COMPARISON_FUNCTION_PRINT("notEqualTo ", newValue, oldValue, userValue);
		T newNum = newValue.getAs<T>();
		T userNum = userValue.getFor<T>();
		if (isNan(newNum) || isNan(userNum)) { return 0; }
		newNum = truncate(newNum);
		userNum = truncate(userNum);
		if (newNum != userNum) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
	static inline size_t changed(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue, MemoryValueTypeFlag& savedFlag) {
		CBOY_UNIT_TEST_FLOAT_COMPARISON_FUNCTION_PRINT("changed ", newValue, oldValue, userValue);
		T newNum = newValue.getAs<T>();
		T oldNum = oldValue.getAs<T>();
		if (isNan(newNum) || isNan(oldNum)) { return 0; }
		if (newNum != oldNum) {
			return returnForSuccess(savedFlag);
		}
		return 0;
	}
private:
	static inline size_t returnForSuccess(MemoryValueTypeFlag& savedFlag) {
		savedFlag.markFor<T>();
		return sizeof(T);
	}
private:
	static inline float truncate(float f) { return truncf(f); }
	static inline double truncate(double d) { return trunc(d); }
	static inline long double truncate(long double ld) { return truncl(ld); }
	static inline bool isNan(T n) { return (n != n); }
#ifdef __CBOY_UNIT_TEST_FLOAT_COMPARISON_FUNCTION__
private:
	static inline void printValue(const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue) {
		pure::print("F[{0}][{1}][{2}]\n", newValue.getAs<T>(), oldValue.getAs<T>(), userValue.getFor<T>());
	}
#endif
};

} /* END of namespace */

#endif
