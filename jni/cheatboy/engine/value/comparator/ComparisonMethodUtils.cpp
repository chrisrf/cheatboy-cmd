#include "ComparisonMethodUtils.h"

#include <cheatboy/engine/value/comparator/ComparisonMethod.h>

namespace cboy {

const std::set<ComparisonMethod> ComparisonMethodUtils::sharedUserValueRequiredMethods = {
		ComparisonMethod::EQUAL_TO, ComparisonMethod::NOT_EQUAL_TO,
		ComparisonMethod::GREATER_THAN, ComparisonMethod::LESS_THAN,
		ComparisonMethod::INCREASED_BY, ComparisonMethod::DECREASED_BY
};

bool ComparisonMethodUtils::isAnOldValueRequiredMethod(ComparisonMethod method) {
	int intMethod = static_cast<int>(method);
	if (intMethod < static_cast<int>(ComparisonMethod::OLD_VALUE_REQUIRED_METHOD_THRESHOLD)) {
		return false;
	}
	return true;
}

bool ComparisonMethodUtils::isAnUserValueRequiredMethod(ComparisonMethod method) {
	if (sharedUserValueRequiredMethods.count(method) == 0) {
		return false;
	}
	return true;
}

} /* END of namespace */
