#include "UserValue.h"

namespace cboy {

UserValue::UserValue()
	:
	_i08(0), _u08(0), _i16(0), _u16(0), _i32(0), _u32(0), _i64(0), _u64(0), _f32(0), _f64(0) {}

UserValue::~UserValue() noexcept {}

UserValue::UserValue(const UserValue&) = default;
UserValue& UserValue::operator =(const UserValue&) = default;

void UserValue::clear() {
	_i08 = 0; _u08 = 0; _i16 = 0; _u16 = 0; _i32 = 0; _u32 = 0; _i64 = 0; _u64 = 0; _f32 = 0; _f64 = 0;
}

} /* END of namespace */
