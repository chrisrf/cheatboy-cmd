#include "ReadValue.h"

#include <PureCppCompatibility.h>

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/MemoryValueTypeIdUtils.h>

namespace cboy {

class ReadValueImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ReadValueImpl);
public:
	MemoryValue _memoryValue;
	uintptr_t _remoteAddress;
	size_t _size;
	MemoryValueTypeID _typeID;

public:
	ReadValueImpl() : _memoryValue(), _remoteAddress(0), _size(0), _typeID(MemoryValueTypeID::UNKNOWN) {}
	virtual ~ReadValueImpl() noexcept {}
};

ReadValue::ReadValue()
	:
	_impl(std::make_unique<ReadValueImpl>()) {}

ReadValue::~ReadValue() noexcept {}

ReadValue::ReadValue(ReadValue&& rhs) = default;
ReadValue& ReadValue::operator =(ReadValue&&) = default;

bool ReadValue::isValidState() const {
	return (_impl != nullptr);
}

uintptr_t ReadValue::getRemoteAddress() const {
	return _impl->_remoteAddress;
}

size_t ReadValue::getSize() const {
	return _impl->_size;
}

MemoryValueTypeID ReadValue::getTypeEnum() const {
	return _impl->_typeID;
}

int ReadValue::getTypeID() const {
	return static_cast<int>(_impl->_typeID);
}

template<typename T>
void ReadValue::wantToReadFor(uintptr_t remoteAddress) {
	_impl->_memoryValue.clear();
	_impl->_remoteAddress = remoteAddress;
	_impl->_size = sizeof(T);
	_impl->_typeID = MemoryValueTypeIdUtils::enumOf<T>();
}
template void ReadValue::wantToReadFor<int8_t>(uintptr_t);
template void ReadValue::wantToReadFor<uint8_t>(uintptr_t);
template void ReadValue::wantToReadFor<int16_t>(uintptr_t);
template void ReadValue::wantToReadFor<uint16_t>(uintptr_t);
template void ReadValue::wantToReadFor<int32_t>(uintptr_t);
template void ReadValue::wantToReadFor<uint32_t>(uintptr_t);
template void ReadValue::wantToReadFor<int64_t>(uintptr_t);
template void ReadValue::wantToReadFor<uint64_t>(uintptr_t);
template void ReadValue::wantToReadFor<float>(uintptr_t);
template void ReadValue::wantToReadFor<double>(uintptr_t);

template<typename T>
T ReadValue::getAs() const {
	return _impl->_memoryValue.getAs<T>();
}
template int8_t ReadValue::getAs() const;
template uint8_t ReadValue::getAs() const;
template int16_t ReadValue::getAs() const;
template uint16_t ReadValue::getAs() const;
template int32_t ReadValue::getAs() const;
template uint32_t ReadValue::getAs() const;
template int64_t ReadValue::getAs() const;
template uint64_t ReadValue::getAs() const;
template float ReadValue::getAs() const;
template double ReadValue::getAs() const;

uint8_t* ReadValue::getBuffer() {
	return _impl->_memoryValue.getBuffer();
}

void ReadValue::clear() {
	_impl->_memoryValue.clear();
	_impl->_remoteAddress = 0;
	_impl->_size = 0;
	_impl->_typeID = MemoryValueTypeID::UNKNOWN;
}

} /* END of namespace */
