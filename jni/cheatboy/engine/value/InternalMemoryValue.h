#ifndef __InternalMemoryValue_H__
#define __InternalMemoryValue_H__

#include <stdint.h>

#include <cheatboy/engine/value/defs/MemoryValueDefs.h>

namespace cboy {

union InternalMemoryValue {
public:
	int8_t		i08;
	uint8_t		u08;
	int16_t		i16;
	uint16_t	u16;
	int32_t		i32;
	uint32_t	u32;
	int64_t		i64;
	uint64_t	u64;
	float		f32;
	double		f64;
	uint8_t		bytes[sizeof(uint64_t)];
	static_assert(sizeof(float) == sizeof(int32_t), "Size of float is NOT 4-bytes.");
	static_assert(sizeof(double) == sizeof(int64_t), "Size of double is NOT 8-bytes.");
	static_assert(sizeof(bytes) == 8, "Size of buffer is NOT 8-bytes.");
};
static_assert(sizeof(InternalMemoryValue) == 8, "Size of internal memory value is NOT 8-bytes.");
static_assert(sizeof(InternalMemoryValue) == MemoryValueDefs::MAX_BYTES, "Size of internal memory value is NOT equal to [MemoryValueDefs::MAX_BYTES].");

} /* END of namespace */

#endif
