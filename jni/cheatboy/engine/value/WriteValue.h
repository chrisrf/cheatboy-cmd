#ifndef __WriteValue_H__
#define __WriteValue_H__

#include <memory>

#include <cheatboy/engine/value/defs/MemoryValueDefs.h>

namespace cboy {
enum class MemoryValueTypeID;
class WriteValueImpl;

class WriteValue {
PURE_DECLARE_CLASS_AS_NONCOPYABLE(WriteValue);
private:
	std::unique_ptr<WriteValueImpl> _impl;

public:
	WriteValue();
	virtual ~WriteValue() noexcept;
	WriteValue(WriteValue&&);
	WriteValue& operator =(WriteValue&&);
public:
	bool isValidState() const;
	uintptr_t getRemoteAddress() const;
	size_t getSize() const;
	MemoryValueTypeID getTypeEnum() const;
	int getTypeID() const;
public:
	template<typename T> void wantToWriteFor(uintptr_t remoteAddress, T value);
	template<typename T> T getAs() const;
public:
	static constexpr size_t getCapacity() { return MemoryValueDefs::MAX_BYTES; }
	uint8_t* getBuffer();
	void clear();
};

} /* END of namespace */

#endif
