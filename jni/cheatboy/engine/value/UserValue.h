#ifndef __UserValue_H__
#define __UserValue_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {

class UserValue {
PURE_DECLARE_CLASS_AS_NONMOVABLE(UserValue);
private:
	int8_t		_i08;
	uint8_t		_u08;
	int16_t		_i16;
	uint16_t	_u16;
	int32_t		_i32;
	uint32_t	_u32;
	int64_t		_i64;
	uint64_t	_u64;
	float		_f32;
	double		_f64;

public:
	UserValue();
	virtual ~UserValue() noexcept;
	UserValue(const UserValue&);
	UserValue& operator =(const UserValue&);
public:
	template<typename T> inline T getFor() const;
	template<typename T> inline void setFor(T value);
public:
	void clear();
};

template<> inline int8_t UserValue::getFor() const { return _i08; }
template<> inline uint8_t UserValue::getFor() const { return _u08; }
template<> inline int16_t UserValue::getFor() const { return _i16; }
template<> inline uint16_t UserValue::getFor() const { return _u16; }
template<> inline int32_t UserValue::getFor() const { return _i32; }
template<> inline uint32_t UserValue::getFor() const { return _u32; }
template<> inline int64_t UserValue::getFor() const { return _i64; }
template<> inline uint64_t UserValue::getFor() const { return _u64; }
template<> inline float UserValue::getFor() const { return _f32; }
template<> inline double UserValue::getFor() const { return _f64; }

template<> inline void UserValue::setFor(int8_t value) { _i08 = value; }
template<> inline void UserValue::setFor(uint8_t value) { _u08 = value; }
template<> inline void UserValue::setFor(int16_t value) { _i16 = value; }
template<> inline void UserValue::setFor(uint16_t value) { _u16 = value; }
template<> inline void UserValue::setFor(int32_t value) { _i32 = value; }
template<> inline void UserValue::setFor(uint32_t value) { _u32 = value; }
template<> inline void UserValue::setFor(int64_t value) { _i64 = value; }
template<> inline void UserValue::setFor(uint64_t value) { _u64 = value; }
template<> inline void UserValue::setFor(float value) { _f32 = value; }
template<> inline void UserValue::setFor(double value) { _f64 = value; }

} /* END of namespace */

#endif
