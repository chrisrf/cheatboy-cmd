#ifndef __MemoryValueTypeFlagUtils_H__
#define __MemoryValueTypeFlagUtils_H__

#include <PureMacro.h>

#include <cheatboy/engine/value/defs/MemoryValueTypeFlagDefs.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>

namespace cboy {

class MemoryValueTypeFlagUtils {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryValueTypeFlagUtils);
private:
	typedef MemoryValueTypeFlagDefs::flag_t flag_t;
private:
	static flag_t sharedFlags[static_cast<int>(MemoryValueTypeID::ENUM_NUMS)];

public:
	static flag_t fromTypeID(int typeID);
	static flag_t fromTypeIdEnum(MemoryValueTypeID typeID);
};

} /* END of namespace */

#endif
