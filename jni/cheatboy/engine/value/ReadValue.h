#ifndef __ReadValue_H__
#define __ReadValue_H__

#include <memory>

#include <cheatboy/engine/value/defs/MemoryValueDefs.h>

namespace cboy {
enum class MemoryValueTypeID;
class ReadValueImpl;

class ReadValue {
PURE_DECLARE_CLASS_AS_NONCOPYABLE(ReadValue);
private:
	std::unique_ptr<ReadValueImpl> _impl;

public:
	ReadValue();
	virtual ~ReadValue() noexcept;
	ReadValue(ReadValue&&);
	ReadValue& operator =(ReadValue&&);
public:
	bool isValidState() const;
	uintptr_t getRemoteAddress() const;
	size_t getSize() const;
	MemoryValueTypeID getTypeEnum() const;
	int getTypeID() const;
public:
	template<typename T> void wantToReadFor(uintptr_t remoteAddress);
	template<typename T> T getAs() const;
public:
	static constexpr size_t getCapacity() { return MemoryValueDefs::MAX_BYTES; }
	uint8_t* getBuffer();
	void clear();
};

} /* END of namespace */

#endif
