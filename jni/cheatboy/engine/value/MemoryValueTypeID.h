#ifndef __MemoryValueTypeID_H__
#define __MemoryValueTypeID_H__

namespace cboy {

enum class MemoryValueTypeID {
	ENUM_MIN = 0,
	// =======
	I08 = 0,
	U08 = 1,
	I16 = 2,
	U16 = 3,
	I32 = 4,
	U32 = 5,
	I64 = 6,
	U64 = 7,
	F32 = 8,
	F64 = 9,
	// =======
	ENUM_MAX = 9,
	ENUM_NUMS = 10,
	// =======
	UNKNOWN = 10,
	ENUM_MAX_PLUS_UNKNOWN = 10,
	ENUM_NUMS_PLUS_UNKNOWN = 11
};

} /* END of namespace */

#endif
