#ifndef __MemoryValueTypeFlagDefs_H__
#define __MemoryValueTypeFlagDefs_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {

class MemoryValueTypeFlagDefs {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryValueTypeFlagDefs);
public:
	typedef uint16_t flag_t;
};

} /* END of namespace */

#endif
