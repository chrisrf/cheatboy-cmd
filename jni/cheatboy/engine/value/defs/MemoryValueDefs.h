#ifndef __MemoryValueDefs_H__
#define __MemoryValueDefs_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {

class MemoryValueDefs {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryValueDefs);
public:
	enum: size_t {
		MAX_BYTES = 8
	};
};

} /* END of namespace */

#endif
