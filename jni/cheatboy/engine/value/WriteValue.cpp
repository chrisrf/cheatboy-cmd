#include "WriteValue.h"

#include <PureCppCompatibility.h>

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/MemoryValueTypeIdUtils.h>

namespace cboy {

class WriteValueImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(WriteValueImpl);
public:
	MemoryValue _memoryValue;
	uintptr_t _remoteAddress;
	size_t _size;
	MemoryValueTypeID _typeID;

public:
	WriteValueImpl() : _memoryValue(), _remoteAddress(0), _size(0), _typeID(MemoryValueTypeID::UNKNOWN) {}
	virtual ~WriteValueImpl() noexcept {}
};

WriteValue::WriteValue()
	:
	_impl(std::make_unique<WriteValueImpl>()) {}

WriteValue::~WriteValue() noexcept {}

WriteValue::WriteValue(WriteValue&&) = default;
WriteValue& WriteValue::operator =(WriteValue&&) = default;

bool WriteValue::isValidState() const {
	return (_impl != nullptr);
}

uintptr_t WriteValue::getRemoteAddress() const {
	return _impl->_remoteAddress;
}

size_t WriteValue::getSize() const {
	return _impl->_size;
}

MemoryValueTypeID WriteValue::getTypeEnum() const {
	return _impl->_typeID;
}

int WriteValue::getTypeID() const {
	return static_cast<int>(_impl->_typeID);
}

template<typename T>
void WriteValue::wantToWriteFor(uintptr_t remoteAddress, T value) {
	_impl->_memoryValue.setAs<T>(value);
	_impl->_remoteAddress = remoteAddress;
	_impl->_size = sizeof(value);
	_impl->_typeID = MemoryValueTypeIdUtils::enumOf<T>();
}
template void WriteValue::wantToWriteFor(uintptr_t, int8_t);
template void WriteValue::wantToWriteFor(uintptr_t, uint8_t);
template void WriteValue::wantToWriteFor(uintptr_t, int16_t);
template void WriteValue::wantToWriteFor(uintptr_t, uint16_t);
template void WriteValue::wantToWriteFor(uintptr_t, int32_t);
template void WriteValue::wantToWriteFor(uintptr_t, uint32_t);
template void WriteValue::wantToWriteFor(uintptr_t, int64_t);
template void WriteValue::wantToWriteFor(uintptr_t, uint64_t);
template void WriteValue::wantToWriteFor(uintptr_t, float);
template void WriteValue::wantToWriteFor(uintptr_t, double);

template<typename T>
T WriteValue::getAs() const {
	return _impl->_memoryValue.getAs<T>();
}
template int8_t WriteValue::getAs() const;
template uint8_t WriteValue::getAs() const;
template int16_t WriteValue::getAs() const;
template uint16_t WriteValue::getAs() const;
template int32_t WriteValue::getAs() const;
template uint32_t WriteValue::getAs() const;
template int64_t WriteValue::getAs() const;
template uint64_t WriteValue::getAs() const;
template float WriteValue::getAs() const;
template double WriteValue::getAs() const;

uint8_t* WriteValue::getBuffer() {
	return _impl->_memoryValue.getBuffer();
}

void WriteValue::clear() {
	_impl->_memoryValue.clear();
	_impl->_remoteAddress = 0;
	_impl->_size = 0;
	_impl->_typeID = MemoryValueTypeID::UNKNOWN;
}

} /* END of namespace */
