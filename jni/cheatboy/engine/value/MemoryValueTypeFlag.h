#ifndef __MemoryValueTypeFlag_H__
#define __MemoryValueTypeFlag_H__

#include <stdint.h>
#include <assert.h>
#include <PureMacro.h>

#include <cheatboy/engine/value/defs/MemoryValueTypeFlagDefs.h>

namespace cboy {

class MemoryValueTypeFlag {
private:
	typedef MemoryValueTypeFlagDefs::flag_t flag_t;
public:
	enum: flag_t {
		I08 = 1,
		U08 = 2,
		I16 = 4,
		U16 = 8,
		I32 = 16,
		U32 = 32,
		I64 = 64,
		U64 = 128,
		F32 = 256,
		F64 = 512,
		ALL_SIGNED_INTEGER = 85,
		ALL_UNSIGNED_INTEGER = 170,
		ALL_INTEGER = 255,
		ALL_FLOAT = 768,
		ALL_SIGNED_NUMBER = 853,
		ALL = 1023
	};
private:
	flag_t _flag;

public:
	static inline flag_t validate(flag_t flag) { return (flag & ALL); }
	static inline bool isValidate(flag_t flag) { return ((flag & (flag_t)(~ALL)) == 0); }
public:
	inline MemoryValueTypeFlag() : _flag(0) {};
	inline MemoryValueTypeFlag(flag_t flag) : _flag(validate(flag)) {};
	inline virtual ~MemoryValueTypeFlag() noexcept {};
	inline MemoryValueTypeFlag(const MemoryValueTypeFlag&) = default;
	inline MemoryValueTypeFlag& operator =(const MemoryValueTypeFlag&) = default;
	inline MemoryValueTypeFlag(MemoryValueTypeFlag&&) = default;
	inline MemoryValueTypeFlag& operator =(MemoryValueTypeFlag&&) = default;
public:
	inline flag_t getFlag() const { return _flag; }
	inline void setFlag(flag_t flag) { _flag = validate(flag); }
	inline void clearFlag() { _flag = 0; }
	inline bool isMarkedForAny() const { return (_flag != 0); }
public:
	template<typename T> inline void markFor();
	template<typename T> inline void unmarkFor();
	template<typename T> inline bool isMarkedFor() const;
	template<typename T> inline bool isMarkedOnlyFor() const;
public:
	inline void mark(flag_t flag) { assert(isValidate(flag)); markBy(flag); }
	inline void unmark(flag_t flag) { assert(isValidate(flag)); unmarkBy(flag); }
	inline bool isMarked(flag_t flag) const { assert(isValidate(flag)); return isMarkedBy(flag); }
	inline bool isMarkedOnly(flag_t flag) const { assert(isValidate(flag)); return isMarkedOnlyBy(flag); }
private:
	inline void markBy(flag_t flag) { _flag |= flag; }
	inline void unmarkBy(flag_t flag) { _flag &= (flag_t)(~flag); }
	inline bool isMarkedBy(flag_t flag) const { return ((_flag & flag) == flag); }
	inline bool isMarkedOnlyBy(flag_t flag) const { return (_flag == flag); }
};

template<> inline void MemoryValueTypeFlag::markFor<int8_t>() { markBy(I08); }
template<> inline void MemoryValueTypeFlag::markFor<uint8_t>() { markBy(U08); }
template<> inline void MemoryValueTypeFlag::markFor<int16_t>() { markBy(I16); }
template<> inline void MemoryValueTypeFlag::markFor<uint16_t>() { markBy(U16); }
template<> inline void MemoryValueTypeFlag::markFor<int32_t>() { markBy(I32); }
template<> inline void MemoryValueTypeFlag::markFor<uint32_t>() { markBy(U32); }
template<> inline void MemoryValueTypeFlag::markFor<int64_t>() { markBy(I64); }
template<> inline void MemoryValueTypeFlag::markFor<uint64_t>() { markBy(U64); }
template<> inline void MemoryValueTypeFlag::markFor<float>() { markBy(F32); }
template<> inline void MemoryValueTypeFlag::markFor<double>() { markBy(F64); }

template<> inline void MemoryValueTypeFlag::unmarkFor<int8_t>() { unmarkBy(I08); }
template<> inline void MemoryValueTypeFlag::unmarkFor<uint8_t>() { unmarkBy(U08); }
template<> inline void MemoryValueTypeFlag::unmarkFor<int16_t>() { unmarkBy(I16); }
template<> inline void MemoryValueTypeFlag::unmarkFor<uint16_t>() { unmarkBy(U16); }
template<> inline void MemoryValueTypeFlag::unmarkFor<int32_t>() { unmarkBy(I32); }
template<> inline void MemoryValueTypeFlag::unmarkFor<uint32_t>() { unmarkBy(U32); }
template<> inline void MemoryValueTypeFlag::unmarkFor<int64_t>() { unmarkBy(I64); }
template<> inline void MemoryValueTypeFlag::unmarkFor<uint64_t>() { unmarkBy(U64); }
template<> inline void MemoryValueTypeFlag::unmarkFor<float>() { unmarkBy(F32); }
template<> inline void MemoryValueTypeFlag::unmarkFor<double>() { unmarkBy(F64); }

template<> inline bool MemoryValueTypeFlag::isMarkedFor<int8_t>() const { return isMarkedBy(I08); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<uint8_t>() const { return isMarkedBy(U08); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<int16_t>() const { return isMarkedBy(I16); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<uint16_t>() const { return isMarkedBy(U16); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<int32_t>() const { return isMarkedBy(I32); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<uint32_t>() const { return isMarkedBy(U32); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<int64_t>() const { return isMarkedBy(I64); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<uint64_t>() const { return isMarkedBy(U64); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<float>() const { return isMarkedBy(F32); }
template<> inline bool MemoryValueTypeFlag::isMarkedFor<double>() const { return isMarkedBy(F64); }

template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<int8_t>() const { return isMarkedOnlyBy(I08); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<uint8_t>() const { return isMarkedOnlyBy(U08); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<int16_t>() const { return isMarkedOnlyBy(I16); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<uint16_t>() const { return isMarkedOnlyBy(U16); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<int32_t>() const { return isMarkedOnlyBy(I32); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<uint32_t>() const { return isMarkedOnlyBy(U32); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<int64_t>() const { return isMarkedOnlyBy(I64); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<uint64_t>() const { return isMarkedOnlyBy(U64); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<float>() const { return isMarkedOnlyBy(F32); }
template<> inline bool MemoryValueTypeFlag::isMarkedOnlyFor<double>() const { return isMarkedOnlyBy(F64); }

} /* END of namespace */

#endif
