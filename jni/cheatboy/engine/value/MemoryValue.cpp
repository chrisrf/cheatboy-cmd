#include "MemoryValue.h"

namespace cboy {

MemoryValue::MemoryValue() {
	clear();
}

MemoryValue::~MemoryValue() noexcept {}

MemoryValue::MemoryValue(const MemoryValue&) = default;
MemoryValue& MemoryValue::operator =(const MemoryValue&) = default;
MemoryValue::MemoryValue(MemoryValue&&) = default;
MemoryValue& MemoryValue::operator =(MemoryValue&&) = default;

} /* END of namespace */
