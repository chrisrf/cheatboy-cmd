#ifndef __MemoryValue_H__
#define __MemoryValue_H__

#include <string.h>
#include <PureMacro.h>

#include <cheatboy/engine/value/InternalMemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTraits.h>
#include <cheatboy/engine/value/defs/MemoryValueDefs.h>

#define __CBOY_MEMORY_VALUE_PERFORMANCE_FIX__

namespace cboy {

class MemoryValue {
private:
#ifdef __CBOY_MEMORY_VALUE_PERFORMANCE_FIX__
	InternalMemoryValue _memoryValue;
#else
	mutable InternalMemoryValue _memoryValue;
#endif

public:
	MemoryValue();
	virtual ~MemoryValue();
	MemoryValue(const MemoryValue&);
	MemoryValue& operator =(const MemoryValue&);
	MemoryValue(MemoryValue&&);
	MemoryValue& operator =(MemoryValue&&);
public:
	inline static constexpr size_t getCapacity() { return MemoryValueDefs::MAX_BYTES; }
	inline uint8_t* getBuffer() { return _memoryValue.bytes; }
	inline void clear() { memset(_memoryValue.bytes, 0, sizeof(_memoryValue.bytes)); }
public:
#ifdef __CBOY_MEMORY_VALUE_PERFORMANCE_FIX__
	// XXX: Use full template specialization to make a template constraint.
	template<typename T> inline T getAs() const;
#else
	template<typename T, typename = typename std::enable_if<cboy::is_memory_value<T>::value>::type>
	inline T getAs() const {
		return *reinterpret_cast<T*>(_memoryValue.bytes);
	}
#endif
	// XXX: Use SFINAE to make a template constraint.
	template<typename T, typename = typename std::enable_if<cboy::is_memory_value<T>::value>::type>
	inline void setAs(T value) {
		clear();
		memcpy(_memoryValue.bytes, &value, sizeof(value));
	}
};
//static_assert(sizeof(MemoryValue) == sizeof(InternalMemoryValue), "Size of memory value is NOT equal to size of internal memory value.");

#ifdef __CBOY_MEMORY_VALUE_PERFORMANCE_FIX__
template<> inline int8_t MemoryValue::getAs() const { return _memoryValue.i08; }
template<> inline uint8_t MemoryValue::getAs() const { return _memoryValue.u08; }
template<> inline int16_t MemoryValue::getAs() const { return _memoryValue.i16; }
template<> inline uint16_t MemoryValue::getAs() const { return _memoryValue.u16; }
template<> inline int32_t MemoryValue::getAs() const { return _memoryValue.i32; }
template<> inline uint32_t MemoryValue::getAs() const { return _memoryValue.u32; }
template<> inline int64_t MemoryValue::getAs() const { return _memoryValue.i64; }
template<> inline uint64_t MemoryValue::getAs() const { return _memoryValue.u64; }
template<> inline float MemoryValue::getAs() const { return _memoryValue.f32; }
template<> inline double MemoryValue::getAs() const { return _memoryValue.f64; }
#endif

} /* END of namespace */

#endif
