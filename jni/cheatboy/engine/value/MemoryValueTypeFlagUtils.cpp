#include "MemoryValueTypeFlagUtils.h"

#include <PureLib.h>

#include <cheatboy/engine/value/MemoryValueTypeFlag.h>

namespace cboy {

MemoryValueTypeFlagUtils::flag_t MemoryValueTypeFlagUtils::sharedFlags[] = {
		MemoryValueTypeFlag::I08,
		MemoryValueTypeFlag::U08,
		MemoryValueTypeFlag::I16,
		MemoryValueTypeFlag::U16,
		MemoryValueTypeFlag::I32,
		MemoryValueTypeFlag::U32,
		MemoryValueTypeFlag::I64,
		MemoryValueTypeFlag::U64,
		MemoryValueTypeFlag::F32,
		MemoryValueTypeFlag::F64
};

MemoryValueTypeFlagUtils::flag_t MemoryValueTypeFlagUtils::fromTypeID(int typeID) {
	if (!pure::EnumUtils<MemoryValueTypeID>::isValid(typeID)) {
		PURE_THROW_EXCEPTION(pure::fmt("Invalid MemoryValueTypeID: [{0}]", typeID));
	}
	return sharedFlags[typeID];
}

MemoryValueTypeFlagUtils::flag_t MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID typeID) {
	if (!pure::EnumUtils<MemoryValueTypeID>::isValid(typeID)) {
		PURE_THROW_EXCEPTION(pure::fmt("Invalid MemoryValueTypeID: [{0}]", static_cast<int>(typeID)));
	}
	return sharedFlags[static_cast<int>(typeID)];
}

} /* END of namespace */
