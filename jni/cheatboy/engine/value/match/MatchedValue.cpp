#include "MatchedValue.h"

#include <assert.h>
#include <PureCppCompatibility.h>

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>

namespace cboy {

class MatchedValueImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(MatchedValueImpl);
private:
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef ScannedDataDefs::match_id_t match_id_t;
public:
	region_id_t _regionID;
	match_id_t _matchID;
	MemoryValue _memoryValue;
	uintptr_t _remoteAddress;
	size_t _size;
	MemoryValueTypeFlag _matchFlag;

public:
	MatchedValueImpl()
		: _regionID(0), _matchID(0), _memoryValue(), _remoteAddress(0), _size(0), _matchFlag() {}
	virtual ~MatchedValueImpl() noexcept {}
};

MatchedValue::MatchedValue()
	:
	_impl(std::make_unique<MatchedValueImpl>()) {}

MatchedValue::~MatchedValue() noexcept {}

MatchedValue::MatchedValue(MatchedValue&&) = default;
MatchedValue& MatchedValue::operator =(MatchedValue&&) = default;

bool MatchedValue::isValidState() const {
	return (_impl != nullptr);
}

MatchedValue::region_id_t MatchedValue::getRegionID() const {
	return _impl->_regionID;
}

MatchedValue::match_id_t MatchedValue::getMatchID() const {
	return _impl->_matchID;
}

uintptr_t MatchedValue::getRemoteAddress() const {
	return _impl->_remoteAddress;
}

size_t MatchedValue::getSize() const {
	return _impl->_size;
}

const MemoryValueTypeFlag& MatchedValue::getMatchFlag() const {
	return _impl->_matchFlag;
}

template<typename T>
T MatchedValue::getAs() const {
	assert(_impl->_matchFlag.isMarkedFor<T>());
	return _impl->_memoryValue.getAs<T>();
}
template int8_t MatchedValue::getAs() const;
template uint8_t MatchedValue::getAs() const;
template int16_t MatchedValue::getAs() const;
template uint16_t MatchedValue::getAs() const;
template int32_t MatchedValue::getAs() const;
template uint32_t MatchedValue::getAs() const;
template int64_t MatchedValue::getAs() const;
template uint64_t MatchedValue::getAs() const;
template float MatchedValue::getAs() const;
template double MatchedValue::getAs() const;

uint8_t* MatchedValue::getBuffer() {
	return _impl->_memoryValue.getBuffer();
}

void MatchedValue::clear() {
	_impl->_regionID = 0;
	_impl->_matchID = 0;
	_impl->_memoryValue.clear();
	_impl->_remoteAddress = 0;
	_impl->_size = 0;
	_impl->_matchFlag.clearFlag();
}

void MatchedValue::copyTo(MatchedValue& value) const {
	assert(getSize() <= MatchedValue::getCapacity());
	value.setRegionID(getRegionID());
	value.setMatchID(getMatchID());
	memcpy(value.getBuffer(), _impl->_memoryValue.getBuffer(), getSize());
	value.setRemoteAddress(getRemoteAddress());
	value.setSize(getSize());
	value.setMatchFlag(getMatchFlag());
}

void MatchedValue::setRegionID(region_id_t regionID) {
	_impl->_regionID = regionID;
}

void MatchedValue::setMatchID(match_id_t matchID) {
	_impl->_matchID = matchID;
}

void MatchedValue::setRemoteAddress(uintptr_t remoteAddress) {
	_impl->_remoteAddress = remoteAddress;
}

void MatchedValue::setSize(size_t size) {
	_impl->_size = size;
}

void MatchedValue::setMatchFlag(const MemoryValueTypeFlag& matchFlag) {
	_impl->_matchFlag = matchFlag;
}

} /* END of namespace */
