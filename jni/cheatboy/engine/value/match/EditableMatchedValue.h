#ifndef __EditableMatchedValue_H__
#define __EditableMatchedValue_H__

#include <memory>

#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>
#include <cheatboy/engine/cheater/default/data/defs/ScannedDataDefs.h>
#include <cheatboy/engine/value/defs/MemoryValueDefs.h>

namespace cboy {
enum class MemoryValueTypeID;
class EditableMatchedValueImpl;

class EditableMatchedValue {
PURE_DECLARE_CLASS_AS_NONCOPYABLE(EditableMatchedValue);
private:
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef ScannedDataDefs::match_id_t match_id_t;
private:
	std::unique_ptr<EditableMatchedValueImpl> _impl;

public:
	EditableMatchedValue();
	virtual ~EditableMatchedValue() noexcept;
	EditableMatchedValue(EditableMatchedValue&&);
	EditableMatchedValue& operator =(EditableMatchedValue&&);
public:
	bool isValidState() const;
	region_id_t getRegionID() const;
	match_id_t getMatchID() const;
	size_t getSize() const;
	MemoryValueTypeID getTypeEnum() const;
	int getTypeID() const;
public:
	template<typename T> void wantToEditFor(region_id_t regioID, match_id_t matchID, T value);
public:
	static constexpr size_t getCapacity() { return MemoryValueDefs::MAX_BYTES; }
	uint8_t* getBuffer();
	void clear();
};

} /* END of namespace */

#endif
