#ifndef __MatchedValue_H__
#define __MatchedValue_H__

#include <memory>

#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>
#include <cheatboy/engine/cheater/default/data/defs/ScannedDataDefs.h>
#include <cheatboy/engine/value/defs/MemoryValueDefs.h>

namespace cboy {
class MemoryValueTypeFlag;
class MatchedValueImpl;

class MatchedValue {
PURE_DECLARE_CLASS_AS_NONCOPYABLE(MatchedValue);
private:
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef ScannedDataDefs::match_id_t match_id_t;
private:
	std::unique_ptr<MatchedValueImpl> _impl;

public:
	MatchedValue();
	virtual ~MatchedValue() noexcept;
	MatchedValue(MatchedValue&&);
	MatchedValue& operator =(MatchedValue&&);
public:
	bool isValidState() const;
	region_id_t getRegionID() const;
	match_id_t getMatchID() const;
	uintptr_t getRemoteAddress() const;
	size_t getSize() const;
	const MemoryValueTypeFlag& getMatchFlag() const;
public:
	template<typename T> T getAs() const;
public:
	static constexpr size_t getCapacity() { return MemoryValueDefs::MAX_BYTES; }
	uint8_t* getBuffer();
	void clear();
public:
	void copyTo(MatchedValue& value) const;
public:
	void setRegionID(region_id_t regionID);
	void setMatchID(match_id_t matchID);
	void setRemoteAddress(uintptr_t remoteAddress);
	void setSize(size_t size);
	void setMatchFlag(const MemoryValueTypeFlag& matchFlag);
};

} /* END of namespace */

#endif
