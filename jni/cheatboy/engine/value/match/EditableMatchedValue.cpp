#include "EditableMatchedValue.h"

#include <PureCppCompatibility.h>

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/MemoryValueTypeIdUtils.h>

namespace cboy {

class EditableMatchedValueImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(EditableMatchedValueImpl);
private:
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef ScannedDataDefs::match_id_t match_id_t;
public:
	region_id_t _regionID;
	match_id_t _matchID;
	MemoryValue _memoryValue;
	size_t _size;
	MemoryValueTypeID _typeID;

public:
	EditableMatchedValueImpl(): _regionID(0), _matchID(0), _memoryValue(), _size(0), _typeID(MemoryValueTypeID::UNKNOWN) {}
	virtual ~EditableMatchedValueImpl() noexcept {}
};

EditableMatchedValue::EditableMatchedValue()
	:
	_impl(std::make_unique<EditableMatchedValueImpl>()) {}

EditableMatchedValue::~EditableMatchedValue() noexcept {}

EditableMatchedValue::EditableMatchedValue(EditableMatchedValue&&) = default;
EditableMatchedValue& EditableMatchedValue::operator =(EditableMatchedValue&&) = default;

bool EditableMatchedValue::isValidState() const {
	return (_impl != nullptr);
}

EditableMatchedValue::region_id_t EditableMatchedValue::getRegionID() const {
	return _impl->_regionID;
}

EditableMatchedValue::match_id_t EditableMatchedValue::getMatchID() const {
	return _impl->_matchID;
}

size_t EditableMatchedValue::getSize() const {
	return _impl->_size;
}

MemoryValueTypeID EditableMatchedValue::getTypeEnum() const {
	return _impl->_typeID;
}

int EditableMatchedValue::getTypeID() const {
	return static_cast<int>(_impl->_typeID);
}

template<typename T>
void EditableMatchedValue::wantToEditFor(region_id_t regioID, match_id_t matchID, T value) {
	_impl->_regionID = regioID;
	_impl->_matchID = matchID;
	_impl->_memoryValue.setAs<T>(value);
	_impl->_size = sizeof(value);
	_impl->_typeID = MemoryValueTypeIdUtils::enumOf<T>();
}
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, int8_t);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, uint8_t);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, int16_t);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, uint16_t);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, int32_t);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, uint32_t);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, int64_t);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, uint64_t);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, float);
template void EditableMatchedValue::wantToEditFor(region_id_t, match_id_t, double);

uint8_t* EditableMatchedValue::getBuffer() {
	return _impl->_memoryValue.getBuffer();
}

void EditableMatchedValue::clear() {
	_impl->_regionID = 0;
	_impl->_matchID = 0;
	_impl->_memoryValue.clear();
	_impl->_size = 0;
	_impl->_typeID = MemoryValueTypeID::UNKNOWN;
}

} /* END of namespace */
