#ifndef __ICheatTerminal_H__
#define __ICheatTerminal_H__

#include <stdint.h>
#include <PureMacro.h>

namespace cboy {

class ICheatTerminal {
PURE_DECLARE_CLASS_AS_INTERFACE(ICheatTerminal);
public:
	virtual int32_t run() = 0;
	virtual int32_t stop() = 0;
};

} /* END of namespace */

#endif
