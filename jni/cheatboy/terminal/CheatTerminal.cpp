#include "CheatTerminal.h"

#include <PureCppCompatibility.h>
#include <PureLib.h>
#include <CheatEngineLib.h>

#include <cheatboy/terminal/Constants.h>
#include <cheatboy/terminal/cmd/base/ITerminalCommand.h>
#include <cheatboy/terminal/cmd/ExitCommand.h>
#include <cheatboy/terminal/cmd/SetPidCommand.h>
#include <cheatboy/terminal/cmd/AttachCommand.h>
#include <cheatboy/terminal/cmd/DetachCommand.h>
#include <cheatboy/terminal/cmd/ReadValueCommand.h>
#include <cheatboy/terminal/cmd/WriteValueCommand.h>
#include <cheatboy/terminal/cmd/ConfigEngineCommand.h>
#include <cheatboy/terminal/cmd/ListRegionsCommand.h>
#include <cheatboy/terminal/cmd/LoadRegionsCommand.h>
#include <cheatboy/terminal/cmd/DeleteRegionsCommand.h>
#include <cheatboy/terminal/cmd/ListCandidatesCommand.h>
#include <cheatboy/terminal/cmd/EditCandidateCommand.h>
#include <cheatboy/terminal/cmd/ScanValueCommand.h>
#include <cheatboy/terminal/cmd/TestCommand.h>
#include <cheatboy/terminal/reader/ITerminalReader.h>
#include <cheatboy/terminal/reader/TerminalReader.h>

namespace cboy {

TunnelTerminal::TunnelTerminal()
	:
	_keepRunning(false),
	_cheatEngine(CheatEngineFactory::createCheatEngine()),
	_terminalReader(std::make_unique<TerminalReader>()) {

	buildCommands();
	_terminalReader->setCommandMatcher(this);
}

TunnelTerminal::~TunnelTerminal() {
	_userIndexes.clear();
	_indexedCmds.clear();
	_terminalCmds.clear();
}

int32_t TunnelTerminal::run() {
	if (_keepRunning) { return 0; }
	_keepRunning = true;

	std::string attachState = "=>";
	std::string detachState = "  ";
	std::string prompt;
	while (_keepRunning) {
		const std::string& currentState = (_cheatEngine->hasAttachedProcess() ? attachState : detachState);
		prompt = pure::fmt("[({0}){1}({2}:{3})] ",
			Constants::LC_APP_NAME, currentState, _cheatEngine->getPid(), _cheatEngine->getNumOfCurrentMatchedValues());

		std::string cmdLine = _terminalReader->readCommandLine(prompt);
		try {
			executeCommand(cmdLine);
		} catch (...) {
			pure::printExceptionInCatchBlock(__PURE_FILE__, __LINE__);
		}
	}
	return 0;
}

int32_t TunnelTerminal::stop() {
	_keepRunning = false;
	return 0;
}

size_t TunnelTerminal::getNumberOfCommands() const {
	return _userIndexes.size();
}

const std::string& TunnelTerminal::findMatchedCommand(const std::string& text, uint32_t& searchIndex) const {
	static const std::string& nullCmdName("");
	while (searchIndex < _userIndexes.size()) {
		const std::string& indexName = _userIndexes[searchIndex];

		searchIndex++;
		if (indexName.compare(0, text.length(), text) == 0) {
			return indexName;
		}
	}
	return nullCmdName;
}

void TunnelTerminal::buildCommands() {
	// XXX: Add commands here
	addCommand(std::make_unique<ExitCommand>(), true, { "quit", "q" });
	addCommand(std::make_unique<SetPidCommand>(), true, "id");
	addCommand(std::make_unique<AttachCommand>(), true, "attach");
	addCommand(std::make_unique<DetachCommand>(), true, "detach");
	addCommand(std::make_unique<ReadValueCommand>(), true, "read");
	addCommand(std::make_unique<WriteValueCommand>(), true, "write");
	addCommand(std::make_unique<ConfigEngineCommand>(), true, "econfig");
	addCommand(std::make_unique<ListRegionsCommand>(), true, "pregions");
	addCommand(std::make_unique<LoadRegionsCommand>(), true, "lregions");
	addCommand(std::make_unique<DeleteRegionsCommand>(), true, "fregions");
	addCommand(std::make_unique<ListCandidatesCommand>(), true, "matches");
	addCommand(std::make_unique<EditCandidateCommand>(), true, "set");
	addCommand(std::make_unique<ScanValueCommand>(), true, ScanValueCommand::CMD_INDEXES);
	addCommand(std::make_unique<TestCommand>(), true, "test");
}

void TunnelTerminal::addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, const char* index) {
	indexCommand(*cmd, isUserCmd, index);
	_terminalCmds.emplace_back(std::move(cmd));
}

void TunnelTerminal::addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, std::initializer_list<const char*>&& indexes) {
	for (const auto& index : indexes) {
		indexCommand(*cmd, isUserCmd, index);
	}
	_terminalCmds.emplace_back(std::move(cmd));
}

void TunnelTerminal::addCommand(std::unique_ptr<ITerminalCommand>&& cmd, bool isUserCmd, const std::vector<std::string>& indexes) {
	for (const auto& index : indexes) {
		indexCommand(*cmd, isUserCmd, index.c_str());
	}
	_terminalCmds.emplace_back(std::move(cmd));
}

void TunnelTerminal::indexCommand(ITerminalCommand& cmd, bool isUserCmd, const char* index) {
	_indexedCmds.emplace(index, cmd);
	if (isUserCmd) {
		_userIndexes.emplace_back(index);
	}
}

int32_t TunnelTerminal::executeCommand(const std::string& cmdLine) {
	std::stringstream ss(cmdLine);
	std::string cmd;
	std::string argsLine;
	ss >> cmd;
	std::getline(ss, argsLine);

	pure::println("Want to execute the command: [{0}] with arguments[{1}]", cmd, argsLine);
	const auto& it = _indexedCmds.find(cmd);
	if (it != _indexedCmds.end()) {
		ITerminalCommand& target = (it->second);
		target.execute(*_cheatEngine, cmd, argsLine);
	} else {
		pure::println("Command: [{0}] not found", cmd);
	}
	return 0;
}

} /* END of namespace */
