#include "LoadRegionsCommand.h"

#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string LoadRegionsCommand::NAME = "LoadRegionsCommand";
const std::string LoadRegionsCommand::DIGEST = "LoadRegionsCommand";
const std::string LoadRegionsCommand::DOCUMENT = "LoadRegionsCommand";

LoadRegionsCommand::LoadRegionsCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

LoadRegionsCommand::~LoadRegionsCommand() {}

int32_t LoadRegionsCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (cheatEngine.getPid() == EngineConstants::NULL_PROCESS_ID) {
		pure::println("Please set pid first");
		return 0;
	}

	pure::println("Loading memory regions...");
	cheatEngine.loadRegions();
	pure::println("Loading memory regions...Done");
	return 0;
}

} /* END of namespace */
