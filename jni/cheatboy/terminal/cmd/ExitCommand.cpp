#include "ExitCommand.h"

#include <PureLib.h>

#include <cheatboy/terminal/ICheatTerminal.h>

namespace cboy {

const std::string ExitCommand::NAME = "ExitCommand";
const std::string ExitCommand::DIGEST = "ExitCommand";
const std::string ExitCommand::DOCUMENT = "ExitCommand";

ExitCommand::ExitCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

ExitCommand::~ExitCommand() {}

int32_t ExitCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	pure::Singleton<ICheatTerminal>::getInstance().stop();
	return 0;
}

} /* END of namespace */
