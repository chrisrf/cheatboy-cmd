#include "ReadValueCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string ReadValueCommand::NAME = "ReadValueCommand";
const std::string ReadValueCommand::DIGEST = "ReadValueCommand";
const std::string ReadValueCommand::DOCUMENT = "ReadValueCommand";

ReadValueCommand::ReadValueCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

ReadValueCommand::~ReadValueCommand() {}

int32_t ReadValueCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (cheatEngine.getPid() == EngineConstants::NULL_PROCESS_ID) {
		pure::println("Please set pid first");
		return 0;
	}

	std::stringstream ss(argsLine);
	std::string addrString;
	std::string type;
	if ((!(ss >> addrString)) || (!(ss >> type))) {
		pure::println("Need 2 arguments!!!");
		return 0;
	}
	if (type.size() < 2) {
		handleErrorOfParsingTypeOfValue();
		return 0;
	}
	std::string typeLenString = type.substr(1, type.size() - 1);

	pure::HexNumberConverter hexNumConverter;
	uintptr_t addr = 0;
	if (!hexNumConverter.convert(addrString, addr)) {
		pure::println("Failed to parse address!!!");
		return 0;
	}

	pure::NumberConverter numConverter;
	int typeLen = 0;
	if (!numConverter.convert(typeLenString, typeLen)) {
		handleErrorOfParsingTypeOfValue();
		return 0;
	}

	if (type[0] == 'i') {
		if (typeLen == 8) {
			return readValueAndPrintForInt08<int8_t>(cheatEngine, addr);
		} else if (typeLen == 16) {
			return readValueAndPrint<int16_t>(cheatEngine, addr);
		} else if (typeLen == 32) {
			return readValueAndPrint<int32_t>(cheatEngine, addr);
		} else if (typeLen == 64) {
			return readValueAndPrint<int64_t>(cheatEngine, addr);
		}
	} else if (type[0] == 'u') {
		if (typeLen == 8) {
			return readValueAndPrintForInt08<uint8_t>(cheatEngine, addr);
		} else if (typeLen == 16) {
			return readValueAndPrint<uint16_t>(cheatEngine, addr);
		} else if (typeLen == 32) {
			return readValueAndPrint<uint32_t>(cheatEngine, addr);
		} else if (typeLen == 64) {
			return readValueAndPrint<uint64_t>(cheatEngine, addr);
		}
	} else if (type[0] == 'f') {
		if (typeLen == 32) {
			return readValueAndPrint<float>(cheatEngine, addr);
		} else if (typeLen == 64) {
			return readValueAndPrint<double>(cheatEngine, addr);
		}
	}
	handleErrorOfParsingTypeOfValue();
	return 0;
}

void ReadValueCommand::handleErrorOfParsingTypeOfValue() {
	pure::println("Failed to parse type of value!!!");
}

template<typename T>
int32_t ReadValueCommand::readValueAndPrint(ICheatEngine& cheatEngine, uintptr_t addr) {
	void* ptr = pure::PointerUtils::toPointer(addr);
	ReadValue readValue;
	readValue.wantToReadFor<T>(addr);
	cheatEngine.readValue(readValue);
	std::string line = pure::MemoryUtils::readableLineBytesToString(readValue.getBuffer(), readValue.getCapacity(), false);
	pure::println("Read a value[{0}] at [{1}] => {2}", readValue.getAs<T>(), ptr, line);
	return 0;
}

template<typename T>
int32_t ReadValueCommand::readValueAndPrintForInt08(ICheatEngine& cheatEngine, uintptr_t addr) {
	void* ptr = pure::PointerUtils::toPointer(addr);
	ReadValue readValue;
	readValue.wantToReadFor<T>(addr);
	cheatEngine.readValue(readValue);
	std::string line = pure::MemoryUtils::readableLineBytesToString(readValue.getBuffer(), readValue.getCapacity(), false);
	pure::println("Read a value[{0}] at [{1}] => {2}", (int32_t)readValue.getAs<T>(), ptr, line);
	return 0;
}

} /* END of namespace */
