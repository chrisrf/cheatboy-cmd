#ifndef __SetPidCommand_H__
#define __SetPidCommand_H__

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace cboy {

class SetPidCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(SetPidCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	SetPidCommand();
	virtual ~SetPidCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
};

} /* END of namespace */

#endif
