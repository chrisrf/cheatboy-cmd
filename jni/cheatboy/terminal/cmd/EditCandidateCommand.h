#ifndef __EditCandidateCommand_H__
#define __EditCandidateCommand_H__

#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>
#include <cheatboy/engine/cheater/default/data/defs/ScannedDataDefs.h>

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace pure {
class NumberConverter;
}

namespace cboy {

class EditCandidateCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(EditCandidateCommand);
private:
	typedef MemoryRegionDefs::region_id_t region_id_t;
	typedef ScannedDataDefs::match_id_t match_id_t;
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	EditCandidateCommand();
	virtual ~EditCandidateCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
private:
	void handleErrorOfParsingTypeOfValue();
	void handleErrorOfParsingRegionID();
	void handleErrorOfParsingMatchID();
	void handleErrorOfParsingValue();
	template<typename T> int32_t editCandidateAndPrint(
		ICheatEngine& cheatEngine, region_id_t regionID, match_id_t matchID,
		std::string& valueString, pure::NumberConverter& numConverter);
	template<typename T> int32_t editCandidateAndPrintForInt08(
		ICheatEngine& cheatEngine, region_id_t regionID, match_id_t matchID,
		std::string& valueString, pure::NumberConverter& numConverter);
};

} /* END of namespace */

#endif
