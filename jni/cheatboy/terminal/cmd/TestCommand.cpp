#include "TestCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string TestCommand::NAME = "TestCommand";
const std::string TestCommand::DIGEST = "TestCommand";
const std::string TestCommand::DOCUMENT = "TestCommand";

TestCommand::TestCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

TestCommand::~TestCommand() {}

int32_t TestCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	std::stringstream ss(argsLine);
	std::string testCaseString;
	std::string newArgsLine;
	if (!(ss >> testCaseString)) {
		pure::println("Need 1 argument!!!");
		return 0;
	}
	std::getline(ss, newArgsLine);

	pure::NumberConverter numConverter;
	uint32_t testCase = 0;
	if (!numConverter.convert(testCaseString, testCase)) {
		pure::println("Failed to parse ID of test case!!!");
		return 0;
	}
	return cheatEngine.runTest(testCase, newArgsLine);
}

} /* END of namespace */
