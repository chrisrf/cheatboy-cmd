#include "ListCandidatesCommand.h"

#include <sstream>
#include <PureCppCompatibility.h>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

class ListCandidatesCommandImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ListCandidatesCommandImpl);
public:
	ListCandidatesCommandImpl() {}
	virtual ~ListCandidatesCommandImpl() noexcept {}
public:
	void printMatchedValue(const MatchedValue& value);
private:
	template<typename T1, typename T2> void checkAndPrint(
		const MatchedValue& value, const MemoryValueTypeFlag& flag, const char* typeName);
};

const std::string ListCandidatesCommand::NAME = "ListCandidatesCommand";
const std::string ListCandidatesCommand::DIGEST = "ListCandidatesCommand";
const std::string ListCandidatesCommand::DOCUMENT = "ListCandidatesCommand";

ListCandidatesCommand::ListCandidatesCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT),
	_impl(std::make_unique<ListCandidatesCommandImpl>()) {}

ListCandidatesCommand::~ListCandidatesCommand() {}

int32_t ListCandidatesCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	typedef ICheatEngine::IMatchedValueHandler IMatchedValueHandler;
	IMatchedValueHandler handler = [this] (const MatchedValue& value) {
		_impl->printMatchedValue(value);
	};
	cheatEngine.iterateOverMatchedValues(handler);
	return 0;
}

void ListCandidatesCommandImpl::printMatchedValue(const MatchedValue& value) {
	pure::println(">>> [{0}] [{1}] [{2}]",
			value.getRegionID(), value.getMatchID(), pure::PointerUtils::toPointer(value.getRemoteAddress()));

	MemoryValueTypeFlag flag = value.getMatchFlag();
	checkAndPrint<int8_t, int64_t>(value, flag, "i08");
	checkAndPrint<uint8_t, uint64_t>(value, flag, "u08");
	checkAndPrint<int16_t, int64_t>(value, flag, "i16");
	checkAndPrint<uint16_t, uint64_t>(value, flag, "u16");
	checkAndPrint<int32_t, int64_t>(value, flag, "i32");
	checkAndPrint<uint32_t, uint64_t>(value, flag, "u32");
	checkAndPrint<int64_t, int64_t>(value, flag, "i64");
	checkAndPrint<uint64_t, uint64_t>(value, flag, "u64");
	checkAndPrint<float, float>(value, flag, "f32");
	checkAndPrint<double, double>(value, flag, "f64");
}

template<typename T1, typename T2>
void ListCandidatesCommandImpl::checkAndPrint(
	const MatchedValue& value, const MemoryValueTypeFlag& flag, const char* typeName) {

	if (flag.isMarkedFor<T1>()) {
		T1 printValue = value.getAs<T1>();
		pure::println("\t\t[{0}] [{1}]", typeName, (T2)printValue);
	}
}


} /* END of namespace */
