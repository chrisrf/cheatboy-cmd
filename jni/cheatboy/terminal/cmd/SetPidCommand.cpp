#include "SetPidCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string SetPidCommand::NAME = "SetPidCommand";
const std::string SetPidCommand::DIGEST = "SetPidCommand";
const std::string SetPidCommand::DOCUMENT = "SetPidCommand";

SetPidCommand::SetPidCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

SetPidCommand::~SetPidCommand() {}

int32_t SetPidCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	std::stringstream ss(argsLine);
	std::string pidString;
	if (!(ss >> pidString)) {
		pure::println("Need 1 argument!!!");
		return 0;
	}
	pure::NumberConverter numConverter;
	pid_t pid = EngineConstants::NULL_PROCESS_ID;
	if (!numConverter.convert(pidString, pid)) {
		pure::println("Failed to parse pid!!!");
		return 0;
	}
	if (pid < EngineConstants::MIN_PROCESS_ID) {
		pure::println("Invalid pid!!!");
		return 0;
	}
	cheatEngine.setPid(pid);
	return 0;
}

} /* END of namespace */
