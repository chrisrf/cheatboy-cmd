#include "AttachCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string AttachCommand::NAME = "AttachCommand";
const std::string AttachCommand::DIGEST = "AttachCommand";
const std::string AttachCommand::DOCUMENT = "AttachCommand";

AttachCommand::AttachCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

AttachCommand::~AttachCommand() {}

int32_t AttachCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (cheatEngine.getPid() == EngineConstants::NULL_PROCESS_ID) {
		pure::println("Please set pid first");
		return 0;
	}
	cheatEngine.attach();
	return 0;
}

} /* END of namespace */
