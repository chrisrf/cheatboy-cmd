#ifndef __LoadRegionsCommand_H__
#define __LoadRegionsCommand_H__

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace cboy {

class LoadRegionsCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(LoadRegionsCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	LoadRegionsCommand();
	virtual ~LoadRegionsCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
};

} /* END of namespace */

#endif
