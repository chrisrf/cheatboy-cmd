#include "ConfigEngineCommand.h"

#include <map>
#include <sstream>
#include <PureCppCompatibility.h>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

class ConfigEngineCommandImpl {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ConfigEngineCommandImpl);
public:
	typedef std::function<int32_t(ICheatEngine&, const std::string&)> OptionHandler;
public:
	static const int SUCCESS = 0;
	static const int ERROR = -1;
public:
	std::map<std::string, OptionHandler> _handlers;

public:
	ConfigEngineCommandImpl(): _handlers() {
		_handlers.insert(std::make_pair("a",
			[this] (ICheatEngine& engine, const std::string& value) { return handleAllOption(engine, value); }));
		_handlers.insert(std::make_pair("l",
			[this] (ICheatEngine& engine, const std::string& value) { return handleLevelOption(engine, value); }));
		_handlers.insert(std::make_pair("t",
			[this] (ICheatEngine& engine, const std::string& value) { return handleTypeOption(engine, value); }));
		_handlers.insert(std::make_pair("s",
			[this] (ICheatEngine& engine, const std::string& value) { return handleThresholdOption(engine, value); }));
		_handlers.insert(std::make_pair("m",
			[this] (ICheatEngine& engine, const std::string& value) { return handleReaderModeOption(engine, value); }));
		_handlers.insert(std::make_pair("r",
			[this] (ICheatEngine& engine, const std::string& value) { return handleSmartReaderModeOption(engine, value); }));
		_handlers.insert(std::make_pair("p",
			[this] (ICheatEngine& engine, const std::string& value) { return handlePageSizeOption(engine, value); }));
	}
	virtual ~ConfigEngineCommandImpl() noexcept {}
public:
	int32_t showConfig(ICheatEngine& cheatEngine);
	int32_t handleAllOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleLevelOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleTypeOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleThresholdOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleReaderModeOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handleSmartReaderModeOption(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t handlePageSizeOption(ICheatEngine& cheatEngine, const std::string& argString);
private:
	bool parseLevel(const std::string& argString, pure::NumberConverter& numConverter, MemoryRegionLevel& outLevel);
	bool parseType(const std::string& argString, MemoryValueTypeFlag& outFlag, std::string& outTypeString);
	bool parseThreshold(const std::string& argString, pure::NumberConverter& numConverter, size_t& outThreshold);
	bool parseReaderMode(const std::string& argString, pure::NumberConverter& numConverter, MemoryReaderMode& outReaderMode);
	bool parseSmartReaderMode(const std::string& argString, pure::NumberConverter& numConverter, bool& outSmartReaderModeEnabled);
	bool parsePageSize(const std::string& argString, pure::NumberConverter& numConverter, size_t& outPageSize);
};

const std::string ConfigEngineCommand::NAME = "ConfigEngineCommand";
const std::string ConfigEngineCommand::DIGEST = "ConfigEngineCommand";
const std::string ConfigEngineCommand::DOCUMENT = "ConfigEngineCommand";

ConfigEngineCommand::ConfigEngineCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT),
	_impl(std::make_unique<ConfigEngineCommandImpl>()) {}

ConfigEngineCommand::~ConfigEngineCommand() {}

int32_t ConfigEngineCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (argsLine.empty()) {
		return _impl->showConfig(cheatEngine);
	}

	std::stringstream ss(argsLine);
	std::string optionString;
	std::string argString;
	if ((!(ss >> optionString)) || (!(std::getline(ss, argString)))) {
		pure::println("Wrong number of arguments!!!");
		return ConfigEngineCommandImpl::ERROR;
	}
	pure::StringTrimmer::trim(argString);

	typedef std::map<std::string, ConfigEngineCommandImpl::OptionHandler> HandlerMap;
	HandlerMap::iterator it = (_impl->_handlers).find(optionString);
	if (it == (_impl->_handlers).end()) {
		pure::println("[{0}] is an invalid option!!!", optionString);
		return ConfigEngineCommandImpl::ERROR;
	}
	auto& handler = it->second;
	return handler(cheatEngine, argString);
}

int32_t ConfigEngineCommandImpl::showConfig(ICheatEngine& cheatEngine) {
	// TODO: Interpret the value accurately.
	MemoryRegionLevel level = cheatEngine.getScannedRegionLevel();
	MemoryValueTypeFlag typeFlag = cheatEngine.getScannedValueTypeFlag();
	size_t threshold = cheatEngine.getThresholdOfListableMatchedValues();
	MemoryReaderMode readerMode = cheatEngine.getMemoryReaderMode();
	bool smartReaderModeEnabled = cheatEngine.isSmartReaderModeEnabled();
	size_t pageSize = cheatEngine.getReaderBufferPageSizeInMB();

	pure::println("======= [Current Configuration]");
	pure::println("[l] Scanned region level            : [{0}]", static_cast<int>(level));
	pure::println("# [{0}]: HEAP", static_cast<int>(MemoryRegionLevel::HEAP));
	pure::println("# [{0}]: HEAP_STACK", static_cast<int>(MemoryRegionLevel::HEAP_STACK));
	pure::println("# [{0}]: ALL_WRITE_PERMISSION_REGION", static_cast<int>(MemoryRegionLevel::ALL_WRITE_PERMISSION_REGION));
	pure::println("# [{0}]: ALL", static_cast<int>(MemoryRegionLevel::ALL));
	pure::println("=======");
	pure::println("[t] Scanned value type              : [{0}]", typeFlag.getFlag());
	pure::println("# i: [{0}]", MemoryValueTypeFlag::ALL_SIGNED_INTEGER);
	pure::println("# u: [{0}]", MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER);
	pure::println("# I: [{0}]", MemoryValueTypeFlag::ALL_INTEGER);
	pure::println("# f: [{0}]", MemoryValueTypeFlag::ALL_FLOAT);
	pure::println("# s: [{0}]", MemoryValueTypeFlag::ALL_SIGNED_NUMBER);
	pure::println("# n: [{0}]", MemoryValueTypeFlag::ALL);
	pure::println("=======");
	pure::println("[s] Max number of listable candidate: [{0}]", threshold);
	pure::println("=======");
	pure::println("[m] Memory reader mode: [{0}]", static_cast<int>(readerMode));
	pure::println("# [{0}]: DEFAULT", static_cast<int>(MemoryReaderMode::DEFAULT));
	pure::println("# [{0}]: STREAMING", static_cast<int>(MemoryReaderMode::STREAMING));
	pure::println("=======");
	pure::println("[r] Smart memory reader mode: [{0}]", smartReaderModeEnabled);
	pure::println("# [0]: DISABLED");
	pure::println("# [1]: ENABLED");
	pure::println("=======");
	pure::println("[p] Reader buffer page size: [{0}] MB", pageSize);
	pure::println("=======");
	return ConfigEngineCommandImpl::SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleAllOption(ICheatEngine& cheatEngine, const std::string& argString) {
	std::stringstream ss(argString);
	std::string levelArgString;
	std::string typeArgString;
	std::string thresholdArgString;
	std::string readerModeArgString;
	std::string smartReaderModeArgString;
	std::string pageSizeArgString;
	if ((!(ss >> levelArgString)) || (!(ss >> typeArgString)) || (!(ss >> thresholdArgString)) ||
		(!(ss >> readerModeArgString)) || (!(ss >> smartReaderModeArgString)) || (!(ss >> pageSizeArgString))) {
		pure::println("Need 6 arguments!!!");
		return ERROR;
	}
	if (handleLevelOption(cheatEngine, levelArgString) != SUCCESS) {
		return ERROR;
	}
	if (handleTypeOption(cheatEngine, typeArgString) != SUCCESS) {
		return ERROR;
	}
	if (handleThresholdOption(cheatEngine, thresholdArgString) != SUCCESS) {
		return ERROR;
	}
	if (handleReaderModeOption(cheatEngine, readerModeArgString) != SUCCESS) {
		return ERROR;
	}
	if (handleSmartReaderModeOption(cheatEngine, smartReaderModeArgString) != SUCCESS) {
		return ERROR;
	}
	if (handlePageSizeOption(cheatEngine, pageSizeArgString) != SUCCESS) {
		return ERROR;
	}
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleLevelOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	MemoryRegionLevel level;
	if (!parseLevel(argString, numConverter, level)) {
		return ERROR;
	}
	pure::println("Set scanned region level to [{0}].", static_cast<int>(level));
	cheatEngine.setScannedRegionLevel(level);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleTypeOption(ICheatEngine& cheatEngine, const std::string& argString) {
	MemoryValueTypeFlag flag;
	std::string typeString;
	if (!parseType(argString, flag, typeString)) {
		return ERROR;
	}
	pure::println("Set scanned value type to [{0}].", typeString);
	cheatEngine.setScannedValueTypeFlag(flag);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleThresholdOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	size_t threshold = 0;
	if (!parseThreshold(argString, numConverter, threshold)) {
		return ERROR;
	}
	pure::println("Set max number of listable candidate to [{0}].", threshold);
	cheatEngine.setThresholdOfListableMatchedValues(threshold);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleReaderModeOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	MemoryReaderMode mode;
	if (!parseReaderMode(argString, numConverter, mode)) {
		return ERROR;
	}
	pure::println("Set memory reader mode to [{0}].", static_cast<int>(mode));
	cheatEngine.setMemoryReaderMode(mode);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handleSmartReaderModeOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	bool smartReaderModeEnabled = 0;
	if (!parseSmartReaderMode(argString, numConverter, smartReaderModeEnabled)) {
		return ERROR;
	}
	pure::println("Set smart memory reader mode to [{0}].", smartReaderModeEnabled);
	cheatEngine.setSmartReaderModeEnabled(smartReaderModeEnabled);
	return SUCCESS;
}

int32_t ConfigEngineCommandImpl::handlePageSizeOption(ICheatEngine& cheatEngine, const std::string& argString) {
	pure::NumberConverter numConverter;
	size_t pageSize = 0;
	if (!parsePageSize(argString, numConverter, pageSize)) {
		return ERROR;
	}
	pure::println("Set reader buffer page size to [{0}] MB.", pageSize);
	cheatEngine.setReaderBufferPageSizeInMB(pageSize);
	return SUCCESS;
}

bool ConfigEngineCommandImpl::parseLevel(const std::string& argString, pure::NumberConverter& numConverter, MemoryRegionLevel& outLevel) {
	int intLevel = 0;
	if (!numConverter.convert(argString, intLevel)) {
		pure::println("Failed to parse value of region level!!!");
		return false;
	}
	if (!pure::EnumUtils<MemoryRegionLevel>::convert(intLevel, outLevel)) {
		pure::println("[{0}] is an invalid region level, should be within ({1} <= level <= {2}).", intLevel,
				static_cast<int>(MemoryRegionLevel::ENUM_MIN),
				static_cast<int>(MemoryRegionLevel::ENUM_MAX));
		return false;
	}
	return true;
}

bool ConfigEngineCommandImpl::parseType(const std::string& argString, MemoryValueTypeFlag& outFlag, std::string& outTypeString) {
	if (argString.compare("i") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_SIGNED_INTEGER);
		outTypeString = "ALL_SIGNED_INTEGER";
	} else if (argString.compare("u") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER);
		outTypeString = "ALL_UNSIGNED_INTEGER";
	} else if (argString.compare("I") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_INTEGER);
		outTypeString = "ALL_INTEGER";
	} else if (argString.compare("f") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_FLOAT);
		outTypeString = "ALL_FLOAT";
	} else if (argString.compare("s") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL_SIGNED_NUMBER);
		outTypeString = "ALL_SIGNED_NUMBER";
	} else if (argString.compare("n") == 0) {
		outFlag.setFlag(MemoryValueTypeFlag::ALL);
		outTypeString = "ALL";
	} else {
		pure::println("Failed to parse type of value!!!");
		return false;
	}
	return true;
}

bool ConfigEngineCommandImpl::parseThreshold(const std::string& argString, pure::NumberConverter& numConverter, size_t& outThreshold) {
	if (!numConverter.convert(argString, outThreshold)) {
		pure::println("Failed to parse value of threshold!!!");
		return false;
	}
	return true;
}

bool ConfigEngineCommandImpl::parseReaderMode(const std::string& argString, pure::NumberConverter& numConverter, MemoryReaderMode& outReaderMode) {
	int intMode = 0;
	if (!numConverter.convert(argString, intMode)) {
		pure::println("Failed to parse value of memory reader mode!!!");
		return false;
	}
	if (!pure::EnumUtils<MemoryReaderMode>::convert(intMode, outReaderMode)) {
		pure::println("[{0}] is an invalid memory reader mode, should be within ({1} <= readerMode <= {2}).", intMode,
				static_cast<int>(MemoryReaderMode::ENUM_MIN),
				static_cast<int>(MemoryReaderMode::ENUM_MAX));
		return false;
	}
	return true;
}

bool ConfigEngineCommandImpl::parseSmartReaderMode(const std::string& argString, pure::NumberConverter& numConverter, bool& outSmartReaderModeEnabled) {
	int intSmartReaderModeEnabled = 0;
	if (!numConverter.convert(argString, intSmartReaderModeEnabled)) {
		pure::println("Failed to parse value of smart memory reader mode!!!");
		return false;
	}
	outSmartReaderModeEnabled = (intSmartReaderModeEnabled == 0 ? false : true);
	return true;
}

bool ConfigEngineCommandImpl::parsePageSize(const std::string& argString, pure::NumberConverter& numConverter, size_t& outPageSize) {
	if (!numConverter.convert(argString, outPageSize)) {
		pure::println("Failed to parse value of reader buffer page size!!!");
		return false;
	}
	return true;
}

} /* END of namespace */
