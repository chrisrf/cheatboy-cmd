#ifndef __ListRegionsCommand_H__
#define __ListRegionsCommand_H__

#include <memory>

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace cboy {
class IMemoryRegion;

class ListRegionsCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ListRegionsCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	ListRegionsCommand();
	virtual ~ListRegionsCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
private:
	int32_t printKernelRegions(ICheatEngine& cheatEngine, const std::string& argString);
	int32_t printLoadedRegions(ICheatEngine& cheatEngine);
	int32_t printScannedRegions(ICheatEngine& cheatEngine);
private:
	static void printRegionPointer(std::unique_ptr<IMemoryRegion>& region);
	static void printRegion(IMemoryRegion& region);
};

} /* END of namespace */

#endif
