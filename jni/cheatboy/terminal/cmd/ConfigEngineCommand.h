#ifndef __ConfigEngineCommand_H__
#define __ConfigEngineCommand_H__

#include <memory>

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace cboy {
class ConfigEngineCommandImpl;

class ConfigEngineCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ConfigEngineCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;
private:
	std::unique_ptr<ConfigEngineCommandImpl> _impl;

public:
	ConfigEngineCommand();
	virtual ~ConfigEngineCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
};

} /* END of namespace */

#endif
