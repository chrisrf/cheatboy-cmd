#include "WriteValueCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string WriteValueCommand::NAME = "WriteValueCommand";
const std::string WriteValueCommand::DIGEST = "WriteValueCommand";
const std::string WriteValueCommand::DOCUMENT = "WriteValueCommand";

WriteValueCommand::WriteValueCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

WriteValueCommand::~WriteValueCommand() {}

int32_t WriteValueCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (cheatEngine.getPid() == EngineConstants::NULL_PROCESS_ID) {
		pure::println("Please set pid first");
		return 0;
	}

	std::stringstream ss(argsLine);
	std::string addrString;
	std::string type;
	std::string valueString;
	if ((!(ss >> addrString)) || (!(ss >> type)) || (!(ss >> valueString))) {
		pure::println("Need 3 arguments!!!");
		return 0;
	}
	if (type.size() < 2) {
		handleErrorOfParsingTypeOfValue();
		return 0;
	}
	std::string typeLenString = type.substr(1, type.size() - 1);

	pure::HexNumberConverter hexNumConverter;
	uintptr_t addr = 0;
	if (!hexNumConverter.convert(addrString, addr)) {
		pure::println("Failed to parse address!!!");
		return 0;
	}

	pure::NumberConverter numConverter;
	int typeLen = 0;
	if (!numConverter.convert(typeLenString, typeLen)) {
		handleErrorOfParsingTypeOfValue();
		return 0;
	}

	if (type[0] == 'i') {
		if (typeLen == 8) {
			return writeValueAndPrintForInt08<int8_t>(cheatEngine, addr, valueString, numConverter);
		} else if (typeLen == 16) {
			return writeValueAndPrint<int16_t>(cheatEngine, addr, valueString, numConverter);
		} else if (typeLen == 32) {
			return writeValueAndPrint<int32_t>(cheatEngine, addr, valueString, numConverter);
		} else if (typeLen == 64) {
			return writeValueAndPrint<int64_t>(cheatEngine, addr, valueString, numConverter);
		}
	} else if (type[0] == 'u') {
		if (typeLen == 8) {
			return writeValueAndPrintForInt08<uint8_t>(cheatEngine, addr, valueString, numConverter);
		} else if (typeLen == 16) {
			return writeValueAndPrint<uint16_t>(cheatEngine, addr, valueString, numConverter);
		} else if (typeLen == 32) {
			return writeValueAndPrint<uint32_t>(cheatEngine, addr, valueString, numConverter);
		} else if (typeLen == 64) {
			return writeValueAndPrint<uint64_t>(cheatEngine, addr, valueString, numConverter);
		}
	} else if (type[0] == 'f') {
		if (typeLen == 32) {
			return writeValueAndPrint<float>(cheatEngine, addr, valueString, numConverter);
		} else if (typeLen == 64) {
			return writeValueAndPrint<double>(cheatEngine, addr, valueString, numConverter);
		}
	}
	handleErrorOfParsingTypeOfValue();
	return 0;
}

void WriteValueCommand::handleErrorOfParsingTypeOfValue() {
	pure::println("Failed to parse type of value!!!");
}

void WriteValueCommand::handleErrorOfParsingValue() {
	pure::println("Failed to parse value!!!");
}

template<typename T>
int32_t WriteValueCommand::writeValueAndPrint(
	ICheatEngine& cheatEngine, uintptr_t addr, std::string& valueString, pure::NumberConverter& numConverter) {

	T value = 0;
	if (!numConverter.convert(valueString, value)) {
		handleErrorOfParsingValue();
		return 0;
	}

	void* ptr = pure::PointerUtils::toPointer(addr);
	WriteValue writeValue;
	writeValue.wantToWriteFor<T>(addr, value);
	cheatEngine.writeValue(writeValue);
	std::string line = pure::MemoryUtils::readableLineBytesToString(writeValue.getBuffer(), writeValue.getCapacity(), false);
	pure::println("Write a value[{0}] at [{1}] <= {2}", value, ptr, line);
	return 0;
}

template<typename T>
int32_t WriteValueCommand::writeValueAndPrintForInt08(
	ICheatEngine& cheatEngine, uintptr_t addr, std::string& valueString, pure::NumberConverter& numConverter) {

	T value = 0;
	if (!numConverter.convert(valueString, value)) {
		handleErrorOfParsingValue();
		return 0;
	}

	void* ptr = pure::PointerUtils::toPointer(addr);
	WriteValue writeValue;
	writeValue.wantToWriteFor<T>(addr, value);
	cheatEngine.writeValue(writeValue);
	std::string line = pure::MemoryUtils::readableLineBytesToString(writeValue.getBuffer(), writeValue.getCapacity(), false);
	pure::println("Write a value[{0}] at [{1}] <= {2}", (int32_t)value, ptr, line);
	return 0;
}

} /* END of namespace */
