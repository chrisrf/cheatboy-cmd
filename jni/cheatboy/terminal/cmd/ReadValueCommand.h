#ifndef __ReadValueCommand_H__
#define __ReadValueCommand_H__

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace cboy {

class ReadValueCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ReadValueCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	ReadValueCommand();
	virtual ~ReadValueCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
private:
	void handleErrorOfParsingTypeOfValue();
	template<typename T> int32_t readValueAndPrint(ICheatEngine& cheatEngine, uintptr_t addr);
	template<typename T> int32_t readValueAndPrintForInt08(ICheatEngine& cheatEngine, uintptr_t addr);
};

} /* END of namespace */

#endif
