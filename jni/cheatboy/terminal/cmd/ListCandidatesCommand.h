#ifndef __ListCandidatesCommand_H__
#define __ListCandidatesCommand_H__

#include <memory>

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace cboy {
class ListCandidatesCommandImpl;

class ListCandidatesCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(ListCandidatesCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;
private:
	std::unique_ptr<ListCandidatesCommandImpl> _impl;

public:
	ListCandidatesCommand();
	virtual ~ListCandidatesCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
};

} /* END of namespace */

#endif
