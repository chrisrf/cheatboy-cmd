#include "DeleteRegionsCommand.h"

#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string DeleteRegionsCommand::NAME = "ResetScannerCommand";
const std::string DeleteRegionsCommand::DIGEST = "ResetScannerCommand";
const std::string DeleteRegionsCommand::DOCUMENT = "ResetScannerCommand";

DeleteRegionsCommand::DeleteRegionsCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

DeleteRegionsCommand::~DeleteRegionsCommand() {}

int32_t DeleteRegionsCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	pure::println("Delete all regions...");
	cheatEngine.deleteRegions();
	pure::println("Delete all regions...Done");
	return 0;
}

} /* END of namespace */
