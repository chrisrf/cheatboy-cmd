#ifndef __TestCommand_H__
#define __TestCommand_H__

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace cboy {

class TestCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(TestCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	TestCommand();
	virtual ~TestCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
};

} /* END of namespace */

#endif
