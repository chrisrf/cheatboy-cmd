#include "EditCandidateCommand.h"

#include <sstream>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string EditCandidateCommand::NAME = "EditCandidateCommand";
const std::string EditCandidateCommand::DIGEST = "EditCandidateCommand";
const std::string EditCandidateCommand::DOCUMENT = "EditCandidateCommand";

EditCandidateCommand::EditCandidateCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

EditCandidateCommand::~EditCandidateCommand() {}

int32_t EditCandidateCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	if (cheatEngine.getPid() == EngineConstants::NULL_PROCESS_ID) {
		pure::println("Please set pid first");
		return 0;
	}

	std::stringstream ss(argsLine);
	std::string regionIdString;
	std::string matchIdString;
	std::string type;
	std::string valueString;
	if ((!(ss >> regionIdString)) || (!(ss >> matchIdString)) || (!(ss >> type)) || (!(ss >> valueString))) {
		pure::println("Need 4 arguments!!!");
		return 0;
	}
	if (type.size() < 2) {
		handleErrorOfParsingTypeOfValue();
		return 0;
	}
	std::string typeLenString = type.substr(1, type.size() - 1);

	pure::NumberConverter numConverter;
	region_id_t regionID = 0;
	if (!numConverter.convert(regionIdString, regionID)) {
		handleErrorOfParsingRegionID();
		return 0;
	}
	match_id_t matchID = 0;
	if (!numConverter.convert(matchIdString, matchID)) {
		handleErrorOfParsingMatchID();
		return 0;
	}

	int typeLen = 0;
	if (!numConverter.convert(typeLenString, typeLen)) {
		handleErrorOfParsingTypeOfValue();
		return 0;
	}

	if (type[0] == 'i') {
		if (typeLen == 8) {
			return editCandidateAndPrintForInt08<int8_t>(cheatEngine, regionID, matchID, valueString, numConverter);
		} else if (typeLen == 16) {
			return editCandidateAndPrint<int16_t>(cheatEngine, regionID, matchID, valueString, numConverter);
		} else if (typeLen == 32) {
			return editCandidateAndPrint<int32_t>(cheatEngine, regionID, matchID, valueString, numConverter);
		} else if (typeLen == 64) {
			return editCandidateAndPrint<int64_t>(cheatEngine, regionID, matchID, valueString, numConverter);
		}
	} else if (type[0] == 'u') {
		if (typeLen == 8) {
			return editCandidateAndPrintForInt08<uint8_t>(cheatEngine, regionID, matchID, valueString, numConverter);
		} else if (typeLen == 16) {
			return editCandidateAndPrint<uint16_t>(cheatEngine, regionID, matchID, valueString, numConverter);
		} else if (typeLen == 32) {
			return editCandidateAndPrint<uint32_t>(cheatEngine, regionID, matchID, valueString, numConverter);
		} else if (typeLen == 64) {
			return editCandidateAndPrint<uint64_t>(cheatEngine, regionID, matchID, valueString, numConverter);
		}
	} else if (type[0] == 'f') {
		if (typeLen == 32) {
			return editCandidateAndPrint<float>(cheatEngine, regionID, matchID, valueString, numConverter);
		} else if (typeLen == 64) {
			return editCandidateAndPrint<double>(cheatEngine, regionID, matchID, valueString, numConverter);
		}
	}
	handleErrorOfParsingTypeOfValue();
	return 0;
}

void EditCandidateCommand::handleErrorOfParsingTypeOfValue() {
	pure::println("Failed to parse type of value!!!");
}

void EditCandidateCommand::handleErrorOfParsingRegionID() {
	pure::println("Failed to parse region ID!!!");
}

void EditCandidateCommand::handleErrorOfParsingMatchID() {
	pure::println("Failed to parse match ID!!!");
}

void EditCandidateCommand::handleErrorOfParsingValue() {
	pure::println("Failed to parse value!!!");
}

template<typename T>
int32_t EditCandidateCommand::editCandidateAndPrint(
	ICheatEngine& cheatEngine, region_id_t regionID, match_id_t matchID,
	std::string& valueString, pure::NumberConverter& numConverter) {

	T value = 0;
	if (!numConverter.convert(valueString, value)) {
		handleErrorOfParsingValue();
		return 0;
	}

	EditableMatchedValue editableValue;
	editableValue.wantToEditFor<T>(regionID, matchID, value);
	cheatEngine.editMatchedValue(editableValue);
	std::string line = pure::MemoryUtils::readableLineBytesToString(
			editableValue.getBuffer(), editableValue.getCapacity(), false);
	pure::println("Edit a matched value[{0}] at [{1}][{2}] <= {3}",
			value, editableValue.getRegionID(), editableValue.getMatchID(), line);
	return 0;
}

template<typename T>
int32_t EditCandidateCommand::editCandidateAndPrintForInt08(
	ICheatEngine& cheatEngine, region_id_t regionID, match_id_t matchID,
	std::string& valueString, pure::NumberConverter& numConverter) {

	T value = 0;
	if (!numConverter.convert(valueString, value)) {
		handleErrorOfParsingValue();
		return 0;
	}

	EditableMatchedValue editableValue;
	editableValue.wantToEditFor<T>(regionID, matchID, value);
	cheatEngine.editMatchedValue(editableValue);
	std::string line = pure::MemoryUtils::readableLineBytesToString(
			editableValue.getBuffer(), editableValue.getCapacity(), false);
	pure::println("Edit a matched value[{0}] at [{1}][{2}] <= {3}",
			(int32_t)value, editableValue.getRegionID(), editableValue.getMatchID(), line);
	return 0;
}

} /* END of namespace */
