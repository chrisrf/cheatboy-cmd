#include "ListRegionsCommand.h"

#include <sstream>
#include <functional>
#include <PureLib.h>
#include <CheatEngineLib.h>

namespace cboy {

const std::string ListRegionsCommand::NAME = "ListRegionsCommand";
const std::string ListRegionsCommand::DIGEST = "ListRegionsCommand";
const std::string ListRegionsCommand::DOCUMENT = "ListRegionsCommand";

ListRegionsCommand::ListRegionsCommand()
	:
	TerminalCommand(NAME, DIGEST, DOCUMENT) {}

ListRegionsCommand::~ListRegionsCommand() {}

int32_t ListRegionsCommand::execute(
	ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) {

	std::stringstream ss(argsLine);
	std::string optionString;
	ss >> optionString;

	if ((optionString.empty()) || (optionString.compare("l") == 0) || (optionString.compare("loaded") == 0)) {
		return printLoadedRegions(cheatEngine);
	} else if ((optionString.compare("s") == 0) || (optionString.compare("scanned") == 0)) {
		return printScannedRegions(cheatEngine);
	} else if ((optionString.compare("k") == 0) || (optionString.compare("kernel") == 0)) {
		std::string argString;
		ss >> argString;
		return printKernelRegions(cheatEngine, argString);
	}
	pure::println("[{0}] is an invalid option!!!", optionString);
	return 0;
}

int32_t ListRegionsCommand::printKernelRegions(ICheatEngine& cheatEngine, const std::string& argString) {
	if (cheatEngine.getPid() == EngineConstants::NULL_PROCESS_ID) {
		pure::println("Please set pid first");
		return 0;
	}

	pure::println("Print regions in [kernel].");
	MemoryRegionLevel level;
	if (argString.empty()) {
		level = MemoryRegionLevel::ALL;
	} else {
		pure::NumberConverter numConverter;
		int intLevel = 0;
		if (!numConverter.convert(argString, intLevel)) {
			pure::println("Failed to parse value of region level!!!");
			return 0;
		}
		if (!pure::EnumUtils<MemoryRegionLevel>::convert(intLevel, level)) {
			pure::println("[{0}] is an invalid region level, should be within ({1} <= level <= {2}).", intLevel,
					static_cast<int>(MemoryRegionLevel::ENUM_MIN),
					static_cast<int>(MemoryRegionLevel::ENUM_MAX));
			return 0;
		}
	}
	std::vector<std::unique_ptr<IMemoryRegion>> regions;
	cheatEngine.getMemoryRegions(level, regions);
	pure::println("Number of regions in [kernel] is [{0}]", regions.size());
	std::for_each(regions.begin(), regions.end(), printRegionPointer);
	return 0;
}

int32_t ListRegionsCommand::printLoadedRegions(ICheatEngine& cheatEngine) {
	pure::println("Print regions [loaded] by scanner.");
	std::vector<std::reference_wrapper<IMemoryRegion>> regions;
	cheatEngine.getLoadedRegions(regions);
	pure::println("Number of regions [loaded] by scanner is [{0}]", regions.size());
	std::for_each(regions.begin(), regions.end(), printRegion);
	return 0;
}

int32_t ListRegionsCommand::printScannedRegions(ICheatEngine& cheatEngine) {
	pure::println("Print regions [scanned] by scanner.");
	std::vector<std::reference_wrapper<IMemoryRegion>> regions;
	cheatEngine.getScannedRegions(regions);
	pure::println("Number of regions [scanned] by scanner is [{0}]", regions.size());
	std::for_each(regions.begin(), regions.end(), printRegion);
	return 0;
}

void ListRegionsCommand::printRegionPointer(std::unique_ptr<IMemoryRegion>& region) {
	printRegion(*region);
}

void ListRegionsCommand::printRegion(IMemoryRegion& region) {
	void* startAddr = pure::PointerUtils::toPointer(region.getStartAddress());
	void* endAddr = pure::PointerUtils::toPointer(region.getStartAddress() + region.getSize());
	pure::println("[{0}] {1}-{2} ({3}) {4}{5}{6}{7} {8} \"{9}\"",
			region.getID(), startAddr, endAddr, region.getSize(),
			(region.hasReadPermission() ? "r" : "-"),
			(region.hasWritePermission() ? "w" : "-"),
			(region.hasExecutePermission() ? "x" : "-"),
			(region.hasSharedPermission() ? "s" : "p"),
			region.getPid(), region.getName());
}

} /* END of namespace */
