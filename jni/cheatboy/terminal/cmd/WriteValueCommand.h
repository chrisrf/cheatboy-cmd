#ifndef __WriteValueCommand_H__
#define __WriteValueCommand_H__

#include <cheatboy/terminal/cmd/base/TerminalCommand.h>

namespace pure {
class NumberConverter;
}

namespace cboy {

class WriteValueCommand: public TerminalCommand {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(WriteValueCommand);
public:
	static const std::string NAME;
	static const std::string DIGEST;
	static const std::string DOCUMENT;

public:
	WriteValueCommand();
	virtual ~WriteValueCommand();
public:
	virtual int32_t execute(ICheatEngine& cheatEngine, const std::string& cmd, const std::string& argsLine) override;
private:
	void handleErrorOfParsingTypeOfValue();
	void handleErrorOfParsingValue();
	template<typename T> int32_t writeValueAndPrint(
		ICheatEngine& cheatEngine, uintptr_t addr, std::string& valueString, pure::NumberConverter& numConverter);
	template<typename T> int32_t writeValueAndPrintForInt08(
		ICheatEngine& cheatEngine, uintptr_t addr, std::string& valueString, pure::NumberConverter& numConverter);
};

} /* END of namespace */

#endif
