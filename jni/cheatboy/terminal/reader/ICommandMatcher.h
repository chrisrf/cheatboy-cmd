#ifndef __ICommandMatcher_H__
#define __ICommandMatcher_H__

#include <stdint.h>
#include <string>
#include <PureMacro.h>

namespace cboy {

class ICommandMatcher {
PURE_DECLARE_CLASS_AS_INTERFACE(ICommandMatcher);
public:
	virtual size_t getNumberOfCommands() const = 0;
	virtual const std::string& findMatchedCommand(const std::string& text, uint32_t& searchIndex) const = 0;
};

} /* END of namespace */

#endif
