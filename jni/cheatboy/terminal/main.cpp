#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <memory>
#include <iostream>
#include <PureLib.h>

#include <cheatboy/terminal/CheatTerminal.h>
#include <cheatboy/terminal/Constants.h>

using namespace pure;
using namespace cboy;

bool createSingletons(std::vector<std::function<void()>>& destroyFuncs);
bool initSingletons(std::vector<std::reference_wrapper<IInitializable>>& cleanInstances);
void cleanSingletons(std::vector<std::reference_wrapper<IInitializable>>& cleanInstances);
void destroySingletons(std::vector<std::function<void()>>& destroyFuncs);

int main(int argc, char* argv[]) {
	if (getuid() != 0) {
		pure::println("Please running {0} as root.", Constants::LC_APP_NAME);
		return EXIT_FAILURE;
	}
	pure::println("[{0} Command Line Tool]", Constants::APP_NAME);

	if (!LoggerInstaller::install()) {
		return EXIT_FAILURE;
	}
	pure::println("Logger is installed successfully.");

	std::vector<std::function<void()>> destroyFuncs;
	if (!createSingletons(destroyFuncs)) {
		return EXIT_FAILURE;
	}
	pure::println("All Singletons are created successfully.");

	std::vector<std::reference_wrapper<IInitializable>> cleanInstances;
	if (!initSingletons(cleanInstances)) {
		destroySingletons(destroyFuncs);
		return EXIT_FAILURE;
	}
	pure::println("All Singletons are initialized successfully.");

	int processReturnCode = EXIT_SUCCESS;
	try {
		pure::println("{0} starts running...", Constants::LC_APP_NAME);
		Singleton<ICheatTerminal>::createInstance<TunnelTerminal>();
		Singleton<ICheatTerminal>::getInstance().run();
	} catch (...) {
		processReturnCode = EXIT_FAILURE;
		pure::printExceptionInCatchBlock(__PURE_FILE__, __LINE__);
	}
	Singleton<ICheatTerminal>::destroyInstance();
	pure::println("{0} stopped running...", Constants::LC_APP_NAME);

	cleanSingletons(cleanInstances);
	destroySingletons(destroyFuncs);
	LoggerInstaller::uninstall();
	return processReturnCode;
}

bool createSingletons(std::vector<std::function<void()>>& destroyFuncs) {
//	try {
//		Singleton<ILogger>::createInstance<Logger>();
//		destroyFuncs.push_back(Singleton<ILogger>::destroyInstance);
//	} catch (...) {
//		pure::printExceptionInCatchBlock(__PURE_FILE__, __LINE__);
//		Singleton<ILogger>::destroyInstance();
//		destroySingletons(destroyFuncs);
//		return false;
//	}
	return true;
}

bool initSingletons(std::vector<std::reference_wrapper<IInitializable>>& cleanInstances) {
//	std::vector<std::reference_wrapper<IInitializable>> initInstances;
//	initInstances.push_back(Singleton<ILogger>::getInstance());
//
//	for (IInitializable& instance : initInstances) {
//		try {
//			instance.init();
//			try {
//				cleanInstances.push_back(instance);
//			} catch (...) {
//				instance.clean();
//				throw;
//			}
//		} catch (...) {
//			pure::printExceptionInCatchBlock(__PURE_FILE__, __LINE__);
//			cleanSingletons(cleanInstances);
//			return false;
//		}
//	}
	return true;
}

void cleanSingletons(std::vector<std::reference_wrapper<IInitializable>>& cleanInstances) {
	for (IInitializable& instance : cleanInstances) {
		instance.clean();
	}
	cleanInstances.clear();
}

void destroySingletons(std::vector<std::function<void()>>& destroyFuncs) {
	for (auto& func : destroyFuncs) {
		func();
	}
	destroyFuncs.clear();
}
