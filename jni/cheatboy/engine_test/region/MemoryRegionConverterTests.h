#ifndef __MemoryRegionConverterTests_H__
#define __MemoryRegionConverterTests_H__

#include <gtest/gtest.h>
#include <cheatboy/engine/region/defs/MemoryRegionDefs.h>

namespace cboy {
enum class MemoryRegionType;
class MemoryRegion;

class MemoryRegionConverterTests: public ::testing::Test {
protected:
	typedef MemoryRegionDefs::region_id_t region_id_t;
protected:
	static const pid_t EXAMPLE_MAIN_PID;
	static const std::string EXAMPLE_REGION_MAIN_STACK;
	static const std::string EXAMPLE_REGION_THREAD_STACK;
	static const std::string EXAMPLE_REGION_HEAP;
	static const std::string EXAMPLE_REGION_SHARED;
	static const std::string EXAMPLE_REGION_PRIVATE;
	static const std::string EXAMPLE_REGION_TEXT;
	static const std::string EXAMPLE_REGION_ANDROID_DALVIK_HEAP;
	static const std::string EXAMPLE_REGION_PRIVATE_ANDROID_DALVIK_HEAP;
	static const std::string EXAMPLE_REGION_ANDROID_ART_NATIVE_HEAP;
	static const std::string EXAMPLE_REGION_PRIVATE_ANDROID_ART_NATIVE_HEAP;
	static const std::string EXAMPLE_REGION_ANDROID_ART_HEAP;
	static const std::string EXAMPLE_REGION_PRIVATE_ANDROID_ART_HEAP;
public:
	MemoryRegionConverterTests() {}
	virtual ~MemoryRegionConverterTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	static void assertMemoryRegion(
		MemoryRegion& region,
		region_id_t id, MemoryRegionType type,
		bool readPermission, bool writePermission, bool execPermission, bool sharedPermission,
		uintptr_t startAddress, uintptr_t endAddress, const char* name, pid_t pid);
};

} /* END of namespace */

#endif
