#include "MemoryRegionConverterTests.h"

#include <assert.h>
#include <cheatboy/engine/region/MemoryRegionConverter.h>
#include <cheatboy/engine/region/MemoryRegion.h>
#include <cheatboy/engine/region/MemoryRegionLevel.h>
#include <cheatboy/engine/region/MemoryRegionType.h>

namespace cboy {

const pid_t MemoryRegionConverterTests::EXAMPLE_MAIN_PID = 20761;
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_MAIN_STACK						= "bfba3000-bfbc4000 rw-p 00000000 00:00 0          [stack]";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_THREAD_STACK					= "99765000-99864000 rw-p 00000000 00:00 0          [stack:20768]";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_HEAP							= "b7c91000-b7e5f000 rw-p 00000000 00:00 0          [heap]";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_SHARED							= "97eaf000-98228000 rw-s 00000000 00:04 521218     /dev/ashmem/gralloc-buffer (deleted)";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_PRIVATE						= "98d85000-98d86000 ---p 00000000 00:00 0 ";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_TEXT							= "9895b000-98960000 r-xp 00000000 08:06 530        /system/lib/hw/gralloc.vbox86.so";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_ANDROID_DALVIK_HEAP			= "a69aa000-a69d5000 rw-p 0086d000 00:04 5664       /dev/ashmem/dalvik-heap (deleted)";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_PRIVATE_ANDROID_DALVIK_HEAP	= "a69d5000-b613d000 ---p 00898000 00:04 5664       /dev/ashmem/dalvik-heap (deleted)";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_ANDROID_ART_NATIVE_HEAP		= "b7100000-b7200000 rw-p 00000000 00:00 0          [anon:libc_malloc]";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_PRIVATE_ANDROID_ART_NATIVE_HEAP= "b7300000-b7500000 ---p 00000000 00:00 0          [anon:libc_malloc]";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_ANDROID_ART_HEAP				= "12c00000-12e01000 rw-p 00000000 00:04 339        /dev/ashmem/dalvik-main space (deleted)";
const std::string MemoryRegionConverterTests::EXAMPLE_REGION_PRIVATE_ANDROID_ART_HEAP		= "12e01000-22c00000 ---p 00201000 00:04 339        /dev/ashmem/dalvik-main space (deleted)";

void MemoryRegionConverterTests::assertMemoryRegion(
	MemoryRegion& region,
	region_id_t id, MemoryRegionType type,
	bool readPermission, bool writePermission, bool execPermission, bool sharedPermission,
	uintptr_t startAddress, uintptr_t endAddress, const char* name, pid_t pid) {

	EXPECT_EQ(id, region.getID());
	EXPECT_EQ(type, region.getType());
	EXPECT_EQ(readPermission, region.hasReadPermission());
	EXPECT_EQ(writePermission, region.hasWritePermission());
	EXPECT_EQ(execPermission, region.hasExecutePermission());
	EXPECT_EQ(sharedPermission, region.hasSharedPermission());
	EXPECT_EQ(startAddress, region.getStartAddress());
	EXPECT_EQ(endAddress, (region.getStartAddress() + region.getSize()));
	EXPECT_STREQ(name, region.getName().c_str());
	EXPECT_EQ(pid, region.getPid());
}

TEST_F(MemoryRegionConverterTests,testGenerateMemoryRegion) {
	const region_id_t testID = 7;
	MemoryRegionConverter converter;
	std::unique_ptr<MemoryRegion> region;

	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_MAIN_STACK));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::MAIN_STACK, true, true, false, false,
			0xbfba3000, 0xbfbc4000, "[stack]", EXAMPLE_MAIN_PID);

	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_THREAD_STACK));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::THREAD_STACK, true, true, false, false,
			0x99765000, 0x99864000, "[stack:20768]", 20768);

	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_HEAP));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::HEAP, true, true, false, false,
			0xb7c91000, 0xb7e5f000, "[heap]", EXAMPLE_MAIN_PID);

	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_SHARED));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::OTHER, true, true, false, true,
			0x97eaf000, 0x98228000, "/dev/ashmem/gralloc-buffer (deleted)", EXAMPLE_MAIN_PID);

	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_PRIVATE));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::OTHER, false, false, false, false,
			0x98d85000, 0x98d86000, "", EXAMPLE_MAIN_PID);

	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_TEXT));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::OTHER, true, false, true, false,
			0x9895b000, 0x98960000, "/system/lib/hw/gralloc.vbox86.so", EXAMPLE_MAIN_PID);

	// XXX: For Android.
	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_ANDROID_DALVIK_HEAP));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::ANDROID_DALVIK_HEAP, true, true, false, false,
			0xa69aa000, 0xa69d5000, "/dev/ashmem/dalvik-heap (deleted)", EXAMPLE_MAIN_PID);

	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_ANDROID_ART_NATIVE_HEAP));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::ANDROID_ART_NATIVE_HEAP, true, true, false, false,
			0xb7100000, 0xb7200000, "[anon:libc_malloc]", EXAMPLE_MAIN_PID);

	EXPECT_TRUE(converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, EXAMPLE_REGION_ANDROID_ART_HEAP));
	region = converter.generate(testID);
	assertMemoryRegion(*region, testID, MemoryRegionType::ANDROID_ART_HEAP, true, true, false, false,
			0x12c00000, 0x12e01000, "/dev/ashmem/dalvik-main space (deleted)", EXAMPLE_MAIN_PID);
}

TEST_F(MemoryRegionConverterTests,testGenerateMemoryRegionWithMemoryRegionLevel) {
	MemoryRegionConverter converter;
	std::unique_ptr<MemoryRegion> region;

	std::vector<std::string> testInputs = {
			EXAMPLE_REGION_MAIN_STACK,
			EXAMPLE_REGION_THREAD_STACK,
			EXAMPLE_REGION_HEAP,
			EXAMPLE_REGION_SHARED,
			EXAMPLE_REGION_PRIVATE,
			EXAMPLE_REGION_TEXT,

			EXAMPLE_REGION_ANDROID_DALVIK_HEAP,
			EXAMPLE_REGION_PRIVATE_ANDROID_DALVIK_HEAP,
			EXAMPLE_REGION_ANDROID_ART_NATIVE_HEAP,
			EXAMPLE_REGION_PRIVATE_ANDROID_ART_NATIVE_HEAP,
			EXAMPLE_REGION_ANDROID_ART_HEAP,
			EXAMPLE_REGION_PRIVATE_ANDROID_ART_HEAP
	};

	{
		std::vector<bool> expectedResults = {
				true, true, true, true, true, true,
				true, true, true, true, true, true
		};
		assert(testInputs.size() == expectedResults.size());
		for (size_t i = 0; i < testInputs.size(); i++) {
			EXPECT_EQ(expectedResults[i], converter.parse(MemoryRegionLevel::ALL, EXAMPLE_MAIN_PID, testInputs[i]));
		}
	}

	{
		std::vector<bool> expectedResults = {
				true, true, true, true, false, false,
				true, false, true, false, true, false
		};
		assert(testInputs.size() == expectedResults.size());
		for (size_t i = 0; i < testInputs.size(); i++) {
			EXPECT_EQ(expectedResults[i], converter.parse(MemoryRegionLevel::ALL_WRITE_PERMISSION_REGION, EXAMPLE_MAIN_PID, testInputs[i]));
		}
	}

	{
		std::vector<bool> expectedResults = {
				true, true, true, false, false, false,
				true, false, true, false, true, false
		};
		assert(testInputs.size() == expectedResults.size());
		for (size_t i = 0; i < testInputs.size(); i++) {
			EXPECT_EQ(expectedResults[i], converter.parse(MemoryRegionLevel::HEAP_STACK, EXAMPLE_MAIN_PID, testInputs[i]));
		}
	}

	{
		std::vector<bool> expectedResults = {
				false, false, true, false, false, false,
				true, false, true, false, true, false
		};
		assert(testInputs.size() == expectedResults.size());
		for (size_t i = 0; i < testInputs.size(); i++) {
			EXPECT_EQ(expectedResults[i], converter.parse(MemoryRegionLevel::HEAP, EXAMPLE_MAIN_PID, testInputs[i]));
		}
	}
}

} /* END of namespace */
