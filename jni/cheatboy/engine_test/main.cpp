#include <stdlib.h>
#include <gtest/gtest.h>
#include <PureLib.h>

using namespace pure;

int main(int argc, char* argv[]) {
	if (!LoggerInstaller::install()) {
		return EXIT_FAILURE;
	}
	pure::println("Logger is installed successfully.");

	::testing::InitGoogleTest(&argc, argv);
	int returnCode = RUN_ALL_TESTS();

	LoggerInstaller::uninstall();
	return returnCode;
}
