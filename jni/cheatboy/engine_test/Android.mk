LOCAL_PATH := $(call my-dir)
$(call __ndk_info, at [$(LOCAL_PATH)])

####### Build CheatBoyEngineTests
include $(CLEAR_VARS)

$(call import-add-path, $(PVAL_MAIN_SOURCE_FILE_ROOT_PATH))

# Set LOCAL_ARM_MODE.
LOCAL_ARM_MODE := $(PVAL_MAIN_ARM_MODE)

LOCAL_MODULE			:= CheatBoyEngineTests

LOCAL_STATIC_LIBRARIES	+= CheatBoyEngine googletest_main
LOCAL_LDLIBS			+= -llog

LOCAL_C_INCLUDES	+= $(PVAL_MAIN_SOURCE_FILE_ROOT_PATH)
LOCAL_SRC_FILES		+= $(call pfunc_get_all_cpp_file_list_under_local_path, $(LOCAL_PATH))
$(foreach e, $(LOCAL_C_INCLUDES), $(call __ndk_info, Will include [$(e)]))
$(foreach e, $(LOCAL_SRC_FILES), $(call __ndk_info, Will build [$(e)]))

include $(BUILD_EXECUTABLE)

$(call import-module, cheatboy/engine)
$(call import-module, third_party/googletest)