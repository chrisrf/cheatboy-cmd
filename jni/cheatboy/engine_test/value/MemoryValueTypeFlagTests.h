#ifndef __MemoryValueTypeFlagTests_H__
#define __MemoryValueTypeFlagTests_H__

#include <gtest/gtest.h>

namespace cboy {
class MemoryValueTypeFlag;

class MemoryValueTypeFlagTests: public ::testing::Test {
public:
	MemoryValueTypeFlagTests() {}
	virtual ~MemoryValueTypeFlagTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	template<typename T1, typename T2> void testMarkForFunction(MemoryValueTypeFlag& typeFlag);
};

} /* END of namespace */

#endif
