#ifndef __WriteValueTests_H__
#define __WriteValueTests_H__

#include <stdint.h>
#include <gtest/gtest.h>

namespace cboy {
class WriteValue;

class WriteValueTests: public ::testing::Test {
public:
	WriteValueTests() {}
	virtual ~WriteValueTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	template<typename T> void testWriteValueFunction(WriteValue& writeValue, uintptr_t fakeAddress, T expectedValue);
};

} /* END of namespace */

#endif
