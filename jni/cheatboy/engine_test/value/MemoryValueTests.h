#ifndef __MemoryValueTests_H__
#define __MemoryValueTests_H__

#include <gtest/gtest.h>

namespace cboy {
class MemoryValue;

class MemoryValueTests: public ::testing::Test {
public:
	MemoryValueTests() {}
	virtual ~MemoryValueTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	template<typename T> void testWriteValueFunction(MemoryValue& writeValue, T expectedValue);
};

} /* END of namespace */

#endif
