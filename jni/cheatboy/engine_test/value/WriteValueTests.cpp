#include "WriteValueTests.h"

#include <cheatboy/engine/value/WriteValue.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/MemoryValueTypeIdUtils.h>
#include <cheatboy/engine_test/value/ValueTestHelper.h>

namespace cboy {

template<typename T>
void WriteValueTests::testWriteValueFunction(WriteValue& writeValue, uintptr_t fakeAddress, T expectedValue) {
	writeValue.wantToWriteFor<T>(fakeAddress, expectedValue);
	EXPECT_TRUE(writeValue.isValidState());
	EXPECT_EQ(MemoryValueTypeIdUtils::enumOf<T>(), writeValue.getTypeEnum());
	EXPECT_EQ(MemoryValueTypeIdUtils::idOf<T>(), writeValue.getTypeID());
	EXPECT_EQ(fakeAddress, writeValue.getRemoteAddress());
	EXPECT_EQ(sizeof(T), writeValue.getSize());
	ValueTestHelper::assertBufferZero(writeValue.getBuffer() + writeValue.getSize(), writeValue.getCapacity() - writeValue.getSize());

	EXPECT_EQ(expectedValue, writeValue.getAs<T>());
	EXPECT_EQ(expectedValue, (*reinterpret_cast<T*>(writeValue.getBuffer())));
	EXPECT_TRUE(writeValue.isValidState());
	EXPECT_EQ(MemoryValueTypeIdUtils::enumOf<T>(), writeValue.getTypeEnum());
	EXPECT_EQ(MemoryValueTypeIdUtils::idOf<T>(), writeValue.getTypeID());
	EXPECT_EQ(fakeAddress, writeValue.getRemoteAddress());
	EXPECT_EQ(sizeof(T), writeValue.getSize());
	ValueTestHelper::assertBufferZero(writeValue.getBuffer() + writeValue.getSize(), writeValue.getCapacity() - writeValue.getSize());

	writeValue.clear();
	EXPECT_TRUE(writeValue.isValidState());
	EXPECT_EQ(MemoryValueTypeID::UNKNOWN, writeValue.getTypeEnum());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::UNKNOWN), writeValue.getTypeID());
	EXPECT_EQ(0, writeValue.getRemoteAddress());
	EXPECT_EQ(0, writeValue.getSize());
	ValueTestHelper::assertBufferZero(writeValue.getBuffer(), writeValue.getCapacity());
}

TEST_F(WriteValueTests,testWriteValue) {
	WriteValue writeValue;
	uintptr_t fakeAddress = 0x05060708;

	testWriteValueFunction<int8_t>(writeValue, fakeAddress, -8);
	testWriteValueFunction<uint8_t>(writeValue, fakeAddress, 248);
	testWriteValueFunction<int16_t>(writeValue, fakeAddress, -2056);
	testWriteValueFunction<uint16_t>(writeValue, fakeAddress, 63480);
	testWriteValueFunction<int32_t>(writeValue, fakeAddress, -168364040);
	testWriteValueFunction<uint32_t>(writeValue, fakeAddress, 4126603256);
	testWriteValueFunction<int64_t>(writeValue, fakeAddress, -1012478732780767240LL);
	testWriteValueFunction<uint64_t>(writeValue, fakeAddress, 17434265340928784376ULL);
	testWriteValueFunction<float>(writeValue, fakeAddress, -6.261399e32F);
	testWriteValueFunction<double>(writeValue, fakeAddress, -7.8986617409766022e240);
}

TEST_F(WriteValueTests,testMove) {
	uintptr_t fakeAddress = 0x05060708;
	std::function<void(WriteValue&)> genWriteValue = [&fakeAddress] (WriteValue& newWriteValue) {
		newWriteValue.wantToWriteFor<uint32_t>(fakeAddress, 4126603256);
	};

	WriteValue expectedWriteValue;
	genWriteValue(expectedWriteValue);
	{
		WriteValue writeValue001;
		genWriteValue(writeValue001);
		ValueTestHelper::assertWriteValueEqual(expectedWriteValue, writeValue001);

		WriteValue writeValue002(std::move(writeValue001));
		EXPECT_FALSE(writeValue001.isValidState());
		ValueTestHelper::assertWriteValueEqual(expectedWriteValue, writeValue002);
	}
	{
		WriteValue writeValue001;
		genWriteValue(writeValue001);
		ValueTestHelper::assertWriteValueEqual(expectedWriteValue, writeValue001);

		WriteValue writeValue002;
		writeValue002 = std::move(writeValue001);
		EXPECT_FALSE(writeValue001.isValidState());
		ValueTestHelper::assertWriteValueEqual(expectedWriteValue, writeValue002);
	}
}

} /* END of namespace */
