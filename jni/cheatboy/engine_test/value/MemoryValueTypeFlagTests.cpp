#include "MemoryValueTypeFlagTests.h"

#include <cheatboy/engine/value/MemoryValueTypeFlag.h>

namespace cboy {

template<typename T1, typename T2>
void MemoryValueTypeFlagTests::testMarkForFunction(MemoryValueTypeFlag& typeFlag) {
	EXPECT_EQ(false, typeFlag.isMarkedForAny());

	typeFlag.markFor<T1>();
	EXPECT_TRUE(typeFlag.isMarkedFor<T1>());
	EXPECT_EQ(true, typeFlag.isMarkedFor<T1>());
	EXPECT_EQ(false, typeFlag.isMarkedFor<T2>());
	EXPECT_EQ(true, typeFlag.isMarkedOnlyFor<T1>());
	EXPECT_EQ(false, typeFlag.isMarkedOnlyFor<T2>());

	typeFlag.markFor<T2>();
	EXPECT_EQ(true, typeFlag.isMarkedFor<T1>());
	EXPECT_EQ(true, typeFlag.isMarkedFor<T2>());
	EXPECT_EQ(false, typeFlag.isMarkedOnlyFor<T1>());
	EXPECT_EQ(false, typeFlag.isMarkedOnlyFor<T2>());

	typeFlag.unmarkFor<T1>();
	EXPECT_EQ(false, typeFlag.isMarkedFor<T1>());
	EXPECT_EQ(true, typeFlag.isMarkedFor<T2>());
	EXPECT_EQ(false, typeFlag.isMarkedOnlyFor<T1>());
	EXPECT_EQ(true, typeFlag.isMarkedOnlyFor<T2>());

	typeFlag.unmarkFor<T2>();
	EXPECT_EQ(false, typeFlag.isMarkedFor<T1>());
	EXPECT_EQ(false, typeFlag.isMarkedFor<T2>());
	EXPECT_EQ(false, typeFlag.isMarkedOnlyFor<T1>());
	EXPECT_EQ(false, typeFlag.isMarkedOnlyFor<T2>());

	EXPECT_EQ(false, typeFlag.isMarkedForAny());
}

TEST_F(MemoryValueTypeFlagTests,testConstructByFlag) {
	MemoryValueTypeFlag typeFlag(1030);	// [0b 0000 0100 0000 0110](1030) validate to [0b 0000 0000 0000 0110](6)

	EXPECT_EQ(6, typeFlag.getFlag());
	EXPECT_EQ(true, typeFlag.isMarkedForAny());
	typeFlag.clearFlag();
	EXPECT_EQ(0, typeFlag.getFlag());
	EXPECT_EQ(false, typeFlag.isMarkedForAny());
}

TEST_F(MemoryValueTypeFlagTests,testSetAndGetFlag) {
	MemoryValueTypeFlag typeFlag;

	typeFlag.setFlag(1030);	// [0b 0000 0100 0000 0110](1030) validate to [0b 0000 0000 0000 0110](6)
	EXPECT_EQ(6, typeFlag.getFlag());
	EXPECT_EQ(true, typeFlag.isMarkedForAny());
	typeFlag.clearFlag();
	EXPECT_EQ(0, typeFlag.getFlag());
	EXPECT_EQ(false, typeFlag.isMarkedForAny());
}

TEST_F(MemoryValueTypeFlagTests,testMarkFor) {
	MemoryValueTypeFlag typeFlag;

	testMarkForFunction<int8_t, uint8_t>(typeFlag);
	testMarkForFunction<uint8_t, int8_t>(typeFlag);

	testMarkForFunction<int16_t, uint16_t>(typeFlag);
	testMarkForFunction<uint16_t, int16_t>(typeFlag);

	testMarkForFunction<int32_t, uint32_t>(typeFlag);
	testMarkForFunction<uint32_t, int32_t>(typeFlag);

	testMarkForFunction<int64_t, uint64_t>(typeFlag);
	testMarkForFunction<uint64_t, int64_t>(typeFlag);

	testMarkForFunction<float, double>(typeFlag);
	testMarkForFunction<double, float>(typeFlag);
}

TEST_F(MemoryValueTypeFlagTests,testMark) {
	MemoryValueTypeFlag typeFlag;

//	typeFlag.mark(1024);	// [0b 0000 0100 0000 0000](1024)	// assertion "(flag & (~ALL)) == 0" failed
	typeFlag.mark(MemoryValueTypeFlag::I08 | MemoryValueTypeFlag::U08);
	EXPECT_EQ(true, typeFlag.isMarkedForAny());
	EXPECT_EQ(true, typeFlag.isMarked(MemoryValueTypeFlag::I08));
	EXPECT_EQ(true, typeFlag.isMarked(MemoryValueTypeFlag::U08));
	EXPECT_EQ(false, typeFlag.isMarkedOnly(MemoryValueTypeFlag::I08));
	EXPECT_EQ(false, typeFlag.isMarkedOnly(MemoryValueTypeFlag::U08));

	typeFlag.unmark(MemoryValueTypeFlag::I08);
	EXPECT_EQ(true, typeFlag.isMarkedForAny());
	EXPECT_EQ(false, typeFlag.isMarked(MemoryValueTypeFlag::I08));
	EXPECT_EQ(true, typeFlag.isMarked(MemoryValueTypeFlag::U08));
	EXPECT_EQ(false, typeFlag.isMarkedOnly(MemoryValueTypeFlag::I08));
	EXPECT_EQ(true, typeFlag.isMarkedOnly(MemoryValueTypeFlag::U08));

	typeFlag.unmark(MemoryValueTypeFlag::U08);
	EXPECT_EQ(false, typeFlag.isMarkedForAny());
}

TEST_F(MemoryValueTypeFlagTests,testMarkGroup) {
	MemoryValueTypeFlag typeFlag;

	typeFlag.mark(MemoryValueTypeFlag::ALL);
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	EXPECT_EQ(true,		typeFlag.isMarkedForAny());
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	typeFlag.unmark(MemoryValueTypeFlag::ALL);
	EXPECT_EQ(false,	typeFlag.isMarkedForAny());

	typeFlag.mark(MemoryValueTypeFlag::ALL_SIGNED_INTEGER);
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	EXPECT_EQ(true,		typeFlag.isMarkedForAny());
	EXPECT_EQ(true,		typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	typeFlag.unmark(MemoryValueTypeFlag::ALL_SIGNED_INTEGER);
	EXPECT_EQ(false,	typeFlag.isMarkedForAny());

	typeFlag.mark(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER);
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	EXPECT_EQ(true,		typeFlag.isMarkedForAny());
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	typeFlag.unmark(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER);
	EXPECT_EQ(false,	typeFlag.isMarkedForAny());

	typeFlag.mark(MemoryValueTypeFlag::ALL_INTEGER);
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	EXPECT_EQ(true,		typeFlag.isMarkedForAny());
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	typeFlag.unmark(MemoryValueTypeFlag::ALL_INTEGER);
	EXPECT_EQ(false,	typeFlag.isMarkedForAny());

	typeFlag.mark(MemoryValueTypeFlag::ALL_FLOAT);
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	EXPECT_EQ(true,		typeFlag.isMarkedForAny());
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	typeFlag.unmark(MemoryValueTypeFlag::ALL_FLOAT);
	EXPECT_EQ(false, typeFlag.isMarkedForAny());

	typeFlag.mark(MemoryValueTypeFlag::ALL_SIGNED_NUMBER);
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarked(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(true,		typeFlag.isMarked(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	EXPECT_EQ(true,		typeFlag.isMarkedForAny());
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_UNSIGNED_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_INTEGER));
	EXPECT_EQ(false,	typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_FLOAT));
	EXPECT_EQ(true,		typeFlag.isMarkedOnly(MemoryValueTypeFlag::ALL_SIGNED_NUMBER));
	typeFlag.unmark(MemoryValueTypeFlag::ALL_SIGNED_NUMBER);
	EXPECT_EQ(false, typeFlag.isMarkedForAny());
}

TEST_F(MemoryValueTypeFlagTests,testCopy) {
	MemoryValueTypeFlag typeFlag001;
	typeFlag001.setFlag(MemoryValueTypeFlag::ALL_SIGNED_INTEGER);
	EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag001.getFlag());

	{
		MemoryValueTypeFlag typeFlag002(typeFlag001);
		EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag001.getFlag());
		EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag002.getFlag());
	}

	{
		MemoryValueTypeFlag typeFlag002;
		EXPECT_EQ(0, typeFlag002.getFlag());

		typeFlag002 = typeFlag001;
		EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag001.getFlag());
		EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag002.getFlag());
	}
}

TEST_F(MemoryValueTypeFlagTests,testMove) {
	MemoryValueTypeFlag typeFlag001;
	typeFlag001.setFlag(MemoryValueTypeFlag::ALL_SIGNED_INTEGER);
	EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag001.getFlag());

	{
		MemoryValueTypeFlag typeFlag002(std::move(typeFlag001));
		EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag001.getFlag());
		EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag002.getFlag());
	}

	{
		MemoryValueTypeFlag typeFlag002;
		EXPECT_EQ(0, typeFlag002.getFlag());

		typeFlag002 = std::move(typeFlag001);
		EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag001.getFlag());
		EXPECT_EQ(MemoryValueTypeFlag::ALL_SIGNED_INTEGER, typeFlag002.getFlag());
	}
}

} /* END of namespace */
