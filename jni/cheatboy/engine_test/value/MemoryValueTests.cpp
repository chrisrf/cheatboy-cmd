#include "MemoryValueTests.h"

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine_test/value/ValueTestHelper.h>

namespace cboy {

template<typename T>
void MemoryValueTests::testWriteValueFunction(MemoryValue& writeValue, T expectedValue) {
	writeValue.clear();

	writeValue.setAs<T>(expectedValue);
	EXPECT_EQ(expectedValue, writeValue.getAs<T>());
	EXPECT_EQ(expectedValue, (*reinterpret_cast<T*>(writeValue.getBuffer())));
	ValueTestHelper::assertBufferZero(writeValue.getBuffer() + sizeof(expectedValue), writeValue.getCapacity() - sizeof(expectedValue));
}

TEST_F(MemoryValueTests,testReadValue) {
	MemoryValue readValue;
	uint8_t memToRead[MemoryValue::getCapacity()] = {
			0xf8, 0xf7, 0xf6, 0xf5,
			0xf4, 0xf3, 0xf2, 0xf1
	};

	readValue.clear();
	memcpy(readValue.getBuffer(), memToRead, readValue.getCapacity());

	EXPECT_EQ(-8, readValue.getAs<int8_t>());
	EXPECT_EQ(248, readValue.getAs<uint8_t>());
	EXPECT_EQ(-2056, readValue.getAs<int16_t>());
	EXPECT_EQ(63480, readValue.getAs<uint16_t>());
	EXPECT_EQ(-168364040, readValue.getAs<int32_t>());
	EXPECT_EQ(4126603256, readValue.getAs<uint32_t>());
	EXPECT_EQ(-1012478732780767240LL, readValue.getAs<int64_t>());
	EXPECT_EQ(17434265340928784376ULL, readValue.getAs<uint64_t>());
	EXPECT_FLOAT_EQ(-6.261399e32F, readValue.getAs<float>());
	EXPECT_DOUBLE_EQ(-7.8986617409766022e240, readValue.getAs<double>());
}

TEST_F(MemoryValueTests,testWriteValue) {
	MemoryValue writeValue;

	testWriteValueFunction<int8_t>(writeValue, -8);
	testWriteValueFunction<uint8_t>(writeValue, 248);
	testWriteValueFunction<int16_t>(writeValue, -2056);
	testWriteValueFunction<uint16_t>(writeValue, 63480);
	testWriteValueFunction<int32_t>(writeValue, -168364040);
	testWriteValueFunction<uint32_t>(writeValue, 4126603256);
	testWriteValueFunction<int64_t>(writeValue, -1012478732780767240LL);
	testWriteValueFunction<uint64_t>(writeValue, 17434265340928784376ULL);
	testWriteValueFunction<float>(writeValue, 123.123F);
	testWriteValueFunction<double>(writeValue, 567.567);
}

TEST_F(MemoryValueTests,testCopy) {
	MemoryValue expectedValue;
	uint8_t memToRead[MemoryValue::getCapacity()] = {
			0xf8, 0xf7, 0xf6, 0xf5,
			0xf4, 0xf3, 0xf2, 0xf1
	};
	expectedValue.clear();
	memcpy(expectedValue.getBuffer(), memToRead, expectedValue.getCapacity());

	MemoryValue value001;
	memcpy(value001.getBuffer(), expectedValue.getBuffer(), value001.getCapacity());
	ValueTestHelper::assertMemoryValueEqual(expectedValue, value001);
	{
		MemoryValue value002(value001);
		ValueTestHelper::assertMemoryValueEqual(expectedValue, value001);
		ValueTestHelper::assertMemoryValueEqual(expectedValue, value002);
	}

	{
		MemoryValue value002;
		ValueTestHelper::assertMemoryValueZero(value002);

		value002 = value001;
		ValueTestHelper::assertMemoryValueEqual(expectedValue, value001);
		ValueTestHelper::assertMemoryValueEqual(expectedValue, value002);
	}
}

TEST_F(MemoryValueTests,testMove) {
	MemoryValue expectedValue;
	uint8_t memToRead[MemoryValue::getCapacity()] = {
			0xf8, 0xf7, 0xf6, 0xf5,
			0xf4, 0xf3, 0xf2, 0xf1
	};
	expectedValue.clear();
	memcpy(expectedValue.getBuffer(), memToRead, expectedValue.getCapacity());

	MemoryValue value001;
	memcpy(value001.getBuffer(), expectedValue.getBuffer(), value001.getCapacity());
	ValueTestHelper::assertMemoryValueEqual(expectedValue, value001);
	{
		MemoryValue value002(std::move(value001));
		ValueTestHelper::assertMemoryValueBufferEqual(expectedValue, value001);
		ValueTestHelper::assertMemoryValueBufferEqual(expectedValue, value002);
	}

	{
		MemoryValue value002;
		ValueTestHelper::assertMemoryValueZero(value002);

		value002 = std::move(value001);
		ValueTestHelper::assertMemoryValueBufferEqual(expectedValue, value001);
		ValueTestHelper::assertMemoryValueBufferEqual(expectedValue, value002);
	}
}

} /* END of namespace */
