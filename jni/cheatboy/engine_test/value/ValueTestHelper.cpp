#include "ValueTestHelper.h"

#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/ReadValue.h>
#include <cheatboy/engine/value/WriteValue.h>

namespace cboy {

void ValueTestHelper::assertBufferZero(uint8_t* buf, size_t size) {
	for (size_t i = 0; i < size; i++) {
		EXPECT_EQ(0, buf[i]);
	}
}

void ValueTestHelper::assertBufferEqual(uint8_t* expcetedBuf, uint8_t* actualBuf, size_t size) {
	for (size_t i = 0; i < size; i++) {
		EXPECT_EQ(expcetedBuf[i], actualBuf[i]);
	}
}

void ValueTestHelper::assertMemoryValueZero(MemoryValue& value) {
	uint8_t* buffer = value.getBuffer();
	assertBufferZero(buffer, MemoryValue::getCapacity());
}

void ValueTestHelper::assertMemoryValueEqual(MemoryValue& expectedValue, MemoryValue& actualValue) {
	assertBufferEqual(expectedValue.getBuffer(), actualValue.getBuffer(), MemoryValue::getCapacity());
}

void ValueTestHelper::assertMemoryValueBufferEqual(MemoryValue& expectedValue, MemoryValue& actualValue) {
	assertBufferEqual(expectedValue.getBuffer(), actualValue.getBuffer(), MemoryValue::getCapacity());
}

void ValueTestHelper::assertReadValueEqual(ReadValue& expectedValue, ReadValue& actualValue) {
	assertReadValueBufferEqual(expectedValue, actualValue);
	EXPECT_EQ(expectedValue.isValidState(), actualValue.isValidState());
	EXPECT_EQ(expectedValue.getTypeEnum(), actualValue.getTypeEnum());
	EXPECT_EQ(expectedValue.getTypeID(), actualValue.getTypeID());
	EXPECT_EQ(expectedValue.getRemoteAddress(), actualValue.getRemoteAddress());
	EXPECT_EQ(expectedValue.getSize(), actualValue.getSize());
}

void ValueTestHelper::assertReadValueBufferEqual(ReadValue& expectedValue, ReadValue& actualValue) {
	assertBufferEqual(expectedValue.getBuffer(), actualValue.getBuffer(), ReadValue::getCapacity());
}

void ValueTestHelper::assertWriteValueEqual(WriteValue& expectedValue, WriteValue& actualValue) {
	assertWriteValueBufferEqual(expectedValue, actualValue);
	EXPECT_EQ(expectedValue.isValidState(), actualValue.isValidState());
	EXPECT_EQ(expectedValue.getTypeEnum(), actualValue.getTypeEnum());
	EXPECT_EQ(expectedValue.getTypeID(), actualValue.getTypeID());
	EXPECT_EQ(expectedValue.getRemoteAddress(), actualValue.getRemoteAddress());
	EXPECT_EQ(expectedValue.getSize(), actualValue.getSize());
}

void ValueTestHelper::assertWriteValueBufferEqual(WriteValue& expectedValue, WriteValue& actualValue) {
	assertBufferEqual(expectedValue.getBuffer(), actualValue.getBuffer(), WriteValue::getCapacity());
}

} /* END of namespace */
