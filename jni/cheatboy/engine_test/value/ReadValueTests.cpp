#include "ReadValueTests.h"

#include <string.h>
#include <functional>
#include <cheatboy/engine/value/ReadValue.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/MemoryValueTypeIdUtils.h>
#include <cheatboy/engine_test/value/ValueTestHelper.h>

namespace cboy {

template<typename T>
T ReadValueTests::testReadValueFunction(ReadValue& readValue, uint8_t* memToRead, uintptr_t fakeAddress) {
	readValue.wantToReadFor<T>(fakeAddress);
	EXPECT_TRUE(readValue.isValidState());
	EXPECT_EQ(MemoryValueTypeIdUtils::enumOf<T>(), readValue.getTypeEnum());
	EXPECT_EQ(MemoryValueTypeIdUtils::idOf<T>(), readValue.getTypeID());
	EXPECT_EQ(fakeAddress, readValue.getRemoteAddress());
	EXPECT_EQ(sizeof(T), readValue.getSize());
	ValueTestHelper::assertBufferZero(readValue.getBuffer(), readValue.getCapacity());

	memcpy(readValue.getBuffer(), memToRead, readValue.getSize());
	T genValue = readValue.getAs<T>();
	//EXPECT_EQ(expectedValue, genValue);
	EXPECT_TRUE(readValue.isValidState());
	EXPECT_EQ(MemoryValueTypeIdUtils::enumOf<T>(), readValue.getTypeEnum());
	EXPECT_EQ(MemoryValueTypeIdUtils::idOf<T>(), readValue.getTypeID());
	EXPECT_EQ(fakeAddress, readValue.getRemoteAddress());
	EXPECT_EQ(sizeof(T), readValue.getSize());
	ValueTestHelper::assertBufferZero(readValue.getBuffer() + readValue.getSize(), readValue.getCapacity() - readValue.getSize());

	readValue.clear();
	EXPECT_TRUE(readValue.isValidState());
	EXPECT_EQ(MemoryValueTypeID::UNKNOWN, readValue.getTypeEnum());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::UNKNOWN), readValue.getTypeID());
	EXPECT_EQ(0, readValue.getRemoteAddress());
	EXPECT_EQ(0, readValue.getSize());
	ValueTestHelper::assertBufferZero(readValue.getBuffer(), readValue.getCapacity());

	return genValue;
}

TEST_F(ReadValueTests,testReadValue) {
	ReadValue readValue;
	uint8_t memToRead[ReadValue::getCapacity()] = {
			0xf8, 0xf7, 0xf6, 0xf5,
			0xf4, 0xf3, 0xf2, 0xf1
	};
	uintptr_t fakeAddress = 0x05060708;

	EXPECT_EQ(-8, testReadValueFunction<int8_t>(readValue, memToRead, fakeAddress));
	EXPECT_EQ(248, testReadValueFunction<uint8_t>(readValue, memToRead, fakeAddress));
	EXPECT_EQ(-2056, testReadValueFunction<int16_t>(readValue, memToRead, fakeAddress));
	EXPECT_EQ(63480, testReadValueFunction<uint16_t>(readValue, memToRead, fakeAddress));
	EXPECT_EQ(-168364040, testReadValueFunction<int32_t>(readValue, memToRead, fakeAddress));
	EXPECT_EQ(4126603256, testReadValueFunction<uint32_t>(readValue, memToRead, fakeAddress));
	EXPECT_EQ(-1012478732780767240LL, testReadValueFunction<int64_t>(readValue, memToRead, fakeAddress));
	EXPECT_EQ(17434265340928784376ULL, testReadValueFunction<uint64_t>(readValue, memToRead, fakeAddress));
	EXPECT_FLOAT_EQ(-6.261399e32F, testReadValueFunction<float>(readValue, memToRead, fakeAddress));
	EXPECT_DOUBLE_EQ(-7.8986617409766022e240, testReadValueFunction<double>(readValue, memToRead, fakeAddress));
}

TEST_F(ReadValueTests,testMove) {
	uint8_t memToRead[ReadValue::getCapacity()] = {
			0xf8, 0xf7, 0xf6, 0xf5,
			0xf4, 0xf3, 0xf2, 0xf1
	};
	uintptr_t fakeAddress = 0x05060708;
	std::function<void(ReadValue&)> genReadValue = [&memToRead, &fakeAddress] (ReadValue& newReadValue) {
		newReadValue.wantToReadFor<uint32_t>(fakeAddress);
		memcpy(newReadValue.getBuffer(), memToRead, newReadValue.getSize());
	};

	ReadValue expectedReadValue;
	genReadValue(expectedReadValue);
	{
		ReadValue readValue001;
		genReadValue(readValue001);
		ValueTestHelper::assertReadValueEqual(expectedReadValue, readValue001);

		ReadValue readValue002(std::move(readValue001));
		EXPECT_FALSE(readValue001.isValidState());
		ValueTestHelper::assertReadValueEqual(expectedReadValue, readValue002);
	}
	{
		ReadValue readValue001;
		genReadValue(readValue001);
		ValueTestHelper::assertReadValueEqual(expectedReadValue, readValue001);

		ReadValue readValue002;
		readValue002 = std::move(readValue001);
		EXPECT_FALSE(readValue001.isValidState());
		ValueTestHelper::assertReadValueEqual(expectedReadValue, readValue002);
	}
}

} /* END of namespace */
