#include "MemoryValueTypeIDTests.h"

#include <stdint.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/MemoryValueTypeIdUtils.h>

namespace cboy {

TEST_F(MemoryValueTypeIDTests,testID) {
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::I08), MemoryValueTypeIdUtils::idOf<int8_t>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::U08), MemoryValueTypeIdUtils::idOf<uint8_t>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::I16), MemoryValueTypeIdUtils::idOf<int16_t>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::U16), MemoryValueTypeIdUtils::idOf<uint16_t>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::I32), MemoryValueTypeIdUtils::idOf<int32_t>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::U32), MemoryValueTypeIdUtils::idOf<uint32_t>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::I64), MemoryValueTypeIdUtils::idOf<int64_t>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::U64), MemoryValueTypeIdUtils::idOf<uint64_t>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::F32), MemoryValueTypeIdUtils::idOf<float>());
	EXPECT_EQ(static_cast<int>(MemoryValueTypeID::F64), MemoryValueTypeIdUtils::idOf<double>());

	EXPECT_EQ(MemoryValueTypeID::I08, MemoryValueTypeIdUtils::enumOf<int8_t>());
	EXPECT_EQ(MemoryValueTypeID::U08, MemoryValueTypeIdUtils::enumOf<uint8_t>());
	EXPECT_EQ(MemoryValueTypeID::I16, MemoryValueTypeIdUtils::enumOf<int16_t>());
	EXPECT_EQ(MemoryValueTypeID::U16, MemoryValueTypeIdUtils::enumOf<uint16_t>());
	EXPECT_EQ(MemoryValueTypeID::I32, MemoryValueTypeIdUtils::enumOf<int32_t>());
	EXPECT_EQ(MemoryValueTypeID::U32, MemoryValueTypeIdUtils::enumOf<uint32_t>());
	EXPECT_EQ(MemoryValueTypeID::I64, MemoryValueTypeIdUtils::enumOf<int64_t>());
	EXPECT_EQ(MemoryValueTypeID::U64, MemoryValueTypeIdUtils::enumOf<uint64_t>());
	EXPECT_EQ(MemoryValueTypeID::F32, MemoryValueTypeIdUtils::enumOf<float>());
	EXPECT_EQ(MemoryValueTypeID::F64, MemoryValueTypeIdUtils::enumOf<double>());

	EXPECT_EQ(10, static_cast<int>(MemoryValueTypeID::ENUM_NUMS));
}

} /* END of namespace */
