#include "MemoryValueTypeFlagUtilsTests.h"

#include <PureLib.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/value/MemoryValueTypeID.h>
#include <cheatboy/engine/value/MemoryValueTypeFlagUtils.h>

namespace cboy {

TEST_F(MemoryValueTypeFlagUtilsTests,testMemoryValueTypeFlagUtils) {
	EXPECT_EQ(MemoryValueTypeFlag::I08, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::I08));
	EXPECT_EQ(MemoryValueTypeFlag::U08, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::U08));
	EXPECT_EQ(MemoryValueTypeFlag::I16, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::I16));
	EXPECT_EQ(MemoryValueTypeFlag::U16, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::U16));
	EXPECT_EQ(MemoryValueTypeFlag::I32, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::I32));
	EXPECT_EQ(MemoryValueTypeFlag::U32, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::U32));
	EXPECT_EQ(MemoryValueTypeFlag::I64, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::I64));
	EXPECT_EQ(MemoryValueTypeFlag::U64, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::U64));
	EXPECT_EQ(MemoryValueTypeFlag::F32, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::F32));
	EXPECT_EQ(MemoryValueTypeFlag::F64, MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID::F64));

	EXPECT_EQ(MemoryValueTypeFlag::I08, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::I08)));
	EXPECT_EQ(MemoryValueTypeFlag::U08, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::U08)));
	EXPECT_EQ(MemoryValueTypeFlag::I16, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::I16)));
	EXPECT_EQ(MemoryValueTypeFlag::U16, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::U16)));
	EXPECT_EQ(MemoryValueTypeFlag::I32, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::I32)));
	EXPECT_EQ(MemoryValueTypeFlag::U32, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::U32)));
	EXPECT_EQ(MemoryValueTypeFlag::I64, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::I64)));
	EXPECT_EQ(MemoryValueTypeFlag::U64, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::U64)));
	EXPECT_EQ(MemoryValueTypeFlag::F32, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::F32)));
	EXPECT_EQ(MemoryValueTypeFlag::F64, MemoryValueTypeFlagUtils::fromTypeID(static_cast<int>(MemoryValueTypeID::F64)));
}

TEST_F(MemoryValueTypeFlagUtilsTests,testMemoryValueTypeFlagConverterForFailure) {
	bool isCaught = false;

	try {
		MemoryValueTypeFlagUtils::fromTypeID(-1);
	} catch (pure::Exception& e) {
		isCaught = true;
	}
	EXPECT_TRUE(isCaught);
	isCaught = false;

	try {
		MemoryValueTypeFlagUtils::fromTypeIdEnum(MemoryValueTypeID(-1));
	} catch (pure::Exception& e) {
		isCaught = true;
	}
	EXPECT_TRUE(isCaught);
	isCaught = false;
}

} /* END of namespace */
