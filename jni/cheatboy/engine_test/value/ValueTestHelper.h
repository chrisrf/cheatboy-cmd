#ifndef __ValueTestHelper_H__
#define __ValueTestHelper_H__

#include <gtest/gtest.h>
#include <PureMacro.h>

namespace cboy {
enum class MemoryValueTypeID;
class MemoryValue;
class ReadValue;
class WriteValue;

class ValueTestHelper {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(ValueTestHelper);
public:
	static void assertBufferZero(uint8_t* buf, size_t size);
	static void assertBufferEqual(uint8_t* expcetedBuf, uint8_t* actualBuf, size_t size);
public:
	static void assertMemoryValueZero(MemoryValue& value);
	static void assertMemoryValueEqual(MemoryValue& expectedValue, MemoryValue& actualValue);
	static void assertMemoryValueBufferEqual(MemoryValue& expectedValue, MemoryValue& actualValue);
public:
	static void assertReadValueEqual(ReadValue& expectedValue, ReadValue& actualValue);
	static void assertReadValueBufferEqual(ReadValue& expectedValue, ReadValue& actualValue);
public:
	static void assertWriteValueEqual(WriteValue& expectedValue, WriteValue& actualValue);
	static void assertWriteValueBufferEqual(WriteValue& expectedValue, WriteValue& actualValue);
};

} /* END of namespace */

#endif
