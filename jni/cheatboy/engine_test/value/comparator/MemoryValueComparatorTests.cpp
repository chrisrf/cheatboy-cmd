#include "MemoryValueComparatorTests.h"

#include <assert.h>
#include <vector>
#include <tuple>
#include <limits>
#include <cheatboy/engine/value/MemoryValue.h>
#include <cheatboy/engine/value/MemoryValueTypeFlag.h>
#include <cheatboy/engine/value/UserValue.h>
#include <cheatboy/engine/value/defs/MemoryValueTypeFlagDefs.h>
#include <cheatboy/engine/value/comparator/ComparisonMethod.h>
#include <cheatboy/engine/value/comparator/MemoryValueComparator.h>

namespace cboy {

TEST_F(MemoryValueComparatorTests,testComapreForComparisonFlag) {
	typedef MemoryValueTypeFlag MVTFlag;
	typedef MemoryValueTypeFlagDefs::flag_t flag_t;

	MemoryValueComparator comparator;
	MemoryValue newValue;
	MemoryValue oldValue;
	UserValue userValue;
	MemoryValueTypeFlag cmpFlag;
	MemoryValueTypeFlag savedFlag;

	std::vector<flag_t> testFlags = {
			MVTFlag::ALL,
			MVTFlag::ALL_SIGNED_INTEGER,
			MVTFlag::ALL_UNSIGNED_INTEGER,
			MVTFlag::ALL_INTEGER,
			MVTFlag::ALL_FLOAT,
			MVTFlag::I08 | MVTFlag::I16 | MVTFlag::I32 | MVTFlag::I64 | MVTFlag::F32,
			MVTFlag::U08 | MVTFlag::U16 | MVTFlag::U32 | MVTFlag::U64 | MVTFlag::F64,
			MVTFlag::I08,
			MVTFlag::U08,
			MVTFlag::I16,
			MVTFlag::U16,
			MVTFlag::I32,
			MVTFlag::U32,
			MVTFlag::I64,
			MVTFlag::U64,
			MVTFlag::F32,
			MVTFlag::F64,
			MVTFlag::I08 | MVTFlag::F64
	};

	comparator.setCurrentComparisonMethod(ComparisonMethod::ANY);
	for (const auto& flag : testFlags) {
		cmpFlag.setFlag(flag);
		savedFlag.clearFlag();
		size_t maxBytes = comparator.compareValue(newValue, oldValue, userValue, cmpFlag, savedFlag);
		EXPECT_EQ(true, maxBytes > 0);
		EXPECT_EQ(true, maxBytes <= MemoryValueDefs::MAX_BYTES);
		EXPECT_EQ(true, savedFlag.isMarkedOnly(flag));
	}
	// 50 ComparisonMethod::ANY function calls.
}

template<typename T>
void MemoryValueComparatorTests::testAllComparisonMethodsFor(
	MemoryValueComparator& comparator,
	T baseNum, T equalNum, T notEqualNum, T biggerNum, T smallerNum, T increasedNum, T decreasedNum) {

	MemoryValue newValue;
	MemoryValue oldValue;
	UserValue userValue;
	MemoryValueTypeFlag cmpFlag;
	MemoryValueTypeFlag savedFlag;

	std::vector<ComparisonMethod> testMethods = {
			ComparisonMethod::EQUAL_TO,
			ComparisonMethod::NOT_EQUAL_TO,
			ComparisonMethod::GREATER_THAN,
			ComparisonMethod::LESS_THAN,
			ComparisonMethod::NOT_CHANGED,
			ComparisonMethod::CHANGED,
			ComparisonMethod::INCREASED,
			ComparisonMethod::DECREASED,
			ComparisonMethod::INCREASED_BY,
			ComparisonMethod::DECREASED_BY
	};
	std::vector<std::tuple<T, T, T>> testValuesForMatched = {
			std::make_tuple(baseNum, notEqualNum, equalNum),
			std::make_tuple(baseNum, equalNum, notEqualNum),

			std::make_tuple(baseNum, biggerNum, smallerNum),
			std::make_tuple(baseNum, smallerNum, biggerNum),

			std::make_tuple(baseNum, baseNum, biggerNum),
			std::make_tuple(baseNum, biggerNum, baseNum),

			std::make_tuple(baseNum, smallerNum, biggerNum),
			std::make_tuple(baseNum, biggerNum, smallerNum),

			std::make_tuple(baseNum, (baseNum - increasedNum), increasedNum),
			std::make_tuple(baseNum, (baseNum + decreasedNum), decreasedNum),
	};
	std::vector<std::tuple<T, T, T>> testValuesForNotMatched = {
			std::make_tuple(baseNum, equalNum, notEqualNum),
			std::make_tuple(baseNum, notEqualNum, equalNum),

			std::make_tuple(baseNum, smallerNum, biggerNum),
			std::make_tuple(baseNum, biggerNum, smallerNum),

			std::make_tuple(baseNum, biggerNum, baseNum),
			std::make_tuple(baseNum, baseNum, biggerNum),

			std::make_tuple(baseNum, biggerNum, smallerNum),
			std::make_tuple(baseNum, smallerNum, biggerNum),

			std::make_tuple(baseNum, baseNum, increasedNum),
			std::make_tuple(baseNum, baseNum, decreasedNum),
	};
	assert(testMethods.size() == testValuesForMatched.size());
	assert(testValuesForMatched.size() == testValuesForNotMatched.size());

	for (size_t i = 0; i < testMethods.size(); i++) {
		comparator.setCurrentComparisonMethod(testMethods[i]);

		auto& matched = testValuesForMatched[i];
		newValue.setAs<T>(std::get<0>(matched));
		oldValue.setAs<T>(std::get<1>(matched));
		userValue.setFor<T>(std::get<2>(matched));
		testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, true, newValue, oldValue, userValue);

		auto& notMatched = testValuesForNotMatched[i];
		newValue.setAs<T>(std::get<0>(notMatched));
		oldValue.setAs<T>(std::get<1>(notMatched));
		userValue.setFor<T>(std::get<2>(notMatched));
		testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, false, newValue, oldValue, userValue);
	}
}

template<typename T>
void MemoryValueComparatorTests::testComparisonMethodFor(
	MemoryValueComparator& comparator, MemoryValueTypeFlag& cmpFlag, MemoryValueTypeFlag& savedFlag, bool isMatched,
	const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue) {

	size_t maxBytes = 0;
	cmpFlag.clearFlag();
	savedFlag.clearFlag();
	cmpFlag.markFor<T>();
	EXPECT_EQ(true, cmpFlag.isMarkedOnlyFor<T>());
	EXPECT_EQ(false, savedFlag.isMarkedForAny());
	maxBytes = comparator.compareValue(newValue, oldValue, userValue, cmpFlag, savedFlag);
	if (isMatched) {
		EXPECT_EQ(sizeof(T), maxBytes);
		EXPECT_EQ(true, maxBytes <= MemoryValueDefs::MAX_BYTES);
		EXPECT_EQ(true, savedFlag.isMarkedOnlyFor<T>());
	} else {
		EXPECT_EQ(0, maxBytes);
		EXPECT_EQ(false, savedFlag.isMarkedForAny());
	}
}

TEST_F(MemoryValueComparatorTests,testComapreForComparisonMethod) {
	MemoryValueComparator comparator;

	testAllComparisonMethodsFor<int8_t>(comparator, -8, -8, 0, -7, -9, 1, 1);
	testAllComparisonMethodsFor<uint8_t>(comparator, 248, 248, 0, 249, 247, 1, 1);
	testAllComparisonMethodsFor<int16_t>(comparator, -2056, -2056, 0, -2055, -2057, 1, 1);
	testAllComparisonMethodsFor<uint16_t>(comparator, 63480, 63480, 0, 63481, 63479, 1, 1);
	testAllComparisonMethodsFor<int32_t>(comparator, -168364040, -168364040, 0, -168364039, -168364041, 1, 1);
	testAllComparisonMethodsFor<uint32_t>(comparator, 4126603256, 4126603256, 0, 4126603257, 4126603255, 1, 1);
	testAllComparisonMethodsFor<int64_t>(comparator, -1012478732780767240LL, -1012478732780767240LL, 0, -1012478732780767239LL, -1012478732780767241LL, 1, 1);
	testAllComparisonMethodsFor<uint64_t>(comparator, 17434265340928784376ULL, 17434265340928784376ULL, 0, 17434265340928784377ULL, 17434265340928784375ULL, 1, 1);

	testAllComparisonMethodsFor<float>(comparator, -123.123F, -123, 0.123F, -123.122F, -123.124F, 1, 1);
	testAllComparisonMethodsFor<double>(comparator, -456.456, -456, 0.456, -456.455, -456.457, 1, 1);
}

template<typename T>
void MemoryValueComparatorTests::testAllComparisonMethodsWithNanFor(MemoryValueComparator& comparator) {
	MemoryValue newValue;
	MemoryValue oldValue;
	UserValue userValue;
	MemoryValueTypeFlag cmpFlag;
	MemoryValueTypeFlag savedFlag;

	// XXX: Generate a NaN value.
	T testNan = 0.0F / 0.0F;
	T quietNan = std::numeric_limits<T>::quiet_NaN();
	EXPECT_TRUE(testNan != quietNan);
	{
		std::vector<ComparisonMethod> testMethods = {
				ComparisonMethod::EQUAL_TO,
				ComparisonMethod::NOT_EQUAL_TO,
				ComparisonMethod::GREATER_THAN,
				ComparisonMethod::LESS_THAN
		};

		for (size_t i = 0; i < testMethods.size(); i++) {
			comparator.setCurrentComparisonMethod(testMethods[i]);

			newValue.setAs<T>(testNan);
			oldValue.setAs<T>(0);
			userValue.setFor<T>(1.23F);
			testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, false, newValue, oldValue, userValue);

			newValue.setAs<T>(1.23F);
			oldValue.setAs<T>(0);
			userValue.setFor<T>(testNan);
			testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, false, newValue, oldValue, userValue);
		}
	}
	{
		std::vector<ComparisonMethod> testMethods = {
				ComparisonMethod::NOT_CHANGED,
				ComparisonMethod::CHANGED,
				ComparisonMethod::INCREASED,
				ComparisonMethod::DECREASED
		};

		for (size_t i = 0; i < testMethods.size(); i++) {
			comparator.setCurrentComparisonMethod(testMethods[i]);

			newValue.setAs<T>(testNan);
			oldValue.setAs<T>(1.23F);
			userValue.setFor<T>(0);
			testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, false, newValue, oldValue, userValue);

			newValue.setAs<T>(1.23F);
			oldValue.setAs<T>(testNan);
			userValue.setFor<T>(0);
			testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, false, newValue, oldValue, userValue);
		}
	}
	{
		std::vector<ComparisonMethod> testMethods = {
				ComparisonMethod::INCREASED_BY,
				ComparisonMethod::DECREASED_BY
		};

		for (size_t i = 0; i < testMethods.size(); i++) {
			comparator.setCurrentComparisonMethod(testMethods[i]);

			newValue.setAs<T>(testNan);
			oldValue.setAs<T>(1.23F);
			userValue.setFor<T>(1.1F);
			testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, false, newValue, oldValue, userValue);

			newValue.setAs<T>(1.23F);
			oldValue.setAs<T>(testNan);
			userValue.setFor<T>(1.1F);
			testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, false, newValue, oldValue, userValue);

			newValue.setAs<T>(1.23F);
			oldValue.setAs<T>(1.1F);
			userValue.setFor<T>(testNan);
			testComparisonMethodFor<T>(comparator, cmpFlag, savedFlag, false, newValue, oldValue, userValue);
		}
	}
}

TEST_F(MemoryValueComparatorTests,testComapreForComparisonMethodWithNan) {
	MemoryValueComparator comparator;

	testAllComparisonMethodsWithNanFor<float>(comparator);
	testAllComparisonMethodsWithNanFor<double>(comparator);
}

} /* END of namespace */
