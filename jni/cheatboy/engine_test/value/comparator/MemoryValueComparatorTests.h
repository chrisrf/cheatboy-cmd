#ifndef __MemoryValueComparatorTests_H__
#define __MemoryValueComparatorTests_H__

#include <gtest/gtest.h>

namespace cboy {
class MemoryValue;
class MemoryValueTypeFlag;
class UserValue;
class MemoryValueComparator;

class MemoryValueComparatorTests: public ::testing::Test {
public:
	MemoryValueComparatorTests() {}
	virtual ~MemoryValueComparatorTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	template<typename T> void testAllComparisonMethodsFor(
		MemoryValueComparator& comparator,
		T baseNum, T equalNum, T notEqualNum, T biggerNum, T smallerNum, T increasedNum, T decreasedNum);
	template<typename T> void testAllComparisonMethodsWithNanFor(MemoryValueComparator& comparator);
	template<typename T> void testComparisonMethodFor(
		MemoryValueComparator& comparator, MemoryValueTypeFlag& cmpFlag, MemoryValueTypeFlag& savedFlag, bool isMatched,
		const MemoryValue& newValue, const MemoryValue& oldValue, const UserValue& userValue);
};

} /* END of namespace */

#endif
