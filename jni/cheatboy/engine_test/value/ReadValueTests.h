#ifndef __ReadValueTests_H__
#define __ReadValueTests_H__

#include <stdint.h>
#include <gtest/gtest.h>

namespace cboy {
class ReadValue;

class ReadValueTests: public ::testing::Test {
public:
	ReadValueTests() {}
	virtual ~ReadValueTests() {}
public:
	virtual void SetUp() {}
	virtual void TearDown() {}
protected:
	template<typename T> T testReadValueFunction(ReadValue& readValue, uint8_t* memToRead, uintptr_t fakeAddress);
};

} /* END of namespace */

#endif
