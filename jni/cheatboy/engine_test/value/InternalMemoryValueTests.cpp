#include "InternalMemoryValueTests.h"

#include <cheatboy/engine/value/InternalMemoryValue.h>
#include <cheatboy/engine_test/value/ValueTestHelper.h>

namespace cboy {

TEST_F(InternalMemoryValueTests,testReadValue) {
	InternalMemoryValue readValue;
	uint8_t memToRead[sizeof(readValue.bytes)] = {
			0xf8, 0xf7, 0xf6, 0xf5,
			0xf4, 0xf3, 0xf2, 0xf1
	};

	memset(readValue.bytes, 0, sizeof(readValue.bytes));
	memcpy(readValue.bytes, memToRead, sizeof(readValue.bytes));

	// 0xf8 = -8
	// 0xf8 = 248
	// 0xf7f8 = -2056
	// 0xf7f8 = 63480
	// 0xf5f6f7f8 = -168364040
	// 0xf5f6f7f8 = 4126603256
	// 0xf1f2f3f4f5f6f7f8 = -1012478732780767240LL
	// 0xf1f2f3f4f5f6f7f8 = 17434265340928784376ULL
	// 0xf5f6f7f8 = -6.261399e+32
	// 0xf1f2f3f4f5f6f7f8 = -7.8986617409766022e+240

	EXPECT_EQ(-8, readValue.i08);
	EXPECT_EQ(248, readValue.u08);
	EXPECT_EQ(-2056, readValue.i16);
	EXPECT_EQ(63480, readValue.u16);
	EXPECT_EQ(-168364040, readValue.i32);
	EXPECT_EQ(4126603256, readValue.u32);
	EXPECT_EQ(-1012478732780767240LL, readValue.i64);
	EXPECT_EQ(17434265340928784376ULL, readValue.u64);
	EXPECT_FLOAT_EQ(-6.261399e32F, readValue.f32);
	EXPECT_DOUBLE_EQ(-7.8986617409766022e240, readValue.f64);

	memset(&readValue, 0, sizeof(readValue));
	memcpy(&(readValue.f32), memToRead, sizeof(readValue.f32));
	EXPECT_FLOAT_EQ(-6.261399e32F, readValue.f32);

	memset(&readValue, 0, sizeof(readValue));
	memcpy(&(readValue.f64), memToRead, sizeof(readValue.f64));
	EXPECT_DOUBLE_EQ(-7.8986617409766022e240, readValue.f64);
}

TEST_F(InternalMemoryValueTests,testWriteValue) {
	InternalMemoryValue writeValue;

	{
		memset(&writeValue, 0, sizeof(writeValue));

		int8_t i08 = -8;
		memcpy(writeValue.bytes, &(i08), sizeof(i08));
		EXPECT_EQ(i08, writeValue.i08);
		EXPECT_EQ(i08, (*reinterpret_cast<int8_t*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(i08), sizeof(writeValue.bytes) - sizeof(i08));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		uint8_t u08 = 248;
		memcpy(writeValue.bytes, &(u08), sizeof(u08));
		EXPECT_EQ(u08, writeValue.u08);
		EXPECT_EQ(u08, (*reinterpret_cast<uint8_t*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(u08), sizeof(writeValue.bytes) - sizeof(u08));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		int16_t i16 = -2056;
		memcpy(writeValue.bytes, &(i16), sizeof(i16));
		EXPECT_EQ(i16, writeValue.i16);
		EXPECT_EQ(i16, (*reinterpret_cast<int16_t*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(i16), sizeof(writeValue.bytes) - sizeof(i16));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		uint16_t u16 = 63480;
		memcpy(writeValue.bytes, &(u16), sizeof(u16));
		EXPECT_EQ(u16, writeValue.u16);
		EXPECT_EQ(u16, (*reinterpret_cast<uint16_t*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(u16), sizeof(writeValue.bytes) - sizeof(u16));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		int32_t i32 = -168364040;
		memcpy(writeValue.bytes, &(i32), sizeof(i32));
		EXPECT_EQ(i32, writeValue.i32);
		EXPECT_EQ(i32, (*reinterpret_cast<int32_t*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(i32), sizeof(writeValue.bytes) - sizeof(i32));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		uint32_t u32 = 4126603256;
		memcpy(writeValue.bytes, &(u32), sizeof(u32));
		EXPECT_EQ(u32, writeValue.u32);
		EXPECT_EQ(u32, (*reinterpret_cast<uint32_t*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(u32), sizeof(writeValue.bytes) - sizeof(u32));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		int64_t i64 = -1012478732780767240LL;
		memcpy(writeValue.bytes, &(i64), sizeof(i64));
		EXPECT_EQ(i64, writeValue.i64);
		EXPECT_EQ(i64, (*reinterpret_cast<int64_t*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(i64), sizeof(writeValue.bytes) - sizeof(i64));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		uint64_t u64 = 17434265340928784376ULL;
		memcpy(writeValue.bytes, &(u64), sizeof(u64));
		EXPECT_EQ(u64, writeValue.u64);
		EXPECT_EQ(u64, (*reinterpret_cast<uint64_t*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(u64), sizeof(writeValue.bytes) - sizeof(u64));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		float f32 = 123.123F;	// 0x42f63efa
		memcpy(writeValue.bytes, &(f32), sizeof(f32));
		EXPECT_EQ(f32, writeValue.f32);
		EXPECT_EQ(f32, (*reinterpret_cast<float*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(f32), sizeof(writeValue.bytes) - sizeof(f32));

		//EXPECT_FLOAT_EQ(f32, writeValue.f32);
		//EXPECT_FLOAT_EQ(f32, (*reinterpret_cast<float*>(writeValue.bytes)));
	}

	{
		memset(&writeValue, 0, sizeof(writeValue));

		double f64 = 567.567;	// 0x4081bc89374bc6a8
		memcpy(writeValue.bytes, &(f64), sizeof(f64));
		EXPECT_EQ(f64, writeValue.f64);
		EXPECT_EQ(f64, (*reinterpret_cast<double*>(writeValue.bytes)));
		ValueTestHelper::assertBufferZero(writeValue.bytes + sizeof(f64), sizeof(writeValue.bytes) - sizeof(f64));

		//EXPECT_DOUBLE_EQ(f64, writeValue.f64);
		//EXPECT_DOUBLE_EQ(f64, (*reinterpret_cast<double*>(writeValue.bytes)));
	}
}

} /* END of namespace */
