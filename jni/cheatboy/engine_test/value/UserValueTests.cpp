#include "UserValueTests.h"

#include <cheatboy/engine/value/UserValue.h>

namespace cboy {

TEST_F(UserValueTests,testUserValue) {
	UserValue userValue;
	userValue.setFor<int8_t>(-1);
	userValue.setFor<uint8_t>(2);
	userValue.setFor<int16_t>(-3);
	userValue.setFor<uint16_t>(4);
	userValue.setFor<int32_t>(-5);
	userValue.setFor<uint32_t>(6);
	userValue.setFor<int64_t>(-7);
	userValue.setFor<uint64_t>(8);
	userValue.setFor<float>(-9);
	userValue.setFor<double>(-10);
	EXPECT_EQ(-1, userValue.getFor<int8_t>());
	EXPECT_EQ(2, userValue.getFor<uint8_t>());
	EXPECT_EQ(-3, userValue.getFor<int16_t>());
	EXPECT_EQ(4, userValue.getFor<uint16_t>());
	EXPECT_EQ(-5, userValue.getFor<int32_t>());
	EXPECT_EQ(6, userValue.getFor<uint32_t>());
	EXPECT_EQ(-7, userValue.getFor<int64_t>());
	EXPECT_EQ(8, userValue.getFor<uint64_t>());
	EXPECT_EQ(-9, userValue.getFor<float>());
	EXPECT_EQ(-10, userValue.getFor<double>());

	userValue.clear();
	EXPECT_EQ(0, userValue.getFor<int8_t>());
	EXPECT_EQ(0, userValue.getFor<uint8_t>());
	EXPECT_EQ(0, userValue.getFor<int16_t>());
	EXPECT_EQ(0, userValue.getFor<uint16_t>());
	EXPECT_EQ(0, userValue.getFor<int32_t>());
	EXPECT_EQ(0, userValue.getFor<uint32_t>());
	EXPECT_EQ(0, userValue.getFor<int64_t>());
	EXPECT_EQ(0, userValue.getFor<uint64_t>());
	EXPECT_EQ(0, userValue.getFor<float>());
	EXPECT_EQ(0, userValue.getFor<double>());
}

} /* END of namespace */
