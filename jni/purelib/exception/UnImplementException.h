#ifndef __UnImplementException_H__
#define __UnImplementException_H__

#include <purelib/exception/Exception.h>
#include <purelib/exception/ExceptionMacro.h>

namespace pure {

PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION(UnImplementException);
PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION_BASED_ON(UltraUnImplementException, UnImplementException);

} /* END of namespace */

#endif
