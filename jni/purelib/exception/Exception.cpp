#include "Exception.h"

#include <purelib/terminal/TerminalPrint.h>

//#define __PURE_UNIT_TEST_EXCEPTION__

namespace pure {

Exception::Exception(const char* name, int32_t code, std::string file, int64_t line, std::string description)
	:
	_name(name), _code(code),
	_file(std::move(file)), _line(line),
	_description(std::move(description)) {

#ifdef __PURE_UNIT_TEST_EXCEPTION__
	pure::println("Constructor (std::string) called: {0}", _description);
#endif
}

Exception::Exception(const char* name, int32_t code, Exception&& e)
	:
	_name(name), _code(code),
	_file(std::move(e._file)), _line(e._line),
	_description(std::move(e._description)) {

#ifdef __PURE_UNIT_TEST_EXCEPTION__
	pure::println("Constructor (Exception&&) called: {0}", _description);
#endif
}

Exception::Exception(const char* name, int32_t code, Exception&& e, std::string description)
	:
	_name(name), _code(code),
	_file(std::move(e._file)), _line(e._line),
	_description(std::move(description)) {

#ifdef __PURE_UNIT_TEST_EXCEPTION__
pure::println("Constructor (Exception&&, std::string) called: {0}", _description);
#endif
}

Exception::~Exception() noexcept {
#ifdef __PURE_UNIT_TEST_EXCEPTION__
	pure::println("Destructor called: {0}", _description);
#endif
}

Exception::Exception(Exception&& rhs) noexcept
	:
	_name(std::move(rhs._name)), _code(std::move(rhs._code)),
	_file(std::move(rhs._file)), _line(std::move(rhs._line)),
	_description(std::move(rhs._description)) {

	rhs._file.clear();
	rhs._line = 0;
	rhs._description.clear();
#ifdef __PURE_UNIT_TEST_EXCEPTION__
	pure::println("Move Constructor called: {0}", _description);
#endif
}

Exception& Exception::operator =(Exception&& rhs) noexcept {
	if (this != &rhs) {
#ifdef __PURE_UNIT_TEST_EXCEPTION__
		pure::println("Move Assignment called: {0}", _description);
#endif
		_name = std::move(rhs._name);
		_code = std::move(rhs._code);
		_file = std::move(rhs._file);
		_line = std::move(rhs._line);
		_description = std::move(rhs._description);

		rhs._file.clear();
		rhs._line = 0;
		rhs._description.clear();
	}
	return *this;
}

} /* END of namespace */
