#ifndef __Exception_H__
#define __Exception_H__

#include <utility>
#include <exception>
#include <string>
#include <PureFileMacro.h>

namespace pure {

class Exception: public std::exception {
protected:
	const char* _name;
	int32_t _code;
	std::string _file;
	int64_t _line;
	std::string _description;

public:
	Exception(const char* name, int32_t code, std::string file, int64_t line, std::string description);
	Exception(const char* name, int32_t code, Exception&& e);
	Exception(const char* name, int32_t code, Exception&& e, std::string description);
	virtual ~Exception() noexcept;
	Exception(Exception&&) noexcept;
	Exception& operator =(Exception&&) noexcept;
private:
	Exception(const Exception&) = delete;
	Exception& operator =(const Exception&) = delete;
public:
	virtual const char* what() const _GLIBCXX_USE_NOEXCEPT final { return getDescription().c_str(); }

	const char* getName() const { return _name; }
	int32_t getCode() const { return _code; }
	const std::string getFile() const { return _file; }
	int64_t getLine() const { return _line; }
	const std::string& getDescription() const { return _description; }
};

} /* END of namespace */

#define PURE_THROW_EXCEPTION(mpDescription)\
		throw pure::Exception("Exception", 0, __PURE_FILE__, __LINE__, mpDescription)
#define PURE_THROW_EXCEPTION_WITH_CODE(mpCode, mpDescription)\
		throw pure::Exception("Exception", mpCode, __PURE_FILE__, __LINE__, mpDescription)
#define PURE_THROW_EXCEPTION_EX(mpFile, mpLine, mpDescription)\
		throw pure::Exception("Exception", 0, mpFile, mpLine, mpDescription)
#define PURE_THROW_EXCEPTION_EX_WITH_CODE(mpCode, mpFile, mpLine, mpDescription)\
		throw pure::Exception("Exception", mpCode, mpFile, mpLine, mpDescription)

#endif
