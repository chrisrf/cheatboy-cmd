#ifndef __ExceptionMacro_H__
#define __ExceptionMacro_H__

// === extends pure::Exception
#define PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION_WITH_CODE(mpExceptionName, mpExceptionCode)\
class mpExceptionName : public pure::Exception {\
public:\
	mpExceptionName(std::string file, int64_t line, std::string description)\
		: pure::Exception(#mpExceptionName, mpExceptionCode, std::move(file), line, std::move(description)) {}\
	mpExceptionName(pure::Exception&& e)\
		: pure::Exception(#mpExceptionName, mpExceptionCode, std::move(e)) {}\
	mpExceptionName(pure::Exception&& e, std::string description)\
		: pure::Exception(#mpExceptionName, mpExceptionCode, std::move(e), std::move(description)) {}\
	virtual ~mpExceptionName() = default;\
	mpExceptionName(mpExceptionName&&) = default;\
	mpExceptionName& operator =(mpExceptionName&&) = default;\
protected:\
	mpExceptionName(const char* name, int32_t code, std::string file, int64_t line, std::string description)\
		: pure::Exception(name, code, std::move(file), line, std::move(description)) {}\
	mpExceptionName(const char* name, int32_t code, pure::Exception&& e)\
		: pure::Exception(name, code, std::move(e)) {}\
	mpExceptionName(const char* name, int32_t code, pure::Exception&& e, std::string description)\
		: pure::Exception(name, code, std::move(e), std::move(description)) {}\
private:\
	mpExceptionName(const mpExceptionName&) = delete;\
	mpExceptionName& operator =(const mpExceptionName&) = delete;\
};
#define PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION(mpExceptionName)\
	PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION_WITH_CODE(mpExceptionName, 0);

// === extends subclass of pure::Exception
#define PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION_WITH_CODE_BASED_ON(mpExceptionName, mpExceptionCode, mpBaseClass)\
class mpExceptionName : public mpBaseClass {\
public:\
	mpExceptionName(std::string file, int64_t line, std::string description)\
		: mpBaseClass(#mpExceptionName, mpExceptionCode, std::move(file), line, std::move(description)) {}\
	mpExceptionName(pure::Exception&& e)\
		: mpBaseClass(#mpExceptionName, mpExceptionCode, std::move(e)) {}\
	mpExceptionName(pure::Exception&& e, std::string description)\
		: mpBaseClass(#mpExceptionName, mpExceptionCode, std::move(e), std::move(description)) {}\
	virtual ~mpExceptionName() = default;\
	mpExceptionName(mpExceptionName&&) = default;\
	mpExceptionName& operator =(mpExceptionName&&) = default;\
protected:\
	mpExceptionName(const char* name, int32_t code, std::string file, int64_t line, std::string description)\
		: mpBaseClass(name, code, std::move(file), line, std::move(description)) {}\
	mpExceptionName(const char* name, int32_t code, pure::Exception&& e)\
		: mpBaseClass(name, code, std::move(e)) {}\
	mpExceptionName(const char* name, int32_t code, pure::Exception&& e, std::string description)\
		: mpBaseClass(name, code, std::move(e), std::move(description)) {}\
private:\
	mpExceptionName(const mpExceptionName&) = delete;\
	mpExceptionName& operator =(const mpExceptionName&) = delete;\
};
#define PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION_BASED_ON(mpExceptionName, mpBaseClass)\
	PURE_DECLARE_AND_IMPLEMENT_DERIVED_EXCEPTION_WITH_CODE_BASED_ON(mpExceptionName, 0, mpBaseClass);

// Macro utilities
#define PURE_THROW_DERIVED_EXCEPTION(mpExceptionName, mpDescription)\
	throw mpExceptionName(__PURE_FILE__, __LINE__, mpDescription)
#define PURE_THROW_WRAPPED_EXCEPTION(mpExceptionName, mpWrappedException)\
	throw mpExceptionName(mpWrappedException)
#define PURE_THROW_WRAPPED_EXCEPTION_EX(mpExceptionName, mpWrappedException, mpDescription)\
	throw mpExceptionName(mpWrappedException, mpDescription)

#endif
