#ifndef __PureFileMacro_H__
#define __PureFileMacro_H__

#ifdef DEBUG
#define __PURE_FILE__ __FILE__
#else
#define __PURE_FILE__ pure::getPureFileName(__FILE__)
#endif

#ifdef _WIN32
#define __PURE_PATH_SEPARATOR__ '\\'
#else
#define __PURE_PATH_SEPARATOR__ '/'
#endif

namespace pure {

const char* getPureFileName(const char* file);

} /* END of namespace */

#endif
