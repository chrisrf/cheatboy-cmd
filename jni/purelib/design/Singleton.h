#ifndef __Singleton_H__
#define __Singleton_H__

#include <utility>
#include <PureMacro.h>

namespace pure {

template<typename TInterface>
class Singleton final {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(Singleton);
private:
	static TInterface* sharedInstance;

public:
	static bool isCreated() {
		return (sharedInstance != nullptr);
	}
	template<typename TClass, typename ... Args>
	static TInterface& createInstance(Args&&... args) {
		if (sharedInstance == nullptr) {
			TInterface* ptr = new TClass(std::forward<Args>(args)...);
			sharedInstance = ptr;
		}
		return *sharedInstance;
	}
	template<typename TClass, typename ... Args>
	static TInterface& recreateInstance(Args&&... args) {
		destroyInstance();
		TInterface* ptr = new TClass(std::forward<Args>(args)...);
		sharedInstance = ptr;
		return *sharedInstance;
	}
	static TInterface& getInstance() {
		return *sharedInstance;
	}
	static void destroyInstance() noexcept {
		if (sharedInstance != nullptr) {
			delete sharedInstance;
			sharedInstance = nullptr;
		}
	}
};

template<class T> T* Singleton<T>::sharedInstance = nullptr;

} /* END of namespace */

#endif
