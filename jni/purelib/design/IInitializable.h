#ifndef __IInitializable_H__
#define __IInitializable_H__

#include <stdint.h>
#include <PureMacro.h>

namespace pure {

class IInitializable {
PURE_DECLARE_CLASS_AS_INTERFACE(IInitializable);
public:
	virtual bool isInitialized() = 0;
	virtual int32_t init() = 0;
	virtual int32_t clean() noexcept = 0;
};

} /* END of namespace */

#endif
