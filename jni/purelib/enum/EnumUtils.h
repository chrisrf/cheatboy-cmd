#ifndef __EnumUtils_H__
#define __EnumUtils_H__

#include <type_traits>
#include <PureMacro.h>

namespace pure {

template<typename T>
class EnumUtils {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(EnumUtils);
private:
	typedef typename std::enable_if<std::is_same<int, typename std::underlying_type<T>::type>::value>::type EnumType;

public:
	static bool convert(int intEnum, T& enumValue) {
		if (!isValid(intEnum)) {
			return false;
		}
		enumValue = T(intEnum);
		return true;
	}
	static bool isValid(int intEnum) {
		if ((intEnum < static_cast<int>(T::ENUM_MIN)) || (intEnum > static_cast<int>(T::ENUM_MAX))) {
			return false;
		}
		return true;
	}
	static bool isValid(T enumValue) {
		if ((static_cast<int>(enumValue) < static_cast<int>(T::ENUM_MIN)) || (static_cast<int>(enumValue) > static_cast<int>(T::ENUM_MAX))) {
			return false;
		}
		return true;
	}
};

} /* END of namespace */

#endif
