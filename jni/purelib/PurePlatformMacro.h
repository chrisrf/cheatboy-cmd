#ifndef __PurePlatformMacro_H__
#define __PurePlatformMacro_H__

#ifdef __ANDROID__

#if defined __arm__ || defined __i386__
#define __PURE_32BIT__
#elif defined __aarch64__ || defined __x86_64__
#define __PURE_64BIT__
#else
#error Unsupported platform
#endif

#endif

#endif
