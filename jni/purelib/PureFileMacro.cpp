#include "PureFileMacro.h"

#include <string.h>

namespace pure {

const char* getPureFileName(const char* file) {
	return (
	    strrchr(file, __PURE_PATH_SEPARATOR__) ? strrchr(file, __PURE_PATH_SEPARATOR__) + 1 : file);
}

} /* END of namespace */
