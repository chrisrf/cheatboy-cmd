#include "MemoryUtils.h"

#include <stdio.h>

#include <purelib/memory/PointerUtils.h>

namespace pure {

std::string MemoryUtils::convertToAddressString(uintptr_t addr) {
	fmt::MemoryWriter writer;
	return convertToAddressString(writer, addr);
}

std::string MemoryUtils::convertToAddressString(fmt::MemoryWriter& writer, uintptr_t addr) {
	writer.clear();
	putAddress(writer, addr);
	return writer.str();
}

void MemoryUtils::putAddress(fmt::MemoryWriter& writer, uintptr_t addr) {
	writer << fmt::pad(fmt::hex(addr), HEX_WIDTH, '0');
}

void MemoryUtils::putReadableAddress(fmt::MemoryWriter& writer, uintptr_t addr) {
	writer << "[0x";
	putAddress(writer, addr);
	writer << "]";
}

void MemoryUtils::printBlockBytes(uint8_t* ptr, size_t size, size_t width, PrintLineBytesFptr printLineBytes) {
	size_t i = 0;
	for (i = 0; ((i + width) <= size); i += width) {
		printLineBytes((ptr + i), width, true);
	}
	if (size - i > 0) {
		printLineBytes((ptr + i), (size - i), true);
	}
}

std::string MemoryUtils::blockBytesToString(uint8_t* ptr, size_t size, size_t width, LineBytesToMemoryWriterFptr lineBytesToMemoryWriter) {
	fmt::MemoryWriter writer;
	size_t i = 0;
	for (i = 0; ((i + width) <= size); i += width) {
		lineBytesToMemoryWriter(writer, (ptr + i), width, true);
	}
	if (size - i > 0) {
		lineBytesToMemoryWriter(writer, (ptr + i), (size - i), true);
	}
	return writer.str();
}

void MemoryUtils::printReadableLineBytes(uint8_t* ptr, size_t size, bool withNewLine) {
	if ((WORD_SIZE == 4)) {
		printf("[%10p]", ptr);
	} else {
		printf("[%18p]", ptr);
	}
	for (size_t i = 0; i < size; i++) {
		printf(" [%02x]", ptr[i]);
	}
	if (withNewLine) {
		printf("\n");
	}
}

void MemoryUtils::printReadableBlockBytes(uint8_t* ptr, size_t size) {
	printBlockBytes(ptr, size, DEFAULT_WIDTH_FOR_READABLE_BLOCK_BYTES, printReadableLineBytes);
}

void MemoryUtils::readableLineBytesToMemoryWriter(fmt::MemoryWriter& writer, uint8_t* ptr, size_t size, bool withNewLine) {
	uintptr_t addr = PointerUtils::toValue(ptr);
	putReadableAddress(writer, addr);
	for (size_t i = 0; i < size; i++) {
		writer << " [" << fmt::pad(fmt::hex(ptr[i]), 2, '0') << "]";
	}
	if (withNewLine) {
		writer << "\n";
	}
}

std::string MemoryUtils::readableLineBytesToString(uint8_t* ptr, size_t size, bool withNewLine) {
	fmt::MemoryWriter writer;
	readableLineBytesToMemoryWriter(writer, ptr, size, withNewLine);
	return writer.str();
}

std::string MemoryUtils::readableBlockBytesToString(uint8_t* ptr, size_t size) {
	return blockBytesToString(ptr, size, DEFAULT_WIDTH_FOR_READABLE_BLOCK_BYTES, readableLineBytesToMemoryWriter);
}

void MemoryUtils::printCopyableLineBytes(uint8_t* ptr, size_t size, bool withNewLine) {
	for (size_t i = 0; i < size; i++) {
		printf("0x%02x, ", ptr[i]);
	}
	if (withNewLine) {
		printf("\n");
	}
}

void MemoryUtils::printCopyableBlockBytes(uint8_t* ptr, size_t size) {
	printBlockBytes(ptr, size, DEFAULT_WIDTH_FOR_COPYABLE_BLOCK_BYTES, printCopyableLineBytes);
}

void MemoryUtils::copyableLineBytesToMemoryWriter(fmt::MemoryWriter& writer, uint8_t* ptr, size_t size, bool withNewLine) {
	for (size_t i = 0; i < size; i++) {
		writer << "0x" << fmt::pad(fmt::hex(ptr[i]), 2, '0') << ", ";
	}
	if (withNewLine) {
		writer << "\n";
	}
}

std::string MemoryUtils::copyableLineBytesToString(uint8_t* ptr, size_t size, bool withNewLine) {
	fmt::MemoryWriter writer;
	copyableLineBytesToMemoryWriter(writer, ptr, size, withNewLine);
	return writer.str();
}

std::string MemoryUtils::copyableBlockBytesToString(uint8_t* ptr, size_t size) {
	return blockBytesToString(ptr, size, DEFAULT_WIDTH_FOR_COPYABLE_BLOCK_BYTES, copyableLineBytesToMemoryWriter);
}

void MemoryUtils::printParsableLineBytes(uint8_t* ptr, size_t size, bool withNewLine) {
	for (size_t i = 0; i < size; i++) {
		printf("%02x ", ptr[i]);
	}
	if (withNewLine) {
		printf("\n");
	}
}

void MemoryUtils::printParsableBlockBytes(uint8_t* ptr, size_t size) {
	printBlockBytes(ptr, size, DEFAULT_WIDTH_FOR_PARSABLE_BLOCK_BYTES, printParsableLineBytes);
}

void MemoryUtils::parsableLineBytesToMemoryWriter(fmt::MemoryWriter& writer, uint8_t* ptr, size_t size, bool withNewLine) {
	for (size_t i = 0; i < size; i++) {
		writer << fmt::pad(fmt::hex(ptr[i]), 2, '0') << " ";
	}
	if (withNewLine) {
		writer << "\n";
	}
}

std::string MemoryUtils::parsableLineBytesToString(uint8_t* ptr, size_t size, bool withNewLine) {
	fmt::MemoryWriter writer;
	parsableLineBytesToMemoryWriter(writer, ptr, size, withNewLine);
	return writer.str();
}

std::string MemoryUtils::parsableBlockBytesToString(uint8_t* ptr, size_t size) {
	return blockBytesToString(ptr, size, DEFAULT_WIDTH_FOR_PARSABLE_BLOCK_BYTES, parsableLineBytesToMemoryWriter);
}

} /* END of namespace */
