#ifndef __MemoryUtils_H__
#define __MemoryUtils_H__

#include <stddef.h>
#include <stdint.h>
#include <string>
#include <PureMacro.h>

#include <purelib/lib/cppformat/format.h>

namespace pure {

class MemoryUtils {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(MemoryUtils);
public:
	enum : size_t {
		WORD_SIZE = sizeof(uintptr_t),
		HEX_WIDTH = sizeof(uintptr_t)*2,
		DEFAULT_WIDTH_FOR_READABLE_BLOCK_BYTES = 8,
		DEFAULT_WIDTH_FOR_COPYABLE_BLOCK_BYTES = 8,
		DEFAULT_WIDTH_FOR_PARSABLE_BLOCK_BYTES = 8
	};
public:
	typedef void (*PrintLineBytesFptr)(uint8_t*, size_t, bool);
	typedef void (*LineBytesToMemoryWriterFptr)(fmt::MemoryWriter&, uint8_t*, size_t, bool);

public:
	static std::string convertToAddressString(uintptr_t addr);
	static std::string convertToAddressString(fmt::MemoryWriter& writer, uintptr_t addr);
	static void putAddress(fmt::MemoryWriter& writer, uintptr_t addr);
	static void putReadableAddress(fmt::MemoryWriter& writer, uintptr_t addr);
public:
	static void printBlockBytes(uint8_t* ptr, size_t size, size_t width, PrintLineBytesFptr printLineBytes);
	static std::string blockBytesToString(uint8_t* ptr, size_t size, size_t width, LineBytesToMemoryWriterFptr lineBytesToMemoryWriter);
public:
	static void printReadableLineBytes(uint8_t* ptr, size_t size, bool withNewLine);
	static void printReadableBlockBytes(uint8_t* ptr, size_t size);
	static void readableLineBytesToMemoryWriter(fmt::MemoryWriter& writer, uint8_t* ptr, size_t size, bool withNewLine);
	static std::string readableLineBytesToString(uint8_t* ptr, size_t size, bool withNewLine);
	static std::string readableBlockBytesToString(uint8_t* ptr, size_t size);
public:
	static void printCopyableLineBytes(uint8_t* ptr, size_t size, bool withNewLine);
	static void printCopyableBlockBytes(uint8_t* ptr, size_t size);
	static void copyableLineBytesToMemoryWriter(fmt::MemoryWriter& writer, uint8_t* ptr, size_t size, bool withNewLine);
	static std::string copyableLineBytesToString(uint8_t* ptr, size_t size, bool withNewLine);
	static std::string copyableBlockBytesToString(uint8_t* ptr, size_t size);
public:
	static void printParsableLineBytes(uint8_t* ptr, size_t size, bool withNewLine);
	static void printParsableBlockBytes(uint8_t* ptr, size_t size);
	static void parsableLineBytesToMemoryWriter(fmt::MemoryWriter& writer, uint8_t* ptr, size_t size, bool withNewLine);
	static std::string parsableLineBytesToString(uint8_t* ptr, size_t size, bool withNewLine);
	static std::string parsableBlockBytesToString(uint8_t* ptr, size_t size);
};

} /* END of namespace */

#endif
