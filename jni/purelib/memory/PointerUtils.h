#ifndef __PointerUtils_H__
#define __PointerUtils_H__

#include <stdint.h>
#include <PureMacro.h>

namespace pure {

class PointerUtils {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(PointerUtils);
public:
	static inline void* toPointer(uintptr_t value) {
		return reinterpret_cast<void*>(value);
	}
	static inline uintptr_t toValue(void* ptr) {
		return reinterpret_cast<uintptr_t>(ptr);
	}
};

} /* END of namespace */

#endif
