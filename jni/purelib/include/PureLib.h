#ifndef __PureLib_H__
#define __PureLib_H__

//#include <purelib/PureMacro.h>
//#include <purelib/PurePlatformMacro.h>
//#include <purelib/PureFileMacro.h>
//#include <purelib/PureCppCompatibility.h>

#include <purelib/design/IInitializable.h>
#include <purelib/design/Singleton.h>

#include <purelib/enum/EnumUtils.h>

#include <purelib/exception/Exception.h>
#include <purelib/exception/ExceptionMacro.h>

#include <purelib/log/ILogger.h>
#include <purelib/log/Log.h>
#include <purelib/log/Logger.h>
#include <purelib/log/LoggerInstaller.h>

#include <purelib/memory/MemoryUtils.h>
#include <purelib/memory/PointerUtils.h>

#include <purelib/string/HexNumberConverter.h>
#include <purelib/string/NumberConverter.h>
#include <purelib/string/StringFormat.h>
#include <purelib/string/StringSplitter.h>
#include <purelib/string/StringTrimmer.h>
#include <purelib/string/StringUtils.h>

#include <purelib/terminal/TerminalPrint.h>

#include <purelib/unit/ByteUnit.h>

#endif
