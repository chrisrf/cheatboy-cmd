#ifndef __Logger_H__
#define __Logger_H__

#include <PureMacro.h>

#include <purelib/log/ILogger.h>

namespace pure {

class Logger: public ILogger {
PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(Logger);
public:
	Logger();
	virtual ~Logger() noexcept;
public:
	virtual bool isInitialized() override;
	virtual int32_t init() override;
	virtual int32_t clean() noexcept override;

	virtual int32_t log(const std::string& msg) const override;
	virtual int32_t logln(const std::string& msg) const override;
	virtual int32_t log(const Exception& exception) const override;
};

} /* END of namespace */

#endif
