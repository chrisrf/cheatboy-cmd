#ifndef __LoggerInstaller_H__
#define __LoggerInstaller_H__

#include <PureMacro.h>

namespace pure {
class Exception;

class LoggerInstaller {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(LoggerInstaller);
public:
	static bool install();
	static void uninstall();
};

} /* END of namespace */

#endif
