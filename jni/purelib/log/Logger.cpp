#include "Logger.h"

#include <stdio.h>

#include <purelib/terminal/TerminalPrint.h>

namespace pure {

Logger::Logger() {}

Logger::~Logger() noexcept {}

bool Logger::isInitialized() {
	return true;
}

int32_t Logger::init() {
	return 0;
}

int32_t Logger::clean() noexcept {
	return 0;
}

int32_t Logger::log(const std::string& msg) const {
	printf("%s", msg.c_str());
	return 0;
}

int32_t Logger::logln(const std::string& msg) const {
	printf("%s\n", msg.c_str());
	return 0;
}

int32_t Logger::log(const Exception& exception) const {
	pure::print(exception);
	return 0;
}

} /* END of namespace */
