#include "Log.h"

#include <purelib/design/Singleton.h>
#include <purelib/exception/Exception.h>
#include <purelib/log/ILogger.h>

namespace pure {

int32_t logln(const char* format, fmt::ArgList args) {
	return Singleton<ILogger>::getInstance().logln(fmt::format(format, args));
}

int32_t log(const char* format, fmt::ArgList args) {
	return Singleton<ILogger>::getInstance().log(fmt::format(format, args));
}

int32_t log(const Exception& exception) {
	return Singleton<ILogger>::getInstance().log(exception);
}

int32_t logExceptionInCatchBlock(const char* file, int64_t line) {
	try {
		throw;
	} catch (Exception& e) {
		log(e);
	} catch (std::exception& e) {
		log(Exception("StdException", 0, file, line, e.what()));
	} catch (...) {
		log(Exception("UnknownException", 0, file, line, "UnknownException"));
	}
	return 0;
}

} /* END of namespace */
