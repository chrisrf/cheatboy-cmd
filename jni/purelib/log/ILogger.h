#ifndef __ILogger_H__
#define __ILogger_H__

#include <stdint.h>
#include <string>
#include <PureMacro.h>

#include <purelib/design/IInitializable.h>

namespace pure {
class Exception;

class ILogger: public IInitializable {
PURE_DECLARE_CLASS_AS_INTERFACE(ILogger);
public:
	virtual int32_t log(const std::string& msg) const = 0;
	virtual int32_t logln(const std::string& msg) const = 0;
	virtual int32_t log(const Exception& exception) const = 0;
};

} /* END of namespace */

#endif
