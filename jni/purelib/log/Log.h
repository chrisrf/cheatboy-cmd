#ifndef __Log_H__
#define __Log_H__

#include <stdint.h>
#include <PureFileMacro.h>

#include <purelib/lib/cppformat/format.h>

namespace pure {
class Exception;

int32_t log(const char* format, fmt::ArgList args);
int32_t logln(const char* format, fmt::ArgList args);
int32_t log(const Exception& exception);
int32_t logExceptionInCatchBlock(const char* file, int64_t line);
FMT_VARIADIC(int32_t, log, const char*);
FMT_VARIADIC(int32_t, logln, const char*);

} /* END of namespace */

#ifdef __PURE_ENABLE_DEBUG_LOG__
#define PURE_D_LOG(...)\
		pure::log(__VA_ARGS__)
#define PURE_D_LOG_LN(...)\
		pure::logln(__VA_ARGS__)
#define PURE_D_LOG_AS_EXCEPTION(mpDescription)\
		pure::log(pure::Exception("Exception", 0, __PURE_FILE__, __LINE__, mpDescription))
#define PURE_D_LOG_EXCEPTION_IN_CATCH_BLOCK()\
		pure::logExceptionInCatchBlock(__PURE_FILE__, __LINE__)
#else
#define PURE_D_LOG(...) ((void)0)
#define PURE_D_LOG_LN(...) ((void)0)
#define PURE_D_LOG_AS_EXCEPTION(mpDescription) ((void)0)
#define PURE_D_LOG_EXCEPTION_IN_CATCH_BLOCK() ((void)0)
#endif

#endif
