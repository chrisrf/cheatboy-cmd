#include "LoggerInstaller.h"

#include <PureFileMacro.h>
#include <purelib/design/Singleton.h>
#include <purelib/log/ILogger.h>
#include <purelib/log/Logger.h>
#include <purelib/terminal/TerminalPrint.h>

namespace pure {

bool LoggerInstaller::install() {
	try {
		Singleton<ILogger>::createInstance<Logger>();
		Singleton<ILogger>::getInstance().init();
	} catch (...) {
		pure::printExceptionInCatchBlock(__PURE_FILE__, __LINE__);
		Singleton<ILogger>::destroyInstance();
		return false;
	}
	return true;
}

void LoggerInstaller::uninstall() {
	if (Singleton<ILogger>::isCreated()) {
		Singleton<ILogger>::getInstance().clean();
		Singleton<ILogger>::destroyInstance();
	}
}

} /* END of namespace */
