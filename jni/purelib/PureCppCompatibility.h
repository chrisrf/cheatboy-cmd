#ifndef __PureCppCompatibility_H__
#define __PureCppCompatibility_H__

#include <memory>
#include <PureMacro.h>

#if !(defined __PURE_CPP_14_SUPPORTED__)
namespace std {

template<typename T, typename ...Args>
std::unique_ptr<T> make_unique(Args&& ...args) {
	return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

}
#endif

#endif
