#include "ByteUnit.h"

namespace pure {

const uint64_t ByteUnit::KB = 1024;
const uint64_t ByteUnit::MB = 1024 * KB;
const uint64_t ByteUnit::GB = 1024 * MB;

} /* END of namespace */
