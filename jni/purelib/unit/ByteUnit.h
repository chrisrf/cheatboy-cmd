#ifndef __ByteUnit_H__
#define __ByteUnit_H__

#include <stdint.h>
#include <PureMacro.h>

namespace pure {

class ByteUnit {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(ByteUnit);
public:
	static const uint64_t KB;
	static const uint64_t MB;
	static const uint64_t GB;
};

} /* END of namespace */

#endif
