#ifndef __PureMacro_H__
#define __PureMacro_H__

#if __cplusplus >= 201402L
#define __PURE_CPP_14_SUPPORTED__
#endif

#if __cplusplus >= 201103L
#define __PURE_CPP_11_SUPPORTED__
#endif

// === Unit

#define PURE_DECLARE_UNIT_AS_NONCOPYABLE(mpClassName)\
	private:\
		mpClassName(const mpClassName&) = delete;\
		mpClassName& operator =(const mpClassName&) = delete;

#define PURE_DECLARE_UNIT_AS_NONMOVABLE(mpClassName)\
	private:\
		mpClassName(mpClassName&&) = delete;\
		mpClassName& operator =(mpClassName&&) = delete;

#define PURE_DECLARE_UNIT_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONCOPYABLE(mpClassName);\
	PURE_DECLARE_UNIT_AS_NONMOVABLE(mpClassName);

// === Class

#define PURE_DECLARE_CLASS_AS_NONCOPYABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONCOPYABLE(mpClassName);

#define PURE_DECLARE_CLASS_AS_NONMOVABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONMOVABLE(mpClassName);

#define PURE_DECLARE_CLASS_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName);

#define PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(mpClassName)\
	private:\
		mpClassName() = delete;\
		virtual ~mpClassName() = delete;\
	PURE_DECLARE_UNIT_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName);

#define PURE_DECLARE_CLASS_AS_INTERFACE(mpClassName)\
	public:\
		virtual ~mpClassName() noexcept {}\
	protected:\
		mpClassName() {}\
    PURE_DECLARE_UNIT_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName);

// === Struct

#define PURE_DECLARE_STRUCT_AS_NONCOPYABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONCOPYABLE(mpClassName);

#define PURE_DECLARE_STRUCT_AS_NONMOVABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONMOVABLE(mpClassName);

#define PURE_DECLARE_STRUCT_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName);

// === Union

#define PURE_DECLARE_UNION_AS_NONCOPYABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONCOPYABLE(mpClassName);

#define PURE_DECLARE_UNION_AS_NONMOVABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONMOVABLE(mpClassName);

#define PURE_DECLARE_UNION_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName)\
	PURE_DECLARE_UNIT_AS_NONCOPYABLE_AND_NONMOVABLE(mpClassName);

#endif
