#include "TerminalPrint.h"

#include <purelib/exception/Exception.h>

namespace pure {

int32_t print(const char* format, fmt::ArgList args) {
	fmt::print(format, args);
	return 0;
}

int32_t println(const char* format, fmt::ArgList args) {
	pure::print(format, args);
	pure::print("\n");
	return 0;
}

int32_t print(const Exception& exception) {
	pure::println("");
	pure::println("=== {0}({1})", exception.getName(), exception.getCode());
	pure::println("File: {0}", exception.getFile());
	pure::println("Line: {0}", exception.getLine());
	pure::println("Description:");
	pure::println("{0}", exception.getDescription());
	pure::println("============================");
	return 0;
}

int32_t printExceptionInCatchBlock(const char* file, int64_t line) {
	try {
		throw;
	} catch (Exception& e) {
		pure::print(e);
	} catch (std::exception& e) {
		pure::print(Exception("StdException", 0, file, line, e.what()));
	} catch (...) {
		pure::print(Exception("UnknownException", 0, file, line, "UnknownException"));
	}
	return 0;
}

} /* END of namespace */
