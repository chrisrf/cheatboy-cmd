#ifndef __TerminalPrint_H__
#define __TerminalPrint_H__

#include <stdint.h>

#include <purelib/lib/cppformat/format.h>

namespace pure {
class Exception;

int32_t print(const char* format, fmt::ArgList args);
int32_t println(const char* format, fmt::ArgList args);
int32_t print(const Exception& exception);
int32_t printExceptionInCatchBlock(const char* file, int64_t line);
FMT_VARIADIC(int32_t, print, const char*);
FMT_VARIADIC(int32_t, println, const char*);

} /* END of namespace */

#endif
