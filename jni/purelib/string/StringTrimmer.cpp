#include "StringTrimmer.h"

#include <ctype.h>
#include <string.h>

#include <purelib/exception/Exception.h>

namespace pure {

//const std::locale StringTrimmer::defalutLocale("C");

char* StringTrimmer::cTrim(char* cstr) {
	throwIfCstrIsInvalid(cstr);
	size_t newLen = 0;
	return cTrim(cstr, strlen(cstr), newLen);
}

char* StringTrimmer::cTrim(char* cstr, size_t& newLen) {
	throwIfCstrIsInvalid(cstr);
	return cTrim(cstr, strlen(cstr), newLen);
}

char* StringTrimmer::cTrim(char* cstr, size_t len, size_t& newLen) {
	size_t tempLen = 0;
	char* tempCstr = cTrimHead(cstr, len, tempLen);
	return cTrimTail(tempCstr, tempLen, newLen);
}

char* StringTrimmer::cTrimHead(char* cstr) {
	throwIfCstrIsInvalid(cstr);
	size_t newLen = 0;
	return cTrimHead(cstr, strlen(cstr), newLen);
}

char* StringTrimmer::cTrimHead(char* cstr, size_t& newLen) {
	throwIfCstrIsInvalid(cstr);
	return cTrimHead(cstr, strlen(cstr), newLen);
}

char* StringTrimmer::cTrimHead(char* cstr, size_t len, size_t& newLen) {
	throwIfCstrIsInvalid(cstr);

	newLen = 0;
	if (len == 0) {
		return cstr;
	}
	for (size_t i = 0; i < len; i++) {
		if (!isWhiteSpace(cstr[i])) {
			newLen = len - i;
			return &(cstr[i]);
		}
	}
	cstr[0] = '\0';
	return cstr;
}

char* StringTrimmer::cTrimTail(char* cstr) {
	throwIfCstrIsInvalid(cstr);
	size_t newLen = 0;
	return cTrimTail(cstr, strlen(cstr), newLen);
}

char* StringTrimmer::cTrimTail(char* cstr, size_t& newLen) {
	throwIfCstrIsInvalid(cstr);
	return cTrimTail(cstr, strlen(cstr), newLen);
}

char* StringTrimmer::cTrimTail(char* cstr, size_t len, size_t& newLen) {
	throwIfCstrIsInvalid(cstr);

	newLen = 0;
	if (len == 0) {
		return cstr;
	}
	for (size_t i = len; i-- > 0;) {
		if (!isWhiteSpace(cstr[i])) {
			newLen = i + 1;
			cstr[i + 1] = '\0';
			return cstr;
		}
	}
	cstr[0] = '\0';
	return cstr;
}

void StringTrimmer::cTrimAndShift(char* cstr) {
	throwIfCstrIsInvalid(cstr);
	size_t newLen = 0;
	cTrimAndShift(cstr, strlen(cstr), newLen);
}

void StringTrimmer::cTrimAndShift(char* cstr, size_t& newLen) {
	throwIfCstrIsInvalid(cstr);
	cTrimAndShift(cstr, strlen(cstr), newLen);
}

void StringTrimmer::cTrimAndShift(char* cstr, size_t len, size_t& newLen) {
	char* newCstr = cTrim(cstr, len, newLen);
	if (newCstr == cstr) {
		return;
	}

	for (size_t i = 0; i < newLen; i++) {
		cstr[i] = newCstr[i];
	}
	cstr[newLen] = '\0';
}

void StringTrimmer::cTrimHeadAndShift(char* cstr) {
	throwIfCstrIsInvalid(cstr);
	size_t newLen = 0;
	cTrimHeadAndShift(cstr, strlen(cstr), newLen);
}

void StringTrimmer::cTrimHeadAndShift(char* cstr, size_t& newLen) {
	throwIfCstrIsInvalid(cstr);
	cTrimHeadAndShift(cstr, strlen(cstr), newLen);
}

void StringTrimmer::cTrimHeadAndShift(char* cstr, size_t len, size_t& newLen) {
	char* newCstr = cTrimHead(cstr, len, newLen);
	if (newCstr == cstr) {
		return;
	}

	for (size_t i = 0; i < newLen; i++) {
		cstr[i] = newCstr[i];
	}
	cstr[newLen] = '\0';
}

void StringTrimmer::trim(std::string& str) {
	trimHead(str);
	trimTail(str);
}

void StringTrimmer::trimHead(std::string& str) {
	if (str.empty()) {
		return;
	}
	for (std::string::size_type i = 0; i < str.size(); i++) {
		if (!isWhiteSpace(str[i])) {
			str.erase(0, i);
			return;
		}
	}
	str.clear();
}

void StringTrimmer::trimTail(std::string& str) {
	if (str.empty()) {
		return;
	}
	for (std::string::size_type i = str.size(); i-- > 0;) {
		if (!isWhiteSpace(str[i])) {
			str.erase(i + 1, str.size() - (i + 1));
			return;
		}
	}
	str.clear();
}

bool StringTrimmer::isWhiteSpace(char c) {
	return isspace(c);
}

void StringTrimmer::throwIfCstrIsInvalid(char* cstr) {
	if (cstr == nullptr) {
		PURE_THROW_EXCEPTION("Invalid Argument (cstr == nullptr).");
	}
}

} /* END of namespace */
