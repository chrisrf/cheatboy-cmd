#include "StringUtils.h"

#include <ctype.h>
#include <string.h>

#include <purelib/exception/Exception.h>

namespace pure {

bool StringUtils::isInteger(const char* cstr) {
	throwIfCstrIsInvalid(cstr);
	return isInteger(cstr, strlen(cstr), true);
}

bool StringUtils::isInteger(const std::string& str) {
	return isInteger(str.c_str(), str.size(), true);
}

bool StringUtils::isInteger(const char* cstr, size_t len) {
	return isInteger(cstr, len, true);
}

bool StringUtils::isInteger(const char* cstr, bool isSignAccepted) {
	throwIfCstrIsInvalid(cstr);
	return isInteger(cstr, strlen(cstr), isSignAccepted);
}

bool StringUtils::isInteger(const std::string& str, bool isSignAccepted) {
	return isInteger(str.c_str(), str.size(), isSignAccepted);
}

bool StringUtils::isInteger(const char* cstr, size_t len, bool isSignAccepted) {
	throwIfCstrIsInvalid(cstr);

	if (len == 0) {
		return false;
	}
	size_t i = 0;
	if ((isSignAccepted) && (len > 1) && (cstr[0] == '-')) {
		i = 1;
	}
	for (; i < len; i++) {
		if (!isDigit(cstr[i])) {
			return false;
		}
	}
	return true;
}

bool StringUtils::isHexNumber(const char* cstr) {
	throwIfCstrIsInvalid(cstr);
	return isHexNumber(cstr, strlen(cstr));
}

bool StringUtils::isHexNumber(const std::string& str) {
	return isHexNumber(str.c_str(), str.size());
}

bool StringUtils::isHexNumber(const char* cstr, size_t len) {
	throwIfCstrIsInvalid(cstr);

	if (len == 0) {
		return false;
	}
	size_t i = 0;
	if ((len > 2) && (cstr[0] == '0') && ((cstr[1] == 'x') || (cstr[1] == 'X'))) {
		i = 2;
	}
	for (; i < len; i++) {
		if (!isHexDigit(cstr[i])) {
			return false;
		}
	}
	return true;
}

bool StringUtils::isDigit(char c) {
	return std::isdigit(c);
}

bool StringUtils::isHexDigit(char c) {
	return std::isxdigit(c);
}

bool StringUtils::isWhiteSpace(char c) {
	return isspace(c);
}

void StringUtils::throwIfCstrIsInvalid(const char* cstr) {
	if (cstr == nullptr) {
		PURE_THROW_EXCEPTION("Invalid Argument (cstr == nullptr).");
	}
}

} /* END of namespace */
