#ifndef __StringFormat_H__
#define __StringFormat_H__

#include <string>

#include <purelib/lib/cppformat/format.h>

namespace pure {

std::string fmt(const char* format, fmt::ArgList args);
FMT_VARIADIC(std::string, fmt, const char*);

} /* END of namespace */

#endif
