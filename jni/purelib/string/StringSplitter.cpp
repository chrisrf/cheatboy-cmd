#include "StringSplitter.h"

namespace pure {

bool StringSplitter::splitForTwoString(const std::string& str, char delimiter, std::string& str1, std::string& str2) {
	std::string::size_type found = str.find(delimiter);
	if (found == std::string::npos) {
		return false;
	}
	str1 = str.substr(0, found);
	str2 = str.substr(found + 1, str.size() - (found + 1));
	return true;
}

} /* END of namespace */
