#ifndef __InternalNumberConverter_H__
#define __InternalNumberConverter_H__

#include <stdint.h>
#include <sstream>
#include <string>
#include <PureMacro.h>

namespace pure {

// TODO: Use boost::spirit::qi to parse the number more efficiently.
class InternalNumberConverter {
PURE_DECLARE_CLASS_AS_NONCOPYABLE(InternalNumberConverter);
public:
	enum class Mode {
		Dec = 0, Hex = 1
	};
private:
	Mode _mode;
	std::stringstream _converter;

public:
	InternalNumberConverter(Mode mode);
	virtual ~InternalNumberConverter() noexcept;
	InternalNumberConverter(InternalNumberConverter&&) = default;
	InternalNumberConverter& operator =(InternalNumberConverter&&) = default;
public:
	InternalNumberConverter::Mode getMode();
	void setMode(Mode mode);
	void useDecParsingMode();
	void useHexParsingMode();
public:
	template<typename T> bool convert(const char* valueCstring, size_t len, T& outValue);
private:
	static bool isWhiteSpace(char c);
private:
	static void throwIfCstrIsInvalid(const char* cstr);
};
static_assert(std::is_unsigned<size_t>::value, "size_t is Not unsigned");

} /* END of namespace */

#endif
