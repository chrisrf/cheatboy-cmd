#include "HexNumberConverter.h"

#include <purelib/PurePlatformMacro.h>
#include <purelib/exception/Exception.h>

namespace pure {

HexNumberConverter::HexNumberConverter()
	:
	_converter(InternalNumberConverter::Mode::Hex) {}

HexNumberConverter::~HexNumberConverter() {}

template<typename T>
bool HexNumberConverter::convert(const char* valueCstring, T& outValue) {
	throwIfCstrIsInvalid(valueCstring);
	return _converter.convert(valueCstring, strlen(valueCstring), outValue);
}
template bool HexNumberConverter::convert(const char*, uint8_t&);
template bool HexNumberConverter::convert(const char*, uint16_t&);
template bool HexNumberConverter::convert(const char*, uint32_t&);
template bool HexNumberConverter::convert(const char*, uint64_t&);
#ifdef __PURE_32BIT__
template bool HexNumberConverter::convert(const char*, unsigned long&);
#endif
/*
 * XXX: Using std::stringstream to parse hex value to SIGNED number may produce overflow.
 * Ex:
 * 		"0xFFFF" (to int16_t) will be parsed as (32767) but NOT (-1),
 * 		and std::stringstream::fail() returns true.
 * So we make constraints (only unsigned supported) to convert function.
 */

template<typename T>
bool HexNumberConverter::convert(const std::string& valueString, T& outValue) {
	return _converter.convert(valueString.c_str(), valueString.size(), outValue);
}
template bool HexNumberConverter::convert(const std::string&, uint8_t&);
template bool HexNumberConverter::convert(const std::string&, uint16_t&);
template bool HexNumberConverter::convert(const std::string&, uint32_t&);
template bool HexNumberConverter::convert(const std::string&, uint64_t&);
#ifdef __PURE_32BIT__
template bool HexNumberConverter::convert(const std::string&, unsigned long&);
#endif

void HexNumberConverter::throwIfCstrIsInvalid(const char* cstr) {
	if (cstr == nullptr) {
		PURE_THROW_EXCEPTION("Invalid Argument (cstr == nullptr).");
	}
}

} /* END of namespace */
