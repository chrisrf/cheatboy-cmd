#ifndef __StringSplitter_H__
#define __StringSplitter_H__

#include <string>
#include <PureMacro.h>

namespace pure {

class StringSplitter {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(StringSplitter);
public:
	static bool splitForTwoString(const std::string& str, char delimiter, std::string& str1, std::string& str2);
};

} /* END of namespace */

#endif
