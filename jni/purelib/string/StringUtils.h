#ifndef __StringUtils_H__
#define __StringUtils_H__

#include <stddef.h>
#include <string>
#include <PureMacro.h>

namespace pure {

class StringUtils {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(StringUtils);
public:
	static bool isInteger(const char* cstr);
	static bool isInteger(const std::string& str);
	static bool isInteger(const char* cstr, size_t len);
	static bool isInteger(const char* cstr, bool isSignAccepted);
	static bool isInteger(const std::string& str, bool isSignAccepted);
	static bool isInteger(const char* cstr, size_t len, bool isSignAccepted);
	static bool isHexNumber(const char* cstr);
	static bool isHexNumber(const std::string& str);
	static bool isHexNumber(const char* cstr, size_t len);
	static bool isDigit(char c);
	static bool isHexDigit(char c);
	static bool isWhiteSpace(char c);
private:
	static void throwIfCstrIsInvalid(const char* cstr);
};
static_assert(std::is_unsigned<size_t>::value, "size_t is Not unsigned");

} /* END of namespace */

#endif
