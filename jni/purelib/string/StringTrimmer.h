#ifndef __StringTrimmer_H__
#define __StringTrimmer_H__

#include <stddef.h>
//#include <locale>
#include <string>
#include <PureMacro.h>

namespace pure {

class StringTrimmer {
PURE_DECLARE_CLASS_AS_NONCONSTRUCTABLE(StringTrimmer);
private:
	//static const std::locale defalutLocale;

public:
	static char* cTrim(char* cstr);
	static char* cTrim(char* cstr, size_t& newLen);
	static char* cTrim(char* cstr, size_t len, size_t& newLen);
	static char* cTrimHead(char* cstr);
	static char* cTrimHead(char* cstr, size_t& newLen);
	static char* cTrimHead(char* cstr, size_t len, size_t& newLen);
	static char* cTrimTail(char* cstr);
	static char* cTrimTail(char* cstr, size_t& newLen);
	static char* cTrimTail(char* cstr, size_t len, size_t& newLen);
	static void cTrimAndShift(char* cstr);
	static void cTrimAndShift(char* cstr, size_t& newLen);
	static void cTrimAndShift(char* cstr, size_t len, size_t& newLen);
	static void cTrimHeadAndShift(char* cstr);
	static void cTrimHeadAndShift(char* cstr, size_t& newLen);
	static void cTrimHeadAndShift(char* cstr, size_t len, size_t& newLen);
	static void trim(std::string& str);
	static void trimHead(std::string& str);
	static void trimTail(std::string& str);
private:
	static bool isWhiteSpace(char c);
private:
	static void throwIfCstrIsInvalid(char* cstr);
};
static_assert(std::is_unsigned<size_t>::value, "size_t is Not unsigned");

} /* END of namespace */

#endif
