#ifndef __NumberConverter_H__
#define __NumberConverter_H__

#include <string>
#include <PureMacro.h>

#include <purelib/string/InternalNumberConverter.h>

namespace pure {

class NumberConverter {
PURE_DECLARE_CLASS_AS_NONCOPYABLE(NumberConverter);
private:
	InternalNumberConverter _converter;

public:
	NumberConverter();
	virtual ~NumberConverter();
	NumberConverter(NumberConverter&&) = default;
	NumberConverter& operator =(NumberConverter&&) = default;
public:
	template<typename T> bool convert(const char* valueCstring, T& outValue);
	template<typename T> bool convert(const std::string& valueString, T& outValue);
private:
	static void throwIfCstrIsInvalid(const char* cstr);
};

} /* END of namespace */

#endif
