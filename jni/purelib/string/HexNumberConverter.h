#ifndef __HexNumberConverter_H__
#define __HexNumberConverter_H__

#include <string>
#include <PureMacro.h>

#include <purelib/string/InternalNumberConverter.h>

namespace pure {

class HexNumberConverter {
PURE_DECLARE_CLASS_AS_NONCOPYABLE(HexNumberConverter);
private:
	InternalNumberConverter _converter;

public:
	HexNumberConverter();
	virtual ~HexNumberConverter();
	HexNumberConverter(HexNumberConverter&&) = default;
	HexNumberConverter& operator =(HexNumberConverter&&) = default;
public:
	template<typename T> bool convert(const char* valueCstring, T& outValue);
	template<typename T> bool convert(const std::string& valueString, T& outValue);
private:
	static void throwIfCstrIsInvalid(const char* cstr);
};

} /* END of namespace */

#endif
