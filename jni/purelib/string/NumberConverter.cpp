#include "NumberConverter.h"

#include <purelib/PurePlatformMacro.h>
#include <purelib/exception/Exception.h>

namespace pure {

NumberConverter::NumberConverter()
	:
	_converter(InternalNumberConverter::Mode::Dec) {}

NumberConverter::~NumberConverter() {}

template<typename T>
bool NumberConverter::convert(const char* valueCstring, T& outValue) {
	throwIfCstrIsInvalid(valueCstring);
	return _converter.convert(valueCstring, strlen(valueCstring), outValue);
}
template bool NumberConverter::convert(const char*, int8_t&);
template bool NumberConverter::convert(const char*, uint8_t&);
template bool NumberConverter::convert(const char*, int16_t&);
template bool NumberConverter::convert(const char*, uint16_t&);
template bool NumberConverter::convert(const char*, int32_t&);
template bool NumberConverter::convert(const char*, uint32_t&);
template bool NumberConverter::convert(const char*, int64_t&);
template bool NumberConverter::convert(const char*, uint64_t&);
template bool NumberConverter::convert(const char*, float&);
template bool NumberConverter::convert(const char*, double&);
template bool NumberConverter::convert(const char*, long double&);
#ifdef __PURE_32BIT__
template bool NumberConverter::convert(const char*, long&);
template bool NumberConverter::convert(const char*, unsigned long&);
#endif

template<typename T>
bool NumberConverter::convert(const std::string& valueString, T& outValue) {
	return _converter.convert(valueString.c_str(), valueString.size(), outValue);
}
template bool NumberConverter::convert(const std::string&, int8_t&);
template bool NumberConverter::convert(const std::string&, uint8_t&);
template bool NumberConverter::convert(const std::string&, int16_t&);
template bool NumberConverter::convert(const std::string&, uint16_t&);
template bool NumberConverter::convert(const std::string&, int32_t&);
template bool NumberConverter::convert(const std::string&, uint32_t&);
template bool NumberConverter::convert(const std::string&, int64_t&);
template bool NumberConverter::convert(const std::string&, uint64_t&);
template bool NumberConverter::convert(const std::string&, float&);
template bool NumberConverter::convert(const std::string&, double&);
template bool NumberConverter::convert(const std::string&, long double&);
#ifdef __PURE_32BIT__
template bool NumberConverter::convert(const std::string&, long&);
template bool NumberConverter::convert(const std::string&, unsigned long&);
#endif

void NumberConverter::throwIfCstrIsInvalid(const char* cstr) {
	if (cstr == nullptr) {
		PURE_THROW_EXCEPTION("Invalid Argument (cstr == nullptr).");
	}
}

} /* END of namespace */
