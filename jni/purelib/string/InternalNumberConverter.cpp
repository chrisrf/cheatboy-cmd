#include "InternalNumberConverter.h"

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits>

#include <purelib/PurePlatformMacro.h>
#include <purelib/exception/Exception.h>
#include <purelib/terminal/TerminalPrint.h>

namespace pure {

InternalNumberConverter::InternalNumberConverter(Mode mode) {
	setMode(mode);
}

InternalNumberConverter::~InternalNumberConverter() noexcept {
}

InternalNumberConverter::Mode InternalNumberConverter::getMode() {
	return _mode;
}

void InternalNumberConverter::setMode(Mode mode) {
	if (mode == Mode::Dec) {
		useDecParsingMode();
	} else {
		useHexParsingMode();
	}
}

void InternalNumberConverter::useDecParsingMode() {
	_converter >> std::dec;
	_mode = Mode::Dec;
}

void InternalNumberConverter::useHexParsingMode() {
	_converter >> std::hex;
	_mode = Mode::Hex;
}

template<typename T>
bool InternalNumberConverter::convert(const char* valueCstring, size_t len, T& outValue) {
	//pure::print("convert<T> ({0})\n", std::is_unsigned<T>::value);
	throwIfCstrIsInvalid(valueCstring);

	if ((len == 0) || (isWhiteSpace(valueCstring[0]))) {
		return false;
	}
	if ((std::is_unsigned<T>::value) && (valueCstring[0] == '-')) {
		return false;
	}

	T tempValue = 0;
	_converter.clear();
	_converter.str(valueCstring);
	if ((!(_converter >> tempValue)) || (!(_converter.eof()))) {
		return false;
	}
	outValue = tempValue;
	return true;
}
template<>
bool InternalNumberConverter::convert(const char* valueCstring, size_t len, int8_t& outValue) {
	//pure::print("convert<int8_t>\n");
	throwIfCstrIsInvalid(valueCstring);

	if ((len == 0) || (isWhiteSpace(valueCstring[0]))) {
		return false;
	}

	int32_t tempValue = 0;
	_converter.clear();
	_converter.str(valueCstring);
	if ((!(_converter >> tempValue)) || (!(_converter.eof()))) {
		return false;
	}
	if ((tempValue < std::numeric_limits<int8_t>::min()) || (tempValue > std::numeric_limits<int8_t>::max())) {
		return false;
	}
	outValue = (int8_t) tempValue;
	return true;
}
template<>
bool InternalNumberConverter::convert(const char* valueCstring, size_t len, uint8_t& outValue) {
	//pure::print("convert<uint8_t>\n");
	throwIfCstrIsInvalid(valueCstring);

	if ((len == 0) || (isWhiteSpace(valueCstring[0]))) {
		return false;
	}
	if (valueCstring[0] == '-') {
		return false;
	}

	uint32_t tempValue = 0;
	_converter.clear();
	_converter.str(valueCstring);
	if ((!(_converter >> tempValue)) || (!(_converter.eof()))) {
		return false;
	}
	if ((tempValue < std::numeric_limits<uint8_t>::min()) || (tempValue > std::numeric_limits<uint8_t>::max())) {
		return false;
	}
	outValue = (uint8_t) tempValue;
	return true;
}
template bool InternalNumberConverter::convert(const char*, size_t len, int16_t&);
template bool InternalNumberConverter::convert(const char*, size_t len, uint16_t&);
template bool InternalNumberConverter::convert(const char*, size_t len, int32_t&);
template bool InternalNumberConverter::convert(const char*, size_t len, uint32_t&);
template bool InternalNumberConverter::convert(const char*, size_t len, int64_t&);
template bool InternalNumberConverter::convert(const char*, size_t len, uint64_t&);
template bool InternalNumberConverter::convert(const char*, size_t len, float&);
template bool InternalNumberConverter::convert(const char*, size_t len, double&);
template bool InternalNumberConverter::convert(const char*, size_t len, long double&);
#ifdef __PURE_32BIT__
template bool InternalNumberConverter::convert(const char*, size_t len, long&);
template bool InternalNumberConverter::convert(const char*, size_t len, unsigned long&);
#endif

bool InternalNumberConverter::isWhiteSpace(char c) {
	return isspace(c);
}

void InternalNumberConverter::throwIfCstrIsInvalid(const char* cstr) {
	if (cstr == nullptr) {
		PURE_THROW_EXCEPTION("Invalid Argument (cstr == nullptr).");
	}
}

} /* END of namespace */
