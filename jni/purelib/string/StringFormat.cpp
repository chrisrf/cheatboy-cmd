#include "StringFormat.h"

namespace pure {

std::string fmt(const char* format, fmt::ArgList args) {
	return fmt::format(format, args);
}

} /* END of namespace */
