#!/bin/bash

export PVAR_PROJ_ROOT_DIR="${MY_DEV_SP}/proj-myboy/CheatBoy-Cmd"
cd "${PVAR_PROJ_ROOT_DIR}"

export PVAR_SDK_ROOT_DIR="${MY_ANDROID_SDK}"
export PVAR_USER_NAME="chris"
#export PVAR_REMOTE_EXE_DIR="/data/${PVAR_USER_NAME}/ane"
export PVAR_REMOTE_EXE_DIR="/data/local/tmp/ane"
export PVAR_TARGET_ARCH_ABI="x86"
#export PVAR_TARGET_ARCH_ABI="armeabi"
#export PVAR_TARGET_ARCH_ABI="armeabi-v7a"
export PVAR_EXE_NAME="cheatboy"

"${PVAR_SDK_ROOT_DIR}/platform-tools/adb" shell mkdir -p "${PVAR_REMOTE_EXE_DIR}"
"${PVAR_SDK_ROOT_DIR}/platform-tools/adb" push "./libs/${PVAR_TARGET_ARCH_ABI}/gdbserver" "${PVAR_REMOTE_EXE_DIR}"
"${PVAR_SDK_ROOT_DIR}/platform-tools/adb" push "./obj/local/${PVAR_TARGET_ARCH_ABI}/${PVAR_EXE_NAME}" "${PVAR_REMOTE_EXE_DIR}"

"${PVAR_SDK_ROOT_DIR}/platform-tools/adb" shell chmod 755 "${PVAR_REMOTE_EXE_DIR}/gdbserver"
"${PVAR_SDK_ROOT_DIR}/platform-tools/adb" shell chmod 755 "${PVAR_REMOTE_EXE_DIR}/${PVAR_EXE_NAME}"
"${PVAR_SDK_ROOT_DIR}/platform-tools/adb" forward tcp:5039 tcp:5039
"${PVAR_SDK_ROOT_DIR}/platform-tools/adb" shell "${PVAR_REMOTE_EXE_DIR}/gdbserver" :5039 "${PVAR_REMOTE_EXE_DIR}/${PVAR_EXE_NAME}"

read -p "Press [Enter] key to exit..."